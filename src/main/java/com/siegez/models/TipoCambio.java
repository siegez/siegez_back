package com.siegez.models;

public class TipoCambio {

	private Integer empresa;
	private Integer vez;
	private String 	fecha;
	private String 	moneda;	
	private String 	descripcionmoneda;	
	private double 	tipocambiocompra;
	private double 	tipocambioventa;
	private String 	indicadorcierre;
	private String 	descripcionindicadorcierre;
	private String 	indicadorreplica;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
		
	public Integer getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}


	public Integer getVez() {
		return vez;
	}


	public void setVez(Integer vez) {
		this.vez = vez;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}


	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}


	public double getTipocambiocompra() {
		return tipocambiocompra;
	}


	public void setTipocambiocompra(double tipocambiocompra) {
		this.tipocambiocompra = tipocambiocompra;
	}


	public double getTipocambioventa() {
		return tipocambioventa;
	}


	public void setTipocambioventa(double tipocambioventa) {
		this.tipocambioventa = tipocambioventa;
	}


	public String getIndicadorcierre() {
		return indicadorcierre;
	}


	public void setIndicadorcierre(String indicadorcierre) {
		this.indicadorcierre = indicadorcierre;
	}


	public String getDescripcionindicadorcierre() {
		return descripcionindicadorcierre;
	}


	public void setDescripcionindicadorcierre(String descripcionindicadorcierre) {
		this.descripcionindicadorcierre = descripcionindicadorcierre;
	}


	public String getIndicadorreplica() {
		return indicadorreplica;
	}


	public void setIndicadorreplica(String indicadorreplica) {
		this.indicadorreplica = indicadorreplica;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getDescripcionestado() {
		return descripcionestado;
	}


	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}


	public String getCreacionUsuario() {
		return creacionUsuario;
	}


	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}


	public String getCreacionFecha() {
		return creacionFecha;
	}


	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}


	public String getModificacionUsuario() {
		return modificacionUsuario;
	}


	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}


	public String getModificacionFecha() {
		return modificacionFecha;
	}


	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	

	public TipoCambio(Integer empresa, Integer vez, String fecha, String moneda, String descripcionmoneda,
			double tipocambiocompra, double tipocambioventa, String indicadorcierre, String descripcionindicadorcierre,
			String indicadorreplica, String estado, String descripcionestado, String creacionUsuario,
			String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.vez = vez;
		this.fecha = fecha;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.tipocambiocompra = tipocambiocompra;
		this.tipocambioventa = tipocambioventa;
		this.indicadorcierre = indicadorcierre;
		this.descripcionindicadorcierre = descripcionindicadorcierre;
		this.indicadorreplica = indicadorreplica;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}


	public TipoCambio() {
		
	}
	
	
	
}
