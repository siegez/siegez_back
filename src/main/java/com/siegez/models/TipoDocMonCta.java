package com.siegez.models;

public class TipoDocMonCta {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String 	tipodocumento;
	private String 	descripciontipodocumento;
	private String 	moneda;
	private String 	descripcionmoneda;
	private String 	cuentacompra;
	private String 	cuentacomprarel;
	private String 	cuentahonorario;
	private String 	cuentaventa;
	private String 	cuentaventarel;
	private String 	cuentacaja;
	private String 	cuentaegreso;
	private String 	cuentaingreso;
	private String 	cuentadiario;
	private String 	cuentaimportacion;
	private String 	cuentanodomiciliado;
	private String 	cuentanodomiciliadorel;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getTipodocumento() {
		return tipodocumento;
	}
	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	public String getDescripciontipodocumento() {
		return descripciontipodocumento;
	}
	public void setDescripciontipodocumento(String descripciontipodocumento) {
		this.descripciontipodocumento = descripciontipodocumento;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}
	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}
	public String getCuentacompra() {
		return cuentacompra;
	}
	public void setCuentacompra(String cuentacompra) {
		this.cuentacompra = cuentacompra;
	}
	public String getCuentacomprarel() {
		return cuentacomprarel;
	}
	public void setCuentacomprarel(String cuentacomprarel) {
		this.cuentacomprarel = cuentacomprarel;
	}
	public String getCuentahonorario() {
		return cuentahonorario;
	}
	public void setCuentahonorario(String cuentahonorario) {
		this.cuentahonorario = cuentahonorario;
	}
	public String getCuentaventa() {
		return cuentaventa;
	}
	public void setCuentaventa(String cuentaventa) {
		this.cuentaventa = cuentaventa;
	}
	public String getCuentaventarel() {
		return cuentaventarel;
	}
	public void setCuentaventarel(String cuentaventarel) {
		this.cuentaventarel = cuentaventarel;
	}
	public String getCuentacaja() {
		return cuentacaja;
	}
	public void setCuentacaja(String cuentacaja) {
		this.cuentacaja = cuentacaja;
	}
	public String getCuentaegreso() {
		return cuentaegreso;
	}
	public void setCuentaegreso(String cuentaegreso) {
		this.cuentaegreso = cuentaegreso;
	}
	public String getCuentaingreso() {
		return cuentaingreso;
	}
	public void setCuentaingreso(String cuentaingreso) {
		this.cuentaingreso = cuentaingreso;
	}
	public String getCuentadiario() {
		return cuentadiario;
	}
	public void setCuentadiario(String cuentadiario) {
		this.cuentadiario = cuentadiario;
	}
	public String getCuentaimportacion() {
		return cuentaimportacion;
	}
	public void setCuentaimportacion(String cuentaimportacion) {
		this.cuentaimportacion = cuentaimportacion;
	}
	public String getCuentanodomiciliado() {
		return cuentanodomiciliado;
	}
	public void setCuentanodomiciliado(String cuentanodomiciliado) {
		this.cuentanodomiciliado = cuentanodomiciliado;
	}
	public String getCuentanodomiciliadorel() {
		return cuentanodomiciliadorel;
	}
	public void setCuentanodomiciliadorel(String cuentanodomiciliadorel) {
		this.cuentanodomiciliadorel = cuentanodomiciliadorel;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public TipoDocMonCta(Integer empresa, Integer ejerciciocontable, String tipodocumento,
			String descripciontipodocumento, String moneda, String descripcionmoneda, String cuentacompra,
			String cuentacomprarel, String cuentahonorario, String cuentaventa, String cuentaventarel,
			String cuentacaja, String cuentaegreso, String cuentaingreso, String cuentadiario, String cuentaimportacion,
			String cuentanodomiciliado, String cuentanodomiciliadorel, String estado, String descripcionestado,
			String creacionUsuario, String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.tipodocumento = tipodocumento;
		this.descripciontipodocumento = descripciontipodocumento;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.cuentacompra = cuentacompra;
		this.cuentacomprarel = cuentacomprarel;
		this.cuentahonorario = cuentahonorario;
		this.cuentaventa = cuentaventa;
		this.cuentaventarel = cuentaventarel;
		this.cuentacaja = cuentacaja;
		this.cuentaegreso = cuentaegreso;
		this.cuentaingreso = cuentaingreso;
		this.cuentadiario = cuentadiario;
		this.cuentaimportacion = cuentaimportacion;
		this.cuentanodomiciliado = cuentanodomiciliado;
		this.cuentanodomiciliadorel = cuentanodomiciliadorel;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public TipoDocMonCta() {

	}
	
	
	

	
}
