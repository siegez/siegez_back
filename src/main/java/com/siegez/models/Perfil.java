package com.siegez.models;
import java.util.Date;

public class Perfil {
	private Integer perfil;
	private String	descripcion;
	private Integer	empresa;
	private String	descripcionempresa;
	private String 	estado;	//
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modUsuario;
	private Date 	modFecha;
	

	public Integer getPerfil() {
		return perfil;
	}
	public void setPerfil(Integer perfil) {
		this.perfil = perfil;
	}
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public Date getModFecha() {
		return modFecha;
	}
	public void setModFecha(Date modFecha) {
		this.modFecha = modFecha;
	}
	public String getDescripcionempresa() {
		return descripcionempresa;
	}
	public void setDescripcionempresa(String descripcionempresa) {
		this.descripcionempresa = descripcionempresa;
	}

	public Perfil(Integer perfil, Integer empresa, String descripcion, String estado, String creacionUsuario,
			Date creacionFecha, String modUsuario, Date modFecha) {
		//super();
		this.perfil = perfil;
		this.descripcion = descripcion;
		this.empresa = empresa;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}

	public Perfil(Integer perfil, String descripcion, Integer empresa, String descripcionempresa, String estado, String creacionUsuario,
			Date creacionFecha, String modUsuario, Date modFecha) {
		//super();
		this.perfil = perfil;
		this.descripcion = descripcion;
		this.empresa = empresa;
		this.descripcionempresa = descripcionempresa;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}
	public Perfil() {
		//super();

	}


	
}
