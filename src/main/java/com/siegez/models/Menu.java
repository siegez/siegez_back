package com.siegez.models;

import java.util.Date;

public class Menu {

	private Integer codigo;
	private String	modulo;
	private String	descripcion;
	private String	opcion;
	private Integer	nivel;
	private Integer correlativo;
	private String 	estado;	
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modUsuario;
	private Date 	modFecha;

	public Integer getCodigo() {
		return codigo;
	}	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public Integer getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public Date getModFecha() {
		return modFecha;
	}
	public void setModFecha(Date modFecha) {
		this.modFecha = modFecha;
	}
	
	public Menu(Integer codigo, String modulo, String descripcion, String opcion, Integer nivel, Integer correlativo, String estado,
			String creacionUsuario, Date creacionFecha, String modUsuario, Date modFecha) {
		super();
		this.codigo = codigo;
		this.modulo = modulo;
		this.descripcion = descripcion;
		this.opcion = opcion;
		this.nivel = nivel;
		this.correlativo = correlativo;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}


}
