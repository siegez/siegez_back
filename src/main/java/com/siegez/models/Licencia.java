package com.siegez.models;

import java.util.Date;

public class Licencia {

	private String	licenciaCodigo;
	private String  descripcion;
	private Integer cantEmpresas;
	private Integer	cantUsuarios;
	private String	estado;
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modUsuario;
	private Date 	modFecha;
	
	public String getLicenciaCodigo() {
		return licenciaCodigo;
	}
	public void setLicenciaCodigo(String licenciaCodigo) {
		this.licenciaCodigo = licenciaCodigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getCantEmpresas() {
		return cantEmpresas;
	}
	public void setCantEmpresas(Integer cantEmpresas) {
		this.cantEmpresas = cantEmpresas;
	}
	public Integer getCantUsuarios() {
		return cantUsuarios;
	}
	public void setCantUsuarios(Integer cantUsuarios) {
		this.cantUsuarios = cantUsuarios;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public Date getModFecha() {
		return modFecha;
	}
	public void setModFecha(Date modFecha) {
		this.modFecha = modFecha;
	}
	
	public Licencia(String licenciaCodigo, String descripcion, Integer cantEmpresas, Integer cantUsuarios,
			String estado, String creacionUsuario, Date creacionFecha, String modUsuario, Date modFecha) {
		super();
		this.licenciaCodigo = licenciaCodigo;
		this.descripcion = descripcion;
		this.cantEmpresas = cantEmpresas;
		this.cantUsuarios = cantUsuarios;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}
	
	public Licencia() {
		
	}	
}
