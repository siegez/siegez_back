package com.siegez.models;
import java.util.Date;
public class Tabla {

	private Integer tabla;
	private String	descripcion;
	private Integer	orden;
	private String	enlace;
	private String	uso;
	private String 	estado;	
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modUsuario;
	private Date 	modFecha;
	
	public Integer getTabla() {
		return tabla;
	}
	public void setTabla(Integer tabla) {
		this.tabla = tabla;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public String getEnlace() {
		return enlace;
	}
	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}
	public String getUso() {
		return uso;
	}
	public void setUso(String uso) {
		this.uso = uso;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public Date getModFecha() {
		return modFecha;
	}
	public void setModFecha(Date modFecha) {
		this.modFecha = modFecha;
	}
	public Tabla(Integer tabla, String descripcion, Integer orden, String enlace, String uso, String estado, String creacionUsuario,
			Date creacionFecha, String modUsuario, Date modFecha) {
		super();
		this.tabla = tabla;
		this.descripcion = descripcion;
		this.orden = orden;
		this.enlace = enlace;
		this.uso = uso;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}
	
	
}
