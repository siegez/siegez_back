package com.siegez.models;


public class PlanContable {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String 	cuenta;
	private String 	descripcioncuenta;
	private String 	categoria;
	private String 	descripcioncategoria;
	private String 	tipocuenta;
	private String 	descripciontipo;
	private String 	moneda; 
	private String 	descripcionmoneda;
	private String 	nivelsaldo;	
	private String 	descripcionnivel;
	private String 	socionegocio;
	private String 	descripcionsocionegocio;
	private String 	documentoreferencia;
	private String 	descripciondocumentoreferencia;
	private String 	cuentamanual;
	private String 	descripcioncuentamanual;
	private String 	mediopago;
	private String 	descripcionmediopago;
	private String 	centrocosto;
	private String 	descripcioncentrocosto;
	private String 	ajustependiente;
	private String 	descripcionajustependiente;
	private String 	ajustecancelado;
	private String 	descripcionajustecancelado;
	private String 	tipocambio;
	private String 	descripciontipocambio;
	private String 	asientodebe;
	private String 	asientohaber;
	private String 	saldoclase9;
	private String 	descripcionsaldoclase9;
	private String 	transferencia;
	private String 	descripciontransferencia;
	private String 	cobro;
	private String 	descripcioncobro;
	private String 	pago;
	private String 	descripcionpago;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getDescripcioncuenta() {
		return descripcioncuenta;
	}
	public void setDescripcioncuenta(String descripcioncuenta) {
		this.descripcioncuenta = descripcioncuenta;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getDescripcioncategoria() {
		return descripcioncategoria;
	}
	public void setDescripcioncategoria(String descripcioncategoria) {
		this.descripcioncategoria = descripcioncategoria;
	}
	public String getTipocuenta() {
		return tipocuenta;
	}
	public void setTipocuenta(String tipocuenta) {
		this.tipocuenta = tipocuenta;
	}
	public String getDescripciontipo() {
		return descripciontipo;
	}
	public void setDescripciontipo(String descripciontipo) {
		this.descripciontipo = descripciontipo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}
	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}
	public String getNivelsaldo() {
		return nivelsaldo;
	}
	public void setNivelsaldo(String nivelsaldo) {
		this.nivelsaldo = nivelsaldo;
	}
	public String getDescripcionnivel() {
		return descripcionnivel;
	}
	public void setDescripcionnivel(String descripcionnivel) {
		this.descripcionnivel = descripcionnivel;
	}
	public String getSocionegocio() {
		return socionegocio;
	}
	public void setSocionegocio(String socionegocio) {
		this.socionegocio = socionegocio;
	}
	public String getDescripcionsocionegocio() {
		return descripcionsocionegocio;
	}
	public void setDescripcionsocionegocio(String descripcionsocionegocio) {
		this.descripcionsocionegocio = descripcionsocionegocio;
	}
	public String getDocumentoreferencia() {
		return documentoreferencia;
	}
	public void setDocumentoreferencia(String documentoreferencia) {
		this.documentoreferencia = documentoreferencia;
	}
	public String getDescripciondocumentoreferencia() {
		return descripciondocumentoreferencia;
	}
	public void setDescripciondocumentoreferencia(String descripciondocumentoreferencia) {
		this.descripciondocumentoreferencia = descripciondocumentoreferencia;
	}
	public String getCuentamanual() {
		return cuentamanual;
	}
	public void setCuentamanual(String cuentamanual) {
		this.cuentamanual = cuentamanual;
	}
	public String getDescripcioncuentamanual() {
		return descripcioncuentamanual;
	}
	public void setDescripcioncuentamanual(String descripcioncuentamanual) {
		this.descripcioncuentamanual = descripcioncuentamanual;
	}
	public String getMediopago() {
		return mediopago;
	}
	public void setMediopago(String mediopago) {
		this.mediopago = mediopago;
	}
	public String getDescripcionmediopago() {
		return descripcionmediopago;
	}
	public void setDescripcionmediopago(String descripcionmediopago) {
		this.descripcionmediopago = descripcionmediopago;
	}
	public String getCentrocosto() {
		return centrocosto;
	}
	public void setCentrocosto(String centrocosto) {
		this.centrocosto = centrocosto;
	}
	public String getDescripcioncentrocosto() {
		return descripcioncentrocosto;
	}
	public void setDescripcioncentrocosto(String descripcioncentrocosto) {
		this.descripcioncentrocosto = descripcioncentrocosto;
	}
	public String getAjustependiente() {
		return ajustependiente;
	}
	public void setAjustependiente(String ajustependiente) {
		this.ajustependiente = ajustependiente;
	}
	public String getDescripcionajustependiente() {
		return descripcionajustependiente;
	}
	public void setDescripcionajustependiente(String descripcionajustependiente) {
		this.descripcionajustependiente = descripcionajustependiente;
	}
	public String getAjustecancelado() {
		return ajustecancelado;
	}
	public void setAjustecancelado(String ajustecancelado) {
		this.ajustecancelado = ajustecancelado;
	}
	public String getDescripcionajustecancelado() {
		return descripcionajustecancelado;
	}
	public void setDescripcionajustecancelado(String descripcionajustecancelado) {
		this.descripcionajustecancelado = descripcionajustecancelado;
	}
	public String getTipocambio() {
		return tipocambio;
	}
	public void setTipocambio(String tipocambio) {
		this.tipocambio = tipocambio;
	}
	public String getDescripciontipocambio() {
		return descripciontipocambio;
	}
	public void setDescripciontipocambio(String descripciontipocambio) {
		this.descripciontipocambio = descripciontipocambio;
	}
	public String getAsientodebe() {
		return asientodebe;
	}
	public void setAsientodebe(String asientodebe) {
		this.asientodebe = asientodebe;
	}
	public String getAsientohaber() {
		return asientohaber;
	}
	public void setAsientohaber(String asientohaber) {
		this.asientohaber = asientohaber;
	}
	public String getSaldoclase9() {
		return saldoclase9;
	}
	public void setSaldoclase9(String saldoclase9) {
		this.saldoclase9 = saldoclase9;
	}
	public String getDescripcionsaldoclase9() {
		return descripcionsaldoclase9;
	}
	public void setDescripcionsaldoclase9(String descripcionsaldoclase9) {
		this.descripcionsaldoclase9 = descripcionsaldoclase9;
	}
	public String getTransferencia() {
		return transferencia;
	}
	public void setTransferencia(String transferencia) {
		this.transferencia = transferencia;
	}
	public String getDescripciontransferencia() {
		return descripciontransferencia;
	}
	public void setDescripciontransferencia(String descripciontransferencia) {
		this.descripciontransferencia = descripciontransferencia;
	}
	public String getCobro() {
		return cobro;
	}
	public void setCobro(String cobro) {
		this.cobro = cobro;
	}
	public String getDescripcioncobro() {
		return descripcioncobro;
	}
	public void setDescripcioncobro(String descripcioncobro) {
		this.descripcioncobro = descripcioncobro;
	}
	public String getPago() {
		return pago;
	}
	public void setPago(String pago) {
		this.pago = pago;
	}
	public String getDescripcionpago() {
		return descripcionpago;
	}
	public void setDescripcionpago(String descripcionpago) {
		this.descripcionpago = descripcionpago;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public PlanContable(Integer empresa, Integer ejerciciocontable, String cuenta, String descripcioncuenta, String categoria,
			String descripcioncategoria, String tipocuenta, String descripciontipo, String moneda,
			String descripcionmoneda, String nivelsaldo, String descripcionnivel, String socionegocio,
			String descripcionsocionegocio, String documentoreferencia, String descripciondocumentoreferencia,
			String cuentamanual, String descripcioncuentamanual, String mediopago, String descripcionmediopago,
			String centrocosto, String descripcioncentrocosto, String ajustependiente,
			String descripcionajustependiente, String ajustecancelado, String descripcionajustecancelado,
			String tipocambio, String descripciontipocambio, String asientodebe, String asientohaber,
			String saldoclase9, String descripcionsaldoclase9, String transferencia, String descripciontransferencia,
			String cobro, String descripcioncobro, String pago, String descripcionpago, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.cuenta = cuenta;
		this.descripcioncuenta = descripcioncuenta;
		this.categoria = categoria;
		this.descripcioncategoria = descripcioncategoria;
		this.tipocuenta = tipocuenta;
		this.descripciontipo = descripciontipo;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.nivelsaldo = nivelsaldo;
		this.descripcionnivel = descripcionnivel;
		this.socionegocio = socionegocio;
		this.descripcionsocionegocio = descripcionsocionegocio;
		this.documentoreferencia = documentoreferencia;
		this.descripciondocumentoreferencia = descripciondocumentoreferencia;
		this.cuentamanual = cuentamanual;
		this.descripcioncuentamanual = descripcioncuentamanual;
		this.mediopago = mediopago;
		this.descripcionmediopago = descripcionmediopago;
		this.centrocosto = centrocosto;
		this.descripcioncentrocosto = descripcioncentrocosto;
		this.ajustependiente = ajustependiente;
		this.descripcionajustependiente = descripcionajustependiente;
		this.ajustecancelado = ajustecancelado;
		this.descripcionajustecancelado = descripcionajustecancelado;
		this.tipocambio = tipocambio;
		this.descripciontipocambio = descripciontipocambio;
		this.asientodebe = asientodebe;
		this.asientohaber = asientohaber;
		this.saldoclase9 = saldoclase9;
		this.descripcionsaldoclase9 = descripcionsaldoclase9;
		this.transferencia = transferencia;
		this.descripciontransferencia = descripciontransferencia;
		this.cobro = cobro;
		this.descripcioncobro = descripcioncobro;
		this.pago = pago;
		this.descripcionpago = descripcionpago;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public PlanContable() {

	}

		
}
