package com.siegez.models;

import java.util.Date;

public class Empresa {

	private Integer	empresa;
	private String	licencia;
	private String  descripcion;
	private String  ruc;
	private String	tipo;
	private String 	logo;	
	private String	estado;
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modificacionUsuario;
	private Date 	modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public Date getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(Date modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public Empresa(Integer empresa, String licencia, String descripcion, String ruc, String tipo, String logo,
			String estado, String creacionUsuario, Date creacionFecha, String modificacionUsuario, Date modificacionFecha) {
		super();
		this.empresa = empresa;
		this.licencia = licencia;
		this.descripcion = descripcion;
		this.ruc = ruc;
		this.tipo = tipo;
		this.logo = logo;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public Empresa() {
		
	}
}
