package com.siegez.models;

public class CentroCosto {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String	centrocosto;	
	private String 	nombrecentrocosto;
	private String 	cuentacontablecargo;
	private String 	cuentacontableabono;
	private double	porcentaje;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getCentrocosto() {
		return centrocosto;
	}
	public void setCentrocosto(String centrocosto) {
		this.centrocosto = centrocosto;
	}
	public String getNombrecentrocosto() {
		return nombrecentrocosto;
	}
	public void setNombrecentrocosto(String nombrecentrocosto) {
		this.nombrecentrocosto = nombrecentrocosto;
	}
	public String getCuentacontablecargo() {
		return cuentacontablecargo;
	}
	public void setCuentacontablecargo(String cuentacontablecargo) {
		this.cuentacontablecargo = cuentacontablecargo;
	}
	public String getCuentacontableabono() {
		return cuentacontableabono;
	}
	public void setCuentacontableabono(String cuentacontableabono) {
		this.cuentacontableabono = cuentacontableabono;
	}
	public double getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public CentroCosto(Integer empresa, Integer ejerciciocontable, String centrocosto, String nombrecentrocosto,
			String cuentacontablecargo, String cuentacontableabono, double porcentaje, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.centrocosto = centrocosto;
		this.nombrecentrocosto = nombrecentrocosto;
		this.cuentacontablecargo = cuentacontablecargo;
		this.cuentacontableabono = cuentacontableabono;
		this.porcentaje = porcentaje;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public CentroCosto() {
		// TODO Auto-generated constructor stub
	}
	
	
}
