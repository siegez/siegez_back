package com.siegez.models;

public class Ejercicio {

	private Integer	empresa;
	private Integer	ejerciciocontable;
	private String  indicador;
	private String	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String 	creacionFecha;
	private String 	modificacionUsuario;
	private String 	modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getIndicador() {
		return indicador;
	}
	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public Ejercicio(Integer empresa, Integer ejerciciocontable, String indicador, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.indicador = indicador;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public Ejercicio() {

	}
	
	
	
}
