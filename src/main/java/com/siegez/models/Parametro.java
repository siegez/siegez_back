package com.siegez.models;

public class Parametro {
	private Integer	empresa;
	private String	modulo;
	private String	parametro;
	private String	descripcion;
	private String	valor;
	private String	descripcionvalor;
	private String	submodulo;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public String getParametro() {
		return parametro;
	}
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDescripcionvalor() {
		return descripcionvalor;
	}
	public void setDescripcionvalor(String descripcionvalor) {
		this.descripcionvalor = descripcionvalor;
	}
	public String getSubmodulo() {
		return submodulo;
	}
	public void setSubmodulo(String submodulo) {
		this.submodulo = submodulo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public Parametro(Integer empresa, String modulo, String parametro, String descripcion, String valor,
			String descripcionvalor, String submodulo, String estado, String descripcionestado, String creacionUsuario,
			String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.modulo = modulo;
		this.parametro = parametro;
		this.descripcion = descripcion;
		this.valor = valor;
		this.descripcionvalor = descripcionvalor;
		this.submodulo = submodulo;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public Parametro() {
		
	}
	
	
	
		
	
}
