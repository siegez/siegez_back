package com.siegez.models;

public class RegistroContable {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String operacioncontable;
	private Integer	asientocontable;		
	private Integer	registrocontable;
	private String 	cuentacontable;
	private String 	descripcioncuentacontable;
	private String 	entidad;
	private String 	nombreentidad;
	private String 	ruc;
	private String 	tipoanotacion;
	private String 	descripciontipoanotacion;
	private String 	tiporegistro;
	private String 	descripciontiporegistro;
	private String 	moneda;
	private String 	descripcionmoneda;
	private String 	tipocambio;
	private String 	descripciontipocambio;
	private double  tipocambiosoles;
	private double 	tipocambiodolares;
	private double 	montoorigen;
	private double 	montonacional;
	private double 	montodolares;
	private String 	tipodocumento;
	private String 	descripciontipodocumento;
	private String 	numeroserie;
	private String 	numerocomprobanteini;
	private String 	numerocomprobantefin;
	private String 	fechaemision;
	private String 	fechavencimiento;
	private String 	mediopago;
	private String 	descripcionmediopago;
	private String 	flujoefectivo;
	private String 	descripcionflujoefectivo;
	private String 	tablacentrocosto;
	private String 	centrocosto;
	private String 	descripcioncentrocosto;
	private double 	porcentajecentrocosto;	
	private Integer	correlativoorigen;
	private String 	glosa;
	private String 	incluirregistrocompra;
	private String 	indicadorcalculo;
	private Integer columnamonto;
	private Integer columnaimpuesto;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	
	public Integer getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}


	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}


	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}


	public String getOperacioncontable() {
		return operacioncontable;
	}


	public void setOperacioncontable(String operacioncontable) {
		this.operacioncontable = operacioncontable;
	}


	public Integer getAsientocontable() {
		return asientocontable;
	}


	public void setAsientocontable(Integer asientocontable) {
		this.asientocontable = asientocontable;
	}


	public Integer getRegistrocontable() {
		return registrocontable;
	}


	public void setRegistrocontable(Integer registrocontable) {
		this.registrocontable = registrocontable;
	}


	public String getCuentacontable() {
		return cuentacontable;
	}


	public void setCuentacontable(String cuentacontable) {
		this.cuentacontable = cuentacontable;
	}


	public String getDescripcioncuentacontable() {
		return descripcioncuentacontable;
	}


	public void setDescripcioncuentacontable(String descripcioncuentacontable) {
		this.descripcioncuentacontable = descripcioncuentacontable;
	}


	public String getEntidad() {
		return entidad;
	}


	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}


	public String getNombreentidad() {
		return nombreentidad;
	}


	public void setNombreentidad(String nombreentidad) {
		this.nombreentidad = nombreentidad;
	}


	public String getRuc() {
		return ruc;
	}


	public void setRuc(String ruc) {
		this.ruc = ruc;
	}


	public String getTipoanotacion() {
		return tipoanotacion;
	}


	public void setTipoanotacion(String tipoanotacion) {
		this.tipoanotacion = tipoanotacion;
	}


	public String getDescripciontipoanotacion() {
		return descripciontipoanotacion;
	}


	public void setDescripciontipoanotacion(String descripciontipoanotacion) {
		this.descripciontipoanotacion = descripciontipoanotacion;
	}


	public String getTiporegistro() {
		return tiporegistro;
	}


	public void setTiporegistro(String tiporegistro) {
		this.tiporegistro = tiporegistro;
	}


	public String getDescripciontiporegistro() {
		return descripciontiporegistro;
	}


	public void setDescripciontiporegistro(String descripciontiporegistro) {
		this.descripciontiporegistro = descripciontiporegistro;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}


	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}


	public String getTipocambio() {
		return tipocambio;
	}


	public void setTipocambio(String tipocambio) {
		this.tipocambio = tipocambio;
	}


	public String getDescripciontipocambio() {
		return descripciontipocambio;
	}


	public void setDescripciontipocambio(String descripciontipocambio) {
		this.descripciontipocambio = descripciontipocambio;
	}


	public double getTipocambiosoles() {
		return tipocambiosoles;
	}


	public void setTipocambiosoles(double tipocambiosoles) {
		this.tipocambiosoles = tipocambiosoles;
	}


	public double getTipocambiodolares() {
		return tipocambiodolares;
	}


	public void setTipocambiodolares(double tipocambiodolares) {
		this.tipocambiodolares = tipocambiodolares;
	}


	public double getMontoorigen() {
		return montoorigen;
	}


	public void setMontoorigen(double montoorigen) {
		this.montoorigen = montoorigen;
	}


	public double getMontonacional() {
		return montonacional;
	}


	public void setMontonacional(double montonacional) {
		this.montonacional = montonacional;
	}


	public double getMontodolares() {
		return montodolares;
	}


	public void setMontodolares(double montodolares) {
		this.montodolares = montodolares;
	}


	public String getTipodocumento() {
		return tipodocumento;
	}


	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}


	public String getDescripciontipodocumento() {
		return descripciontipodocumento;
	}


	public void setDescripciontipodocumento(String descripciontipodocumento) {
		this.descripciontipodocumento = descripciontipodocumento;
	}


	public String getNumeroserie() {
		return numeroserie;
	}


	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}


	public String getNumerocomprobanteini() {
		return numerocomprobanteini;
	}


	public void setNumerocomprobanteini(String numerocomprobanteini) {
		this.numerocomprobanteini = numerocomprobanteini;
	}


	public String getNumerocomprobantefin() {
		return numerocomprobantefin;
	}


	public void setNumerocomprobantefin(String numerocomprobantefin) {
		this.numerocomprobantefin = numerocomprobantefin;
	}


	public String getFechaemision() {
		return fechaemision;
	}


	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}


	public String getFechavencimiento() {
		return fechavencimiento;
	}


	public void setFechavencimiento(String fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}


	public String getMediopago() {
		return mediopago;
	}


	public void setMediopago(String mediopago) {
		this.mediopago = mediopago;
	}


	public String getDescripcionmediopago() {
		return descripcionmediopago;
	}


	public void setDescripcionmediopago(String descripcionmediopago) {
		this.descripcionmediopago = descripcionmediopago;
	}


	public String getFlujoefectivo() {
		return flujoefectivo;
	}


	public void setFlujoefectivo(String flujoefectivo) {
		this.flujoefectivo = flujoefectivo;
	}


	public String getDescripcionflujoefectivo() {
		return descripcionflujoefectivo;
	}


	public void setDescripcionflujoefectivo(String descripcionflujoefectivo) {
		this.descripcionflujoefectivo = descripcionflujoefectivo;
	}


	public String getTablacentrocosto() {
		return tablacentrocosto;
	}


	public void setTablacentrocosto(String tablacentrocosto) {
		this.tablacentrocosto = tablacentrocosto;
	}


	public String getCentrocosto() {
		return centrocosto;
	}


	public void setCentrocosto(String centrocosto) {
		this.centrocosto = centrocosto;
	}


	public String getDescripcioncentrocosto() {
		return descripcioncentrocosto;
	}


	public void setDescripcioncentrocosto(String descripcioncentrocosto) {
		this.descripcioncentrocosto = descripcioncentrocosto;
	}


	public double getPorcentajecentrocosto() {
		return porcentajecentrocosto;
	}


	public void setPorcentajecentrocosto(double porcentajecentrocosto) {
		this.porcentajecentrocosto = porcentajecentrocosto;
	}


	public Integer getCorrelativoorigen() {
		return correlativoorigen;
	}


	public void setCorrelativoorigen(Integer correlativoorigen) {
		this.correlativoorigen = correlativoorigen;
	}


	public String getGlosa() {
		return glosa;
	}


	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}


	public String getIncluirregistrocompra() {
		return incluirregistrocompra;
	}


	public void setIncluirregistrocompra(String incluirregistrocompra) {
		this.incluirregistrocompra = incluirregistrocompra;
	}


	public String getIndicadorcalculo() {
		return indicadorcalculo;
	}


	public void setIndicadorcalculo(String indicadorcalculo) {
		this.indicadorcalculo = indicadorcalculo;
	}


	public Integer getColumnamonto() {
		return columnamonto;
	}


	public void setColumnamonto(Integer columnamonto) {
		this.columnamonto = columnamonto;
	}


	public Integer getColumnaimpuesto() {
		return columnaimpuesto;
	}


	public void setColumnaimpuesto(Integer columnaimpuesto) {
		this.columnaimpuesto = columnaimpuesto;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getDescripcionestado() {
		return descripcionestado;
	}


	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}


	public String getCreacionUsuario() {
		return creacionUsuario;
	}


	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}


	public String getCreacionFecha() {
		return creacionFecha;
	}


	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}


	public String getModificacionUsuario() {
		return modificacionUsuario;
	}


	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}


	public String getModificacionFecha() {
		return modificacionFecha;
	}


	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}


	public RegistroContable(Integer empresa, Integer ejerciciocontable, String operacioncontable,
			Integer asientocontable, Integer registrocontable, String cuentacontable, String descripcioncuentacontable,
			String entidad, String nombreentidad, String ruc, String tipoanotacion, String descripciontipoanotacion,
			String tiporegistro, String descripciontiporegistro, String moneda, String descripcionmoneda,
			String tipocambio, String descripciontipocambio, double tipocambiosoles, double tipocambiodolares,
			double montoorigen, double montonacional, double montodolares, String tipodocumento,
			String descripciontipodocumento, String numeroserie, String numerocomprobanteini,
			String numerocomprobantefin, String fechaemision, String fechavencimiento, String mediopago,
			String descripcionmediopago, String flujoefectivo, String descripcionflujoefectivo, String tablacentrocosto,
			String centrocosto, String descripcioncentrocosto, double porcentajecentrocosto, Integer correlativoorigen,
			String glosa, String incluirregistrocompra, String indicadorcalculo, Integer columnamonto,
			Integer columnaimpuesto, String estado, String descripcionestado, String creacionUsuario,
			String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.operacioncontable = operacioncontable;
		this.asientocontable = asientocontable;
		this.registrocontable = registrocontable;
		this.cuentacontable = cuentacontable;
		this.descripcioncuentacontable = descripcioncuentacontable;
		this.entidad = entidad;
		this.nombreentidad = nombreentidad;
		this.ruc = ruc;
		this.tipoanotacion = tipoanotacion;
		this.descripciontipoanotacion = descripciontipoanotacion;
		this.tiporegistro = tiporegistro;
		this.descripciontiporegistro = descripciontiporegistro;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.tipocambio = tipocambio;
		this.descripciontipocambio = descripciontipocambio;
		this.tipocambiosoles = tipocambiosoles;
		this.tipocambiodolares = tipocambiodolares;
		this.montoorigen = montoorigen;
		this.montonacional = montonacional;
		this.montodolares = montodolares;
		this.tipodocumento = tipodocumento;
		this.descripciontipodocumento = descripciontipodocumento;
		this.numeroserie = numeroserie;
		this.numerocomprobanteini = numerocomprobanteini;
		this.numerocomprobantefin = numerocomprobantefin;
		this.fechaemision = fechaemision;
		this.fechavencimiento = fechavencimiento;
		this.mediopago = mediopago;
		this.descripcionmediopago = descripcionmediopago;
		this.flujoefectivo = flujoefectivo;
		this.descripcionflujoefectivo = descripcionflujoefectivo;
		this.tablacentrocosto = tablacentrocosto;
		this.centrocosto = centrocosto;
		this.descripcioncentrocosto = descripcioncentrocosto;
		this.porcentajecentrocosto = porcentajecentrocosto;
		this.correlativoorigen = correlativoorigen;
		this.glosa = glosa;
		this.incluirregistrocompra = incluirregistrocompra;
		this.indicadorcalculo = indicadorcalculo;
		this.columnamonto = columnamonto;
		this.columnaimpuesto = columnaimpuesto;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}


	public RegistroContable() {


	}
	
	
}

	      