package com.siegez.models;

public class EnteDireccion {
	private Integer empresa;
	private String 	entidad;
	private Integer	vez;
	private String 	tipodomicilio;
	private String 	descripciontipodomicilio;
	private String 	pais;
	private String 	nombrepais;
	private String 	ubigeo;
	private String 	descripcionubigeo;
	private String  direccion;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public Integer getVez() {
		return vez;
	}
	public void setVez(Integer vez) {
		this.vez = vez;
	}
	public String getTipodomicilio() {
		return tipodomicilio;
	}
	public void setTipodomicilio(String tipodomicilio) {
		this.tipodomicilio = tipodomicilio;
	}
	public String getDescripciontipodomicilio() {
		return descripciontipodomicilio;
	}
	public void setDescripciontipodomicilio(String descripciontipodomicilio) {
		this.descripciontipodomicilio = descripciontipodomicilio;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombrepais() {
		return nombrepais;
	}
	public void setNombrepais(String nombrepais) {
		this.nombrepais = nombrepais;
	}
	public String getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(String ubigeo) {
		this.ubigeo = ubigeo;
	}
	public String getDescripcionubigeo() {
		return descripcionubigeo;
	}
	public void setDescripcionubigeo(String descripcionubigeo) {
		this.descripcionubigeo = descripcionubigeo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public EnteDireccion(Integer empresa, String entidad, Integer vez, String tipodomicilio,
			String descripciontipodomicilio, String pais, String nombrepais, String ubigeo, String descripcionubigeo,
			String direccion, String estado, String descripcionestado, String creacionUsuario, String creacionFecha,
			String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.entidad = entidad;
		this.vez = vez;
		this.tipodomicilio = tipodomicilio;
		this.descripciontipodomicilio = descripciontipodomicilio;
		this.pais = pais;
		this.nombrepais = nombrepais;
		this.ubigeo = ubigeo;
		this.descripcionubigeo = descripcionubigeo;
		this.direccion = direccion;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public EnteDireccion() {
		
	}
	
	
}
