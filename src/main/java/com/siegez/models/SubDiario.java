package com.siegez.models;

public class SubDiario {

	private Integer empresa;
	private String 	subdiario;
	private String 	descripcion;	
	private String 	indicadorcompra;
	private Integer columnacompra;
	private Integer columnaigvcompra;
	private String 	indicadorhonorario;
	private Integer columnahonorario;
	private Integer columnaigvhonorario;
	private String 	indicadorventa;
	private Integer columnaventa;
	private Integer columnaigvventa;
	private String 	indicadorcaja;
	private Integer columnacaja;
	private Integer columnaigvcaja;
	private String 	indicadoregreso;
	private Integer columnaegreso;
	private Integer columnaigvegreso;
	private String 	indicadoringreso;
	private Integer columnaingreso;
	private Integer columnaigvingreso;
	private String 	indicadordiario;
	private Integer columnadiario;
	private Integer columnaigvdiario;
	private String 	indicadorimportacion;
	private Integer columnaimportacion;
	private Integer columnaigvimportacion;
	private String 	indicadornodomiciliado;
	private Integer columnanodomiciliado;
	private Integer columnaigvnodomiciliado;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getSubdiario() {
		return subdiario;
	}
	public void setSubdiario(String subdiario) {
		this.subdiario = subdiario;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getIndicadorcompra() {
		return indicadorcompra;
	}
	public void setIndicadorcompra(String indicadorcompra) {
		this.indicadorcompra = indicadorcompra;
	}
	public Integer getColumnacompra() {
		return columnacompra;
	}
	public void setColumnacompra(Integer columnacompra) {
		this.columnacompra = columnacompra;
	}
	public Integer getColumnaigvcompra() {
		return columnaigvcompra;
	}
	public void setColumnaigvcompra(Integer columnaigvcompra) {
		this.columnaigvcompra = columnaigvcompra;
	}
	public String getIndicadorhonorario() {
		return indicadorhonorario;
	}
	public void setIndicadorhonorario(String indicadorhonorario) {
		this.indicadorhonorario = indicadorhonorario;
	}
	public Integer getColumnahonorario() {
		return columnahonorario;
	}
	public void setColumnahonorario(Integer columnahonorario) {
		this.columnahonorario = columnahonorario;
	}
	public Integer getColumnaigvhonorario() {
		return columnaigvhonorario;
	}
	public void setColumnaigvhonorario(Integer columnaigvhonorario) {
		this.columnaigvhonorario = columnaigvhonorario;
	}
	public String getIndicadorventa() {
		return indicadorventa;
	}
	public void setIndicadorventa(String indicadorventa) {
		this.indicadorventa = indicadorventa;
	}
	public Integer getColumnaventa() {
		return columnaventa;
	}
	public void setColumnaventa(Integer columnaventa) {
		this.columnaventa = columnaventa;
	}
	public Integer getColumnaigvventa() {
		return columnaigvventa;
	}
	public void setColumnaigvventa(Integer columnaigvventa) {
		this.columnaigvventa = columnaigvventa;
	}
	public String getIndicadorcaja() {
		return indicadorcaja;
	}
	public void setIndicadorcaja(String indicadorcaja) {
		this.indicadorcaja = indicadorcaja;
	}
	public Integer getColumnacaja() {
		return columnacaja;
	}
	public void setColumnacaja(Integer columnacaja) {
		this.columnacaja = columnacaja;
	}
	public Integer getColumnaigvcaja() {
		return columnaigvcaja;
	}
	public void setColumnaigvcaja(Integer columnaigvcaja) {
		this.columnaigvcaja = columnaigvcaja;
	}
	public String getIndicadoregreso() {
		return indicadoregreso;
	}
	public void setIndicadoregreso(String indicadoregreso) {
		this.indicadoregreso = indicadoregreso;
	}
	public Integer getColumnaegreso() {
		return columnaegreso;
	}
	public void setColumnaegreso(Integer columnaegreso) {
		this.columnaegreso = columnaegreso;
	}
	public Integer getColumnaigvegreso() {
		return columnaigvegreso;
	}
	public void setColumnaigvegreso(Integer columnaigvegreso) {
		this.columnaigvegreso = columnaigvegreso;
	}
	public String getIndicadoringreso() {
		return indicadoringreso;
	}
	public void setIndicadoringreso(String indicadoringreso) {
		this.indicadoringreso = indicadoringreso;
	}
	public Integer getColumnaingreso() {
		return columnaingreso;
	}
	public void setColumnaingreso(Integer columnaingreso) {
		this.columnaingreso = columnaingreso;
	}
	public Integer getColumnaigvingreso() {
		return columnaigvingreso;
	}
	public void setColumnaigvingreso(Integer columnaigvingreso) {
		this.columnaigvingreso = columnaigvingreso;
	}
	public String getIndicadordiario() {
		return indicadordiario;
	}
	public void setIndicadordiario(String indicadordiario) {
		this.indicadordiario = indicadordiario;
	}
	public Integer getColumnadiario() {
		return columnadiario;
	}
	public void setColumnadiario(Integer columnadiario) {
		this.columnadiario = columnadiario;
	}
	public Integer getColumnaigvdiario() {
		return columnaigvdiario;
	}
	public void setColumnaigvdiario(Integer columnaigvdiario) {
		this.columnaigvdiario = columnaigvdiario;
	}
	public String getIndicadorimportacion() {
		return indicadorimportacion;
	}
	public void setIndicadorimportacion(String indicadorimportacion) {
		this.indicadorimportacion = indicadorimportacion;
	}
	public Integer getColumnaimportacion() {
		return columnaimportacion;
	}
	public void setColumnaimportacion(Integer columnaimportacion) {
		this.columnaimportacion = columnaimportacion;
	}
	public Integer getColumnaigvimportacion() {
		return columnaigvimportacion;
	}
	public void setColumnaigvimportacion(Integer columnaigvimportacion) {
		this.columnaigvimportacion = columnaigvimportacion;
	}
	public String getIndicadornodomiciliado() {
		return indicadornodomiciliado;
	}
	public void setIndicadornodomiciliado(String indicadornodomiciliado) {
		this.indicadornodomiciliado = indicadornodomiciliado;
	}
	public Integer getColumnanodomiciliado() {
		return columnanodomiciliado;
	}
	public void setColumnanodomiciliado(Integer columnanodomiciliado) {
		this.columnanodomiciliado = columnanodomiciliado;
	}
	public Integer getColumnaigvnodomiciliado() {
		return columnaigvnodomiciliado;
	}
	public void setColumnaigvnodomiciliado(Integer columnaigvnodomiciliado) {
		this.columnaigvnodomiciliado = columnaigvnodomiciliado;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public SubDiario(Integer empresa, String subdiario, String descripcion, String indicadorcompra,
			Integer columnacompra, Integer columnaigvcompra, String indicadorhonorario, Integer columnahonorario,
			Integer columnaigvhonorario, String indicadorventa, Integer columnaventa, Integer columnaigvventa,
			String indicadorcaja, Integer columnacaja, Integer columnaigvcaja, String indicadoregreso,
			Integer columnaegreso, Integer columnaigvegreso, String indicadoringreso, Integer columnaingreso,
			Integer columnaigvingreso, String indicadordiario, Integer columnadiario, Integer columnaigvdiario,
			String indicadorimportacion, Integer columnaimportacion, Integer columnaigvimportacion,
			String indicadornodomiciliado, Integer columnanodomiciliado, Integer columnaigvnodomiciliado, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.subdiario = subdiario;
		this.descripcion = descripcion;
		this.indicadorcompra = indicadorcompra;
		this.columnacompra = columnacompra;
		this.columnaigvcompra = columnaigvcompra;
		this.indicadorhonorario = indicadorhonorario;
		this.columnahonorario = columnahonorario;
		this.columnaigvhonorario = columnaigvhonorario;
		this.indicadorventa = indicadorventa;
		this.columnaventa = columnaventa;
		this.columnaigvventa = columnaigvventa;
		this.indicadorcaja = indicadorcaja;
		this.columnacaja = columnacaja;
		this.columnaigvcaja = columnaigvcaja;
		this.indicadoregreso = indicadoregreso;
		this.columnaegreso = columnaegreso;
		this.columnaigvegreso = columnaigvegreso;
		this.indicadoringreso = indicadoringreso;
		this.columnaingreso = columnaingreso;
		this.columnaigvingreso = columnaigvingreso;
		this.indicadordiario = indicadordiario;
		this.columnadiario = columnadiario;
		this.columnaigvdiario = columnaigvdiario;
		this.indicadorimportacion = indicadorimportacion;
		this.columnaimportacion = columnaimportacion;
		this.columnaigvimportacion = columnaigvimportacion;
		this.indicadornodomiciliado = indicadornodomiciliado;
		this.columnanodomiciliado = columnanodomiciliado;
		this.columnaigvnodomiciliado = columnaigvnodomiciliado;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public SubDiario() {

	}
	
}
