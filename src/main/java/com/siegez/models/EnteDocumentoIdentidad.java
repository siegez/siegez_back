package com.siegez.models;

public class EnteDocumentoIdentidad {

	private Integer empresa;
	private String 	entidad;
	private Integer	vez;
	private String 	documentoidentidad;
	private String 	descripciondocumentoidentidad;
	private String 	numerodocumento;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public Integer getVez() {
		return vez;
	}
	public void setVez(Integer vez) {
		this.vez = vez;
	}
	public String getDocumentoidentidad() {
		return documentoidentidad;
	}
	public void setDocumentoidentidad(String documentoidentidad) {
		this.documentoidentidad = documentoidentidad;
	}
	public String getDescripciondocumentoidentidad() {
		return descripciondocumentoidentidad;
	}
	public void setDescripciondocumentoidentidad(String descripciondocumentoidentidad) {
		this.descripciondocumentoidentidad = descripciondocumentoidentidad;
	}
	public String getNumerodocumento() {
		return numerodocumento;
	}
	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public EnteDocumentoIdentidad(Integer empresa, String entidad, Integer vez, String documentoidentidad,
			String descripciondocumentoidentidad, String numerodocumento, String estado, String descripcionestado,
			String creacionUsuario, String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.entidad = entidad;
		this.vez = vez;
		this.documentoidentidad = documentoidentidad;
		this.descripciondocumentoidentidad = descripciondocumentoidentidad;
		this.numerodocumento = numerodocumento;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public EnteDocumentoIdentidad() {
		
	}
	
}
