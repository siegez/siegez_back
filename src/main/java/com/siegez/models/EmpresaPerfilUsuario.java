package com.siegez.models;
import java.util.Date;

public class EmpresaPerfilUsuario {
	
	private Integer	empresa;
	private String	descEmpresa;
	private Integer perfil;
	private String  descPerfil;
	private String	username;
	private String	indDefecto;
	private String 	estado;		
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modificacionUsuario;
	private Date 	modificacionFecha;
	
	
	public Integer getPerfil() {
		return perfil;
	}
	public void setPerfil(Integer perfil) {
		this.perfil = perfil;
	}
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIndDefecto() {
		return indDefecto;
	}
	public void setIndDefecto(String indDefecto) {
		this.indDefecto = indDefecto;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public Date getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(Date modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public String getDescEmpresa() {
		return descEmpresa;
	}
	public void setDescEmpresa(String descEmpresa) {
		this.descEmpresa = descEmpresa;
	}
	public String getDescPerfil() {
		return descPerfil;
	}
	public void setDescPerfil(String descPerfil) {
		this.descPerfil = descPerfil;
	}
	
	public EmpresaPerfilUsuario(Integer empresa, String descEmpresa, Integer perfil, String descPerfil, String username, String indDefecto, String estado, 
			String creacionUsuario, Date creacionFecha, String modificacionUsuario, Date modificacionFecha) {
		//super();		
		this.empresa = empresa;
		this.descEmpresa = descEmpresa;
		this.perfil = perfil;
		this.descPerfil = descPerfil;
		this.username = username;
		this.indDefecto = indDefecto;
		this.estado = estado;		
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public EmpresaPerfilUsuario() {

	}

}
