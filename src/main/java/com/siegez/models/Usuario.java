package com.siegez.models;

import java.util.Date;

public class Usuario {

	//You can decorate with annotations, I don't do it because I don't know which fields are not null, etc.
	//Examples
	//@NotEmpty
	//@NotNull(message = "No puede estar vacio")
	//@Min(1), @Max(100)
	private String username;
	private String	nombre;
	private String	password;
	private String	email;
	private String	telefono;
	private Date fechaUltimoIngreso;
	private String idUserDB; 
	private String sexo;	
	private String foto;
	private String codLicencia;
	private String estado;	
	private String creacionUsuario;
	private Date creacionFecha;
	private String modificacionUsuario;
	private Date modificacionFecha;
	
	//*****************************************GETTERS AND SETTERES******************************************
	
	public String getNombre() {
		return nombre;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Date getFechaUltimoIngreso() {
		return fechaUltimoIngreso;
	}
	public void setFechaUltimoIngreso(Date fechaUltimoIngreso) {
		this.fechaUltimoIngreso = fechaUltimoIngreso;
	}
	public String getIdUserDB() {
		return idUserDB;
	}
	public void setIdUserDB(String idUserDB) {
		this.idUserDB = idUserDB;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getCodLicencia() {
		return codLicencia;
	}
	public void setCodLicencia(String codLicencia) {
		this.codLicencia = codLicencia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public Date getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(Date modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}

	//****************************************************************************************************
	public Usuario(String username, String nombre, String password, String email, String telefono,
			Date fechaUltimoIngreso, String idUserDB, String sexo, String foto, String codLicencia, String estado,
			String creacionUsuario, Date creacionFecha, String modificacionUsuario, Date modificacionFecha) {
		super();
		this.username = username;
		this.nombre = nombre;
		this.password = password;
		this.email = email;
		this.telefono = telefono;
		this.fechaUltimoIngreso = fechaUltimoIngreso;
		this.idUserDB = idUserDB;
		this.sexo = sexo;
		this.foto = foto;
		this.codLicencia = codLicencia;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public Usuario() {
		
	}	
}
