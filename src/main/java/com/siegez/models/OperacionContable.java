package com.siegez.models;

public class OperacionContable {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String 	operacioncontable;
	private String 	periodocontable;
	private String 	descripcionperiodocontable;
	private String 	tipooperacion;
	private String 	descripciontipooperacion;
	private String 	fechaoperacion;
	private String 	fechaemision;
	private String 	fechavencimiento;
	private String 	entidad;
	private String 	nombreentidad;
	private String 	ruc;
	private String 	banco;
	private String 	nombrebanco;
	private String 	cuentabancaria;
	private String 	tipodocumento;
	private String 	descripciontipodocumento;
	private String 	subdiario;
	private String 	descripcionsubdiario;
	private String 	aduana;
	private String 	descripcionaduana;
	private String 	numeroserie;
	private String 	numerocomprobanteini;
	private String 	numerocomprobantefin;
	private String 	moneda;
	private String 	descripcionmoneda;
	private String 	tipocambio;
	private String 	descripciontipocambio;
	private double  tipocambiosoles;
	private double 	tipocambiodolares;
	private double 	baseimponible;
	private double 	inafecto;
	private double 	adicionalnodom;
	private String 	indimpuestorenta;
	private double 	porcentajeimpuesto;
	private double 	impuesto;
	private double 	impuestovar;
	private double 	deduccionnodom;
	private double 	precioorigen;
	private double 	preciosoles;
	private double 	preciodolares;
	private String 	mediopago;
	private String 	descripcionmediopago;
	private String 	flujoefectivo;
	private String 	descripcionflujoefectivo;
	private String 	bienservicio;
	private String 	descripcionbienservicio;
	private String 	detraccion;
	private String 	descripciondetraccion;
	private double 	tasadetraccion;
	private String 	numerodetraccion;
	private String 	fechadetraccion;
	private double 	basedetraccion;
	private double 	montodetraccion;
	private String 	tipodocumentoref;
	private String 	descripciontipodocumentoref;
	private Integer	ejerciciocontableref;
	private String 	operacioncontableref;	
	private String	numeroserieref;
	private String	numerocomprobanteiniref;
	private String 	fechaemisionref;
	private double  tipocambiosolesref;
	private double 	baseimponibleref;
	private double 	inafectoref;
	private double 	impuestoref;
	private double 	precioorigenref;	
	private String 	girado;
	private String 	glosa;
	private String 	tipodocumentonodom;
	private String 	descripciontipodocumentonodom;
	private String 	numeroserienodom;
	private Integer	annocomprobantenodom;
	private String 	numerocomprobantenodom;
	private String 	entidadnodom;
	private String 	nombreentidadnodom;
	private String 	rucnodom;
	private String 	vinculoeconomiconodom;
	private String 	descripcionvinculoeconomiconodom;
	private double 	tasaretencionnodom;
	private double 	impuestoretenidonodom;
	private String 	convenionodom;
	private String 	descripcionconvenionodom;
	private String 	exoneracionnodom;
	private String 	descripcionexoneracionnodom;
	private String 	tiporentanodom;
	private String 	descripciontiporentanodom;
	private String 	modalidadnodom;
	private String 	descripcionmodalidadnodom;
	private String 	articulonodom;
	private Integer columnabaseimponible;
	private Integer columnaimpuesto;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getOperacioncontable() {
		return operacioncontable;
	}
	public void setOperacioncontable(String operacioncontable) {
		this.operacioncontable = operacioncontable;
	}
	public String getPeriodocontable() {
		return periodocontable;
	}
	public void setPeriodocontable(String periodocontable) {
		this.periodocontable = periodocontable;
	}
	public String getDescripcionperiodocontable() {
		return descripcionperiodocontable;
	}
	public void setDescripcionperiodocontable(String descripcionperiodocontable) {
		this.descripcionperiodocontable = descripcionperiodocontable;
	}
	public String getTipooperacion() {
		return tipooperacion;
	}
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}
	public String getDescripciontipooperacion() {
		return descripciontipooperacion;
	}
	public void setDescripciontipooperacion(String descripciontipooperacion) {
		this.descripciontipooperacion = descripciontipooperacion;
	}
	public String getFechaoperacion() {
		return fechaoperacion;
	}
	public void setFechaoperacion(String fechaoperacion) {
		this.fechaoperacion = fechaoperacion;
	}
	public String getFechaemision() {
		return fechaemision;
	}
	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}
	public String getFechavencimiento() {
		return fechavencimiento;
	}
	public void setFechavencimiento(String fechavencimiento) {
		this.fechavencimiento = fechavencimiento;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getNombreentidad() {
		return nombreentidad;
	}
	public void setNombreentidad(String nombreentidad) {
		this.nombreentidad = nombreentidad;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getNombrebanco() {
		return nombrebanco;
	}
	public void setNombrebanco(String nombrebanco) {
		this.nombrebanco = nombrebanco;
	}
	public String getCuentabancaria() {
		return cuentabancaria;
	}
	public void setCuentabancaria(String cuentabancaria) {
		this.cuentabancaria = cuentabancaria;
	}
	public String getTipodocumento() {
		return tipodocumento;
	}
	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	public String getDescripciontipodocumento() {
		return descripciontipodocumento;
	}
	public void setDescripciontipodocumento(String descripciontipodocumento) {
		this.descripciontipodocumento = descripciontipodocumento;
	}
	public String getSubdiario() {
		return subdiario;
	}
	public void setSubdiario(String subdiario) {
		this.subdiario = subdiario;
	}
	public String getDescripcionsubdiario() {
		return descripcionsubdiario;
	}
	public void setDescripcionsubdiario(String descripcionsubdiario) {
		this.descripcionsubdiario = descripcionsubdiario;
	}
	public String getAduana() {
		return aduana;
	}
	public void setAduana(String aduana) {
		this.aduana = aduana;
	}
	public String getDescripcionaduana() {
		return descripcionaduana;
	}
	public void setDescripcionaduana(String descripcionaduana) {
		this.descripcionaduana = descripcionaduana;
	}
	public String getNumeroserie() {
		return numeroserie;
	}
	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}
	public String getNumerocomprobanteini() {
		return numerocomprobanteini;
	}
	public void setNumerocomprobanteini(String numerocomprobanteini) {
		this.numerocomprobanteini = numerocomprobanteini;
	}
	public String getNumerocomprobantefin() {
		return numerocomprobantefin;
	}
	public void setNumerocomprobantefin(String numerocomprobantefin) {
		this.numerocomprobantefin = numerocomprobantefin;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}
	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}
	public String getTipocambio() {
		return tipocambio;
	}
	public void setTipocambio(String tipocambio) {
		this.tipocambio = tipocambio;
	}
	public String getDescripciontipocambio() {
		return descripciontipocambio;
	}
	public void setDescripciontipocambio(String descripciontipocambio) {
		this.descripciontipocambio = descripciontipocambio;
	}
	public double getTipocambiosoles() {
		return tipocambiosoles;
	}
	public void setTipocambiosoles(double tipocambiosoles) {
		this.tipocambiosoles = tipocambiosoles;
	}
	public double getTipocambiodolares() {
		return tipocambiodolares;
	}
	public void setTipocambiodolares(double tipocambiodolares) {
		this.tipocambiodolares = tipocambiodolares;
	}
	public double getBaseimponible() {
		return baseimponible;
	}
	public void setBaseimponible(double baseimponible) {
		this.baseimponible = baseimponible;
	}
	public double getInafecto() {
		return inafecto;
	}
	public void setInafecto(double inafecto) {
		this.inafecto = inafecto;
	}
	public double getAdicionalnodom() {
		return adicionalnodom;
	}
	public void setAdicionalnodom(double adicionalnodom) {
		this.adicionalnodom = adicionalnodom;
	}
	public String getIndimpuestorenta() {
		return indimpuestorenta;
	}
	public void setIndimpuestorenta(String indimpuestorenta) {
		this.indimpuestorenta = indimpuestorenta;
	}
	public double getPorcentajeimpuesto() {
		return porcentajeimpuesto;
	}
	public void setPorcentajeimpuesto(double porcentajeimpuesto) {
		this.porcentajeimpuesto = porcentajeimpuesto;
	}
	public double getImpuesto() {
		return impuesto;
	}
	public void setImpuesto(double impuesto) {
		this.impuesto = impuesto;
	}
	public double getImpuestovar() {
		return impuestovar;
	}
	public void setImpuestovar(double impuestovar) {
		this.impuestovar = impuestovar;
	}
	public double getDeduccionnodom() {
		return deduccionnodom;
	}
	public void setDeduccionnodom(double deduccionnodom) {
		this.deduccionnodom = deduccionnodom;
	}
	public double getPrecioorigen() {
		return precioorigen;
	}
	public void setPrecioorigen(double precioorigen) {
		this.precioorigen = precioorigen;
	}
	public double getPreciosoles() {
		return preciosoles;
	}
	public void setPreciosoles(double preciosoles) {
		this.preciosoles = preciosoles;
	}
	public double getPreciodolares() {
		return preciodolares;
	}
	public void setPreciodolares(double preciodolares) {
		this.preciodolares = preciodolares;
	}
	public String getMediopago() {
		return mediopago;
	}
	public void setMediopago(String mediopago) {
		this.mediopago = mediopago;
	}
	public String getDescripcionmediopago() {
		return descripcionmediopago;
	}
	public void setDescripcionmediopago(String descripcionmediopago) {
		this.descripcionmediopago = descripcionmediopago;
	}
	public String getFlujoefectivo() {
		return flujoefectivo;
	}
	public void setFlujoefectivo(String flujoefectivo) {
		this.flujoefectivo = flujoefectivo;
	}
	public String getDescripcionflujoefectivo() {
		return descripcionflujoefectivo;
	}
	public void setDescripcionflujoefectivo(String descripcionflujoefectivo) {
		this.descripcionflujoefectivo = descripcionflujoefectivo;
	}
	public String getBienservicio() {
		return bienservicio;
	}
	public void setBienservicio(String bienservicio) {
		this.bienservicio = bienservicio;
	}
	public String getDescripcionbienservicio() {
		return descripcionbienservicio;
	}
	public void setDescripcionbienservicio(String descripcionbienservicio) {
		this.descripcionbienservicio = descripcionbienservicio;
	}
	public String getDetraccion() {
		return detraccion;
	}
	public void setDetraccion(String detraccion) {
		this.detraccion = detraccion;
	}
	public String getDescripciondetraccion() {
		return descripciondetraccion;
	}
	public void setDescripciondetraccion(String descripciondetraccion) {
		this.descripciondetraccion = descripciondetraccion;
	}
	public double getTasadetraccion() {
		return tasadetraccion;
	}
	public void setTasadetraccion(double tasadetraccion) {
		this.tasadetraccion = tasadetraccion;
	}
	public String getNumerodetraccion() {
		return numerodetraccion;
	}
	public void setNumerodetraccion(String numerodetraccion) {
		this.numerodetraccion = numerodetraccion;
	}
	public String getFechadetraccion() {
		return fechadetraccion;
	}
	public void setFechadetraccion(String fechadetraccion) {
		this.fechadetraccion = fechadetraccion;
	}
	public double getBasedetraccion() {
		return basedetraccion;
	}
	public void setBasedetraccion(double basedetraccion) {
		this.basedetraccion = basedetraccion;
	}
	public double getMontodetraccion() {
		return montodetraccion;
	}
	public void setMontodetraccion(double montodetraccion) {
		this.montodetraccion = montodetraccion;
	}
	public String getTipodocumentoref() {
		return tipodocumentoref;
	}
	public void setTipodocumentoref(String tipodocumentoref) {
		this.tipodocumentoref = tipodocumentoref;
	}
	public String getDescripciontipodocumentoref() {
		return descripciontipodocumentoref;
	}
	public void setDescripciontipodocumentoref(String descripciontipodocumentoref) {
		this.descripciontipodocumentoref = descripciontipodocumentoref;
	}
	public Integer getEjerciciocontableref() {
		return ejerciciocontableref;
	}
	public void setEjerciciocontableref(Integer ejerciciocontableref) {
		this.ejerciciocontableref = ejerciciocontableref;
	}
	public String getOperacioncontableref() {
		return operacioncontableref;
	}
	public void setOperacioncontableref(String operacioncontableref) {
		this.operacioncontableref = operacioncontableref;
	}
	public String getNumeroserieref() {
		return numeroserieref;
	}
	public void setNumeroserieref(String numeroserieref) {
		this.numeroserieref = numeroserieref;
	}
	public String getNumerocomprobanteiniref() {
		return numerocomprobanteiniref;
	}
	public void setNumerocomprobanteiniref(String numerocomprobanteiniref) {
		this.numerocomprobanteiniref = numerocomprobanteiniref;
	}
	public String getFechaemisionref() {
		return fechaemisionref;
	}
	public void setFechaemisionref(String fechaemisionref) {
		this.fechaemisionref = fechaemisionref;
	}
	public double getTipocambiosolesref() {
		return tipocambiosolesref;
	}
	public void setTipocambiosolesref(double tipocambiosolesref) {
		this.tipocambiosolesref = tipocambiosolesref;
	}
	public double getBaseimponibleref() {
		return baseimponibleref;
	}
	public void setBaseimponibleref(double baseimponibleref) {
		this.baseimponibleref = baseimponibleref;
	}
	public double getInafectoref() {
		return inafectoref;
	}
	public void setInafectoref(double inafectoref) {
		this.inafectoref = inafectoref;
	}
	public double getImpuestoref() {
		return impuestoref;
	}
	public void setImpuestoref(double impuestoref) {
		this.impuestoref = impuestoref;
	}
	public double getPrecioorigenref() {
		return precioorigenref;
	}
	public void setPrecioorigenref(double precioorigenref) {
		this.precioorigenref = precioorigenref;
	}
	public String getGirado() {
		return girado;
	}
	public void setGirado(String girado) {
		this.girado = girado;
	}
	public String getGlosa() {
		return glosa;
	}
	public void setGlosa(String glosa) {
		this.glosa = glosa;
	}
	public String getTipodocumentonodom() {
		return tipodocumentonodom;
	}
	public void setTipodocumentonodom(String tipodocumentonodom) {
		this.tipodocumentonodom = tipodocumentonodom;
	}
	public String getDescripciontipodocumentonodom() {
		return descripciontipodocumentonodom;
	}
	public void setDescripciontipodocumentonodom(String descripciontipodocumentonodom) {
		this.descripciontipodocumentonodom = descripciontipodocumentonodom;
	}
	public String getNumeroserienodom() {
		return numeroserienodom;
	}
	public void setNumeroserienodom(String numeroserienodom) {
		this.numeroserienodom = numeroserienodom;
	}
	public Integer getAnnocomprobantenodom() {
		return annocomprobantenodom;
	}
	public void setAnnocomprobantenodom(Integer annocomprobantenodom) {
		this.annocomprobantenodom = annocomprobantenodom;
	}
	public String getNumerocomprobantenodom() {
		return numerocomprobantenodom;
	}
	public void setNumerocomprobantenodom(String numerocomprobantenodom) {
		this.numerocomprobantenodom = numerocomprobantenodom;
	}
	public String getEntidadnodom() {
		return entidadnodom;
	}
	public void setEntidadnodom(String entidadnodom) {
		this.entidadnodom = entidadnodom;
	}
	public String getNombreentidadnodom() {
		return nombreentidadnodom;
	}
	public void setNombreentidadnodom(String nombreentidadnodom) {
		this.nombreentidadnodom = nombreentidadnodom;
	}
	public String getRucnodom() {
		return rucnodom;
	}
	public void setRucnodom(String rucnodom) {
		this.rucnodom = rucnodom;
	}
	public String getVinculoeconomiconodom() {
		return vinculoeconomiconodom;
	}
	public void setVinculoeconomiconodom(String vinculoeconomiconodom) {
		this.vinculoeconomiconodom = vinculoeconomiconodom;
	}
	public String getDescripcionvinculoeconomiconodom() {
		return descripcionvinculoeconomiconodom;
	}
	public void setDescripcionvinculoeconomiconodom(String descripcionvinculoeconomiconodom) {
		this.descripcionvinculoeconomiconodom = descripcionvinculoeconomiconodom;
	}
	public double getTasaretencionnodom() {
		return tasaretencionnodom;
	}
	public void setTasaretencionnodom(double tasaretencionnodom) {
		this.tasaretencionnodom = tasaretencionnodom;
	}
	public double getImpuestoretenidonodom() {
		return impuestoretenidonodom;
	}
	public void setImpuestoretenidonodom(double impuestoretenidonodom) {
		this.impuestoretenidonodom = impuestoretenidonodom;
	}
	public String getConvenionodom() {
		return convenionodom;
	}
	public void setConvenionodom(String convenionodom) {
		this.convenionodom = convenionodom;
	}
	public String getDescripcionconvenionodom() {
		return descripcionconvenionodom;
	}
	public void setDescripcionconvenionodom(String descripcionconvenionodom) {
		this.descripcionconvenionodom = descripcionconvenionodom;
	}
	public String getExoneracionnodom() {
		return exoneracionnodom;
	}
	public void setExoneracionnodom(String exoneracionnodom) {
		this.exoneracionnodom = exoneracionnodom;
	}
	public String getDescripcionexoneracionnodom() {
		return descripcionexoneracionnodom;
	}
	public void setDescripcionexoneracionnodom(String descripcionexoneracionnodom) {
		this.descripcionexoneracionnodom = descripcionexoneracionnodom;
	}
	public String getTiporentanodom() {
		return tiporentanodom;
	}
	public void setTiporentanodom(String tiporentanodom) {
		this.tiporentanodom = tiporentanodom;
	}
	public String getDescripciontiporentanodom() {
		return descripciontiporentanodom;
	}
	public void setDescripciontiporentanodom(String descripciontiporentanodom) {
		this.descripciontiporentanodom = descripciontiporentanodom;
	}
	public String getModalidadnodom() {
		return modalidadnodom;
	}
	public void setModalidadnodom(String modalidadnodom) {
		this.modalidadnodom = modalidadnodom;
	}
	public String getDescripcionmodalidadnodom() {
		return descripcionmodalidadnodom;
	}
	public void setDescripcionmodalidadnodom(String descripcionmodalidadnodom) {
		this.descripcionmodalidadnodom = descripcionmodalidadnodom;
	}
	public String getArticulonodom() {
		return articulonodom;
	}
	public void setArticulonodom(String articulonodom) {
		this.articulonodom = articulonodom;
	}
	public Integer getColumnabaseimponible() {
		return columnabaseimponible;
	}
	public void setColumnabaseimponible(Integer columnabaseimponible) {
		this.columnabaseimponible = columnabaseimponible;
	}
	public Integer getColumnaimpuesto() {
		return columnaimpuesto;
	}
	public void setColumnaimpuesto(Integer columnaimpuesto) {
		this.columnaimpuesto = columnaimpuesto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public OperacionContable(Integer empresa, Integer ejerciciocontable, String operacioncontable,
			String periodocontable, String descripcionperiodocontable, String tipooperacion,
			String descripciontipooperacion, String fechaoperacion, String fechaemision, String fechavencimiento,
			String entidad, String nombreentidad, String ruc, String banco, String nombrebanco, String cuentabancaria,
			String tipodocumento, String descripciontipodocumento, String subdiario, String descripcionsubdiario,
			String aduana, String descripcionaduana, String numeroserie, String numerocomprobanteini,
			String numerocomprobantefin, String moneda, String descripcionmoneda, String tipocambio,
			String descripciontipocambio, double tipocambiosoles, double tipocambiodolares, double baseimponible,
			double inafecto, double adicionalnodom, String indimpuestorenta, double porcentajeimpuesto, double impuesto,
			double impuestovar, double deduccionnodom, double precioorigen, double preciosoles, double preciodolares,
			String mediopago, String descripcionmediopago, String flujoefectivo, String descripcionflujoefectivo,
			String bienservicio, String descripcionbienservicio, String detraccion, String descripciondetraccion,
			double tasadetraccion, String numerodetraccion, String fechadetraccion, double basedetraccion,
			double montodetraccion, String tipodocumentoref, String descripciontipodocumentoref,
			Integer ejerciciocontableref, String operacioncontableref, String numeroserieref,
			String numerocomprobanteiniref, String fechaemisionref, double tipocambiosolesref, double baseimponibleref,
			double inafectoref, double impuestoref, double precioorigenref, String girado, String glosa,
			String tipodocumentonodom, String descripciontipodocumentonodom, String numeroserienodom,
			Integer annocomprobantenodom, String numerocomprobantenodom, String entidadnodom, String nombreentidadnodom,
			String rucnodom, String vinculoeconomiconodom, String descripcionvinculoeconomiconodom,
			double tasaretencionnodom, double impuestoretenidonodom, String convenionodom,
			String descripcionconvenionodom, String exoneracionnodom, String descripcionexoneracionnodom,
			String tiporentanodom, String descripciontiporentanodom, String modalidadnodom,
			String descripcionmodalidadnodom, String articulonodom, Integer columnabaseimponible,
			Integer columnaimpuesto, String estado, String descripcionestado, String creacionUsuario,
			String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.operacioncontable = operacioncontable;
		this.periodocontable = periodocontable;
		this.descripcionperiodocontable = descripcionperiodocontable;
		this.tipooperacion = tipooperacion;
		this.descripciontipooperacion = descripciontipooperacion;
		this.fechaoperacion = fechaoperacion;
		this.fechaemision = fechaemision;
		this.fechavencimiento = fechavencimiento;
		this.entidad = entidad;
		this.nombreentidad = nombreentidad;
		this.ruc = ruc;
		this.banco = banco;
		this.nombrebanco = nombrebanco;
		this.cuentabancaria = cuentabancaria;
		this.tipodocumento = tipodocumento;
		this.descripciontipodocumento = descripciontipodocumento;
		this.subdiario = subdiario;
		this.descripcionsubdiario = descripcionsubdiario;
		this.aduana = aduana;
		this.descripcionaduana = descripcionaduana;
		this.numeroserie = numeroserie;
		this.numerocomprobanteini = numerocomprobanteini;
		this.numerocomprobantefin = numerocomprobantefin;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.tipocambio = tipocambio;
		this.descripciontipocambio = descripciontipocambio;
		this.tipocambiosoles = tipocambiosoles;
		this.tipocambiodolares = tipocambiodolares;
		this.baseimponible = baseimponible;
		this.inafecto = inafecto;
		this.adicionalnodom = adicionalnodom;
		this.indimpuestorenta = indimpuestorenta;
		this.porcentajeimpuesto = porcentajeimpuesto;
		this.impuesto = impuesto;
		this.impuestovar = impuestovar;
		this.deduccionnodom = deduccionnodom;
		this.precioorigen = precioorigen;
		this.preciosoles = preciosoles;
		this.preciodolares = preciodolares;
		this.mediopago = mediopago;
		this.descripcionmediopago = descripcionmediopago;
		this.flujoefectivo = flujoefectivo;
		this.descripcionflujoefectivo = descripcionflujoefectivo;
		this.bienservicio = bienservicio;
		this.descripcionbienservicio = descripcionbienservicio;
		this.detraccion = detraccion;
		this.descripciondetraccion = descripciondetraccion;
		this.tasadetraccion = tasadetraccion;
		this.numerodetraccion = numerodetraccion;
		this.fechadetraccion = fechadetraccion;
		this.basedetraccion = basedetraccion;
		this.montodetraccion = montodetraccion;
		this.tipodocumentoref = tipodocumentoref;
		this.descripciontipodocumentoref = descripciontipodocumentoref;
		this.ejerciciocontableref = ejerciciocontableref;
		this.operacioncontableref = operacioncontableref;
		this.numeroserieref = numeroserieref;
		this.numerocomprobanteiniref = numerocomprobanteiniref;
		this.fechaemisionref = fechaemisionref;
		this.tipocambiosolesref = tipocambiosolesref;
		this.baseimponibleref = baseimponibleref;
		this.inafectoref = inafectoref;
		this.impuestoref = impuestoref;
		this.precioorigenref = precioorigenref;
		this.girado = girado;
		this.glosa = glosa;
		this.tipodocumentonodom = tipodocumentonodom;
		this.descripciontipodocumentonodom = descripciontipodocumentonodom;
		this.numeroserienodom = numeroserienodom;
		this.annocomprobantenodom = annocomprobantenodom;
		this.numerocomprobantenodom = numerocomprobantenodom;
		this.entidadnodom = entidadnodom;
		this.nombreentidadnodom = nombreentidadnodom;
		this.rucnodom = rucnodom;
		this.vinculoeconomiconodom = vinculoeconomiconodom;
		this.descripcionvinculoeconomiconodom = descripcionvinculoeconomiconodom;
		this.tasaretencionnodom = tasaretencionnodom;
		this.impuestoretenidonodom = impuestoretenidonodom;
		this.convenionodom = convenionodom;
		this.descripcionconvenionodom = descripcionconvenionodom;
		this.exoneracionnodom = exoneracionnodom;
		this.descripcionexoneracionnodom = descripcionexoneracionnodom;
		this.tiporentanodom = tiporentanodom;
		this.descripciontiporentanodom = descripciontiporentanodom;
		this.modalidadnodom = modalidadnodom;
		this.descripcionmodalidadnodom = descripcionmodalidadnodom;
		this.articulonodom = articulonodom;
		this.columnabaseimponible = columnabaseimponible;
		this.columnaimpuesto = columnaimpuesto;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public OperacionContable() {
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	

}
