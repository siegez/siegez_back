package com.siegez.models;

import java.util.Date;

public class TablaModulo {

	private String  modulo;
	private Integer	tabla;
	private String  descripcion;
	private Integer orden;
	private String  enlace;
	private String 	estado;	
	private String 	creacionUsuario;
	private Date 	creacionFecha;
	private String 	modUsuario;
	private Date 	modFecha;
	
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	public Integer getTabla() {
		return tabla;
	}
	public void setTabla(Integer tabla) {
		this.tabla = tabla;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public Date getModFecha() {
		return modFecha;
	}
	public void setModFecha(Date modFecha) {
		this.modFecha = modFecha;
	}
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getOrden() {
		return orden;
	}
	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	public String getEnlace() {
		return enlace;
	}
	public void setEnlace(String enlace) {
		this.enlace = enlace;
	}
	public TablaModulo(String modulo, Integer tabla, String descripcion, Integer orden, String enlace, String estado, String creacionUsuario, Date creacionFecha,
			String modUsuario, Date modFecha) {
		//super();
		this.modulo = modulo;
		this.tabla = tabla;
		this.descripcion = descripcion;
		this.orden = orden;
		this.enlace = enlace;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}
	public TablaModulo() {
		
	}	
}
