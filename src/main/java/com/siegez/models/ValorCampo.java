package com.siegez.models;

import java.util.Date;

public class ValorCampo {
	
	private Integer empresa;
	private String ambito;
	private String campo;
	private String valor;
	private String descripcion;
	private String abreviatura; 
	private String codigoreferencia;
	private String estado;	
	private String creacionUsuario;
	private Date creacionFecha;
	private String modificacionUsuario;
	private Date modificacionFecha;
	

	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getAmbito() {
		return ambito;
	}
	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	public String getCodigoreferencia() {
		return codigoreferencia;
	}
	public void setCodigoreferencia(String codigoreferencia) {
		this.codigoreferencia = codigoreferencia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public Date getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(Date creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public Date getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(Date modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public ValorCampo(Integer empresa, String ambito, String campo, String valor, String descripcion, String abreviatura,
			String codigoreferencia, String estado, String creacionUsuario, Date creacionFecha, String modificacionUsuario,
			Date modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ambito = ambito;
		this.campo = campo;
		this.valor = valor;
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
		this.codigoreferencia = codigoreferencia;
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public ValorCampo() {
		
	}	
}
