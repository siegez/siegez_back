package com.siegez.models;

import java.util.List;
import com.siegez.models.EnteSocioNegocio;
import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.models.EnteDireccion;

public class EnteRegistro {

	private Integer empresa;
	private String 	entidad;
	private String 	apellidopaterno;
	private String 	apellidomaterno;
	private String 	nombres;
	private String 	tipopersona;
	private String 	descripciontipopersona;
	private String 	ruc;
	private String 	nodomiciliado;
	private String 	descripcionnodomiciliado;
	private String 	pais;
	private String 	nombrepais;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	private List<EnteSocioNegocio> entesocionegocio;
	private List<EnteDocumentoIdentidad> entedocumentoidentidad;
	private List<EnteDireccion> entedireccion;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getApellidopaterno() {
		return apellidopaterno;
	}
	public void setApellidopaterno(String apellidopaterno) {
		this.apellidopaterno = apellidopaterno;
	}
	public String getApellidomaterno() {
		return apellidomaterno;
	}
	public void setApellidomaterno(String apellidomaterno) {
		this.apellidomaterno = apellidomaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getTipopersona() {
		return tipopersona;
	}
	public void setTipopersona(String tipopersona) {
		this.tipopersona = tipopersona;
	}
	public String getDescripciontipopersona() {
		return descripciontipopersona;
	}
	public void setDescripciontipopersona(String descripciontipopersona) {
		this.descripciontipopersona = descripciontipopersona;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getNodomiciliado() {
		return nodomiciliado;
	}
	public void setNodomiciliado(String nodomiciliado) {
		this.nodomiciliado = nodomiciliado;
	}
	public String getDescripcionnodomiciliado() {
		return descripcionnodomiciliado;
	}
	public void setDescripcionnodomiciliado(String descripcionnodomiciliado) {
		this.descripcionnodomiciliado = descripcionnodomiciliado;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getNombrepais() {
		return nombrepais;
	}
	public void setNombrepais(String nombrepais) {
		this.nombrepais = nombrepais;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public List<EnteSocioNegocio> getEntesocionegocio() {
		return entesocionegocio;
	}
	public void setEntesocionegocio(List<EnteSocioNegocio> entesocionegocio) {
		this.entesocionegocio = entesocionegocio;
	}
	public List<EnteDocumentoIdentidad> getEntedocumentoidentidad() {
		return entedocumentoidentidad;
	}
	public void setEntedocumentoidentidad(List<EnteDocumentoIdentidad> entedocumentoidentidad) {
		this.entedocumentoidentidad = entedocumentoidentidad;
	}
	public List<EnteDireccion> getEntedireccion() {
		return entedireccion;
	}
	public void setEntedireccion(List<EnteDireccion> entedireccion) {
		this.entedireccion = entedireccion;
	}
	
	public EnteRegistro(Integer empresa, String entidad, String apellidopaterno, String apellidomaterno, String nombres,
			String tipopersona, String descripciontipopersona, String ruc, String nodomiciliado,
			String descripcionnodomiciliado, String pais, String nombrepais, String estado, String descripcionestado,
			String creacionUsuario, String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.entidad = entidad;
		this.apellidopaterno = apellidopaterno;
		this.apellidomaterno = apellidomaterno;
		this.nombres = nombres;
		this.tipopersona = tipopersona;
		this.descripciontipopersona = descripciontipopersona;
		this.ruc = ruc;
		this.nodomiciliado = nodomiciliado;
		this.descripcionnodomiciliado = descripcionnodomiciliado;
		this.pais = pais;
		this.nombrepais = nombrepais;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	public EnteRegistro() {
		
	}
	
}
