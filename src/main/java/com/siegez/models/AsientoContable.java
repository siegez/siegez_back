package com.siegez.models;

public class AsientoContable {
	
	private Integer empresa;
	private Integer ejerciciocontable;
	private Integer	asientocontable;	
	private String 	operacioncontable;
	private String 	tipoasiento;
	private String 	descripciontipoasiento;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public Integer getAsientocontable() {
		return asientocontable;
	}
	public void setAsientocontable(Integer asientocontable) {
		this.asientocontable = asientocontable;
	}
	public String getOperacioncontable() {
		return operacioncontable;
	}
	public void setOperacioncontable(String operacioncontable) {
		this.operacioncontable = operacioncontable;
	}
	public String getTipoasiento() {
		return tipoasiento;
	}
	public void setTipoasiento(String tipoasiento) {
		this.tipoasiento = tipoasiento;
	}
	public String getDescripciontipoasiento() {
		return descripciontipoasiento;
	}
	public void setDescripciontipoasiento(String descripciontipoasiento) {
		this.descripciontipoasiento = descripciontipoasiento;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public AsientoContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable,
			String operacioncontable, String tipoasiento, String descripciontipoasiento, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.asientocontable = asientocontable;
		this.operacioncontable = operacioncontable;
		this.tipoasiento = tipoasiento;
		this.descripciontipoasiento = descripciontipoasiento;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public AsientoContable() {


	}
	
	
}

