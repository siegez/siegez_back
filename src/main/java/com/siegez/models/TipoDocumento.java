package com.siegez.models;

public class TipoDocumento {

	private Integer empresa;
	private String 	tipodocumento;
	private String 	descripcion;
	private String 	abreviatura;
	private String 	indicadorcompra;
	private String 	indicadorhonorario;
	private String 	indicadorventa;	
	private String 	indicadorcaja;
	private String 	indicadoregreso;
	private String 	indicadoringreso;
	private String 	indicadordiario;
	private String 	indicadorimportacion;
	private String 	indicadornodomiciliado;
	private String 	indicadorsustentonodom;
	private String 	indicadorsunat;	    		  
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getTipodocumento() {
		return tipodocumento;
	}
	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	public String getIndicadorcompra() {
		return indicadorcompra;
	}
	public void setIndicadorcompra(String indicadorcompra) {
		this.indicadorcompra = indicadorcompra;
	}
	public String getIndicadorhonorario() {
		return indicadorhonorario;
	}
	public void setIndicadorhonorario(String indicadorhonorario) {
		this.indicadorhonorario = indicadorhonorario;
	}
	public String getIndicadorventa() {
		return indicadorventa;
	}
	public void setIndicadorventa(String indicadorventa) {
		this.indicadorventa = indicadorventa;
	}
	public String getIndicadorcaja() {
		return indicadorcaja;
	}
	public void setIndicadorcaja(String indicadorcaja) {
		this.indicadorcaja = indicadorcaja;
	}
	public String getIndicadoregreso() {
		return indicadoregreso;
	}
	public void setIndicadoregreso(String indicadoregreso) {
		this.indicadoregreso = indicadoregreso;
	}
	public String getIndicadoringreso() {
		return indicadoringreso;
	}
	public void setIndicadoringreso(String indicadoringreso) {
		this.indicadoringreso = indicadoringreso;
	}
	public String getIndicadordiario() {
		return indicadordiario;
	}
	public void setIndicadordiario(String indicadordiario) {
		this.indicadordiario = indicadordiario;
	}
	public String getIndicadorimportacion() {
		return indicadorimportacion;
	}
	public void setIndicadorimportacion(String indicadorimportacion) {
		this.indicadorimportacion = indicadorimportacion;
	}
	public String getIndicadornodomiciliado() {
		return indicadornodomiciliado;
	}
	public void setIndicadornodomiciliado(String indicadornodomiciliado) {
		this.indicadornodomiciliado = indicadornodomiciliado;
	}
	public String getIndicadorsustentonodom() {
		return indicadorsustentonodom;
	}
	public void setIndicadorsustentonodom(String indicadorsustentonodom) {
		this.indicadorsustentonodom = indicadorsustentonodom;
	}
	public String getIndicadorsunat() {
		return indicadorsunat;
	}
	public void setIndicadorsunat(String indicadorsunat) {
		this.indicadorsunat = indicadorsunat;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public TipoDocumento(Integer empresa, String tipodocumento, String descripcion, String abreviatura,
			String indicadorcompra, String indicadorhonorario, String indicadorventa, String indicadorcaja,
			String indicadoregreso, String indicadoringreso, String indicadordiario, String indicadorimportacion,
			String indicadornodomiciliado, String indicadorsustentonodom, String indicadorsunat, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.tipodocumento = tipodocumento;
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
		this.indicadorcompra = indicadorcompra;
		this.indicadorhonorario = indicadorhonorario;
		this.indicadorventa = indicadorventa;
		this.indicadorcaja = indicadorcaja;
		this.indicadoregreso = indicadoregreso;
		this.indicadoringreso = indicadoringreso;
		this.indicadordiario = indicadordiario;
		this.indicadorimportacion = indicadorimportacion;
		this.indicadornodomiciliado = indicadornodomiciliado;
		this.indicadorsustentonodom = indicadorsustentonodom;
		this.indicadorsunat = indicadorsunat;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public TipoDocumento() {

	}
	
	
}
