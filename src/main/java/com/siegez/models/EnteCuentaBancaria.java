package com.siegez.models;

public class EnteCuentaBancaria {
	
	private Integer empresa;
	private Integer ejerciciocontable;
	private String 	entidad;
	private String 	cuentabancaria;
	private String 	cuentainterbancaria;
	private String 	nombrecuentabancaria;
	private String 	tipocuentabancaria;
	private String 	descripciontipocuentabancaria;
	private String 	cuenta;
	private String 	descripcioncuenta;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public String getCuentabancaria() {
		return cuentabancaria;
	}
	public void setCuentabancaria(String cuentabancaria) {
		this.cuentabancaria = cuentabancaria;
	}
	public String getCuentainterbancaria() {
		return cuentainterbancaria;
	}
	public void setCuentainterbancaria(String cuentainterbancaria) {
		this.cuentainterbancaria = cuentainterbancaria;
	}
	public String getNombrecuentabancaria() {
		return nombrecuentabancaria;
	}
	public void setNombrecuentabancaria(String nombrecuentabancaria) {
		this.nombrecuentabancaria = nombrecuentabancaria;
	}
	public String getTipocuentabancaria() {
		return tipocuentabancaria;
	}
	public void setTipocuentabancaria(String tipocuentabancaria) {
		this.tipocuentabancaria = tipocuentabancaria;
	}
	public String getDescripciontipocuentabancaria() {
		return descripciontipocuentabancaria;
	}
	public void setDescripciontipocuentabancaria(String descripciontipocuentabancaria) {
		this.descripciontipocuentabancaria = descripciontipocuentabancaria;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getDescripcioncuenta() {
		return descripcioncuenta;
	}
	public void setDescripcioncuenta(String descripcioncuenta) {
		this.descripcioncuenta = descripcioncuenta;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	public EnteCuentaBancaria(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria,
			String cuentainterbancaria, String nombrecuentabancaria, String tipocuentabancaria,
			String descripciontipocuentabancaria, String cuenta, String descripcioncuenta, String estado,
			String descripcionestado, String creacionUsuario, String creacionFecha, String modificacionUsuario,
			String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.entidad = entidad;
		this.cuentabancaria = cuentabancaria;
		this.cuentainterbancaria = cuentainterbancaria;
		this.nombrecuentabancaria = nombrecuentabancaria;
		this.tipocuentabancaria = tipocuentabancaria;
		this.descripciontipocuentabancaria = descripciontipocuentabancaria;
		this.cuenta = cuenta;
		this.descripcioncuenta = descripcioncuenta;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public EnteCuentaBancaria() {
		
	}
	
	
	
	
}
