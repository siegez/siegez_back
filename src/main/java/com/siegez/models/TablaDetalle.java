package com.siegez.models;
public class TablaDetalle {

	private Integer empresa;
	private Integer tabla;
	private String	valor;
	private String	descripcion;
	private String	abreviatura;
	private String	codigoreferencia;
	private String 	estado;	
	private String 	creacionUsuario;
	private String 	creacionFecha;
	private String 	modUsuario;
	private String 	modFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getTabla() {
		return tabla;
	}
	public void setTabla(Integer tabla) {
		this.tabla = tabla;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
	
	public String getCodigoreferencia() {
		return codigoreferencia;
	}
	public void setCodigoreferencia(String codigoreferencia) {
		this.codigoreferencia = codigoreferencia;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModUsuario() {
		return modUsuario;
	}
	public void setModUsuario(String modUsuario) {
		this.modUsuario = modUsuario;
	}
	public String getModFecha() {
		return modFecha;
	}
	public void setModFecha(String modFecha) {
		this.modFecha = modFecha;
	}
	public TablaDetalle(Integer empresa, Integer tabla, String valor, String descripcion, String abreviatura,
			String codigoreferencia, String estado, String creacionUsuario, String creacionFecha, String modUsuario, String modFecha) {
		//super();
		this.empresa = empresa;
		this.tabla = tabla;
		this.valor = valor;
		this.descripcion = descripcion;
		this.abreviatura = abreviatura;
		this.codigoreferencia = codigoreferencia;		
		this.estado = estado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modUsuario = modUsuario;
		this.modFecha = modFecha;
	}	
	public TablaDetalle() {
	}
	
	
}
