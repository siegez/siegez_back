package com.siegez.models;

public class EnteSocioNegocio {
	
	private Integer empresa;
	private String 	entidad;
	private Integer	vez;
	private String 	socionegocio;
	private String 	descripcionsocionegocio;
	private String 	estado;
	private String 	descripcionestado;
	private String 	creacionUsuario;
	private String  creacionFecha;
	private String 	modificacionUsuario;
	private String  modificacionFecha;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getEntidad() {
		return entidad;
	}
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	public Integer getVez() {
		return vez;
	}
	public void setVez(Integer vez) {
		this.vez = vez;
	}
	public String getSocionegocio() {
		return socionegocio;
	}
	public void setSocionegocio(String socionegocio) {
		this.socionegocio = socionegocio;
	}
	public String getDescripcionsocionegocio() {
		return descripcionsocionegocio;
	}
	public void setDescripcionsocionegocio(String descripcionsocionegocio) {
		this.descripcionsocionegocio = descripcionsocionegocio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public String getCreacionUsuario() {
		return creacionUsuario;
	}
	public void setCreacionUsuario(String creacionUsuario) {
		this.creacionUsuario = creacionUsuario;
	}
	public String getCreacionFecha() {
		return creacionFecha;
	}
	public void setCreacionFecha(String creacionFecha) {
		this.creacionFecha = creacionFecha;
	}
	public String getModificacionUsuario() {
		return modificacionUsuario;
	}
	public void setModificacionUsuario(String modificacionUsuario) {
		this.modificacionUsuario = modificacionUsuario;
	}
	public String getModificacionFecha() {
		return modificacionFecha;
	}
	public void setModificacionFecha(String modificacionFecha) {
		this.modificacionFecha = modificacionFecha;
	}
	
	public EnteSocioNegocio(Integer empresa, String entidad, Integer vez, String socionegocio,
			String descripcionsocionegocio, String estado, String descripcionestado, String creacionUsuario,
			String creacionFecha, String modificacionUsuario, String modificacionFecha) {
		super();
		this.empresa = empresa;
		this.entidad = entidad;
		this.vez = vez;
		this.socionegocio = socionegocio;
		this.descripcionsocionegocio = descripcionsocionegocio;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
		this.creacionUsuario = creacionUsuario;
		this.creacionFecha = creacionFecha;
		this.modificacionUsuario = modificacionUsuario;
		this.modificacionFecha = modificacionFecha;
	}
	
	public EnteSocioNegocio() {
		
	}
	
	
}
