package com.siegez.models;

import java.io.Serializable;
import java.util.Date;

public class JwtUsuario implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String	username;
	private String	password;
	private String	userestado;
	private String  licenciaestado;
	private Date	licenciainicio;
	private Date	licenciacaduca;
	
	// the user should be mapped with their role and permissions
	//private List<Permiso> permisos 
	
	public String getUsername() {
		return username;
	}
	public String getLicenciaestado() {
		return licenciaestado;
	}
	public void setLicenciaestado(String licenciaestado) {
		this.licenciaestado = licenciaestado;
	}
	public Date getLicenciainicio() {
		return licenciainicio;
	}
	public void setLicenciainicio(Date licenciainicio) {
		this.licenciainicio = licenciainicio;
	}
	public Date getLicenciacaduca() {
		return licenciacaduca;
	}
	public void setLicenciacaduca(Date licenciacaduca) {
		this.licenciacaduca = licenciacaduca;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getUserestado() {
		return userestado;
	}
	public void setUserestado(String userestado) {
		this.userestado = userestado;
	}
	
	public JwtUsuario(String username, String password, String  licenciaestado, String userestado, Date licenciainicio, Date licenciacaduca) {
		//super();
		this.username = username;
		this.password = password;
		this.userestado = userestado;
		this.licenciaestado = licenciaestado;
		this.licenciainicio = licenciainicio;
		this.licenciacaduca = licenciacaduca;
	}
	public JwtUsuario() {
		//super();
	}	
}
