package com.siegez.models;


public class MenuAcceso {

	private Integer codigo;
	private String	descripcion;
	private String	opcion;
	private Integer	nivel;
	private Integer correlativo;
	private String 	estado;	
	private String	empresa;
	private String	perfil;
	
	public String getEmpresa() {
		return empresa;
	}	
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}	
	public String getPerfil() {
		return perfil;
	}	
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}	
	public Integer getCodigo() {
		return codigo;
	}	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getOpcion() {
		return opcion;
	}
	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	public Integer getNivel() {
		return nivel;
	}
	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}
	public Integer getCorrelativo() {
		return correlativo;
	}
	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	public MenuAcceso(Integer codigo, String descripcion, String opcion, Integer nivel, Integer correlativo, String estado,
			String empresa, String perfil) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.opcion = opcion;
		this.nivel = nivel;
		this.correlativo = correlativo;
		this.estado = estado;
		this.empresa = empresa;
		this.perfil = perfil;
	}
	

}