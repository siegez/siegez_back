package com.siegez.views;

public class Catalogo {

	private String codigo;
	private String descripcion;
	private String detalle;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDetalle() {
		return detalle;
	}
	public void SetDetalle(String detalle) {
		this.detalle = detalle;
	}
	
	public Catalogo(String codigo, String descripcion, String detalle) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.detalle = detalle;
	}

	public Catalogo() {

	}
}
