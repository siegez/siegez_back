package com.siegez.views;

public class VistaPlanContable {
	private Integer empresa;
	private Integer ejerciciocontable;
	private String 	cuenta;
	private String 	descripcioncuenta;
	private String 	categoria;
	private String 	descripcioncategoria;
	private String 	tipocuenta;
	private String 	descripciontipo;
	private String 	moneda; 
	private String 	descripcionmoneda;	
	private String 	estado;
	private String 	descripcionestado;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getDescripcioncuenta() {
		return descripcioncuenta;
	}
	public void setDescripcioncuenta(String descripcioncuenta) {
		this.descripcioncuenta = descripcioncuenta;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public String getDescripcioncategoria() {
		return descripcioncategoria;
	}
	public void setDescripcioncategoria(String descripcioncategoria) {
		this.descripcioncategoria = descripcioncategoria;
	}
	public String getTipocuenta() {
		return tipocuenta;
	}
	public void setTipocuenta(String tipocuenta) {
		this.tipocuenta = tipocuenta;
	}
	public String getDescripciontipo() {
		return descripciontipo;
	}
	public void setDescripciontipo(String descripciontipo) {
		this.descripciontipo = descripciontipo;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getDescripcionmoneda() {
		return descripcionmoneda;
	}
	public void setDescripcionmoneda(String descripcionmoneda) {
		this.descripcionmoneda = descripcionmoneda;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDescripcionestado() {
		return descripcionestado;
	}
	public void setDescripcionestado(String descripcionestado) {
		this.descripcionestado = descripcionestado;
	}
	public VistaPlanContable(Integer empresa, Integer ejerciciocontable, String cuenta, String descripcioncuenta,
			String categoria, String descripcioncategoria, String tipocuenta, String descripciontipo, String moneda,
			String descripcionmoneda, String estado, String descripcionestado) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.cuenta = cuenta;
		this.descripcioncuenta = descripcioncuenta;
		this.categoria = categoria;
		this.descripcioncategoria = descripcioncategoria;
		this.tipocuenta = tipocuenta;
		this.descripciontipo = descripciontipo;
		this.moneda = moneda;
		this.descripcionmoneda = descripcionmoneda;
		this.estado = estado;
		this.descripcionestado = descripcionestado;
	}
	public VistaPlanContable() {
		
	}
	
}
