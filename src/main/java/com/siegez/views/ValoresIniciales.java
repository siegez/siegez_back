package com.siegez.views;



public class ValoresIniciales {

	private String licencia;
	private String username;
	private String nombreusuario;
	private Integer empresa;
	private String nombreempresa;
	private String ruc;
	private String perfil; 
	private String nombreperfil;	
	private Integer annoproceso;
	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNombreusuario() {
		return nombreusuario;
	}
	public void setNombreusuario(String nombreusuario) {
		this.nombreusuario = nombreusuario;
	}
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getNombreempresa() {
		return nombreempresa;
	}
	public void setNombreempresa(String nombreempresa) {
		this.nombreempresa = nombreempresa;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getNombreperfil() {
		return nombreperfil;
	}
	public void setNombreperfil(String nombreperfil) {
		this.nombreperfil = nombreperfil;
	}
	public Integer getAnnoproceso() {
		return annoproceso;
	}
	public void setAnnoproceso(Integer annoproceso) {
		this.annoproceso = annoproceso;
	}
	public ValoresIniciales(String licencia, String username, String nombreusuario, Integer empresa,
			String nombreempresa, String ruc, String perfil, String nombreperfil, Integer annoproceso) {
		super();
		this.licencia = licencia;
		this.username = username;
		this.nombreusuario = nombreusuario;
		this.empresa = empresa;
		this.nombreempresa = nombreempresa;
		this.ruc = ruc;
		this.perfil = perfil;
		this.nombreperfil = nombreperfil;
		this.annoproceso = annoproceso;
	}
	public ValoresIniciales() {
		
	}
	
	
	
	
}
