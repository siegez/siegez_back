package com.siegez.views;

public class SocioNegocio {

	private Integer empresa;
	private String codigo;
	private String documento; 
	private String nombre;
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public SocioNegocio(Integer empresa, String codigo, String documento, String nombre) {
		super();
		this.empresa = empresa;
		this.codigo = codigo;
		this.documento = documento;
		this.nombre = nombre;
	}
	public SocioNegocio() {

	}	

	
}
