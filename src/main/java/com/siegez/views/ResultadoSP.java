package com.siegez.views;

public class ResultadoSP {

	private Integer codigointeger;
	private String  codigovarchar;
	private String  mensaje;
	private String  indicadorexito;
	
	public Integer getCodigointeger() {
		return codigointeger;
	}
	public void setCodigointeger(Integer codigointeger) {
		this.codigointeger = codigointeger;
	}
	public String getCodigovarchar() {
		return codigovarchar;
	}
	public void setCodigovarchar(String codigovarchar) {
		this.codigovarchar = codigovarchar;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getIndicadorexito() {
		return indicadorexito;
	}
	public void setIndicadorexito(String indicadorexito) {
		this.indicadorexito = indicadorexito;
	}
	
	public ResultadoSP(Integer codigointeger, String codigovarchar, String mensaje, String indicadorexito) {
		super();
		this.codigointeger = codigointeger;
		this.codigovarchar = codigovarchar;
		this.mensaje = mensaje;
		this.indicadorexito = indicadorexito;
	}
	
	public ResultadoSP() {
		
	}
	
	
}
