package com.siegez.views;

public class VistaCentroCosto {

	private Integer empresa;
	private Integer ejerciciocontable;
	private String	codigo;	
	private String 	descripcion;
	private String 	tipo;
	private String 	descripciontipo;
	
	public Integer getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Integer empresa) {
		this.empresa = empresa;
	}
	public Integer getEjerciciocontable() {
		return ejerciciocontable;
	}
	public void setEjerciciocontable(Integer ejerciciocontable) {
		this.ejerciciocontable = ejerciciocontable;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripciontipo() {
		return descripciontipo;
	}
	public void setDescripciontipo(String descripciontipo) {
		this.descripciontipo = descripciontipo;
	}
	
	public VistaCentroCosto(Integer empresa, Integer ejerciciocontable, String codigo, String descripcion, String tipo,
			String descripciontipo) {
		super();
		this.empresa = empresa;
		this.ejerciciocontable = ejerciciocontable;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.tipo = tipo;
		this.descripciontipo = descripciontipo;
	}
	
	public VistaCentroCosto() {

	}

	
}
