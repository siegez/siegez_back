package com.siegez.views;

public class Valor {
	private double valor;

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}


	public Valor(double valor) {
		super();
		this.valor = valor;
	}

	public Valor() {

	}
	
	
}
