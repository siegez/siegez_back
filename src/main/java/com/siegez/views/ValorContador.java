package com.siegez.views;

public class ValorContador {
	private Integer contador;

	public Integer getContador() {
		return contador;
	}

	public void setContador(Integer contador) {
		this.contador = contador;
	}

	public ValorContador(Integer contador) {
		super();
		this.contador = contador;
	}
	
	public ValorContador() {

	}
}
