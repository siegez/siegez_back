package com.siegez.reports;

public class EstadoResultado {
	private String c1;
	private String c2;
	private String c3;
	private String c4;
	private String c5;
	private Integer c6;
	private Integer c7;
	private String c8;
	private double c9;
	private double c10;
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	}
	public Integer getC6() {
		return c6;
	}
	public void setC6(Integer c6) {
		this.c6 = c6;
	}
	public Integer getC7() {
		return c7;
	}
	public void setC7(Integer c7) {
		this.c7 = c7;
	}
	public String getC8() {
		return c8;
	}
	public void setC8(String c8) {
		this.c8 = c8;
	}
	public double getC9() {
		return c9;
	}
	public void setC9(double c9) {
		this.c9 = c9;
	}
	public double getC10() {
		return c10;
	}
	public void setC10(double c10) {
		this.c10 = c10;
	}
	public EstadoResultado(String c1, String c2, String c3, String c4, String c5, Integer c6, Integer c7, String c8,
			double c9, double c10) {
		super();
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
		this.c6 = c6;
		this.c7 = c7;
		this.c8 = c8;
		this.c9 = c9;
		this.c10 = c10;
	}
	public EstadoResultado() {
		// super();
		// TODO Auto-generated constructor stub
	}

	
}
