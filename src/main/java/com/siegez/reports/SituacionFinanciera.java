package com.siegez.reports;

public class SituacionFinanciera {
	private String  c1;
	private String  c2;
	private String  c3;
	private String  c4;
	private String  c5;
	private Integer c6;
	private String  c7;
	private Integer c8;
	private String  c9;
	private Integer c10;
	private String  c11;
	private String  c12;
	private double  c13;
	private String  c14;
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	}
	public Integer getC6() {
		return c6;
	}
	public void setC6(Integer c6) {
		this.c6 = c6;
	}
	public String getC7() {
		return c7;
	}
	public void setC7(String c7) {
		this.c7 = c7;
	}
	public Integer getC8() {
		return c8;
	}
	public void setC8(Integer c8) {
		this.c8 = c8;
	}
	public String getC9() {
		return c9;
	}
	public void setC9(String c9) {
		this.c9 = c9;
	}
	public Integer getC10() {
		return c10;
	}
	public void setC10(Integer c10) {
		this.c10 = c10;
	}
	public String getC11() {
		return c11;
	}
	public void setC11(String c11) {
		this.c11 = c11;
	}
	public String getC12() {
		return c12;
	}
	public void setC12(String c12) {
		this.c12 = c12;
	}
	public double getC13() {
		return c13;
	}
	public void setC13(double c13) {
		this.c13 = c13;
	}
	public String getC14() {
		return c14;
	}
	public void setC14(String c14) {
		this.c14 = c14;
	}
	public SituacionFinanciera(String c1, String c2, String c3, String c4, String c5, Integer c6, String c7, Integer c8,
			String c9, Integer c10, String c11, String c12, double c13, String c14) {
		super();
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
		this.c6 = c6;
		this.c7 = c7;
		this.c8 = c8;
		this.c9 = c9;
		this.c10 = c10;
		this.c11 = c11;
		this.c12 = c12;
		this.c13 = c13;
		this.c14 = c14;
	}
	public SituacionFinanciera() {
		// super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
