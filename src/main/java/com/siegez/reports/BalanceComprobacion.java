package com.siegez.reports;

public class BalanceComprobacion {
	private String c1;
	private String c2;
	private String c3;
	private String c4;
	private String c5;
	private String c6;
	private String c7;
	private double c8;
	private double c9;
	private double c10;
	private double c11;
	private double c12;
	private double c13;
	private double c14;
	private double c15;
	private double c16;
	private double c17;
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	}
	public String getC6() {
		return c6;
	}
	public void setC6(String c6) {
		this.c6 = c6;
	}
	public String getC7() {
		return c7;
	}
	public void setC7(String c7) {
		this.c7 = c7;
	}
	public double getC8() {
		return c8;
	}
	public void setC8(double c8) {
		this.c8 = c8;
	}
	public double getC9() {
		return c9;
	}
	public void setC9(double c9) {
		this.c9 = c9;
	}
	public double getC10() {
		return c10;
	}
	public void setC10(double c10) {
		this.c10 = c10;
	}
	public double getC11() {
		return c11;
	}
	public void setC11(double c11) {
		this.c11 = c11;
	}
	public double getC12() {
		return c12;
	}
	public void setC12(double c12) {
		this.c12 = c12;
	}
	public double getC13() {
		return c13;
	}
	public void setC13(double c13) {
		this.c13 = c13;
	}
	public double getC14() {
		return c14;
	}
	public void setC14(double c14) {
		this.c14 = c14;
	}
	public double getC15() {
		return c15;
	}
	public void setC15(double c15) {
		this.c15 = c15;
	}
	public double getC16() {
		return c16;
	}
	public void setC16(double c16) {
		this.c16 = c16;
	}
	public double getC17() {
		return c17;
	}
	public void setC17(double c17) {
		this.c17 = c17;
	}
	public BalanceComprobacion(String c1, String c2, String c3, String c4, String c5, String c6, String c7, double c8,
			double c9, double c10, double c11, double c12, double c13, double c14, double c15, double c16, double c17) {
		super();
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
		this.c6 = c6;
		this.c7 = c7;
		this.c8 = c8;
		this.c9 = c9;
		this.c10 = c10;
		this.c11 = c11;
		this.c12 = c12;
		this.c13 = c13;
		this.c14 = c14;
		this.c15 = c15;
		this.c16 = c16;
		this.c17 = c17;
	}
	public BalanceComprobacion() {
		// super();
		// TODO Auto-generated constructor stub
	}

	
}
