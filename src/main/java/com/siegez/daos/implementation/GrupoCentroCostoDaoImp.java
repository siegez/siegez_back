package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.siegez.daos.interfaces.GrupoCentroCostoDao;
import com.siegez.models.GrupoCentroCosto;
import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.models.GrupoCentroCostoRegistro;
import com.siegez.rowmapper.extractors.GrupoCentroCostoExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.GrupoCentroCostoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@Repository
public class GrupoCentroCostoDaoImp implements GrupoCentroCostoDao {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	@Transactional
	public ResultadoSP registrarGrupoCentroCosto(GrupoCentroCostoRegistro grupocentrocostoregistro) {
		
		String 	mensaje;
		
		mensaje = "No Exitoso, revisar datos";
		
		// Transacción		
		// Asignar lista de centro de costos
		List<GrupoCentroCostoDetalle> listagrupocentrocostodetalle = (List<GrupoCentroCostoDetalle>) grupocentrocostoregistro.getGrupocentrocostodetalle();

		
		// Validar nuevo o edición -- || OR; && AND
		

		if (grupocentrocostoregistro.getCreacionFecha().equals("0000-00-00")) {
			// Nuevo Grupo	
			
			jdbcTemplate.update("insert into T_CON_GRUPOCENTROCOSTO (IDEMPCOD, IDEJECONTABLE, GCCCOD " 
					+ ", GCCNOMBRE, GCCESTADO " 
					+ ", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " 
					+ " values (?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					grupocentrocostoregistro.getEmpresa(),
					grupocentrocostoregistro.getEjerciciocontable(),
					grupocentrocostoregistro.getGrupocentrocosto(),
					grupocentrocostoregistro.getNombregrupocentrocosto(),
					grupocentrocostoregistro.getEstado(),
					grupocentrocostoregistro.getCreacionUsuario(),
					grupocentrocostoregistro.getModificacionUsuario()
					);			
		}
		else
		{
			jdbcTemplate.update("update T_CON_GRUPOCENTROCOSTO set GCCNOMBRE = ?, "
					+ " GCCESTADO = ?, "
					+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
					+ "	where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ?  ",				
					grupocentrocostoregistro.getNombregrupocentrocosto(),
					grupocentrocostoregistro.getEstado(),
					grupocentrocostoregistro.getModificacionUsuario(),
					grupocentrocostoregistro.getEmpresa(),
					grupocentrocostoregistro.getEjerciciocontable(),
					grupocentrocostoregistro.getGrupocentrocosto()
					);
		}
		// eliminar centro de costo 
		jdbcTemplate.update("delete from T_CON_GRUPOCENTROCOSTO_DET " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ? ", 
				grupocentrocostoregistro.getEmpresa(),
				grupocentrocostoregistro.getEjerciciocontable(),
				grupocentrocostoregistro.getGrupocentrocosto());	
		
		// insertar centro de costo
		for(int i=0; i<listagrupocentrocostodetalle.size(); i++)
		{
			
			GrupoCentroCostoDetalle grupocentrocostodetalle = listagrupocentrocostodetalle.get(i);
			
			jdbcTemplate.update("insert into T_CON_GRUPOCENTROCOSTO_DET (IDEMPCOD, IDEJECONTABLE, GCCCOD " 
					+ ", CCOCOD, DGCPORCENTAJE, DGCESTADO "
					+ ", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " 
					+ " values (?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					grupocentrocostodetalle.getEmpresa(),
					grupocentrocostodetalle.getEjerciciocontable(),
					grupocentrocostodetalle.getGrupocentrocosto(),
					grupocentrocostodetalle.getCentrocosto(),
					grupocentrocostodetalle.getPorcentaje(),
					grupocentrocostodetalle.getEstado(),
					grupocentrocostodetalle.getCreacionUsuario(),
					grupocentrocostodetalle.getModificacionUsuario()
					);     				
        }				
		

		mensaje = "Grabación exitosa";
		return new ResultadoSP(0, grupocentrocostoregistro.getGrupocentrocosto(), mensaje, "S");
	}
	
	
	@Override
	public void insertar(GrupoCentroCosto grupocentrocosto) {
		jdbcTemplate.update("insert into T_CON_GRUPOCENTROCOSTO (IDEMPCOD, IDEJECONTABLE, GCCCOD " 
				+ ", GCCNOMBRE, GCCESTADO " 
				+ ", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " 
				+ " values (?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				grupocentrocosto.getEmpresa(),
				grupocentrocosto.getEjerciciocontable(),
				grupocentrocosto.getGrupocentrocosto(),
				grupocentrocosto.getNombregrupocentrocosto(),
				grupocentrocosto.getEstado(),
				grupocentrocosto.getCreacionUsuario(),
				grupocentrocosto.getModificacionUsuario()
				);
	}

	@Override
	public void editar(GrupoCentroCosto grupocentrocosto) {
		jdbcTemplate.update("update T_CON_GRUPOCENTROCOSTO set GCCNOMBRE = ?, "
				+ " GCCESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ?  ",				
				grupocentrocosto.getNombregrupocentrocosto(),
				grupocentrocosto.getEstado(),
				grupocentrocosto.getModificacionUsuario(),
				grupocentrocosto.getEmpresa(),
				grupocentrocosto.getEjerciciocontable(),
				grupocentrocosto.getGrupocentrocosto()
				);	
		
	}

	@Override
	public int eliminar(GrupoCentroCosto grupocentrocosto) {
		
		jdbcTemplate.update("delete from T_CON_GRUPOCENTROCOSTO_DET " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ? ", 
				grupocentrocosto.getEmpresa(),
				grupocentrocosto.getEjerciciocontable(),
				grupocentrocosto.getGrupocentrocosto());
		
		return jdbcTemplate.update("delete from T_CON_GRUPOCENTROCOSTO " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ? ", 
				grupocentrocosto.getEmpresa(),
				grupocentrocosto.getEjerciciocontable(),
				grupocentrocosto.getGrupocentrocosto());	
	}

	@Override
	public GrupoCentroCosto consultar(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, grupocentrocosto);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.GCCCOD, t1.GCCNOMBRE " +
						", t1.GCCESTADO " +
						", t2.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_GRUPOCENTROCOSTO t1 " +	
						"left outer join V_T_ESTADOREG t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.GCCESTADO = t2.IDESTADO " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? " +
						"and t1.gcccod = ? ", 
						new GrupoCentroCostoExtractor(), 
						new Object[] {empresa, ejerciciocontable, grupocentrocosto});
		} else {
			return new GrupoCentroCosto();
		} 		
		
		
	}
	
	@Override
	public List<GrupoCentroCosto> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.GCCCOD, t1.GCCNOMBRE " +
						", t1.GCCESTADO " +
						", t2.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_GRUPOCENTROCOSTO t1 " +	
						"left outer join V_T_ESTADOREG t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.GCCESTADO = t2.IDESTADO " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? ", 
					new GrupoCentroCostoRowMapper(), new Object[] {empresa, ejerciciocontable});
				
	}
		
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_CON_GRUPOCENTROCOSTO " +
				"where idempcod = ?  " + 
					"and idejecontable = ? " +
					"and gcccod = ? ", 
					new ValorContadorExtractor(), 
					new Object[] {empresa, ejerciciocontable, grupocentrocosto});
	}
	
	@Override
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable){
		
		return jdbcTemplate.query(
				"select t1.GCCCOD as codigo, t1.GCCNOMBRE as descripcion " +
					", t2.DESCRIPCION as estado " +
				"from T_CON_GRUPOCENTROCOSTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.GCCESTADO = t2.IDESTADO " +		
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and t1.GCCESTADO = 'A' " +
				"order by t1.GCCCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable});
	}	
	
	@Override
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable, String grupocentrocosto){
		
		return jdbcTemplate.query(
				"select t1.GCCCOD as codigo, t1.GCCNOMBRE as descripcion " +
						", t2.DESCRIPCION as estado " +
				"from T_CON_GRUPOCENTROCOSTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.GCCESTADO = t2.IDESTADO " +	
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and (t1.GCCCOD like ? or t1.GCCNOMBRE like ?) " +
				"order by t1.GCCCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable, grupocentrocosto, grupocentrocosto});
	}
	
}
