package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.PeriodoDao;
import com.siegez.models.Periodo;
import com.siegez.rowmapper.extractors.PeriodoExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.PeriodoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class PeriodoDaoImp implements PeriodoDao{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(Periodo periodo) {
		jdbcTemplate.update("insert into t_con_periodocontable(idempcod, idejecontable, idpercod, perindact, perestado, "
				+ " mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate) "
				+ " values (?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate())",
				periodo.getEmpresa(),
				periodo.getEjerciciocontable(),
				periodo.getPeriodocontable(),
				periodo.getIndicador(),
				periodo.getEstado(),
				periodo.getCreacionUsuario(),
				periodo.getModificacionUsuario());
	}

	@Override
	public void editar(Periodo periodo) {
		jdbcTemplate.update("update t_con_periodocontable set perindact = ?, perestado = ?, "
				+ "									mgbmodiuser = ?, mgbmodidate = dateadd(hour, -5, getdate())"
				+ "									where idempcod like ? and idejecontable like ? and idpercod like ?",				
				periodo.getIndicador(),
				periodo.getEstado(),
				periodo.getModificacionUsuario(),
				periodo.getEmpresa(),
				periodo.getEjerciciocontable(),
				periodo.getPeriodocontable());
	}

	@Override
	public int eliminar(Periodo periodo) {
		return jdbcTemplate.update("delete from t_con_periodocontable where idempcod like ? and idejecontable like ? and idpercod like ?", 
				periodo.getEmpresa(),
				periodo.getEjerciciocontable(),
				periodo.getPeriodocontable());
	}

	@Override
	public Periodo consultar(Integer empresa, Integer ejerciciocontable, String periococontable) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, periococontable);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select T1.idempcod, T1.idejecontable, T1.idpercod, '' as mes, T1.perindact, T1.perestado, '' as descripcionestado," + 
							" T1.mgbcreauser, T1.mgbcreadate, T1.mgbmodiuser, T1.mgbmodidate "+
							" from t_con_periodocontable T1 " +
					"where t1.IDEMPCOD = ?  and t1.idejecontable = ? and t1.idpercod = ? ", 
					new PeriodoExtractor(), empresa, ejerciciocontable, periococontable);
		} else {
			return new Periodo();
		}
		
		
	}
	
	@Override
	public List<Periodo> consultarLista(Integer empresa, Integer ejerciciocontable) {
		return jdbcTemplate.query("select T1.idempcod, T1.idejecontable, T1.idpercod, T3.DESCRIPCION as mes, " +
				" T1.perindact, T1.perestado, T2.DESCRIPCION as descripcionestado, " + 
				" T1.mgbcreauser, T1.mgbcreadate, T1.mgbmodiuser, T1.mgbmodidate "+
				" from t_con_periodocontable T1 " +
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.PERESTADO = t2.IDESTADO " +				
				" left outer join V_T_MES t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.IDPERCOD = t3.IDMES " +				
				" where T1.idempcod = ? and t1.idejecontable = ? order by T1.idpercod ",
				new PeriodoRowMapper(),empresa, ejerciciocontable);
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String periococontable) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from t_con_periodocontable " + 
				"where IDEMPCOD = ?  and idejecontable = ? and idpercod = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable, periococontable);
	}

	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable){
		
		return jdbcTemplate.query(
				" select t1.idpercod as codigo, " +
				" t1.idejecontable as descripcion, " + 
				" t2.DESCRIPCION as estado " +
				" from t_con_periodocontable t1 " +
					" left outer join V_T_ESTADOREG t2 " +
					" on  t1.IDEMPCOD = t2.IDEMPCOD " +
					" and t1.PERESTADO = t2.IDESTADO " +
				" where t1.IDEMPCOD = ? and t1.idejecontable = ? " +
				" order by t1.idpercod ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable});
	}

}
