package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.TablaModuloDao;
import com.siegez.models.TablaModulo;
import com.siegez.rowmapper.mappers.TablaModuloRowMapper;

@Repository
public class TablaModuloDaoImp implements TablaModuloDao{

	
	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public List<TablaModulo> findAll(String modulo) {
		return jdbcTemplate.query("SELECT t1.IDMODCOD,t1.IDTABCOD,t2.TABDESCRIP, t2.TABORDEN, t2.TABENLACE, t1.TMOESTADO, " + 
				"t1.MGBCREAUSER,t1.MGBCREADATE,t1.MGBMODIUSER,t1.MGBMODIDATE " + 
				"FROM T_M_TABLAMODULO t1 " + 
				"left outer join T_M_TABLA t2 " + 
				"on t1.IDTABCOD = T2.IDTABCOD " +
				"where t1.IDMODCOD = ? ", new TablaModuloRowMapper(), modulo);
	}

}
