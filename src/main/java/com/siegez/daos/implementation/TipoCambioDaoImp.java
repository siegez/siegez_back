package com.siegez.daos.implementation;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.siegez.daos.interfaces.TipoCambioDao;
import com.siegez.models.TipoCambio;
import com.siegez.rowmapper.extractors.TipoCambioExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.TipoCambioRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;
@Repository
public class TipoCambioDaoImp implements TipoCambioDao {
	
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall tipocambioSp;
	private SimpleJdbcCall resultadoSp;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.tipocambioSp = new SimpleJdbcCall(dataSource);
		this.resultadoSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public ResultadoSP registrarTipoCambio(TipoCambio tipocambio) {
		this.resultadoSp.withSchemaName("dbo").withProcedureName("sp_con_inserttipocambio").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("tcavez", Types.INTEGER),
				new SqlParameter("tcafecha", Types.VARCHAR),
				new SqlParameter("idmoneda", Types.VARCHAR),
				new SqlParameter("tcacompra", Types.DOUBLE),
				new SqlParameter("tcaventa", Types.DOUBLE),
				new SqlParameter("tcaindcierre", Types.VARCHAR),
				new SqlParameter("tcaindreplica", Types.VARCHAR),
				new SqlParameter("estado", Types.VARCHAR),
				new SqlParameter("username", Types.VARCHAR),				
				new SqlOutParameter("mensaje", Types.VARCHAR),
				new SqlOutParameter("indexito", Types.VARCHAR));
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", tipocambio.getEmpresa(), Types.INTEGER)
																			.addValue("tcavez", tipocambio.getVez(), Types.INTEGER)
																			.addValue("tcafecha", tipocambio.getFecha(), Types.VARCHAR)
																			.addValue("idmoneda", tipocambio.getMoneda(), Types.VARCHAR)
																			.addValue("tcacompra", tipocambio.getTipocambiocompra(), Types.DOUBLE)
																			.addValue("tcaventa", tipocambio.getTipocambioventa(), Types.DOUBLE)
																			.addValue("tcaindcierre", tipocambio.getIndicadorcierre(), Types.VARCHAR)
																			.addValue("tcaindreplica", tipocambio.getIndicadorreplica(), Types.VARCHAR)
																			.addValue("estado", tipocambio.getEstado(), Types.VARCHAR)
																			.addValue("username", tipocambio.getModificacionUsuario(), Types.VARCHAR);
				Map<String, Object> out = resultadoSp.execute(parameters);
				return new ResultadoSP(0, "", (String) out.get("mensaje"), (String) out.get("indexito"));					
	}
	
	@Override
	public void insertar(TipoCambio tipocambio) {
		jdbcTemplate.update("insert into T_M_TIPOCAMBIO (IDEMPCOD, TCAVEZ, TCAFECHA, IDMONEDA " + 
				",TCACOMPRA, TCAVENTA, TCAINDCIERRE, TCAESTADO " + 
				", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				tipocambio.getEmpresa(),
				tipocambio.getVez(),
				tipocambio.getFecha(),
				tipocambio.getMoneda(),
				tipocambio.getTipocambiocompra(),
				tipocambio.getTipocambioventa(),
				tipocambio.getIndicadorcierre(),				
				tipocambio.getEstado(),
				tipocambio.getCreacionUsuario(),
				tipocambio.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(TipoCambio tipocambio) {
		jdbcTemplate.update("update T_M_TIPOCAMBIO set TCAFECHA = ?, IDMONEDA = ?, "
				+ "TCACOMPRA = ?, TCAVENTA = ?, TCAINDCIERRE = ?, TCAESTADO = ?, "				
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and TCAVEZ = ? ",				
				tipocambio.getFecha(),
				tipocambio.getMoneda(),
				tipocambio.getTipocambiocompra(),
				tipocambio.getTipocambioventa(),
				tipocambio.getIndicadorcierre(),
				tipocambio.getEstado(),
				tipocambio.getModificacionUsuario(),
				tipocambio.getEmpresa(),
				tipocambio.getVez()
				);	
		
	}
	
	@Override
	public int eliminar(TipoCambio tipocambio) {
		return jdbcTemplate.update("delete from T_M_TIPOCAMBIO " 
				+ " where IDEMPCOD = ? and TCAVEZ = ? ", 
				tipocambio.getEmpresa(),
				tipocambio.getVez());	
	}
	
	@Override
	public TipoCambio consultar(Integer empresa, Integer vez) {
		
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.TCAVEZ, CONVERT(DATE,t1.TCAFECHA), t1.IDMONEDA, t2.DESCRIPCION as descripcionmoneda " +
					", t1.TCACOMPRA, t1.TCAVENTA, t1.TCAINDCIERRE, '' as escripcionindicadorcierre, '' as indicadorreplica " +
					", t1.TCAESTADO, '' as descripcionestado, '' as indicadorreplica " +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
				"from T_M_TIPOCAMBIO t1 " +	
				" left outer join V_T_MONEDA t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDMONEDA = t2.IDMONEDA " +
				"where t1.idempcod = ?  " + 
					"and t1.TCAVEZ = ? " , 
					new TipoCambioExtractor(), empresa, vez);
	}		
	
	@Override
	public TipoCambio consultarDia(Integer empresa, String fecha, String moneda, String indicadorcierre) {
		
		this.tipocambioSp.withSchemaName("dbo").withProcedureName("sp_sha_gettipocambiodia").declareParameters(
		new SqlParameter("idempcod", Types.INTEGER),
		new SqlParameter("fecha", Types.VARCHAR),
		new SqlParameter("moneda", Types.VARCHAR),
		new SqlParameter("indcierre", Types.VARCHAR),
		new SqlOutParameter("compra", Types.DOUBLE),
		new SqlOutParameter("venta", Types.DOUBLE));
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																	.addValue("fecha", fecha, Types.VARCHAR)	
																	.addValue("moneda", moneda, Types.VARCHAR)
																	.addValue("indcierre", indicadorcierre , Types.VARCHAR);
		Map<String, Object> out = tipocambioSp.execute(parameters);
		return new TipoCambio(empresa, 1, fecha, moneda, "", (Double) out.get("compra"), (Double) out.get("venta"),
				indicadorcierre, "", "A", "", "", "", "", "", "");		
	}

	/*
	@Override
	public TipoCambio consultarDia(Integer empresa, String fecha, String moneda, String indicadorcierre) {
		
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.TCAVEZ, t1.TCAFECHA, t1.IDMONEDA, '' as descripcionmoneda " +
					", t1.TCACOMPRA, t1.TCAVENTA, t1.TCAINDCIERRE, '' as descripcionindicadorcierre " +
					", t1.TCAESTADO, '' as descripcionestado " +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
				"from T_M_TIPOCAMBIO t1 " +					
				"where t1.idempcod = ?  " + 
					"and CONVERT(DATE,t1.TCAFECHA) = CONVERT(DATE, ? ) " +
					"and t1.IDMONEDA = ? " +
					"and t1.TCAINDCIERRE = ? " +
					"and t1.TCAESTADO = 'A' " , 
					new TipoCambioExtractor(), empresa, fecha, moneda, indicadorcierre);
	}
	*/
	@Override
	public List<TipoCambio> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.TCAVEZ, CONVERT(VARCHAR,t1.TCAFECHA,103) as TCAFECHA, t1.IDMONEDA, t2.DESCRIPCION as descripcionmoneda " +
				" , t1.TCACOMPRA, t1.TCAVENTA, t1.TCAINDCIERRE " +
				" , case when t1.TCAINDCIERRE = 'S' then 'Si' else 'No' end as descripcionindicadorcierre, '' as indicadorreplica " +
				" , t1.TCAESTADO, t3.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_TIPOCAMBIO t1 " +
				" left outer join V_T_MONEDA t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDMONEDA = t2.IDMONEDA " +
				" left outer join V_T_ESTADOOPE t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.TCAESTADO = t3.OPEESTADO " +
			"where t1.IDEMPCOD = ? " +
			" and year(t1.TCAFECHA) = ? " +
			"order by t1.TCAFECHA desc ", 
				new TipoCambioRowMapper(), new Object[] {empresa, ejerciciocontable});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, String fecha, String moneda, String indicadorcierre) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_M_TIPOCAMBIO " + 
				"where 1.idempcod = ?  and t1.TCAFECHA = ? and t1.IDMONEDA = ? and t1.TCAINDCIERRE = ? ",
				new ValorContadorExtractor(), empresa, fecha, moneda, indicadorcierre);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, String fecha){
		
		return jdbcTemplate.query(
				"select t1.TCAFECHA as codigo, t2.DESCRIPCION as descripcion,  " +
				"'Compra: ' + convert(varchar, Cast(t1.TCACOMPRA as decimal(10,3))) + ' | Venta: ' + convert(varchar, Cast(t1.TCAVENTA as decimal(10,3))) as detalle " +
				"from T_M_TIPOCAMBIO t1 " +	
				" left outer join V_T_MONEDA t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDMONEDA = t2.IDMONEDA " +
				"where t1.IDEMPCOD = ?  and t1.TCAFECHA like ?  " +
				"order by t1.TCAFECHA ",
					new CatalogoRowMapper(), new Object[] {empresa, fecha});
	}

}
