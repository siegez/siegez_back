package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.EnteCuentaBancariaDao;
import com.siegez.models.EnteCuentaBancaria;
import com.siegez.rowmapper.extractors.EnteCuentaBancariaExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.EnteCuentaBancariaRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class EnteCuentaBancariaDaoImp implements EnteCuentaBancariaDao{
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(EnteCuentaBancaria entecuentabancaria) {
		jdbcTemplate.update("insert into T_M_ENTE_CUENTABANCARIA  (" +
				" IDEMPCOD, IDEJECONTABLE, ENTCOD, CBACOD, CBACODINTERBANCARIA, CBANOMBRE, IDTIPOCUENTABANCARIA, CTACOD " +				
				", CBAESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				entecuentabancaria.getEmpresa(),
				entecuentabancaria.getEjerciciocontable(),				
				entecuentabancaria.getEntidad(),
				entecuentabancaria.getCuentabancaria(),
				entecuentabancaria.getCuentainterbancaria(),
				entecuentabancaria.getNombrecuentabancaria(),
				entecuentabancaria.getTipocuentabancaria(),
				entecuentabancaria.getCuenta(),
				entecuentabancaria.getEstado(),
				entecuentabancaria.getCreacionUsuario(),
				entecuentabancaria.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(EnteCuentaBancaria entecuentabancaria) {
		jdbcTemplate.update("update T_M_ENTE_CUENTABANCARIA set " +
				"  CBACODINTERBANCARIA = ?, CBANOMBRE = ?, IDTIPOCUENTABANCARIA = ?, CTACOD = ? " +				
				", CBAESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and IDEJECONTABLE = ? and ENTCOD = ? and CBACOD = ? ",					
				entecuentabancaria.getCuentainterbancaria(),
				entecuentabancaria.getNombrecuentabancaria(),
				entecuentabancaria.getTipocuentabancaria(),
				entecuentabancaria.getCuenta(),
				entecuentabancaria.getEstado(),
				entecuentabancaria.getModificacionUsuario(),
				entecuentabancaria.getEmpresa(),
				entecuentabancaria.getEjerciciocontable(),				
				entecuentabancaria.getEntidad(),
				entecuentabancaria.getCuentabancaria()
				);	
		
	}
	
	@Override
	public int eliminar(EnteCuentaBancaria entecuentabancaria) {
		return jdbcTemplate.update("delete from T_M_ENTE_CUENTABANCARIA " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and ENTCOD = ? and CBACOD = ? ", 
				entecuentabancaria.getEmpresa(),
				entecuentabancaria.getEjerciciocontable(),				
				entecuentabancaria.getEntidad(),
				entecuentabancaria.getCuentabancaria());	
	}
	
	@Override
	public EnteCuentaBancaria consultar(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria) {
		// System.out.println(entecuentabancaria.getEmpresa());

		return jdbcTemplate.query(
				"select  " +
					" t1.IDEMPCOD, t1.IDEJECONTABLE, t1.ENTCOD, t1.CBACOD, t1.CBACODINTERBANCARIA, t1.CBANOMBRE, " +
					" t1.IDTIPOCUENTABANCARIA, '' as descripciontipocuentabancaria, t1.CTACOD, '' as descripcioncuenta, " +
					" t1.CBAESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_M_ENTE_CUENTABANCARIA t1 " +
				"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.ENTCOD = ? and t1.CBACOD = ?  ", 
				new EnteCuentaBancariaExtractor(), empresa, ejerciciocontable, entidad, cuentabancaria);
		
	}
	
	@Override
	public List<EnteCuentaBancaria> consultarLista(Integer empresa, Integer ejerciciocontable, String entidad) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.IDEJECONTABLE, t1.ENTCOD, t1.CBACOD, t1.CBACODINTERBANCARIA, t1.CBANOMBRE, " +
				" t1.IDTIPOCUENTABANCARIA, t2.DESCRIPCION as descripciontipocuentabancaria, t1.CTACOD, T3.CTADESCRIP as descripcioncuenta, " + 
				" t1.CBAESTADO, t4.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_ENTE_CUENTABANCARIA t1 " +	
			" left outer join V_T_TIPOCUENTABANCARIA t2 " +
			" on  t1.IDEMPCOD = t2.IDEMPCOD " +
			" and t1.IDTIPOCUENTABANCARIA = t2.IDTIPOCUENTABANCARIA " + 
			" left outer join T_CON_PLANCONTABLE t3 " +
			" on  t1.IDEMPCOD = t3.IDEMPCOD  " +
			" and t1.IDEJECONTABLE = t3.IDEJECONTABLE " + 
			" and t1.CTACOD = t3.CTACOD " +
			" left outer join V_T_ESTADOREG t4 " + 
			" on  t1.IDEMPCOD = t4.IDEMPCOD  " +
			" and t1.CBAESTADO = t4.IDESTADO " +			
			"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.ENTCOD = ?  ", 
				new EnteCuentaBancariaRowMapper(), new Object[] {empresa, ejerciciocontable, entidad});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_M_ENTE_CUENTABANCARIA " + 
				"where IDEMPCOD = ? and IDEJECONTABLE = ? and ENTCOD = ? and CBACOD = ?",
					new ValorContadorExtractor(), empresa, ejerciciocontable, entidad, cuentabancaria);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, String entidad){
		
		return jdbcTemplate.query(
				" select t1.CBACOD as codigo, " +
				" t1.CBACOD + ' - ' + t1.CBANOMBRE as descripcion, " + 
				" t3.DESCRIPCION as estado " +
				" from T_M_ENTE_CUENTABANCARIA t1 " +					
					" left outer join V_T_ESTADOREG t3 " +
					" on  t1.IDEMPCOD = t3.IDEMPCOD " +
					" and t1.CBAESTADO = t3.IDESTADO " +
				" where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.ENTCOD = ?  " +
				" order by t1.CBACOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable, entidad});
	}


}
