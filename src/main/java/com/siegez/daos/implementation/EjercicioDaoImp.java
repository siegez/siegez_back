package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.siegez.daos.interfaces.EjercicioDao;
import com.siegez.models.Ejercicio;
import com.siegez.rowmapper.extractors.EjercicioExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.EjercicioRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class EjercicioDaoImp  implements EjercicioDao{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	@Transactional
	public void insertar(Ejercicio ejercicio) {
		jdbcTemplate.update("insert into t_con_ejerciciocontable(idempcod, idejecontable, ejeindact, ejeestado, "
				+ " mgbcreauser, mgbcreadate, mgbmodiuser, mgbmodidate) " 
				+ " values (?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",
				ejercicio.getEmpresa(),
				ejercicio.getEjerciciocontable(),
				ejercicio.getIndicador(),
				ejercicio.getEstado(),
				ejercicio.getCreacionUsuario(),
				ejercicio.getModificacionUsuario());
		
		jdbcTemplate.update("insert into T_CON_PERIODOCONTABLE(idempcod, idejecontable, idpercod, perindact, perestado, "
				+ " mgbcreauser, mgbcreadate, mgbmodiuser, mgbmodidate) "
				+ " SELECT IDEMPCOD, ?, IDMES, 'A', "
				+ " ESTADO, ?, dateadd(hour, -5, getdate()), ?,dateadd(hour, -5, getdate()) "  
				+ " FROM V_T_MES "  
				+ " WHERE IDEMPCOD = ? ",
				ejercicio.getEjerciciocontable(),
				ejercicio.getCreacionUsuario(),
				ejercicio.getModificacionUsuario(),
				ejercicio.getEmpresa());
	}

	@Override
	public void editar(Ejercicio ejercicio) {
		jdbcTemplate.update("update t_con_ejerciciocontable set ejeindact = ?, ejeestado = ?, "
				+ "									mgbmodiuser = ?, mgbmodidate = dateadd(hour, -5, getdate()) "
				+ "									where idempcod like ? and idejecontable = ?",
				ejercicio.getIndicador(),
				ejercicio.getEstado(),
				ejercicio.getModificacionUsuario(),
				ejercicio.getEmpresa(),
				ejercicio.getEjerciciocontable());
	}

	@Override
	public int eliminar(Ejercicio ejercicio) {
		return jdbcTemplate.update("delete from t_con_ejerciciocontable where idempcod like ? and idejecontable like ?", 
				ejercicio.getEmpresa(),
				ejercicio.getEjerciciocontable());
	}

	@Override
	public Ejercicio consultar(Integer empresa, Integer ejerciciocontable) {		
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select T1.idempcod, T1.idejecontable, T1.ejeindact, T1.ejeestado, '' as descripcionestado, " + 
							" T1.mgbcreauser, T1.mgbcreadate, T1.mgbmodiuser, T1.mgbmodidate "+
							" from t_con_ejerciciocontable T1 " +
					"where t1.IDEMPCOD = ?  and t1.idejecontable = ?  ", 
					new EjercicioExtractor(), empresa, ejerciciocontable);
		} else {
			return new Ejercicio();
		} 
	}
	
	@Override
	public List<Ejercicio> consultarLista(Integer empresa) {
		return jdbcTemplate.query("select T1.idempcod, T1.idejecontable, T1.ejeindact, T1.ejeestado, T2.DESCRIPCION as descripcionestado, " + 
				" T1.mgbcreauser, T1.mgbcreadate, T1.mgbmodiuser, T1.mgbmodidate "+
				" from t_con_ejerciciocontable T1 " +
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.EJEESTADO = t2.IDESTADO " +	
				" where T1.idempcod like ?",
				new EjercicioRowMapper(),empresa);
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from t_con_ejerciciocontable " + 
				"where IDEMPCOD = ?  and idejecontable = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable);
	}

	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa){
		
		return jdbcTemplate.query(
				" select t1.idejecontable as codigo, " +
				" t1.idejecontable as descripcion, " + 
				" t2.DESCRIPCION as estado " +
				" from t_con_ejerciciocontable t1 " +
					" left outer join V_T_ESTADOREG t2 " +
					" on  t1.IDEMPCOD = t2.IDEMPCOD " +
					" and t1.EJEESTADO = t2.IDESTADO " +
				" where t1.IDEMPCOD = ?  " +
				" order by t1.idejecontable ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
}
