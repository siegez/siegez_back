package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.RegistroContableDao;
import com.siegez.models.RegistroContable;
import com.siegez.rowmapper.extractors.RegistroContableExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.RegistroContableRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class RegistroContableDaoImp implements RegistroContableDao 
{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) 
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(RegistroContable registrocontable) 
	{
		jdbcTemplate.update("insert into T_CON_REGISTROCONTABLE (" +
				" IDEMPCOD, IDEJECONTABLE, IDASINUM, REGVEZ, " + 
				" CTACOD, ENTCOD, ENTRUC, IDTIPOANOTACION, IDTIPOREGISTRO, " + 
				" IDMONEDA, IDTIPOCAMBIO, REGTIPOCAMBIOSOL, REGTIPOCAMBIODOL, " + 
				" REGMTOORIGEN, REGMTONACIONAL, REGMTODOLARES, TDOCOD, " + 
				" REGSERCOMPROBANTE, REGNUMCOMPROBANTE, REGNUMCOMPROBANTEFIN, REGFECHAEMISION, " + 
				" REGFECHAVENCIMIENTO, IDMEDIOPAGO, IDFLUJOEFECTIVO, REGINDCENTROCOSTO, IDCENTROCOSTO, " + 
				" REGPORCENTROCOSTO, REGVEZORIGEN, REGGLOSA, REGINDINCLUIRREGCOM, " + 
				" REGINDCALCULO, REGREGCOMREF, REGREGCOMIGV, " +			
				" REGESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
				"         ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				"         ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				registrocontable.getEmpresa(),
				registrocontable.getEjerciciocontable(),
				registrocontable.getAsientocontable(),		
				registrocontable.getRegistrocontable(),
				registrocontable.getCuentacontable(),
				registrocontable.getEntidad(),
				registrocontable.getRuc(),
				registrocontable.getTipoanotacion(),
				registrocontable.getTiporegistro(),
				registrocontable.getMoneda(),
				registrocontable.getTipocambio(),
				registrocontable.getTipocambiosoles(),
				registrocontable.getTipocambiodolares(),
				registrocontable.getMontoorigen(),
				registrocontable.getMontonacional(),
				registrocontable.getMontodolares(),
				registrocontable.getTipodocumento(),
				registrocontable.getNumeroserie(),
				registrocontable.getNumerocomprobanteini(),
				registrocontable.getNumerocomprobantefin(),
				registrocontable.getFechaemision(),
				registrocontable.getFechavencimiento(),
				registrocontable.getMediopago(),
				registrocontable.getFlujoefectivo(),
				registrocontable.getTablacentrocosto(),
				registrocontable.getCentrocosto(),
				registrocontable.getPorcentajecentrocosto(),	
				registrocontable.getCorrelativoorigen(),
				registrocontable.getGlosa(),
				registrocontable.getIncluirregistrocompra(),
				registrocontable.getIndicadorcalculo(),
				registrocontable.getColumnamonto(),
				registrocontable.getColumnaimpuesto(),
				registrocontable.getEstado(),
				registrocontable.getCreacionUsuario(),
				registrocontable.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(RegistroContable registrocontable) 
	{
		jdbcTemplate.update("update T_CON_REGISTROCONTABLE set " +
				" CTACOD = ?, ENTCOD = ?, ENTRUC = ?, IDTIPOANOTACION = ?, IDTIPOREGISTRO = ?, " + 
				" IDMONEDA = ?, IDTIPOCAMBIO = ?, REGTIPOCAMBIOSOL = ?, REGTIPOCAMBIODOL = ?, " + 
				" REGMTOORIGEN = ?, REGMTONACIONAL = ?, REGMTODOLARES = ?, TDOCOD = ?, " + 
				" REGSERCOMPROBANTE = ?, REGNUMCOMPROBANTE = ?, REGNUMCOMPROBANTEFIN = ?, REGFECHAEMISION = ?, " + 
				" REGFECHAVENCIMIENTO = ?, IDMEDIOPAGO = ?, IDFLUJOEFECTIVO = ?, REGINDCENTROCOSTO = ?, IDCENTROCOSTO = ?, " + 
				" REGPORCENTROCOSTO = ?, REGVEZORIGEN = ?, REGGLOSA = ?, REGINDINCLUIRREGCOM = ?, " + 
				" REGINDCALCULO = ?, REGREGCOMREF = ?, REGREGCOMIGV = ?, " +	
				", REGESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate())) " +
				"  where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? and REGVEZ = ? ",	
				registrocontable.getCuentacontable(),
				registrocontable.getEntidad(),
				registrocontable.getRuc(),
				registrocontable.getTipoanotacion(),
				registrocontable.getTiporegistro(),
				registrocontable.getMoneda(),
				registrocontable.getTipocambio(),
				registrocontable.getTipocambiosoles(),
				registrocontable.getTipocambiodolares(),
				registrocontable.getMontoorigen(),
				registrocontable.getMontonacional(),
				registrocontable.getMontodolares(),
				registrocontable.getTipodocumento(),
				registrocontable.getNumeroserie(),
				registrocontable.getNumerocomprobanteini(),
				registrocontable.getNumerocomprobantefin(),
				registrocontable.getFechaemision(),
				registrocontable.getFechavencimiento(),
				registrocontable.getMediopago(),
				registrocontable.getFlujoefectivo(),
				registrocontable.getTablacentrocosto(),
				registrocontable.getCentrocosto(),
				registrocontable.getPorcentajecentrocosto(),	
				registrocontable.getCorrelativoorigen(),
				registrocontable.getGlosa(),
				registrocontable.getIncluirregistrocompra(),
				registrocontable.getIndicadorcalculo(),
				registrocontable.getColumnamonto(),
				registrocontable.getColumnaimpuesto(),
				registrocontable.getEstado(),
				registrocontable.getModificacionUsuario(),
				registrocontable.getEmpresa(),
				registrocontable.getEjerciciocontable(),
				registrocontable.getAsientocontable(),		
				registrocontable.getRegistrocontable()
				);	
		
	}	
	
	@Override
	public int eliminar(RegistroContable registrocontable) 
	{
		return jdbcTemplate.update("delete from T_CON_REGISTROCONTABLE " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? and REGVEZ = ? ", 
				registrocontable.getEmpresa(),
				registrocontable.getEjerciciocontable(),
				registrocontable.getAsientocontable(),		
				registrocontable.getRegistrocontable()
				);	
	}
	
	@Override
	public int eliminarOperacion(RegistroContable registrocontable) 
	{
		return jdbcTemplate.update("DELETE T_CON_REGISTROCONTABLE FROM T_CON_REGISTROCONTABLE t1 " +
				"	INNER JOIN T_CON_ASIENTOCONTABLE t2 " +
				"	on t1.IDEMPCOD = t2.IDEMPCOD " +
				"	and t1.IDEJECONTABLE = t2.IDEJECONTABLE " +
				"	AND t1.IDASINUM = t2.IDASINUM " +
				" WHERE t1.IDEMPCOD = ? " +
				"	and t1.IDEJECONTABLE = ? " +
				"	and t2.IDOPENUM = ? ", 
				registrocontable.getEmpresa(),
				registrocontable.getEjerciciocontable(),
				registrocontable.getOperacioncontable()
				);	
	}
	
	@Override
	public RegistroContable consultar(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable)
	{		
		return jdbcTemplate.query(
				"select  " +
					"	t1.IDEMPCOD, t1.IDEJECONTABLE, '' as IDOPENUM, t1.IDASINUM, t1.REGVEZ, " +
					"	t1.CTACOD, '' as descripcioncuentacontable, t1.ENTCOD, " + 
					"	'' as nombreentidad, " + 
					"	t1.ENTRUC, t1.IDTIPOANOTACION, '' as descripciontipoanotacion, t1.IDTIPOREGISTRO, '' as descripciontiporegistro,  " +
					"	t1.IDMONEDA, '' as descripcionmoneda, t1.IDTIPOCAMBIO, '' as descripciontipocambio, " +
					"	t1.REGTIPOCAMBIOSOL, t1.REGTIPOCAMBIODOL, t1.REGMTOORIGEN, t1.REGMTONACIONAL, t1.REGMTODOLARES, " +
					"	t1.TDOCOD, '' as descripciontipodocumento,  " +
					"	t1.REGSERCOMPROBANTE, t1.REGNUMCOMPROBANTE, t1.REGNUMCOMPROBANTEFIN, t1.REGFECHAEMISION, " + 
					"	t1.REGFECHAVENCIMIENTO, t1.IDMEDIOPAGO, '' as descripcionmediopago, " +
					"	t1.IDFLUJOEFECTIVO, '' as descripcionflujoefectivo, " +
					"	t1.REGINDCENTROCOSTO, t1.IDCENTROCOSTO, '' as descripcioncentrocosto,  " +
					"	t1.REGPORCENTROCOSTO, t1.REGVEZORIGEN, t1.REGGLOSA, t1.REGINDINCLUIRREGCOM, " +
					"	t1.REGINDCALCULO, t1.REGREGCOMREF, t1.REGREGCOMIGV, " +
					"	t1.REGESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_CON_REGISTROCONTABLE t1 " +
				"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.IDASINUM = ? and t1.REGVEZ = ?  ", 
				new RegistroContableExtractor(), empresa, ejerciciocontable, asientocontable, registrocontable);
	}
	
	@Override
	public List<RegistroContable> consultarLista(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
	
		return jdbcTemplate.query(
			"select  " +
				"	t1.IDEMPCOD, t1.IDEJECONTABLE, '' as IDOPENUM, t1.IDASINUM, t1.REGVEZ, " +
				"	t1.CTACOD, t2.CTADESCRIP as descripcioncuentacontable, t1.ENTCOD, " + 
				"	case when t3.IDTIPOPERSONA = 'N' then t3.ENTPATERNO + ' ' + t3.ENTMATERNO + ', ' + t3.ENTNOMBRES ELSE t3.ENTNOMBRES end as nombreentidad, " + 
				"	t1.ENTRUC, t1.IDTIPOANOTACION, t4.DESCRIPCION as descripciontipoanotacion, t1.IDTIPOREGISTRO, t5.DESCRIPCION as descripciontiporegistro,  " +
				"	t1.IDMONEDA, t6.DESCRIPCION as descripcionmoneda, t1.IDTIPOCAMBIO, t7.DESCRIPCION as descripciontipocambio, " +
				"	t1.REGTIPOCAMBIOSOL, t1.REGTIPOCAMBIODOL, t1.REGMTOORIGEN, t1.REGMTONACIONAL, t1.REGMTODOLARES, " +
				"	t1.TDOCOD, T8.TDODESCRIP as descripciontipodocumento,  " +
				"	t1.REGSERCOMPROBANTE, t1.REGNUMCOMPROBANTE, t1.REGNUMCOMPROBANTEFIN, t1.REGFECHAEMISION, " + 
				"	t1.REGFECHAVENCIMIENTO, t1.IDMEDIOPAGO, t9.DESCRIPCION as descripcionmediopago, " +
				"	t1.IDFLUJOEFECTIVO, t13.DESCRIPCION as descripcionflujoefectivo, " +
				"	t1.REGINDCENTROCOSTO, t1.IDCENTROCOSTO, " +
				"   case when t1.REGINDCENTROCOSTO = 'G' then t11.GCCNOMBRE else t10.CCONOMBRE end as descripcioncentrocosto,  " +
				"	t1.REGPORCENTROCOSTO, t1.REGVEZORIGEN, t1.REGGLOSA, t1.REGINDINCLUIRREGCOM, " +
				"	t1.REGINDCALCULO, t1.REGREGCOMREF, t1.REGREGCOMIGV, " +
				"	t1.REGESTADO, t12.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +  
			"from T_CON_REGISTROCONTABLE t1 " +	
				"	left outer join T_CON_PLANCONTABLE t2 " +
				"	on t1.IDEMPCOD = t2.IDEMPCOD " +
				"	and t1.IDEJECONTABLE = t2.IDEJECONTABLE " +
				"	and t1.CTACOD = t2.CTACOD " +
				"	left outer join T_M_ENTE t3 " +
				"	on  t1.IDEMPCOD = t3.IDEMPCOD  " +
				"	and t1.ENTCOD = t3.ENTCOD " +
				"	left outer join V_T_TIPOANOTACION t4 " +
				"	on  t1.IDEMPCOD = t4.IDEMPCOD " +
				"	and t1.IDTIPOANOTACION = t4.IDTIPOANOTACION	 " +
				"	left outer join V_T_TIPOREGISTRO t5 " +
				"	on  t1.IDEMPCOD = t5.IDEMPCOD " +
				"	and t1.IDTIPOREGISTRO = t5.IDTIPOREGISTRO " +
				"	left outer join V_T_MONEDA t6 " +
				"	on  t1.IDEMPCOD = t6.IDEMPCOD " +
				"	and t1.IDMONEDA = t6.IDMONEDA " +
				"	left outer join V_T_TIPOCAMBIO t7 " +
				"	on  t1.IDEMPCOD = t7.IDEMPCOD " +
				"	and t1.IDTIPOCAMBIO = t7.IDTIPOCAMBIO " +
				"	left outer join T_M_TIPODOCUMENTO t8 " +
				"	on  t1.IDEMPCOD = t8.IDEMPCOD " +
				"	and t1.TDOCOD = t8.TDOCOD " +
				"	left outer join V_T_MEDIOPAGO t9 " +
				"	on  t1.IDEMPCOD = t9.IDEMPCOD " +
				"	and t1.IDMEDIOPAGO = t9.IDMEDIOPAGO " +
				"	left outer join T_CON_CENTROCOSTO t10 " +
				"	on  t1.IDEMPCOD = t10.IDEMPCOD " +
				"	and t1.IDEJECONTABLE = t10.IDEJECONTABLE " +
				"	and t1.IDCENTROCOSTO = t10.CCOCOD " +
				"	left outer join T_CON_GRUPOCENTROCOSTO t11 " +
				"	on  t1.IDEMPCOD = t11.IDEMPCOD " +
				"	and t1.IDEJECONTABLE = t11.IDEJECONTABLE " +
				"	and t1.IDCENTROCOSTO = t11.GCCCOD " +
				"	left outer join V_T_ESTADOREG t12 " +
				"	on  t1.IDEMPCOD = t12.IDEMPCOD " +
				"	and t1.REGESTADO = t12.IDESTADO " +	
				"   left outer join V_T_FLUJOEFECTIVO t13 " +
				"   on  t1.IDEMPCOD = t13.IDEMPCOD " +
				"   and t1.IDFLUJOEFECTIVO = t13.IDFLUJOEFECTIVO " +
			"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.IDASINUM = ?  ", 
				new RegistroContableRowMapper(), new Object[] {empresa, ejerciciocontable, asientocontable});
		
	}
	
	@Override
	public List<RegistroContable> consultarListaOperacion(Integer empresa, Integer ejerciciocontable, String operacioncontable) {
		
		return jdbcTemplate.query(
				"select  " +
					"	t1.IDEMPCOD, t1.IDEJECONTABLE, tt1.IDOPENUM, t1.IDASINUM, t1.REGVEZ, " +
					"	t1.CTACOD, t1.CTACOD + '-' + t2.CTADESCRIP as descripcioncuentacontable, t1.ENTCOD, " + 
					"	t1.ENTRUC + '-' + case when t3.IDTIPOPERSONA = 'N' then t3.ENTPATERNO + ' ' + t3.ENTMATERNO + ', ' + t3.ENTNOMBRES ELSE t3.ENTNOMBRES end as nombreentidad, " + 
					"	t1.ENTRUC, t1.IDTIPOANOTACION, t4.DESCRIPCION as descripciontipoanotacion, t1.IDTIPOREGISTRO, t5.DESCRIPCION as descripciontiporegistro,  " +
					"	t1.IDMONEDA, t6.DESCRIPCION as descripcionmoneda, isnull(t1.IDTIPOCAMBIO,'') as IDTIPOCAMBIO, t7.DESCRIPCION as descripciontipocambio, " +
					"	t1.REGTIPOCAMBIOSOL, t1.REGTIPOCAMBIODOL, t1.REGMTOORIGEN, t1.REGMTONACIONAL, t1.REGMTODOLARES, " +
					"	t1.TDOCOD, t1.TDOCOD + '-' + T8.TDODESCRIP as descripciontipodocumento,  " +
					"	t1.REGSERCOMPROBANTE, t1.REGNUMCOMPROBANTE, t1.REGNUMCOMPROBANTEFIN, CONVERT(VARCHAR,t1.REGFECHAEMISION,103) as REGFECHAEMISION, " + 
					"	CONVERT(VARCHAR,t1.REGFECHAVENCIMIENTO,103) as REGFECHAVENCIMIENTO, isnull(t1.IDMEDIOPAGO,'') as IDMEDIOPAGO, t1.IDMEDIOPAGO + '-' + t9.DESCRIPCION as descripcionmediopago, " +
					"	isnull(t1.IDFLUJOEFECTIVO,'') as IDFLUJOEFECTIVO, t1.IDFLUJOEFECTIVO + '-' + t13.DESCRIPCION as descripcionflujoefectivo, " +
					"	t1.REGINDCENTROCOSTO, t1.IDCENTROCOSTO, " +
					"   case when t1.REGINDCENTROCOSTO = 'G' then t11.GCCNOMBRE else t10.CCONOMBRE end as descripcioncentrocosto,  " +
					"	t1.REGPORCENTROCOSTO, t1.REGVEZORIGEN, t1.REGGLOSA, t1.REGINDINCLUIRREGCOM, " +
					"	t1.REGINDCALCULO, t1.REGREGCOMREF, t1.REGREGCOMIGV, " +
					"	t1.REGESTADO, t12.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_CON_REGISTROCONTABLE t1 " +	
					"	left outer join T_CON_ASIENTOCONTABLE tt1 " +
					"	on t1.IDEMPCOD = tt1.IDEMPCOD " +
					"	and t1.IDEJECONTABLE = tt1.IDEJECONTABLE " +
					"	and t1.IDASINUM = tt1.IDASINUM " +
					"	left outer join T_CON_PLANCONTABLE t2 " +
					"	on t1.IDEMPCOD = t2.IDEMPCOD " +
					"	and t1.IDEJECONTABLE = t2.IDEJECONTABLE " +
					"	and t1.CTACOD = t2.CTACOD " +
					"	left outer join T_M_ENTE t3 " +
					"	on  t1.IDEMPCOD = t3.IDEMPCOD  " +
					"	and t1.ENTCOD = t3.ENTCOD " +
					"	left outer join V_T_TIPOANOTACION t4 " +
					"	on  t1.IDEMPCOD = t4.IDEMPCOD " +
					"	and t1.IDTIPOANOTACION = t4.IDTIPOANOTACION	 " +
					"	left outer join V_T_TIPOREGISTRO t5 " +
					"	on  t1.IDEMPCOD = t5.IDEMPCOD " +
					"	and t1.IDTIPOREGISTRO = t5.IDTIPOREGISTRO " +
					"	left outer join V_T_MONEDA t6 " +
					"	on  t1.IDEMPCOD = t6.IDEMPCOD " +
					"	and t1.IDMONEDA = t6.IDMONEDA " +
					"	left outer join V_T_TIPOCAMBIO t7 " +
					"	on  t1.IDEMPCOD = t7.IDEMPCOD " +
					"	and t1.IDTIPOCAMBIO = t7.IDTIPOCAMBIO " +
					"	left outer join T_M_TIPODOCUMENTO t8 " +
					"	on  t1.IDEMPCOD = t8.IDEMPCOD " +
					"	and t1.TDOCOD = t8.TDOCOD " +
					"	left outer join V_T_MEDIOPAGO t9 " +
					"	on  t1.IDEMPCOD = t9.IDEMPCOD " +
					"	and t1.IDMEDIOPAGO = t9.IDMEDIOPAGO " +
					"	left outer join T_CON_CENTROCOSTO t10 " +
					"	on  t1.IDEMPCOD = t10.IDEMPCOD " +
					"	and t1.IDEJECONTABLE = t10.IDEJECONTABLE " +
					"	and t1.IDCENTROCOSTO = t10.CCOCOD " +
					"	left outer join T_CON_GRUPOCENTROCOSTO t11 " +
					"	on  t1.IDEMPCOD = t11.IDEMPCOD " +
					"	and t1.IDEJECONTABLE = t11.IDEJECONTABLE " +
					"	and t1.IDCENTROCOSTO = t11.GCCCOD " +
					"	left outer join V_T_ESTADOREG t12 " +
					"	on  t1.IDEMPCOD = t12.IDEMPCOD " +
					"	and t1.REGESTADO = t12.IDESTADO " +		
					"   left outer join V_T_FLUJOEFECTIVO t13 " +
					"   on  t1.IDEMPCOD = t13.IDEMPCOD " +
					"   and t1.IDFLUJOEFECTIVO = t13.IDFLUJOEFECTIVO " +
				"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and tt1.IDOPENUM = ?  " +
				"order by t1.IDTIPOREGISTRO, t1.IDTIPOANOTACION " , 
					new RegistroContableRowMapper(), new Object[] {empresa, ejerciciocontable, operacioncontable});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_REGISTROCONTABLE " + 
				"where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? and REGVEZ = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable, asientocontable, registrocontable);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, Integer asientocontable){
		
		return jdbcTemplate.query(
				" select t1.CTACOD as codigo, " +
				" t1.IDTIPOANOTACION as descripcion, " + 
				" ltrim(convert(varchar(10), convert(DEC(6,2),t1.REGMTOORIGEN))) as estado " +
				" from T_CON_REGISTROCONTABLE t1 " +					
				" where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.IDASINUM = ? " +
				" order by t1.IDASINUM ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable, asientocontable});
	}
	
}

