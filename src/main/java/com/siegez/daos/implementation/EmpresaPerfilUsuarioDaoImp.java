package com.siegez.daos.implementation;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.models.EmpresaPerfilUsuario;

import com.siegez.daos.interfaces.EmpresaPerfilUsuarioDao;
import com.siegez.rowmapper.extractors.EmpresaPerfilUsuarioExtractor;
import com.siegez.rowmapper.mappers.EmpresaPerfilUsuarioRowMapper;

@Repository
public class EmpresaPerfilUsuarioDaoImp implements EmpresaPerfilUsuarioDao{

	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public void insertar(EmpresaPerfilUsuario empresaperfilusuario) {	
		jdbcTemplate.update("insert into T_M_EMPRESAPERFILUSUARIO (IDEMPCOD, IDPEFCOD, USERNAME, EXUDEFECTO, EXUESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				empresaperfilusuario.getEmpresa(),
				empresaperfilusuario.getPerfil(),
				empresaperfilusuario.getUsername(),
				empresaperfilusuario.getIndDefecto(),
				empresaperfilusuario.getEstado(),
				empresaperfilusuario.getCreacionUsuario(),
				empresaperfilusuario.getModificacionUsuario());
		
		if (empresaperfilusuario.getIndDefecto().equals("A")) {
		jdbcTemplate.update("update T_M_EMPRESAPERFILUSUARIO "
				+ "	set EXUDEFECTO = case when IDEMPCOD = ? and IDPEFCOD = ? then 'A' else 'I' end "
				+ "	where USERNAME = ? ",
				empresaperfilusuario.getEmpresa(),
				empresaperfilusuario.getPerfil(),
				empresaperfilusuario.getUsername());
		}
	}

	@Override
	public void editar(EmpresaPerfilUsuario empresaperfilusuario) {
		jdbcTemplate.update("update T_M_EMPRESAPERFILUSUARIO set EXUDEFECTO = ?, EXUESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDPEFCOD = ? and USERNAME = ? ",						
				empresaperfilusuario.getIndDefecto(),
				empresaperfilusuario.getEstado(),
				empresaperfilusuario.getModificacionUsuario(),
				empresaperfilusuario.getEmpresa(),
				empresaperfilusuario.getPerfil(),
				empresaperfilusuario.getUsername());	
		
		if (empresaperfilusuario.getIndDefecto().equals("A")) {
		jdbcTemplate.update("update T_M_EMPRESAPERFILUSUARIO "
				+ "	set EXUDEFECTO = case when IDEMPCOD = ? and IDPEFCOD = ? then 'A' else 'I' end "
				+ "	where USERNAME = ? ",
				empresaperfilusuario.getEmpresa(),
				empresaperfilusuario.getPerfil(),
				empresaperfilusuario.getUsername());
		}		
	}

	@Override
	public int eliminar(EmpresaPerfilUsuario empresaperfilusuario) {		
		return jdbcTemplate.update("delete from T_M_EMPRESAPERFILUSUARIO " 
				+ " where IDEMPCOD = ? and IDPEFCOD = ? and  USERNAME = ? ", 
				empresaperfilusuario.getEmpresa(),
				empresaperfilusuario.getPerfil(),
				empresaperfilusuario.getUsername());	
	}
	
	
	@Override
	public EmpresaPerfilUsuario consultar(EmpresaPerfilUsuario empresaperfilusuario) {
		return jdbcTemplate.query("select t1.IDEMPCOD, t2.EMPDESCRIP, t1.IDPEFCOD, t3.PEFDESCRIP, t1.USERNAME, t1.EXUDEFECTO, t1.EXUESTADO, " + 
				"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 			
				"				from T_M_EMPRESAPERFILUSUARIO t1 " + 
				"				left outer join T_M_EMPRESA t2 " + 
				"				on t1.IDEMPCOD = t2.IDEMPCOD " + 
				"				left outer join T_M_PERFIL t3 " + 
				"				on t1.IDPEFCOD = t3.IDPEFCOD " + 
				"				where t1.IDEMPCOD = ? and t1.IDPEFCOD = ? and t1.USERNAME = ? ", 
				new EmpresaPerfilUsuarioExtractor(), 
				new Object[] {empresaperfilusuario.getEmpresa(), empresaperfilusuario.getPerfil(), empresaperfilusuario.getUsername()});
	}
	
	@Override
	public List<EmpresaPerfilUsuario> consultarLista(String username) {
		return jdbcTemplate.query("select t1.IDEMPCOD, t2.EMPDESCRIP, t1.IDPEFCOD, t3.PEFDESCRIP, t1.USERNAME, t1.EXUDEFECTO, t1.EXUESTADO, " + 
				"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"				from T_M_EMPRESAPERFILUSUARIO t1 " + 
				"				left outer join T_M_EMPRESA t2 " + 
				"				on t1.IDEMPCOD = t2.IDEMPCOD " + 
				"				left outer join T_M_PERFIL t3 " + 
				"				on t1.IDPEFCOD = t3.IDPEFCOD " + 
				"				where t1.USERNAME = ? ", new EmpresaPerfilUsuarioRowMapper(), username);
	}
}
