package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.MenuDao;
import com.siegez.models.Menu;
import com.siegez.models.MenuOpcionesConsulta;
import com.siegez.rowmapper.extractors.MenuExtractor;

import com.siegez.rowmapper.mappers.MenuRowMapper;
import com.siegez.rowmapper.mappers.MenuOpcionesRowMapper;

@Repository
public class MenuDaoImp implements MenuDao {

	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public void save(Menu menu) {	
		jdbcTemplate.update("insert into t_m_menu(IDMENCOD, IDMODCOD, MENDESCRIP, MENOPCION, MENNIVEL, MENCORRE, MENESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?)",
				menu.getCodigo(),
				menu.getModulo(),
				menu.getDescripcion(),
				menu.getOpcion(),
				menu.getNivel(),
				menu.getCorrelativo(),
				menu.getEstado(),
				menu.getCreacionUsuario(),
				menu.getCreacionFecha(),
				menu.getModUsuario(),
				menu.getModFecha());
					
	}

	@Override
	public void update(Menu menu) {
		jdbcTemplate.update("update t_m_menu set IDMODCOD = ?, MENDESCRIP = ?, MENOPCION = ?, MENNIVEL = ?, MENCORRE = ?, MENESTADO = ?, MGBCREAUSER = ?, MGBCREADATE = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = ? "
				+ "	where IDMENCOD like ?",
				menu.getModulo(),
				menu.getDescripcion(),
				menu.getOpcion(),
				menu.getNivel(),
				menu.getCorrelativo(),
				menu.getEstado(),
				menu.getCreacionUsuario(),
				menu.getCreacionFecha(),
				menu.getModUsuario(),
				menu.getModFecha(),
				menu.getCodigo());
		
	}

	@Override
	public Menu find(Integer codigo) {
		
		return jdbcTemplate.query("select IDMENCOD, IDMODCOD, MENDESCRIP, MENOPCION, MENNIVEL, MENCORRE, MENESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE from t_m_menu where IDMENCOD like ?", new MenuExtractor(), codigo);
		   
	}

	@Override
	public int delete(Integer codigo) {		
		return jdbcTemplate.update("delete from t_m_menu where IDMENCOD like ?", codigo);	
	}

	@Override
	public List<Menu> findAll() {
		return jdbcTemplate.query("select IDMENCOD, IDMODCOD, MENDESCRIP, MENOPCION, MENNIVEL, MENCORRE, MENESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE from t_m_menu", new MenuRowMapper());
	}
	//Lista de opciones de menú, por usuario y empresa por default
	@Override
	public List<MenuOpcionesConsulta> findAllMenuOpciones(String username) {
		/*
		return jdbcTemplate.query("select t1.IDMENCOD, t1.MENDESCRIP, t1.MENOPCION, t1.MENNIVEL, t1.MENCORRE, t1.MENESTADO, " +
				" t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +
				" from T_M_MENU t1 left outer join T_M_PERFILMENU t2 " + 
				"		on t1.IDMENCOD = t2.IDMENCOD " + 
				"		left outer join T_M_EMPRESAPERFILUSUARIO t3 " +
				"		on t2.IDPEFCOD = t3.IDPEFCOD " + 
				"		and t3.EXUDEFECTO = '1' " + 
				"		where t3.USERNAME = ?", new MenuRowMapper(), username);
		*/
		return jdbcTemplate.query("select t10.IDMENCOD, t10.MENDESCRIP, t10.MENOPCION, t10.MENNIVEL, t10.MENCORRE, t10.MENESTADO, " + 
				" t2.IDEMPCOD, t2.IDPEFCOD " + 
				" From T_M_USUARIO t1 left outer join T_M_EMPRESAPERFILUSUARIO t2 " + 
				" 	on t1.USERNAME = t2.USERNAME " + 
				" 	and t2.EXUDEFECTO = '1' " + 
				" 	left outer join T_M_PERFIL t3 " + 
				" 	on t2.IDEMPCOD = t3.IDEMPCOD " + 
				" 	and t2.IDPEFCOD = t3.IDPEFCOD " + 
				" 	left outer join T_M_PERFILMENU t4 " + 
				" 	on t3.IDPEFCOD = t4.IDPEFCOD  " + 
				" 	left outer join T_M_MENU t10 " + 
				" 	on t4.IDMENCOD = t10.IDMENCOD " + 
				" 	left outer join T_M_LICENCIAMODULO T5 " + 
				" 	on T10.IDMODCOD = T5.IDMODCOD " + 
				" where T1.IDLICCOD = T5.IDLICCOD " + 
				" 	and T5.LXMFECINICIO < GETDATE() and isnull(T5.LXMFECCADUCA, dateadd(day, 1, GETDATE())) > GETDATE() " +
				" 	and t1.USERNAME = ? ", new MenuOpcionesRowMapper(), username);
	}
	
	//Lista de opciones de menú, por usuario y empresa
	@Override
	public List<MenuOpcionesConsulta> findAllMenuEmpresa(String username, Integer idempcod){
		return jdbcTemplate.query("select t10.IDMENCOD, t10.MENDESCRIP, t10.MENOPCION, t10.MENNIVEL, t10.MENCORRE, t10.MENESTADO, " + 
				" t2.IDEMPCOD, t2.IDPEFCOD " + 
				" From T_M_USUARIO t1 left outer join T_M_EMPRESAPERFILUSUARIO t2 " + 
				" 	on t1.USERNAME = t2.USERNAME " + 
				" 	left outer join T_M_PERFIL t3 " + 
				" 	on t2.IDEMPCOD = t3.IDEMPCOD " + 
				" 	and t2.IDPEFCOD = t3.IDPEFCOD " + 
				" 	left outer join T_M_PERFILMENU t4 " + 
				" 	on t3.IDPEFCOD = t4.IDPEFCOD  " + 
				" 	left outer join T_M_MENU t10 " + 
				" 	on t4.IDMENCOD = t10.IDMENCOD " + 
				" 	left outer join T_M_LICENCIAMODULO T5 " + 
				" 	on T10.IDMODCOD = T5.IDMODCOD " +
				" where T1.IDLICCOD = T5.IDLICCOD " + 
				" 	and T5.LXMFECINICIO < GETDATE() and isnull(T5.LXMFECCADUCA, dateadd(day, 1, GETDATE())) > GETDATE() " +
				" 	and t1.USERNAME = ? " +
				"	and t2.IDEMPCOD = ? ", new MenuOpcionesRowMapper(), new Object[] { username, idempcod});		
	}
	
	//Lista de opciones de menú, por licencia
	@Override
	public List<MenuOpcionesConsulta> findAllMenuPerfilNuevo(String licencia){

		return jdbcTemplate.query("select t10.IDMENCOD, t10.MENDESCRIP, t10.MENOPCION, t10.MENNIVEL, t10.MENCORRE, t10.MENESTADO,  " + 
				"'', '0'  " +
				" From T_M_LICENCIA t1 left outer join T_M_LICENCIAMODULO t2 " +
				"	on t1.IDLICCOD = t2.IDLICCOD " +
				"	left outer join T_M_MENU t10 " +
				" 	on t2.IDMODCOD = t10.IDMODCOD " +
				" where T2.LXMFECINICIO < GETDATE() and isnull(T2.LXMFECCADUCA, dateadd(day, 1, GETDATE())) > GETDATE() " +
				"  and t1.IDLICCOD = ? ", new MenuOpcionesRowMapper(), licencia);
				
	}
	
	//Lista de opciones de menú, por perfil y licencia, para actualizar accesos
	@Override
	public List<MenuOpcionesConsulta> findAllMenuPerfilModifica(String perfil, String licencia){
		return jdbcTemplate.query("select t10.IDMENCOD, t10.MENDESCRIP, t10.MENOPCION, t10.MENNIVEL, t10.MENCORRE, t10.MENESTADO, " +
				" '', case when t3.IDMENCOD is null then '0' else '1' end " +
				" From T_M_LICENCIA t1 left outer join T_M_LICENCIAMODULO t2 " +
				"	on t1.IDLICCOD = t2.IDLICCOD " +
				"	left outer join T_M_MENU t10 " +
				" 	on t2.IDMODCOD = t10.IDMODCOD " +
				"	left outer join T_M_PERFILMENU t3 " +
				" 	on t10.IDMENCOD = t3.IDMENCOD " +
				"	and t3.IDPEFCOD = ? " +
				" where T2.LXMFECINICIO < GETDATE() and isnull(T2.LXMFECCADUCA, dateadd(day, 1, GETDATE())) > GETDATE() " +
				" 	and t1.IDLICCOD = ? ", new MenuOpcionesRowMapper(), new Object[] { perfil, licencia});
				
	}	
	
}
