package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.DetraccionDao;
import com.siegez.models.Detraccion;
import com.siegez.rowmapper.extractors.DetraccionExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.DetraccionRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class DetraccionDaoImp implements DetraccionDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertar(Detraccion detraccion) {
		jdbcTemplate.update("insert into T_CON_DETRACCION (" +
				" IDEMPCOD, DETCOD, DETDEFINICION, DETDESCRIP, DETTASA, DETMONTOMINIMO " +				
				", DETESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				detraccion.getEmpresa(),		
				detraccion.getDetraccion(),
				detraccion.getDefinicion(),
				detraccion.getDescripcion(),
				detraccion.getTasa(),
				detraccion.getMontominimo(),
				detraccion.getEstado(),
				detraccion.getCreacionUsuario(),
				detraccion.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(Detraccion detraccion) {
		jdbcTemplate.update("update T_CON_DETRACCION set " +
				"  DETDEFINICION = ?, DETDESCRIP = ?, DETTASA = ?, DETMONTOMINIMO = ? " +				
				", DETESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and DETCOD = ? ",					
				detraccion.getDefinicion(),
				detraccion.getDescripcion(),
				detraccion.getTasa(),
				detraccion.getMontominimo(),
				detraccion.getEstado(),
				detraccion.getModificacionUsuario(),
				detraccion.getEmpresa(),
				detraccion.getDetraccion()
				);	
		
	}	
	
	@Override
	public int eliminar(Detraccion detraccion) {
		return jdbcTemplate.update("delete from T_CON_DETRACCION " 
				+ " where IDEMPCOD = ? and DETCOD = ? ", 
				detraccion.getEmpresa(),
				detraccion.getDetraccion());	
	}
	
	@Override
	public Detraccion consultar(Integer empresa, String detraccion) {
		
		return jdbcTemplate.query(
				"select  " +
					" t1.IDEMPCOD, t1.DETCOD, t1.DETDEFINICION, t1.DETDESCRIP,  t1.DETTASA, t1.DETMONTOMINIMO " +
					", t1.DETESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_CON_DETRACCION t1 " +
				"where t1.IDEMPCOD = ?  and t1.DETCOD = ?  ", 
				new DetraccionExtractor(), empresa, detraccion);
	}
	
	@Override
	public List<Detraccion> consultarLista(Integer empresa) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.DETCOD, t1.DETDEFINICION, t1.DETDESCRIP " +
				", ltrim(convert(varchar(10), convert(DEC(6,2),t1.DETTASA))) as DETTASA " +
				", ltrim(convert(varchar(20), convert(DEC(20,2),t1.DETMONTOMINIMO))) as DETMONTOMINIMO " +
				", t1.DETESTADO, t3.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_DETRACCION t1 " +					
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.DETESTADO = t3.IDESTADO " +				
			"where t1.IDEMPCOD = ?  ", 
				new DetraccionRowMapper(), new Object[] {empresa});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, String detraccion) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_DETRACCION " + 
				"where IDEMPCOD = ?  and DETCOD = ? ",
					new ValorContadorExtractor(), empresa, detraccion);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa){
		
		return jdbcTemplate.query(
				" select t1.DETCOD as codigo, " +
				" t1.DETDEFINICION + '('+ltrim(convert(varchar(10), convert(DEC(6,2),t1.DETTASA))) + " +
				" ' %' + '; mínimo = ' + LTRIM(convert(varchar(20), convert(DEC(20,2),t1.DETMONTOMINIMO) )) + ')' as descripcion, " + 
				" t3.DESCRIPCION as estado " +
				" from T_CON_DETRACCION t1 " +
					" left outer join V_T_ESTADOREG t3 " +
					" on  t1.IDEMPCOD = t3.IDEMPCOD " +
					" and t1.DETESTADO = t3.IDESTADO " +
				" where t1.IDEMPCOD = ?  " +
				" order by t1.DETCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}


}
