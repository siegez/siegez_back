package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.PlanContableDao;
import com.siegez.models.PlanContable;
import com.siegez.rowmapper.extractors.ConsultaStringExtractor;
import com.siegez.rowmapper.extractors.PlanContableExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.PlanContableRowMapper;
import com.siegez.rowmapper.mappers.VistaPlanContableRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;
import com.siegez.views.VistaPlanContable;

@Repository
public class PlanContableDaoImp implements PlanContableDao {

	
	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public void insertar(PlanContable plancontable) {
		jdbcTemplate.update("insert into T_CON_PLANCONTABLE (IDEMPCOD, IDEJECONTABLE, CTACOD, CTADESCRIP " + 
				", IDCATEGORIACUENTA, IDTIPOCUENTA, IDMONEDA, IDNIVELSALDO, IDSOCIONEGOCIO " + 
				", CTAINDDOC, CTAINDING, CTAINDMEDIOPAGO " + 
				", CTAINDCENCOS, CTAINDAJUDIFCAMPEN, CTAINDAJUDIFCAMCAN, CTAINDTIPOCAMBIO " + 
				", CTADEBE, CTAHABER, CTAINDCIECLA9, CTAINDCIENAT " + 
				", CTAINDCOBRO, CTAINDPAGO, CTAESTADO " + 
				", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				plancontable.getEmpresa(),
				plancontable.getEjerciciocontable(),
				plancontable.getCuenta(),
				plancontable.getDescripcioncuenta(),
				plancontable.getCategoria(),
				plancontable.getTipocuenta(),
				plancontable.getMoneda(),
				plancontable.getNivelsaldo(),
				plancontable.getSocionegocio(),
				plancontable.getDocumentoreferencia(),
				plancontable.getCuentamanual(),
				plancontable.getMediopago(),
				plancontable.getCentrocosto(),
				plancontable.getAjustependiente(),
				plancontable.getAjustecancelado(),
				plancontable.getTipocambio(),
				plancontable.getAsientodebe(),
				plancontable.getAsientohaber(),
				plancontable.getSaldoclase9(),
				plancontable.getTransferencia(),
				plancontable.getCobro(),
				plancontable.getPago(),
				plancontable.getEstado(),
				plancontable.getCreacionUsuario(),
				plancontable.getModificacionUsuario()
				);
	}

	@Override
	public void editar(PlanContable plancontable) {
		jdbcTemplate.update("update T_CON_PLANCONTABLE set CTADESCRIP = ?, IDCATEGORIACUENTA = ?, IDTIPOCUENTA = ?, IDMONEDA = ?, "
				+ "IDNIVELSALDO = ?, IDSOCIONEGOCIO = ?, CTAINDDOC = ?, CTAINDING = ?, CTAINDMEDIOPAGO = ?, "
				+ "CTAINDCENCOS = ?, CTAINDAJUDIFCAMPEN = ?, CTAINDAJUDIFCAMCAN = ?, CTAINDTIPOCAMBIO = ?, CTADEBE = ?, "
				+ "CTAHABER = ?, CTAINDCIECLA9 = ?, CTAINDCIENAT = ?, CTAINDCOBRO = ?, CTAINDPAGO = ?, "
				+ "CTAESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDEJECONTABLE = ? and CTACOD = ?  ",				
				plancontable.getDescripcioncuenta(),
				plancontable.getCategoria(),
				plancontable.getTipocuenta(),
				plancontable.getMoneda(),
				plancontable.getNivelsaldo(),
				plancontable.getSocionegocio(),
				plancontable.getDocumentoreferencia(),
				plancontable.getCuentamanual(),
				plancontable.getMediopago(),
				plancontable.getCentrocosto(),
				plancontable.getAjustependiente(),
				plancontable.getAjustecancelado(),
				plancontable.getTipocambio(),
				plancontable.getAsientodebe(),
				plancontable.getAsientohaber(),
				plancontable.getSaldoclase9(),
				plancontable.getTransferencia(),
				plancontable.getCobro(),
				plancontable.getPago(),
				plancontable.getEstado(),
				plancontable.getModificacionUsuario(),
				plancontable.getEmpresa(),
				plancontable.getEjerciciocontable(),
				plancontable.getCuenta()
				);	
		
	}

	@Override
	public int eliminar(PlanContable plancontable) {
		return jdbcTemplate.update("delete from T_CON_PLANCONTABLE " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and CTACOD = ? ", 
				plancontable.getEmpresa(),
				plancontable.getEjerciciocontable(),
				plancontable.getCuenta());	
	}

	@Override
	public PlanContable consultar(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.CTACOD, t1.CTADESCRIP " +
					", t1.IDCATEGORIACUENTA, '' as descripcioncategoria, t1.IDTIPOCUENTA, '' as descripciontipo " +
					", t1.IDMONEDA, '' as descripcionmoneda, t1.IDNIVELSALDO, '' as descripcionnivel " +
					", t1.IDSOCIONEGOCIO, '' as descripcionsocionegocio " +
					", t1.CTAINDDOC " +
					", case when t1.CTAINDDOC = 'S' then 'Si' when t1.CTAINDDOC = 'N' then 'No' else '' end as descripcionCTAINDDOC " +
					", t1.CTAINDING " +
					", case when t1.CTAINDING = 'M' then 'Manual' when t1.CTAINDING = 'A' then 'Automática' else '' end as descripcionCTAINDING " +
					", t1.CTAINDMEDIOPAGO " +
					", case when t1.CTAINDMEDIOPAGO = 'S' then 'Si' when t1.CTAINDMEDIOPAGO = 'N' then 'No' else '' end as descripcionCTAINDMEDIOPAGO " +
					", t1.CTAINDCENCOS " +
					", case when t1.CTAINDCENCOS = 'S' then 'Si' when t1.CTAINDCENCOS = 'N' then 'No' else '' end as descripcionCTAINDCENCOS " +
					", t1.CTAINDAJUDIFCAMPEN " +
					", case when t1.CTAINDAJUDIFCAMPEN = 'S' then 'Si' when t1.CTAINDAJUDIFCAMPEN = 'N' then 'No' else '' end as descripcionCTAINDAJUDIFCAMPEN " +
					", t1.CTAINDAJUDIFCAMCAN " +
					", case when t1.CTAINDAJUDIFCAMCAN = 'S' then 'Si' when t1.CTAINDAJUDIFCAMCAN = 'N' then 'No' else '' end as descripcionCTAINDAJUDIFCAMCAN " +
					", t1.CTAINDTIPOCAMBIO " +
					", case when t1.CTAINDTIPOCAMBIO = 'C' then 'Compra' when t1.CTAINDTIPOCAMBIO = 'V' then 'Venta' else '' end as descripcionCTAINDTIPOCAMBIO	 " +			
					", t1.CTADEBE, t1.CTAHABER " +
					", t1.CTAINDCIECLA9 " +
					", case when t1.CTAINDCIECLA9 = 'S' then 'Si' when t1.CTAINDCIECLA9 = 'N' then 'No' else '' end as descripcionCTAINDCIECLA9 " +
					", t1.CTAINDCIENAT " +
					", case when t1.CTAINDCIENAT = 'S' then 'Si' when t1.CTAINDCIENAT = 'N' then 'No' else '' end as descripcionCTAINDCIENAT " +
					", t1.CTAINDCOBRO " +
					", case when t1.CTAINDCOBRO = 'S' then 'Si' when t1.CTAINDCOBRO = 'N' then 'No' else '' end as descripcionCTAINDCOBRO " +
					", t1.CTAINDPAGO " +
					", case when t1.CTAINDPAGO = 'S' then 'Si' when t1.CTAINDPAGO = 'N' then 'No' else '' end as descripcionCTAINDPAGO " +
					", t1.CTAESTADO " +
					", case when t1.CTAESTADO = 'A' then 'Activo' when t1.CTAESTADO = 'I' then 'Inactivo' else '' end as descripcionCTAESTADO " +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
				"from T_CON_PLANCONTABLE t1 " +					
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and t1.ctacod = ? ", 
					new PlanContableExtractor(), 
					new Object[] {empresa, ejerciciocontable, cuentacontable});
	}
	
	@Override
	public List<PlanContable> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
			"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.CTACOD, t1.CTADESCRIP " +
				", t1.IDCATEGORIACUENTA, t2.DESCRIPCION as descripcioncategoria, t1.IDTIPOCUENTA, t3.DESCRIPCION as descripciontipo " +
				", t1.IDMONEDA, isnull(t4.DESCRIPCION,'') as descripcionmoneda, t1.IDNIVELSALDO, t5.DESCRIPCION as descripcionnivel " +
				", t1.IDSOCIONEGOCIO, t6.DESCRIPCION as descripcionsocionegocio " +
				", t1.CTAINDDOC " +
				", case when t1.CTAINDDOC = 'S' then 'Si' when t1.CTAINDDOC = 'N' then 'No' else '' end as descripcionCTAINDDOC " +
				", t1.CTAINDING " +
				", case when t1.CTAINDING = 'M' then 'Manual' when t1.CTAINDING = 'A' then 'Automática' else '' end as descripcionCTAINDING " +
				", t1.CTAINDMEDIOPAGO " +
				", case when t1.CTAINDMEDIOPAGO = 'S' then 'Si' when t1.CTAINDMEDIOPAGO = 'N' then 'No' else '' end as descripcionCTAINDMEDIOPAGO " +
				", t1.CTAINDCENCOS " +
				", case when t1.CTAINDCENCOS = 'S' then 'Si' when t1.CTAINDCENCOS = 'N' then 'No' else '' end as descripcionCTAINDCENCOS " +
				", t1.CTAINDAJUDIFCAMPEN " +
				", case when t1.CTAINDAJUDIFCAMPEN = 'S' then 'Si' when t1.CTAINDAJUDIFCAMPEN = 'N' then 'No' else '' end as descripcionCTAINDAJUDIFCAMPEN " +
				", t1.CTAINDAJUDIFCAMCAN " +
				", case when t1.CTAINDAJUDIFCAMCAN = 'S' then 'Si' when t1.CTAINDAJUDIFCAMCAN = 'N' then 'No' else '' end as descripcionCTAINDAJUDIFCAMCAN " +
				", t1.CTAINDTIPOCAMBIO " +
				", case when t1.CTAINDTIPOCAMBIO = 'C' then 'Compra' when t1.CTAINDTIPOCAMBIO = 'V' then 'Venta' else '' end as descripcionCTAINDTIPOCAMBIO	 " +			
				", t1.CTADEBE, t1.CTAHABER " +
				", t1.CTAINDCIECLA9 " +
				", case when t1.CTAINDCIECLA9 = 'S' then 'Si' when t1.CTAINDCIECLA9 = 'N' then 'No' else '' end as descripcionCTAINDCIECLA9 " +
				", t1.CTAINDCIENAT " +
				", case when t1.CTAINDCIENAT = 'S' then 'Si' when t1.CTAINDCIENAT = 'N' then 'No' else '' end as descripcionCTAINDCIENAT " +
				", t1.CTAINDCOBRO " +
				", case when t1.CTAINDCOBRO = 'S' then 'Si' when t1.CTAINDCOBRO = 'N' then 'No' else '' end as descripcionCTAINDCOBRO " +
				", t1.CTAINDPAGO " +
				", case when t1.CTAINDPAGO = 'S' then 'Si' when t1.CTAINDPAGO = 'N' then 'No' else '' end as descripcionCTAINDPAGO " +
				", t1.CTAESTADO, t7.DESCRIPCION as descripcionCTAESTADO " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
			"from T_CON_PLANCONTABLE t1 " +
				"left outer join V_T_CATEGORIACUENTA t2 " +
				"on t1.IDEMPCOD = t2.IDEMPCOD " +
				"and t1.IDCATEGORIACUENTA = t2.IDCATEGORIACUENTA " +
				"left outer join V_T_TIPOCUENTA t3 " +
				"on t1.IDEMPCOD = t3.IDEMPCOD " +
				"and t1.IDTIPOCUENTA = t3.IDTIPOCUENTA " +
				"left outer join V_T_MONEDA t4 " +
				"on t1.IDEMPCOD = t4.IDEMPCOD " +
				"and t1.IDMONEDA = t4.IDMONEDA " +
				"left outer join V_T_NIVELSALDO t5 " +
				"on t1.IDEMPCOD = t5.IDEMPCOD " +
				"and t1.IDNIVELSALDO = t5.IDNIVELSALDO " +
				"left outer join V_T_SOCIONEGOCIO t6 " +
				"on t1.IDEMPCOD = t6.IDEMPCOD " +
				"and t1.IDSOCIONEGOCIO = t6.IDSOCIONEGOCIO " +
				"left outer join V_T_ESTADOREG t7 " +
				"on t1.IDEMPCOD = t7.IDEMPCOD " +
				"and t1.CTAESTADO = t7.IDESTADO " +
			"where t1.idempcod = ?  " + 
				"and t1.idejecontable = ? ", new PlanContableRowMapper(), new Object[] {empresa, ejerciciocontable});
		
	}
	
	@Override
	public List<VistaPlanContable> consultarVista(Integer empresa, Integer ejerciciocontable) {
		
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.CTACOD, t1.CTADESCRIP " +
					", t1.IDCATEGORIACUENTA, t2.DESCRIPCION as descripcioncategoria, t1.IDTIPOCUENTA, t3.DESCRIPCION as descripciontipo " +
					", t1.IDMONEDA, isnull(t4.DESCRIPCION,'') as descripcionmoneda " +
					", t1.CTAESTADO, '' as descripcionCTAESTADO " +
				"from T_CON_PLANCONTABLE t1 " +
					"left outer join V_T_CATEGORIACUENTA t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.IDCATEGORIACUENTA = t2.IDCATEGORIACUENTA " +
					"left outer join V_T_TIPOCUENTA t3 " +
					"on t1.IDEMPCOD = t3.IDEMPCOD " +
					"and t1.IDTIPOCUENTA = t3.IDTIPOCUENTA " +
					"left outer join V_T_MONEDA t4 " +
					"on t1.IDEMPCOD = t4.IDEMPCOD " +
					"and t1.IDMONEDA = t4.IDMONEDA " +		
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? ", new VistaPlanContableRowMapper(), new Object[] {empresa, ejerciciocontable});
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_CON_PLANCONTABLE " +
				"where idempcod = ?  " + 
					"and idejecontable = ? " +
					"and ctacod = ? ", 
					new ValorContadorExtractor(), 
					new Object[] {empresa, ejerciciocontable, cuentacontable});
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, String cuentacontable){
		
		String	criteriobusqueda;
		
		criteriobusqueda = '%' + cuentacontable + '%';
		
		return jdbcTemplate.query(
				"select t1.CTACOD as codigo, t1.CTADESCRIP as descripcion " +
				", t2.DESCRIPCION as estado " +
				"from T_CON_PLANCONTABLE t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.CTAESTADO = t2.IDESTADO " +
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and (t1.CTACOD like ? or t1.CTADESCRIP like ?) " +
				"order by t1.CTACOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable, criteriobusqueda, criteriobusqueda});
	}
	
	@Override
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable){
		
		return jdbcTemplate.query(
				"select t1.CTACOD as codigo, t1.CTADESCRIP as descripcion " +
				", t2.DESCRIPCION as estado " +
				"from T_CON_PLANCONTABLE t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.CTAESTADO = t2.IDESTADO " +				
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and t1.CTAESTADO = 'A' " +
					"and t1.IDCATEGORIACUENTA = '004' " +
				"order by t1.CTACOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable});
	}
	
	@Override
	public ConsultaString consultarCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, cuentacontable);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select  " +
						" t1.CTACOD, ISNULL(t1.ctaindcencos,'N') as ctaindcencos, t1.IDSOCIONEGOCIO, t1.idmoneda, " +
						" (SELECT top 1 t2.VALDESCRIP FROM t_m_valorcampo t2" +
						"  WHERE t2.IDEMPCOD = ? and t2.IDVALFUN = 'CON' " + 
						"    and t2.IDVALCOD = 'IDSOCIONEGOCIO' and t2.VALVALOR = t1.IDSOCIONEGOCIO) as entidad, " +
						" t1.CTAINDDOC as campo6, t1.IDSOCIONEGOCIO as campo7, '' as campo8,'' as campo9, '' as campo10, " +
						" '' as campo11, '' as campo12, '' as campo13, '' as campo14,'' as campo15, " +
						" '' as campo16, '' as campo17, '' as campo18,'' as campo19, '' as campo20 " +
					"from T_CON_PLANCONTABLE t1 " +
					"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.CTACOD = ?  ", 
					new ConsultaStringExtractor(), empresa, empresa, ejerciciocontable, cuentacontable);
		} else {
			return new ConsultaString();
		} 		
		
	}
	
}
