package com.siegez.daos.implementation;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.siegez.daos.interfaces.EmpresaDao;
import com.siegez.models.Empresa;
import com.siegez.rowmapper.extractors.EmpresaExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.EmpresaRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;

@Repository
public class EmpresaDaoImp implements EmpresaDao{

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall simpleJdbcCallO;
//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcCallO = new SimpleJdbcCall(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	@Transactional
	public ResultadoSP registrarEmpresa(Empresa empresa) {
		
		int codigoempresa;
		String 	mensaje;
		
		codigoempresa = 0;
		mensaje = "No Exitoso, revisar datos";
		
		//System.out.println(out.get("idopenum"));
		//System.out.println(listaregistrocontable);

		// Validar nuevo o edición -- || OR; && AND

		if (empresa.getEmpresa() == null || empresa.getEmpresa() == 0) {
			// NUEVA EMPRESA	
			// obtener código			
			codigoempresa = jdbcTemplate.queryForObject(" SELECT isnull(max(IDEMPCOD),0) + 1 FROM T_M_EMPRESA ", Integer.class);
			
			empresa.setEmpresa(codigoempresa);
			
			jdbcTemplate.update("insert into t_m_empresa(idempcod,idliccod, empdescrip, empruc,emptipo, emplogo, empestado,"
					+ "mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate) values (?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",
					empresa.getEmpresa(),
					empresa.getLicencia(),
					empresa.getDescripcion(),
					empresa.getRuc(),
					empresa.getTipo(),
					empresa.getLogo(),
					empresa.getEstado(),
					empresa.getCreacionUsuario(),
					empresa.getModificacionUsuario());			
		}
		else
		{
			jdbcTemplate.update("update t_m_empresa set idliccod = ?, empdescrip = ?, empruc = ?, emptipo = ?, emplogo = ?, empestado = ?, "
					+ "									mgbmodiuser = ?, mgbmodidate = dateadd(hour, -5, getdate())"
					+ "									where idempcod like ?",
					empresa.getLicencia(),
					empresa.getDescripcion(),
					empresa.getRuc(),
					empresa.getTipo(),
					empresa.getLogo(),
					empresa.getEstado(),					
					empresa.getModificacionUsuario(),
					empresa.getEmpresa());
		}					
				
		mensaje = "Grabación exitosa";
		return new ResultadoSP(empresa.getEmpresa(), "", mensaje, "S");
	}
	
	@Override
	public int save(Empresa empresa) {
		int codigo=jdbcTemplate.queryForObject("select max(idempcod) from t_m_empresa", int.class)+1;
		jdbcTemplate.update("insert into t_m_empresa(idempcod,idliccod, empdescrip, empruc,emptipo, emplogo, empestado,"
				+ "mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate) values (?,?,?,?,?,?,?,?,?,?,?)",
				codigo,
				empresa.getLicencia(),
				empresa.getDescripcion(),
				empresa.getRuc(),
				empresa.getTipo(),
				empresa.getLogo(),
				empresa.getEstado(),
				empresa.getCreacionUsuario(),
				empresa.getCreacionFecha(),
				empresa.getModificacionUsuario(),
				empresa.getModificacionFecha());
		return codigo;
	}

	@Override
	public void update(Empresa empresa) {
		jdbcTemplate.update("update t_m_empresa set idliccod = ?, empdescrip = ?, empruc = ?, emptipo = ?, emplogo = ?, empestado = ?, "
				+ "									mgbcreauser = ?, mgbcreadate = ?, mgbmodiuser = ?, mgbmodidate = ?"
				+ "									where idempcod like ?",
				empresa.getLicencia(),
				empresa.getDescripcion(),
				empresa.getRuc(),
				empresa.getTipo(),
				empresa.getLogo(),
				empresa.getEstado(),
				empresa.getCreacionUsuario(),
				empresa.getCreacionFecha(),
				empresa.getModificacionUsuario(),
				empresa.getModificacionFecha(),
				empresa.getEmpresa());
		
	}

	@Override
	public Empresa find(Integer empresaCodigo) {
		return jdbcTemplate.query("select idempcod,idliccod, empdescrip, empruc,emptipo, emplogo, empestado, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate from t_m_empresa where idempcod like ?", new EmpresaExtractor(), empresaCodigo);
	}

	@Override
	public int delete(Integer empresaCodigo) {
		return jdbcTemplate.update("delete from t_m_empresa where idempcod like ?", empresaCodigo);
	}

	@Override
	public List<Empresa> findAll() {
		return jdbcTemplate.query("select idempcod,idliccod, empdescrip, empruc,emptipo, emplogo, empestado, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate from t_m_empresa", new EmpresaRowMapper());
	}

	@Override
	public List<Empresa> findAllUsuarioLicencia(String licencia) {
		return jdbcTemplate.query("select idempcod,idliccod, empdescrip, empruc,emptipo, emplogo, empestado, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate from t_m_empresa where idliccod like ? order by empdescrip", new EmpresaRowMapper(), licencia);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(String licencia){
		
		return jdbcTemplate.query(
				"select t1.idempcod as codigo " +
				", t1.empdescrip as descripcion " +
				", t2.DESCRIPCION as estado " +
				"from t_m_empresa t1 " +
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.empestado = t2.IDESTADO " +
				"where t1.IDLICCOD = ? " +
				"order by t1.empdescrip ",new CatalogoRowMapper(), licencia);
	}

	@Override
	public List<Catalogo> consultarCatalogoUsuario(String username){
		
		return jdbcTemplate.query(
				"select t1.idempcod as codigo " +
				", t1.empdescrip as descripcion " +
				", t3.DESCRIPCION as estado " +
				"from t_m_empresa t1 " +
				" left outer join (SELECT distinct IDEMPCOD, USERNAME FROM T_M_EMPRESAPERFILUSUARIO WHERE EXUESTADO = 'A') t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.empestado = t3.IDESTADO " +
				"where t2.USERNAME = ? " +
				"order by t1.empdescrip ",new CatalogoRowMapper(), username);
	}
	
	@Override
	@Transactional
	public ResultadoSP replicarEmpresa(Integer empresaorigen, Integer ejercicioorigen, Integer empresadestino, Integer ejerciciodestino)
	{
		this.simpleJdbcCallO.withSchemaName("dbo").withProcedureName("sp_mae_clonar_empresa_anno").declareParameters(
				new SqlParameter("idempcodori", Types.INTEGER),
				new SqlParameter("annoori", Types.INTEGER),
				new SqlParameter("idempcoddes", Types.INTEGER),
				new SqlParameter("annodes", Types.INTEGER),
				new SqlOutParameter("mensaje", Types.VARCHAR),
				new SqlOutParameter("indproc", Types.VARCHAR));	
				SqlParameterSource parametersO = new MapSqlParameterSource().addValue("idempcodori", empresaorigen, Types.INTEGER)
																			.addValue("annoori", ejercicioorigen, Types.INTEGER)	
																			.addValue("idempcoddes", empresadestino, Types.INTEGER)
																			.addValue("annodes", ejerciciodestino, Types.INTEGER);
				Map<String, Object> out = simpleJdbcCallO.execute(parametersO);				
		
		return new ResultadoSP(0, "", (String) out.get("mensaje"), (String) out.get("indproc"));
	}
	

}
