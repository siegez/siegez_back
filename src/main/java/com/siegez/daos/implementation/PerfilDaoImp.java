package com.siegez.daos.implementation;


import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.PerfilDao;
import com.siegez.models.Perfil;

import com.siegez.rowmapper.extractors.PerfilExtractor;
import com.siegez.rowmapper.mappers.PerfilRowMapper;


@Repository
public class PerfilDaoImp implements PerfilDao{

	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public void save(Perfil perfil) {	
		jdbcTemplate.update("insert into t_m_perfil(IDPEFCOD, IDEMPCOD, PEFDESCRIP, PEFESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?)",
				perfil.getPerfil(),
				perfil.getEmpresa(),
				perfil.getDescripcion(),
				perfil.getEstado(),
				perfil.getCreacionUsuario(),
				perfil.getCreacionFecha(),
				perfil.getModUsuario(),
				perfil.getModFecha());
	}

	@Override
	public void update(Perfil perfil) {
		jdbcTemplate.update("update t_m_perfil set IDEMPCOD = ?, PEFDESCRIP = ?, PEFESTADO = ?, MGBCREAUSER = ?, MGBCREADATE = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = ? "
				+ "	where IDPEFCOD like ?",				
				perfil.getEmpresa(),
				perfil.getDescripcion(),
				perfil.getEstado(),
				perfil.getCreacionUsuario(),
				perfil.getCreacionFecha(),
				perfil.getModUsuario(),
				perfil.getModFecha(),
				perfil.getPerfil());		
	}

	@Override
	public int delete(Integer codigo) {		
		return jdbcTemplate.update("delete from t_m_perfil where IDPEFCOD like ?", codigo);	
	}
	
	@Override
	public Perfil find(Integer codigo) {
		return jdbcTemplate.query("select t1.IDPEFCOD, t1.PEFDESCRIP, t1.IDEMPCOD, t2.EMPDESCRIP, t1.PEFESTADO, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_M_PERFIL t1 " + 
				"left outer join T_M_EMPRESA t2 " + 
				"on t1.IDEMPCOD = t2.IDEMPCOD " + 
				"where t1.IDPEFCOD = ? ", new PerfilExtractor(), codigo);		

	}
	
	@Override
	public List<Perfil> findAll(Integer empresa) {
		return jdbcTemplate.query("select t1.IDPEFCOD, t1.PEFDESCRIP, t1.IDEMPCOD, t2.EMPDESCRIP, t1.PEFESTADO, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_M_PERFIL t1 " + 
				"left outer join T_M_EMPRESA t2 " + 
				"on t1.IDEMPCOD = t2.IDEMPCOD " + 
				"where t1.IDEMPCOD = ? ", new PerfilRowMapper(), empresa);
	}

	
}
