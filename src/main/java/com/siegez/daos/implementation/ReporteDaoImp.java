package com.siegez.daos.implementation;

import java.io.File;
import java.sql.Connection;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.util.ResourceUtils;

import com.siegez.daos.interfaces.ReporteDao;
import com.siegez.reports.BalanceComprobacion;
import com.siegez.reports.CajaBanco;
import com.siegez.reports.EstadoResultado;
import com.siegez.reports.LibroDiario;
import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.reports.LibroMayor;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.reports.PLELibroDiario;
import com.siegez.reports.PLERegistroCompra;
import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.reports.PLERegistroVenta;
import com.siegez.reports.RegistroCompra;
import com.siegez.reports.RegistroVenta;
import com.siegez.reports.SaldoCuenta;
import com.siegez.reports.SituacionFinanciera;
import com.siegez.rowmapper.mappers.PLERegistroCompraRowMapper;
import com.siegez.rowmapper.mappers.BalanceComprobacionRowMapper;
import com.siegez.rowmapper.mappers.CajaBancoRowMapper;
import com.siegez.rowmapper.mappers.EstadoResultadoRowMapper;
import com.siegez.rowmapper.mappers.LibroDiarioRowMapper;
import com.siegez.rowmapper.mappers.LibroDiarioSimRowMapper;
import com.siegez.rowmapper.mappers.LibroMayorBiMonRowMapper;
import com.siegez.rowmapper.mappers.LibroMayorRowMapper;
import com.siegez.rowmapper.mappers.PLAMEPrestadorServicioRowMapper;
import com.siegez.rowmapper.mappers.PLELibroDiarioRowMapper;
import com.siegez.rowmapper.mappers.PLERegistroCompraNoDomRowMapper;
import com.siegez.rowmapper.mappers.PLERegistroVentaRowMapper;
import com.siegez.rowmapper.mappers.RegistroCompraRowMapper;
import com.siegez.rowmapper.mappers.RegistroVentaRowMapper;
import com.siegez.rowmapper.mappers.SaldoCuentaRowMapper;
import com.siegez.rowmapper.mappers.SituacionFinancieraRowMapper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Repository
public class ReporteDaoImp implements ReporteDao {
	
	
	private SimpleJdbcCall simpleJdbcCall81;
	private SimpleJdbcCall simpleJdbcCall82;
	private SimpleJdbcCall simpleJdbcCall141;
	private SimpleJdbcCall simpleJdbcCallPLAME;
	private SimpleJdbcCall simpleJdbcCall51;	
	private SimpleJdbcCall simpleJdbcCallRC;
	private SimpleJdbcCall simpleJdbcCallRV;
	private SimpleJdbcCall simpleJdbcLibroDiario;
	private SimpleJdbcCall simpleJdbcLibroDiarioSim;
	private SimpleJdbcCall simpleJdbcLibroMayor;
	private SimpleJdbcCall simpleJdbcLibroMayorBiMon;
	private SimpleJdbcCall simpleJdbcCajaBanco;
	private SimpleJdbcCall simpleJdbcBalanceComprobacion;
	private SimpleJdbcCall simpleJdbcSaldoCuenta;
	private SimpleJdbcCall simpleJdbcSituacionFinanciera;
	private SimpleJdbcCall simpleJdbcEstadoResultadoNat;
	private SimpleJdbcCall simpleJdbcEstadoResultadoFun;
	private JdbcTemplate jdbcTemplate;


	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);		
		this.simpleJdbcCall81 = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCall82 = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCall141 = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCallPLAME = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCall51 = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCallRC = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCallRV = new SimpleJdbcCall(dataSource);
		this.simpleJdbcLibroDiario = new SimpleJdbcCall(dataSource);
		this.simpleJdbcLibroDiarioSim = new SimpleJdbcCall(dataSource);
		this.simpleJdbcLibroMayor = new SimpleJdbcCall(dataSource);
		this.simpleJdbcLibroMayorBiMon = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCajaBanco = new SimpleJdbcCall(dataSource);
		this.simpleJdbcBalanceComprobacion = new SimpleJdbcCall(dataSource);
		this.simpleJdbcSaldoCuenta = new SimpleJdbcCall(dataSource);
		this.simpleJdbcSituacionFinanciera = new SimpleJdbcCall(dataSource);
		this.simpleJdbcEstadoResultadoNat = new SimpleJdbcCall(dataSource);
		this.simpleJdbcEstadoResultadoFun = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public List<PLERegistroCompra> consultarPLERegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable) {

		this.simpleJdbcCall81.withSchemaName("dbo").withProcedureName("sp_con_rep_ple8_1_registrocompras").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR))
				.returningResultSet("RESULT", new PLERegistroCompraRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcCall81.execute(parameters);
				@SuppressWarnings("unchecked")
				List<PLERegistroCompra> listPLERegistroCompra = (List<PLERegistroCompra>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listPLERegistroCompra;	
	}
	
	@Override
	public List<PLERegistroCompraNoDomiciliado> consultarPLERegistroCompraNoDom(Integer empresa, Integer ejerciciocontable, String periodocontable) {

		this.simpleJdbcCall82.withSchemaName("dbo").withProcedureName("sp_con_rep_ple8_2_registrocompras").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR))
				.returningResultSet("RESULT", new PLERegistroCompraNoDomRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcCall82.execute(parameters);
				@SuppressWarnings("unchecked")
				List<PLERegistroCompraNoDomiciliado> listPLERegistroCompraNoDom = (List<PLERegistroCompraNoDomiciliado>) out.get("RESULT");
				return listPLERegistroCompraNoDom;	
	}
	
	@Override
	public List<PLERegistroVenta> consultarPLERegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable) {

		this.simpleJdbcCall141.withSchemaName("dbo").withProcedureName("sp_con_rep_ple14_1_registroventas").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR))
				.returningResultSet("RESULT", new PLERegistroVentaRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcCall141.execute(parameters);
				@SuppressWarnings("unchecked")
				List<PLERegistroVenta> listPLERegistroVenta = (List<PLERegistroVenta>) out.get("RESULT");
				return listPLERegistroVenta;	
	}
	
	@Override
	public List<PLAMEPrestadorServicio> consultarPLAMEPreSer(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			String ruc, String nombreempresa, String moneda, String orden){
		
		this.simpleJdbcCallPLAME.withSchemaName("dbo").withProcedureName("sp_con_rep_plame_prestadorservicio").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("orden", Types.VARCHAR))
				.returningResultSet("RESULT", new PLAMEPrestadorServicioRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("orden", orden, Types.VARCHAR)
																			;
				Map<String, Object> out = simpleJdbcCallPLAME.execute(parameters);
				@SuppressWarnings("unchecked")
				List<PLAMEPrestadorServicio> listPLAMEPrestadorServicio = (List<PLAMEPrestadorServicio>) out.get("RESULT");
				return listPLAMEPrestadorServicio;
		
	}
	
	@Override
	public List<PLELibroDiario> consultarPLELibroDiario(Integer empresa, Integer ejerciciocontable, String periodocontable){
		this.simpleJdbcCall51.withSchemaName("dbo").withProcedureName("sp_con_rep_ple5_1_diario").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR))
				.returningResultSet("RESULT", new PLELibroDiarioRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcCall51.execute(parameters);
				@SuppressWarnings("unchecked")
				List<PLELibroDiario> listPLELibroDiario = (List<PLELibroDiario>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listPLELibroDiario;	
	}
	
	@Override
	public JasperPrint exportarPDFRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
											String nombreempresa, String moneda, String orden)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_RegistroCompras.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("orden", orden);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<RegistroCompra> consultarRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, 
														String ruc, String nombreempresa, String moneda, String orden) {

		this.simpleJdbcCallRC.withSchemaName("dbo").withProcedureName("sp_con_rep_registrocompras").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("orden", Types.VARCHAR))
				.returningResultSet("RESULT", new RegistroCompraRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("orden", orden, Types.VARCHAR)
																			;
				Map<String, Object> out = simpleJdbcCallRC.execute(parameters);
				@SuppressWarnings("unchecked")
				List<RegistroCompra> listRegistroCompra = (List<RegistroCompra>) out.get("RESULT");
				return listRegistroCompra;	
	}
	
	@Override
	public JasperPrint exportarPDFRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
											String nombreempresa, String moneda, String orden)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_RegistroVentas.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("orden", orden);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<RegistroVenta> consultarRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, 
														String ruc, String nombreempresa, String moneda, String orden) {

		this.simpleJdbcCallRV.withSchemaName("dbo").withProcedureName("sp_con_rep_registroventas").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("orden", Types.VARCHAR))
				.returningResultSet("RESULT", new RegistroVentaRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("orden", orden, Types.VARCHAR)
																			;
				Map<String, Object> out = simpleJdbcCallRV.execute(parameters);
				@SuppressWarnings("unchecked")
				List<RegistroVenta> listRegistroVenta = (List<RegistroVenta>) out.get("RESULT");
				return listRegistroVenta;	
	}
	
	@Override
	public JasperPrint exportarPDFDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, 
												String subini, String subfin, String ruc, String nombreempresa, 
												String moneda, String glosa)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			// File file = ResourceUtils.getFile(resourceLocation, "classpath:DHAGEZ_LibroDiario.jrxml");
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_LibroDiario.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("subini", subini);
			parameters.put("subfin", subfin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("glosa", glosa);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<LibroDiario> consultarDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String glosa){
		this.simpleJdbcLibroDiario.withSchemaName("dbo").withProcedureName("sp_con_rep_diario").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("subcodini", Types.VARCHAR),
				new SqlParameter("subcodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("glosa", Types.VARCHAR))
				.returningResultSet("RESULT", new LibroDiarioRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("subcodini", subini, Types.VARCHAR)
																			.addValue("subcodfin", subfin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("glosa", glosa, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcLibroDiario.execute(parameters);
				@SuppressWarnings("unchecked")
				List<LibroDiario> listLibroDiario = (List<LibroDiario>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listLibroDiario;	
	}
	
	@Override
	public JasperPrint exportarPDFDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
											String subfin, String ruc, String nombreempresa, String moneda, String categoria)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_LibroDiarioSimplificado.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("subini", subini);
			parameters.put("subfin", subfin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("categoria", categoria);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<LibroDiarioSimplificado> consultarDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
															String subfin, String ruc, String nombreempresa, String moneda, String categoria){
		this.simpleJdbcLibroDiarioSim.withSchemaName("dbo").withProcedureName("sp_con_rep_diario_simplificado").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("subcodini", Types.VARCHAR),
				new SqlParameter("subcodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("idcategoria", Types.VARCHAR))
				.returningResultSet("RESULT", new LibroDiarioSimRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("subcodini", subini, Types.VARCHAR)
																			.addValue("subcodfin", subfin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("idcategoria", categoria, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcLibroDiarioSim.execute(parameters);
				@SuppressWarnings("unchecked")
				List<LibroDiarioSimplificado> listLibroDiarioSimplificado = (List<LibroDiarioSimplificado>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listLibroDiarioSimplificado;	
	}
	
	@Override
	public JasperPrint exportarPDFMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
										String ctafin, String ruc, String nombreempresa, String moneda, String glosa)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_LibroMayor.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ctaini", ctaini);
			parameters.put("ctafin", ctafin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("glosa", glosa);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<LibroMayor> consultarMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
															String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		this.simpleJdbcLibroMayor.withSchemaName("dbo").withProcedureName("sp_con_rep_mayor").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ctacodini", Types.VARCHAR),
				new SqlParameter("ctacodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("glosa", Types.VARCHAR))
				.returningResultSet("RESULT", new LibroMayorRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ctacodini", ctaini, Types.VARCHAR)
																			.addValue("ctacodfin", ctafin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("glosa", glosa, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcLibroMayor.execute(parameters);
				@SuppressWarnings("unchecked")
				List<LibroMayor> listLibroMayor = (List<LibroMayor>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listLibroMayor;	
	}
	@Override
	public JasperPrint exportarPDFMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_LibroMayorBiMoneda.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ctaini", ctaini);
			parameters.put("ctafin", ctafin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("glosa", glosa);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<LibroMayorBiMoneda> consultarMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
												String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		this.simpleJdbcLibroMayorBiMon.withSchemaName("dbo").withProcedureName("sp_con_rep_mayor").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ctacodini", Types.VARCHAR),
				new SqlParameter("ctacodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("glosa", Types.VARCHAR))
				.returningResultSet("RESULT", new LibroMayorBiMonRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ctacodini", ctaini, Types.VARCHAR)
																			.addValue("ctacodfin", ctafin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("glosa", glosa, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcLibroMayorBiMon.execute(parameters);
				@SuppressWarnings("unchecked")
				List<LibroMayorBiMoneda> listLibroMayorBiMoneda = (List<LibroMayorBiMoneda>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listLibroMayorBiMoneda;	
	}
	
	@Override
	public JasperPrint exportarPDFCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_CajaBanco.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ctaini", ctaini);
			parameters.put("ctafin", ctafin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);
			parameters.put("glosa", glosa);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<CajaBanco> consultarCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
												String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		this.simpleJdbcCajaBanco.withSchemaName("dbo").withProcedureName("sp_con_rep_cajabanco").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ctacodini", Types.VARCHAR),
				new SqlParameter("ctacodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR),
				new SqlParameter("glosa", Types.VARCHAR))
				.returningResultSet("RESULT", new CajaBancoRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ctacodini", ctaini, Types.VARCHAR)
																			.addValue("ctacodfin", ctafin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR)
																			.addValue("glosa", glosa, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcCajaBanco.execute(parameters);
				@SuppressWarnings("unchecked")
				List<CajaBanco> listCajaBanco = (List<CajaBanco>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listCajaBanco;	
	}
	
	@Override
	public JasperPrint exportarPDFBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
													String ruc, String nombreempresa, String moneda)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_BalanceComprobacion.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<BalanceComprobacion> consultarBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
														String ruc, String nombreempresa, String moneda){
		this.simpleJdbcBalanceComprobacion.withSchemaName("dbo").withProcedureName("sp_con_rep_balancecomprobacion").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR))
				.returningResultSet("RESULT", new BalanceComprobacionRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcBalanceComprobacion.execute(parameters);
				@SuppressWarnings("unchecked")
				List<BalanceComprobacion> listBalanceComprobacion = (List<BalanceComprobacion>) out.get("RESULT");
				// System.out.print(listPLERegistroCompra.forEach(););
				return listBalanceComprobacion;	
	}
	
	@Override
	public JasperPrint exportarPDFSaldoCtaEntDoc(Integer empresa, Integer ejerciciocontable,
												String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_SaldoCuentaEntidadDocumento.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("tiposaldo", tiposaldo);
			parameters.put("ctaini", ctaini);
			parameters.put("ctafin", ctafin);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("fecha", fecha);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<SaldoCuenta> consultarSaldoCuenta(Integer empresa, Integer ejerciciocontable,
													String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha){
		this.simpleJdbcSaldoCuenta.withSchemaName("dbo").withProcedureName("sp_con_rep_saldo_ctaentdoc").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("tiposaldo", Types.VARCHAR),
				new SqlParameter("ctacodini", Types.VARCHAR),
				new SqlParameter("ctacodfin", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("date", Types.VARCHAR))
				.returningResultSet("RESULT", new SaldoCuentaRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)																				
																			.addValue("tiposaldo", tiposaldo, Types.VARCHAR)
																			.addValue("ctacodini", ctaini, Types.VARCHAR)
																			.addValue("ctacodfin", ctafin, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("date", fecha, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcSaldoCuenta.execute(parameters);
				@SuppressWarnings("unchecked")
				List<SaldoCuenta> listSaldoCuenta = (List<SaldoCuenta>) out.get("RESULT");
				return listSaldoCuenta;	
	}
	
	@Override
	public JasperPrint exportarPDFSitFin(Integer empresa, Integer ejerciciocontable, String periodocontable,
													String ruc, String nombreempresa, String moneda)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_EstadoSituacion.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public JasperPrint exportarPDFSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
													String ruc, String nombreempresa, String moneda)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_EstadoSituacionVertical.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			// log.error("Error");
			return null;
		}
		
	}
	
	@Override
	public List<SituacionFinanciera> consultarSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
														String ruc, String nombreempresa, String moneda){
		this.simpleJdbcSituacionFinanciera.withSchemaName("dbo").withProcedureName("sp_con_rep_estadosituacionfinanciera_ver").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR))
				.returningResultSet("RESULT", new SituacionFinancieraRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcSituacionFinanciera.execute(parameters);
				@SuppressWarnings("unchecked")
				List<SituacionFinanciera> listSituacionFinanciera = (List<SituacionFinanciera>) out.get("RESULT");
				return listSituacionFinanciera;	
	}
	
	@Override
	public JasperPrint exportarPDFEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
													String ruc, String nombreempresa, String moneda)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_EstadoResultadoNaturaleza.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			return null;
		}
		
	}
	
	@Override
	public List<EstadoResultado> consultarEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
														String ruc, String nombreempresa, String moneda){
		this.simpleJdbcEstadoResultadoNat.withSchemaName("dbo").withProcedureName("sp_con_rep_reporte_eerr_naturaleza").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR))
				.returningResultSet("RESULT", new EstadoResultadoRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcEstadoResultadoNat.execute(parameters);
				@SuppressWarnings("unchecked")
				List<EstadoResultado> listEstadoResultado = (List<EstadoResultado>) out.get("RESULT");
				return listEstadoResultado;	
	}
	
	@Override
	public JasperPrint exportarPDFEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
													String ruc, String nombreempresa, String moneda)    
	{
		try {
		
			Connection connection = jdbcTemplate.getDataSource().getConnection();
			
			File file = ResourceUtils.getFile("src/main/java/com/siegez/jasper/DHAGEZ_EstadoResultadoFuncion.jrxml");
					
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath()); 

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("empresa", empresa);
			parameters.put("annoproceso", ejerciciocontable);	
			parameters.put("periodo", periodocontable);
			parameters.put("ruc", ruc);
			parameters.put("nombreempresa", nombreempresa);
			parameters.put("moneda", moneda);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection); 
		
			return jasperPrint;
			
		} catch (Exception exception) {
			return null;
		}
		
	}
	
	@Override
	public List<EstadoResultado> consultarEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
														String ruc, String nombreempresa, String moneda){
		this.simpleJdbcEstadoResultadoFun.withSchemaName("dbo").withProcedureName("sp_con_rep_reporte_eerr_funcion").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("idejecontable", Types.INTEGER),
				new SqlParameter("idpercod", Types.VARCHAR),
				new SqlParameter("ruc", Types.VARCHAR),
				new SqlParameter("nombreempresa", Types.VARCHAR),
				new SqlParameter("moneda", Types.VARCHAR))
				.returningResultSet("RESULT", new EstadoResultadoRowMapper());
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																			.addValue("idejecontable", ejerciciocontable, Types.INTEGER)	
																			.addValue("idpercod", periodocontable, Types.VARCHAR)
																			.addValue("ruc", ruc, Types.VARCHAR)
																			.addValue("nombreempresa", nombreempresa, Types.VARCHAR)
																			.addValue("moneda", moneda, Types.VARCHAR);
				Map<String, Object> out = simpleJdbcEstadoResultadoFun.execute(parameters);
				@SuppressWarnings("unchecked")
				List<EstadoResultado> listEstadoResultado = (List<EstadoResultado>) out.get("RESULT");
				return listEstadoResultado;	
	}
}


