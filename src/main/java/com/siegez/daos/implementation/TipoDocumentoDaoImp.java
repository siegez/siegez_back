package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.TipoDocumentoDao;
import com.siegez.models.TipoDocumento;
import com.siegez.rowmapper.extractors.TipoDocumentoExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.TipoDocumentoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class TipoDocumentoDaoImp implements TipoDocumentoDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(TipoDocumento tipodocumento) {
		jdbcTemplate.update("insert into T_M_TIPODOCUMENTO (" +
				" IDEMPCOD, TDOCOD, TDODESCRIP, TDODESCABR, TDOINDCOMPRA, TDOINDHONORARIO " +
				", TDOINDVENTA, TDOINDCAJA, TDOINDEGRESO, TDOINDINGRESO, TDOINDDIARIO " +
				", TDOINDIMPORTACION, TDOINDNODOMICILIADO, TDOINDSUSTENTONODOM, TDOINDSUNAT " +
				", TDOESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()), ?,dateadd(hour, -5, getdate()))",				
				tipodocumento.getEmpresa(),
				tipodocumento.getTipodocumento(),
				tipodocumento.getDescripcion(),
				tipodocumento.getAbreviatura(),
				tipodocumento.getIndicadorcompra(),
				tipodocumento.getIndicadorhonorario(),
				tipodocumento.getIndicadorventa(),
				tipodocumento.getIndicadorcaja(),
				tipodocumento.getIndicadoregreso(),
				tipodocumento.getIndicadoringreso(),
				tipodocumento.getIndicadordiario(),
				tipodocumento.getIndicadorimportacion(),
				tipodocumento.getIndicadornodomiciliado(),
				tipodocumento.getIndicadorsustentonodom(),
				tipodocumento.getIndicadorsunat(),
				tipodocumento.getEstado(),
				tipodocumento.getCreacionUsuario(),
				tipodocumento.getModificacionUsuario()
				);
	}

	@Override
	public void editar(TipoDocumento tipodocumento) {
		jdbcTemplate.update("update T_M_TIPODOCUMENTO set " +
				"  TDODESCRIP = ?, TDODESCABR = ?, TDOINDCOMPRA = ?, TDOINDHONORARIO = ? " +
				", TDOINDVENTA = ?, TDOINDCAJA = ?, TDOINDEGRESO = ?, TDOINDINGRESO = ?, TDOINDDIARIO = ? " +
				", TDOINDIMPORTACION = ?, TDOINDNODOMICILIADO = ?, TDOINDSUSTENTONODOM = ?, TDOINDSUNAT = ? " +
				", TDOESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and TDOCOD = ? ",					
				tipodocumento.getDescripcion(),
				tipodocumento.getAbreviatura(),
				tipodocumento.getIndicadorcompra(),
				tipodocumento.getIndicadorhonorario(),
				tipodocumento.getIndicadorventa(),
				tipodocumento.getIndicadorcaja(),
				tipodocumento.getIndicadoregreso(),
				tipodocumento.getIndicadoringreso(),
				tipodocumento.getIndicadordiario(),
				tipodocumento.getIndicadorimportacion(),
				tipodocumento.getIndicadornodomiciliado(),
				tipodocumento.getIndicadorsustentonodom(),
				tipodocumento.getIndicadorsunat(),
				tipodocumento.getEstado(),
				tipodocumento.getModificacionUsuario(),
				tipodocumento.getEmpresa(),
				tipodocumento.getTipodocumento()
				);	
		
	}	
	
	@Override
	public int eliminar(TipoDocumento tipodocumento) {
		return jdbcTemplate.update("delete from T_M_TIPODOCUMENTO " 
				+ " where IDEMPCOD = ? and TDOCOD = ? ", 
				tipodocumento.getEmpresa(),
				tipodocumento.getTipodocumento());	
	}
	
	@Override
	public TipoDocumento consultar(Integer empresa, String tipodocumento) {
		
		return jdbcTemplate.query(
				"select  " +
					"   t1.IDEMPCOD, t1.TDOCOD, t1.TDODESCRIP, t1.TDODESCABR " +
					" , t1.TDOINDCOMPRA " +
					" , t1.TDOINDHONORARIO  " +
					" , t1.TDOINDVENTA  " +
					" , t1.TDOINDCAJA  " +
					" , t1.TDOINDEGRESO  " +
					" , t1.TDOINDINGRESO  " +
					" , t1.TDOINDDIARIO  " +
					" , t1.TDOINDIMPORTACION  " +
					" , t1.TDOINDNODOMICILIADO  " +
					" , t1.TDOINDSUSTENTONODOM  " +
					" , t1.TDOINDSUNAT " +
					" , t1.TDOESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_M_TIPODOCUMENTO t1 " +
				"where t1.IDEMPCOD = ?  and t1.TDOCOD = ?  ", 
				new TipoDocumentoExtractor(), empresa, tipodocumento);
	}
	
	@Override
	public List<TipoDocumento> consultarLista(Integer empresa) {
	
		return jdbcTemplate.query(
			"select  " +
				"   t1.IDEMPCOD, t1.TDOCOD, t1.TDODESCRIP, t1.TDODESCABR " +
				" , case when t1.TDOINDCOMPRA = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDHONORARIO = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDVENTA = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDCAJA = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDEGRESO = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDINGRESO = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDDIARIO = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDIMPORTACION = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDNODOMICILIADO = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDSUSTENTONODOM = 'S' then 'Si' else 'No' end " +
				" , case when t1.TDOINDSUNAT = 'S' then 'Si' else 'No' end " +
				" , t1.TDOESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_TIPODOCUMENTO t1 " +				
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.TDOESTADO = t2.IDESTADO " +				
			"where t1.IDEMPCOD = ?  ", 
				new TipoDocumentoRowMapper(), new Object[] {empresa});
		
	}	
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, String tipodocumento) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_M_TIPODOCUMENTO " + 
				"where IDEMPCOD = ?  and TDOCOD = ? ",
					new ValorContadorExtractor(), empresa, tipodocumento);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ?  " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}

	@Override
	public List<Catalogo> consultarCatalogoCompras(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDCOMPRA = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoVentas(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDVENTA = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoHonorarios(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDHONORARIO = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoNoDomiciliados(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDNODOMICILIADO = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoNoDomSustentos(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDSUSTENTONODOM = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoImportaciones(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDIMPORTACION = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoEgresos(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDEGRESO	 = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoIngresos(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDINGRESO = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoDiarios(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.TDOCOD as codigo, t1.TDODESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_M_TIPODOCUMENTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.TDOESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.TDOINDDIARIO = 'S' " +
				"order by t1.TDOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	
}
