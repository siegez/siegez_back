package com.siegez.daos.implementation;


import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.TablaDetalleDao;
import com.siegez.models.TablaDetalle;
import com.siegez.rowmapper.extractors.TablaDetalleExtractor;
import com.siegez.rowmapper.mappers.TablaDetalleRowMapper;

@Repository
public class TablaDetalleDaoImp implements TablaDetalleDao{

	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}

	@Override
	public void insertar(TablaDetalle tabladetalle) {	
		jdbcTemplate.update("insert into T_M_TABLADETALLE (IDEMPCOD,IDTABCOD,DETVALOR,DETDESCRIP,DETABREVIA,DETREFCOD,DETESTADO,MGBCREAUSER,MGBCREADATE,MGBMODIUSER,MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				tabladetalle.getEmpresa(),
				tabladetalle.getTabla(),
				tabladetalle.getValor(),
				tabladetalle.getDescripcion(),
				tabladetalle.getAbreviatura(),
				tabladetalle.getCodigoreferencia(),
				tabladetalle.getEstado(),
				tabladetalle.getCreacionUsuario(),
				tabladetalle.getModUsuario());
		
	}

	@Override
	public void editar(TablaDetalle tabladetalle) {
		jdbcTemplate.update("update T_M_TABLADETALLE set DETDESCRIP = ?, DETABREVIA = ?, DETREFCOD = ?, DETESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDTABCOD = ? and DETVALOR = ? ",						
				tabladetalle.getDescripcion(),
				tabladetalle.getAbreviatura(),
				tabladetalle.getCodigoreferencia(),
				tabladetalle.getEstado(),
				tabladetalle.getModUsuario(),
				tabladetalle.getEmpresa(),
				tabladetalle.getTabla(),
				tabladetalle.getValor());		
	}

	@Override
	public int eliminar(TablaDetalle tabladetalle) {		
		return jdbcTemplate.update("delete from T_M_TABLADETALLE " 
				+ " where IDEMPCOD = ? and IDTABCOD = ? and DETVALOR = ? ", 
				tabladetalle.getEmpresa(),
				tabladetalle.getTabla(),
				tabladetalle.getValor());	
	}
	
	@Override
	public TablaDetalle consultar(TablaDetalle tabladetalle) {
		return jdbcTemplate.query("select IDEMPCOD,IDTABCOD,DETVALOR,DETDESCRIP,DETABREVIA,DETREFCOD, DETESTADO,MGBCREAUSER,MGBCREADATE,MGBMODIUSER,MGBMODIDATE " + 
				"				from T_M_TABLADETALLE where IDEMPCOD = ? and IDTABCOD = ? and DETVALOR = ? ", 
				new TablaDetalleExtractor(), 
				new Object[] {tabladetalle.getEmpresa(), tabladetalle.getTabla(), tabladetalle.getValor()});
	}
	
	@Override
	public List<TablaDetalle> consultarListaDescripcion(Integer empresa, Integer tabla) {
		return jdbcTemplate.query("select IDEMPCOD,IDTABCOD,DETVALOR,DETDESCRIP,DETABREVIA,DETREFCOD,DETESTADO,MGBCREAUSER,MGBCREADATE,MGBMODIUSER,MGBMODIDATE " + 
				"				from T_M_TABLADETALLE where IDEMPCOD = ? and IDTABCOD = ? order by DETDESCRIP ",
				new TablaDetalleRowMapper(), new Object[] {empresa, tabla});
	}
	
	@Override
	public List<TablaDetalle> consultarListaCodigo(Integer empresa, Integer tabla) {
		return jdbcTemplate.query("select IDEMPCOD,IDTABCOD,DETVALOR,DETDESCRIP,DETABREVIA,DETREFCOD,DETESTADO,MGBCREAUSER,MGBCREADATE,MGBMODIUSER,MGBMODIDATE " + 
				"				from T_M_TABLADETALLE where IDEMPCOD = ? and IDTABCOD = ? order by DETVALOR ", 
				new TablaDetalleRowMapper(), new Object[] {empresa, tabla});
	}
	
}
