package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.LicenciaDao;
import com.siegez.models.Licencia;
import com.siegez.rowmapper.extractors.LicenciaExtractor;
import com.siegez.rowmapper.mappers.LicenciaRowMapper;

@Repository
public class LicenciaDaoImp implements LicenciaDao{

	private JdbcTemplate jdbcTemplate;
//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public Licencia find(String licenciaCodigo) {
		return jdbcTemplate.query("select idliccod, licdescrip, LICCANEMPRESAS, liccanusuarios, LICESTADO, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate from t_m_licencia where idliccod like ?", new LicenciaExtractor(), licenciaCodigo);
	}

	@Override
	public List<Licencia> findAll() {
		return jdbcTemplate.query("select idliccod, licdescrip, LICCANEMPRESAS, liccanusuarios, LICESTADO, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate from t_m_licencia", new LicenciaRowMapper());
	}

}
