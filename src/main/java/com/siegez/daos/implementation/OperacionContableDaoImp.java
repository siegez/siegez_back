package com.siegez.daos.implementation;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.siegez.daos.interfaces.OperacionContableDao;
import com.siegez.models.OperacionContable;
import com.siegez.models.OperacionRegistroContable;
import com.siegez.models.RegistroContable;
import com.siegez.rowmapper.extractors.OperacionContableExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.ConsultaStringRowMapper;
import com.siegez.rowmapper.mappers.OperacionContableRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;

@Repository
public class OperacionContableDaoImp implements OperacionContableDao {

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall simpleJdbcCallO;
	private SimpleJdbcCall simpleJdbcCallA;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.simpleJdbcCallO = new SimpleJdbcCall(dataSource);
		this.simpleJdbcCallA = new SimpleJdbcCall(dataSource);

	}
	
	@Override
	@Transactional
	public OperacionContable registrarOperacion(OperacionRegistroContable operacionregistrocontable) {
		
		String 	numerooperacioncontable;
		int		numeroasientocontable;
		String	tipoasiento;
		Boolean exitoso = false;
		
		// Transacción
		
		// Asignar lista de registros contables
		List<RegistroContable> listaregistrocontable = (List<RegistroContable>) operacionregistrocontable.getRegistrocontable();
		
		// Validar nuevo o edición -- || OR; && AND

		if (operacionregistrocontable.getOperacioncontable() != null &&  operacionregistrocontable.getOperacioncontable() == "") {
			// NUEVA OPERACIÓN CONTABLE			
			// obtener número operación			
			this.simpleJdbcCallO.withSchemaName("dbo").withProcedureName("sp_sha_getnumerooperacion").declareParameters(
					new SqlParameter("idempcod", Types.INTEGER),
					new SqlParameter("idejecontable", Types.INTEGER),
					new SqlParameter("subcod", Types.VARCHAR),
					new SqlParameter("idpercod", Types.VARCHAR),
					new SqlOutParameter("idopenum", Types.VARCHAR));	
					SqlParameterSource parametersO = new MapSqlParameterSource().addValue("idempcod", operacionregistrocontable.getEmpresa(), Types.INTEGER)
																				.addValue("idejecontable", operacionregistrocontable.getEjerciciocontable(), Types.INTEGER)	
																				.addValue("subcod", operacionregistrocontable.getSubdiario(), Types.VARCHAR)
																				.addValue("idpercod", operacionregistrocontable.getPeriodocontable(), Types.VARCHAR);
					Map<String, Object> out = simpleJdbcCallO.execute(parametersO);				
			
			// asignar número operación a operacionregistrocontable
			numerooperacioncontable = (String) out.get("idopenum");
			operacionregistrocontable.setOperacioncontable(numerooperacioncontable);

			// insertar operación
			jdbcTemplate.update("insert into T_CON_OPERACION (" +
					" IDEMPCOD, IDEJECONTABLE, IDOPENUM, IDPERCOD, IDTIPOOPERACION " +
					",OPEFECHAOPERACION, OPEFECHAEMISION, OPEFECHAVENCIMIENTO, ENTCOD, ENTRUC " +
					",ENTCODBCO, CBACOD, TDOCOD, SUBCOD, IDADUANA " +
					",OPESERCOMPROBANTE, OPENUMCOMPROBANTE, OPENUMCOMPROBANTEFIN, IDMONEDA, IDTIPOCAMBIO " +
					",OPETIPOCAMBIOSOL, OPETIPOCAMBIODOL, OPEBASEIMPONIBLE, OPEINAFECTO, OPEADICIONALNODOM " +
					",OPEINDIMPUESTO, OPEPORCENTAJEIGV, OPEIMPUESTO, OPEIMPUESTOVAR, OPEDEDCOSTONODOM, OPEPRECIOORI " +
					",OPEPRECIOSOL, OPEPRECIODOL, IDMEDIOPAGO, IDFLUJOEFECTIVO, IDBIENSERVICIO, DETCOD " +
					",OPETASADETRACCION, OPENUMDETRACCION, OPEFECHADETRACCION, OPEBASEDETRACCION, OPEDETRACCION " +
					",TDOCODREF, IDEJECONTABLEREF, IDOPENUMREF, OPEGIRADO, OPEGLOSA " +
					",TDOCODNODOM, OPESERCOMPROBANTENODOM, OPEANOCOMPROBANTENODOM, OPENUMCOMPROBANTENODOM, ENTCODNODOM, ENTRUCNODOM " +
					",IDVINCULOECONODOM, OPETASARETENCIONNODOM, OPEIMPUESTORETNODOM, IDCONVENIOTRIBNODOM, IDEXONERACIONNODOM " +
					",IDTIPORENTANODOM, IDMODALIDADSERVNODOM, OPEINDARTICULONODOM, OPEREGCOMREF, OPEREGCOMIGV " +
					",OPEESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
					" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
					" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
					" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
					" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					operacionregistrocontable.getEmpresa(),
					operacionregistrocontable.getEjerciciocontable(),
					operacionregistrocontable.getOperacioncontable(),
					operacionregistrocontable.getPeriodocontable(),
					operacionregistrocontable.getTipooperacion(),
					operacionregistrocontable.getFechaoperacion(),
					operacionregistrocontable.getFechaemision(),
					operacionregistrocontable.getFechavencimiento(),
					operacionregistrocontable.getEntidad(),
					operacionregistrocontable.getRuc(),
					operacionregistrocontable.getBanco(),
					operacionregistrocontable.getCuentabancaria(),
					operacionregistrocontable.getTipodocumento(),
					operacionregistrocontable.getSubdiario(),
					operacionregistrocontable.getAduana(),
					operacionregistrocontable.getNumeroserie(),
					operacionregistrocontable.getNumerocomprobanteini(),
					operacionregistrocontable.getNumerocomprobantefin(),
					operacionregistrocontable.getMoneda(),
					operacionregistrocontable.getTipocambio(),
					operacionregistrocontable.getTipocambiosoles(),
					operacionregistrocontable.getTipocambiodolares(),
					operacionregistrocontable.getBaseimponible(),
					operacionregistrocontable.getInafecto(),
					operacionregistrocontable.getAdicionalnodom(),
					operacionregistrocontable.getIndimpuestorenta(),
					operacionregistrocontable.getPorcentajeimpuesto(),
					operacionregistrocontable.getImpuesto(),
					operacionregistrocontable.getImpuestovar(),
					operacionregistrocontable.getDeduccionnodom(),
					operacionregistrocontable.getPrecioorigen(),
					operacionregistrocontable.getPreciosoles(),
					operacionregistrocontable.getPreciodolares(),
					operacionregistrocontable.getMediopago(),
					operacionregistrocontable.getFlujoefectivo(),
					operacionregistrocontable.getBienservicio(),
					operacionregistrocontable.getDetraccion(),
					operacionregistrocontable.getTasadetraccion(),
					operacionregistrocontable.getNumerodetraccion(),
					operacionregistrocontable.getFechadetraccion(),
					operacionregistrocontable.getBasedetraccion(),
					operacionregistrocontable.getMontodetraccion(),
					operacionregistrocontable.getTipodocumentoref(),
					operacionregistrocontable.getEjerciciocontableref(),
					operacionregistrocontable.getOperacioncontableref(),
					operacionregistrocontable.getGirado(),
					operacionregistrocontable.getGlosa(),
					operacionregistrocontable.getTipodocumentonodom(),
					operacionregistrocontable.getNumeroserienodom(),
					operacionregistrocontable.getAnnocomprobantenodom(),
					operacionregistrocontable.getNumerocomprobantenodom(),
					operacionregistrocontable.getEntidadnodom(),
					operacionregistrocontable.getRucnodom(),
					operacionregistrocontable.getVinculoeconomiconodom(),
					operacionregistrocontable.getTasaretencionnodom(),
					operacionregistrocontable.getImpuestoretenidonodom(),
					operacionregistrocontable.getConvenionodom(),
					operacionregistrocontable.getExoneracionnodom(),
					operacionregistrocontable.getTiporentanodom(),
					operacionregistrocontable.getModalidadnodom(),
					operacionregistrocontable.getArticulonodom(),
					operacionregistrocontable.getColumnabaseimponible(),
					operacionregistrocontable.getColumnaimpuesto(),
					operacionregistrocontable.getEstado(),
					operacionregistrocontable.getCreacionUsuario(),
					operacionregistrocontable.getModificacionUsuario()
					);			
				
			// nuevo asiento
			// insertar asiento
			switch (operacionregistrocontable.getTipooperacion()) {
			  case "A":
				tipoasiento = "D";
				break;
			  case "C":
				tipoasiento = "O";
				break;
			  case "D":
				tipoasiento = "O";
				break;
			  case "E":
				tipoasiento = "C";
				break;
			  case "H":
				tipoasiento = "O";
				break;
			  case "I":
				tipoasiento = "C";
				break;
			  case "V":
				tipoasiento = "O";
				break;
			  default:
				tipoasiento = "X";
				break;
			}
			
			this.simpleJdbcCallA.withSchemaName("dbo").withProcedureName("sp_con_insertasientocontable").declareParameters(
					new SqlParameter("idempcod", Types.INTEGER),
					new SqlParameter("idejecontable", Types.INTEGER),
					new SqlParameter("idopenum", Types.VARCHAR),
					new SqlParameter("idtipoasiento", Types.VARCHAR),
					new SqlParameter("estado", Types.VARCHAR),
					new SqlParameter("username", Types.VARCHAR),
					new SqlOutParameter("idasinum", Types.INTEGER),
					new SqlOutParameter("mensaje", Types.VARCHAR),
					new SqlOutParameter("indexito", Types.VARCHAR));
					SqlParameterSource parametersAI = new MapSqlParameterSource().addValue("idempcod", operacionregistrocontable.getEmpresa(), Types.INTEGER)
															.addValue("idejecontable", operacionregistrocontable.getEjerciciocontable(), Types.INTEGER)	
															.addValue("idopenum", operacionregistrocontable.getOperacioncontable(), Types.VARCHAR)
															.addValue("idtipoasiento", tipoasiento , Types.VARCHAR)
															.addValue("estado", 'A' , Types.VARCHAR)
															.addValue("username", operacionregistrocontable.getCreacionUsuario() , Types.VARCHAR);
					out = simpleJdbcCallA.execute(parametersAI);
			
			numeroasientocontable = (Integer) out.get("idasinum");
				
			// nuevos registros
			// insertar registros	
			/*
			for(int i=0; i<listaregistrocontable.size(); i++){
				
				RegistroContable registrocontable = listaregistrocontable.get(i);
				
				registrocontable.setAsientocontable(numeroasientocontable);	

				jdbcTemplate.update("insert into T_CON_REGISTROCONTABLE (" +
						" IDEMPCOD, IDEJECONTABLE, IDASINUM, REGVEZ, " + 
						" CTACOD, ENTCOD, ENTRUC, IDTIPOANOTACION, IDTIPOREGISTRO, " + 
						" IDMONEDA, IDTIPOCAMBIO, REGTIPOCAMBIOSOL, REGTIPOCAMBIODOL, " + 
						" REGMTOORIGEN, REGMTONACIONAL, REGMTODOLARES, TDOCOD, " + 
						" REGSERCOMPROBANTE, REGNUMCOMPROBANTE, REGNUMCOMPROBANTEFIN, " + 
						" REGFECHAEMISION, REGFECHAVENCIMIENTO, IDMEDIOPAGO, IDFLUJOEFECTIVO, REGINDCENTROCOSTO, IDCENTROCOSTO, " + 
						" REGPORCENTROCOSTO, REGVEZORIGEN, REGGLOSA, REGINDINCLUIRREGCOM, " + 
						" REGINDCALCULO, REGREGCOMREF, REGREGCOMIGV, " +			
						" REGESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
						" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
						"         convert(date, ?, 103), convert(date, ?, 103), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
						"         ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
						registrocontable.getEmpresa(),
						registrocontable.getEjerciciocontable(),
						registrocontable.getAsientocontable(),		
						registrocontable.getRegistrocontable(),
						registrocontable.getCuentacontable(),
						registrocontable.getEntidad(),
						registrocontable.getRuc(),
						registrocontable.getTipoanotacion(),
						registrocontable.getTiporegistro(),
						registrocontable.getMoneda(),
						registrocontable.getTipocambio(),
						registrocontable.getTipocambiosoles(),
						registrocontable.getTipocambiodolares(),
						registrocontable.getMontoorigen(),
						registrocontable.getMontonacional(),
						registrocontable.getMontodolares(),
						registrocontable.getTipodocumento(),
						registrocontable.getNumeroserie(),
						registrocontable.getNumerocomprobanteini(),
						registrocontable.getNumerocomprobantefin(),
						registrocontable.getFechaemision(),
						registrocontable.getFechavencimiento(),
						registrocontable.getMediopago(),
						registrocontable.getFlujoefectivo(),
						registrocontable.getTablacentrocosto(),
						registrocontable.getCentrocosto(),
						registrocontable.getPorcentajecentrocosto(),	
						registrocontable.getCorrelativoorigen(),
						registrocontable.getGlosa(),
						registrocontable.getIncluirregistrocompra(),
						registrocontable.getIndicadorcalculo(),
						registrocontable.getColumnamonto(),
						registrocontable.getColumnaimpuesto(),
						registrocontable.getEstado(),
						registrocontable.getCreacionUsuario(),
						registrocontable.getModificacionUsuario()
						);          				
            }
            */
			
			exitoso = true;
				
		} else {
			
			// EDICIÓN OPERACIÓN CONTABLE			
			// actualizar operación
			jdbcTemplate.update("update T_CON_OPERACION set " +
					" IDPERCOD = ?, IDTIPOOPERACION = ? " +
					",OPEFECHAOPERACION = ?, OPEFECHAEMISION = ?, OPEFECHAVENCIMIENTO = ?, ENTCOD = ?, ENTRUC = ? " +
					",ENTCODBCO = ?, CBACOD = ?, TDOCOD = ?, SUBCOD = ?, IDADUANA = ? " +
					",OPESERCOMPROBANTE = ?, OPENUMCOMPROBANTE = ?, OPENUMCOMPROBANTEFIN = ?, IDMONEDA = ?, IDTIPOCAMBIO = ? " +
					",OPETIPOCAMBIOSOL = ?, OPETIPOCAMBIODOL = ?, OPEBASEIMPONIBLE = ?, OPEINAFECTO = ?, OPEADICIONALNODOM = ? " +
					",OPEINDIMPUESTO = ?, OPEPORCENTAJEIGV = ?, OPEIMPUESTO = ?, OPEIMPUESTOVAR = ?, OPEDEDCOSTONODOM = ?, OPEPRECIOORI = ? " +
					",OPEPRECIOSOL = ?, OPEPRECIODOL = ?, IDMEDIOPAGO = ?, IDFLUJOEFECTIVO = ?, IDBIENSERVICIO = ?, DETCOD = ? " +
					",OPETASADETRACCION = ?, OPENUMDETRACCION = ?, OPEFECHADETRACCION = ?, OPEBASEDETRACCION = ?, OPEDETRACCION = ? " +
					",TDOCODREF = ?, IDEJECONTABLEREF = ?, IDOPENUMREF = ?, OPEGIRADO = ?, OPEGLOSA = ? " +
					",TDOCODNODOM = ?, OPESERCOMPROBANTENODOM = ?, OPEANOCOMPROBANTENODOM = ?, OPENUMCOMPROBANTENODOM = ? " +
					",ENTCODNODOM = ?, ENTRUCNODOM = ? " +
					",IDVINCULOECONODOM = ?, OPETASARETENCIONNODOM = ?, OPEIMPUESTORETNODOM = ?, IDCONVENIOTRIBNODOM = ?, IDEXONERACIONNODOM = ? " +
					",IDTIPORENTANODOM = ?, IDMODALIDADSERVNODOM = ?, OPEINDARTICULONODOM = ?, OPEREGCOMREF = ?, OPEREGCOMIGV = ? " +
					",OPEESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
					"	where IDEMPCOD = ? and IDEJECONTABLE = ? and IDOPENUM = ?  ",					
					operacionregistrocontable.getPeriodocontable(),
					operacionregistrocontable.getTipooperacion(),
					operacionregistrocontable.getFechaoperacion(),
					operacionregistrocontable.getFechaemision(),
					operacionregistrocontable.getFechavencimiento(),
					operacionregistrocontable.getEntidad(),
					operacionregistrocontable.getRuc(),
					operacionregistrocontable.getBanco(),
					operacionregistrocontable.getCuentabancaria(),
					operacionregistrocontable.getTipodocumento(),
					operacionregistrocontable.getSubdiario(),
					operacionregistrocontable.getAduana(),
					operacionregistrocontable.getNumeroserie(),
					operacionregistrocontable.getNumerocomprobanteini(),
					operacionregistrocontable.getNumerocomprobantefin(),
					operacionregistrocontable.getMoneda(),
					operacionregistrocontable.getTipocambio(),
					operacionregistrocontable.getTipocambiosoles(),
					operacionregistrocontable.getTipocambiodolares(),
					operacionregistrocontable.getBaseimponible(),
					operacionregistrocontable.getInafecto(),
					operacionregistrocontable.getAdicionalnodom(),
					operacionregistrocontable.getIndimpuestorenta(),
					operacionregistrocontable.getPorcentajeimpuesto(),
					operacionregistrocontable.getImpuesto(),
					operacionregistrocontable.getImpuestovar(),
					operacionregistrocontable.getDeduccionnodom(),
					operacionregistrocontable.getPrecioorigen(),
					operacionregistrocontable.getPreciosoles(),
					operacionregistrocontable.getPreciodolares(),
					operacionregistrocontable.getMediopago(),
					operacionregistrocontable.getFlujoefectivo(),
					operacionregistrocontable.getBienservicio(),
					operacionregistrocontable.getDetraccion(),
					operacionregistrocontable.getTasadetraccion(),
					operacionregistrocontable.getNumerodetraccion(),
					operacionregistrocontable.getFechadetraccion(),
					operacionregistrocontable.getBasedetraccion(),
					operacionregistrocontable.getMontodetraccion(),
					operacionregistrocontable.getTipodocumentoref(),
					operacionregistrocontable.getEjerciciocontableref(),
					operacionregistrocontable.getOperacioncontableref(),
					operacionregistrocontable.getGirado(),
					operacionregistrocontable.getGlosa(),
					operacionregistrocontable.getTipodocumentonodom(),
					operacionregistrocontable.getNumeroserienodom(),
					operacionregistrocontable.getAnnocomprobantenodom(),
					operacionregistrocontable.getNumerocomprobantenodom(),
					operacionregistrocontable.getEntidadnodom(),
					operacionregistrocontable.getRucnodom(),
					operacionregistrocontable.getVinculoeconomiconodom(),
					operacionregistrocontable.getTasaretencionnodom(),
					operacionregistrocontable.getImpuestoretenidonodom(),
					operacionregistrocontable.getConvenionodom(),
					operacionregistrocontable.getExoneracionnodom(),
					operacionregistrocontable.getTiporentanodom(),
					operacionregistrocontable.getModalidadnodom(),
					operacionregistrocontable.getArticulonodom(),
					operacionregistrocontable.getColumnabaseimponible(),
					operacionregistrocontable.getColumnaimpuesto(),
					operacionregistrocontable.getEstado(),
					operacionregistrocontable.getModificacionUsuario(),
					operacionregistrocontable.getEmpresa(),
					operacionregistrocontable.getEjerciciocontable(),
					operacionregistrocontable.getOperacioncontable()
					);		
		
			// obtener número asiento
			
			numeroasientocontable = jdbcTemplate.queryForObject(" SELECT ISNULL(IDASINUM,0) FROM T_CON_ASIENTOCONTABLE " + 
															 " WHERE IDEMPCOD = ? and IDEJECONTABLE = ? and IDOPENUM = ? ", 
															 new Object[]{operacionregistrocontable.getEmpresa(), operacionregistrocontable.getEjerciciocontable(), operacionregistrocontable.getOperacioncontable()}, Integer.class); 
			if (numeroasientocontable == 0)	{
				
				// actualizar asiento
				switch (operacionregistrocontable.getTipooperacion()) {
				  case "A":
					tipoasiento = "D";
					break;
				  case "C":
					tipoasiento = "O";
					break;
				  case "D":
					tipoasiento = "O";
					break;
				  case "E":
					tipoasiento = "C";
					break;
				  case "H":
					tipoasiento = "O";
					break;
				  case "I":
					tipoasiento = "C";
					break;
				  case "V":
					tipoasiento = "O";
					break;
				  default:
					tipoasiento = "X";
					break;
				}
				
				this.simpleJdbcCallA.withSchemaName("dbo").withProcedureName("sp_con_insertasientocontable").declareParameters(
						new SqlParameter("idempcod", Types.INTEGER),
						new SqlParameter("idejecontable", Types.INTEGER),
						new SqlParameter("idopenum", Types.VARCHAR),
						new SqlParameter("idtipoasiento", Types.VARCHAR),
						new SqlParameter("estado", Types.VARCHAR),
						new SqlParameter("username", Types.VARCHAR),
						new SqlOutParameter("idasinum", Types.INTEGER),
						new SqlOutParameter("mensaje", Types.VARCHAR),
						new SqlOutParameter("indexito", Types.VARCHAR));
						SqlParameterSource parametersAU = new MapSqlParameterSource().addValue("idempcod", operacionregistrocontable.getEmpresa(), Types.INTEGER)
																.addValue("idejecontable", operacionregistrocontable.getEjerciciocontable(), Types.INTEGER)	
																.addValue("idopenum", operacionregistrocontable.getOperacioncontable(), Types.VARCHAR)
																.addValue("idtipoasiento", tipoasiento , Types.VARCHAR)
																.addValue("estado", 'A' , Types.VARCHAR)
																.addValue("username", operacionregistrocontable.getCreacionUsuario() , Types.VARCHAR);
						Map<String, Object> out = simpleJdbcCallA.execute(parametersAU);
				
				numeroasientocontable = (Integer) out.get("idasinum");
			}
			else {
				jdbcTemplate.update("update T_CON_ASIENTOCONTABLE set " +	
						" ASIESTADO = 'A', MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
						"  where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? ",					
						operacionregistrocontable.getModificacionUsuario(),
						operacionregistrocontable.getEmpresa(),
						operacionregistrocontable.getEjerciciocontable(),
						numeroasientocontable
						);
			}
				
			// eliminar registros 
			jdbcTemplate.update("delete from T_CON_REGISTROCONTABLE  " +	
					"  where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? ",					
					operacionregistrocontable.getEmpresa(),
					operacionregistrocontable.getEjerciciocontable(),
					numeroasientocontable
					);
			// nuevos registros
			// insertar registros				
			for(int i=0; i<listaregistrocontable.size(); i++){
				
				RegistroContable registrocontable = listaregistrocontable.get(i);
				
				registrocontable.setAsientocontable(numeroasientocontable);	

				jdbcTemplate.update("insert into T_CON_REGISTROCONTABLE (" +
						" IDEMPCOD, IDEJECONTABLE, IDASINUM, REGVEZ, " + 
						" CTACOD, ENTCOD, ENTRUC, IDTIPOANOTACION, IDTIPOREGISTRO, " + 
						" IDMONEDA, IDTIPOCAMBIO, REGTIPOCAMBIOSOL, REGTIPOCAMBIODOL, " + 
						" REGMTOORIGEN, REGMTONACIONAL, REGMTODOLARES, TDOCOD, " + 
						" REGSERCOMPROBANTE, REGNUMCOMPROBANTE, REGNUMCOMPROBANTEFIN, " + 
						" REGFECHAEMISION, REGFECHAVENCIMIENTO, IDMEDIOPAGO, IDFLUJOEFECTIVO, REGINDCENTROCOSTO, IDCENTROCOSTO, " + 
						" REGPORCENTROCOSTO, REGVEZORIGEN, REGGLOSA, REGINDINCLUIRREGCOM, " + 
						" REGINDCALCULO, REGREGCOMREF, REGREGCOMIGV, " +			
						" REGESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
						" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " +
						"         convert(date, ?, 103), convert(date, ?, 103), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
						"         ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
						registrocontable.getEmpresa(),
						registrocontable.getEjerciciocontable(),
						registrocontable.getAsientocontable(),		
						registrocontable.getRegistrocontable(),
						registrocontable.getCuentacontable(),
						registrocontable.getEntidad(),
						registrocontable.getRuc(),
						registrocontable.getTipoanotacion(),
						registrocontable.getTiporegistro(),
						registrocontable.getMoneda(),
						registrocontable.getTipocambio(),
						registrocontable.getTipocambiosoles(),
						registrocontable.getTipocambiodolares(),
						registrocontable.getMontoorigen(),
						registrocontable.getMontonacional(),
						registrocontable.getMontodolares(),
						registrocontable.getTipodocumento(),
						registrocontable.getNumeroserie(),
						registrocontable.getNumerocomprobanteini(),
						registrocontable.getNumerocomprobantefin(),
						registrocontable.getFechaemision(),
						registrocontable.getFechavencimiento(),
						registrocontable.getMediopago(),
						registrocontable.getFlujoefectivo(),
						registrocontable.getTablacentrocosto(),
						registrocontable.getCentrocosto(),
						registrocontable.getPorcentajecentrocosto(),	
						registrocontable.getCorrelativoorigen(),
						registrocontable.getGlosa(),
						registrocontable.getIncluirregistrocompra(),
						registrocontable.getIndicadorcalculo(),
						registrocontable.getColumnamonto(),
						registrocontable.getColumnaimpuesto(),
						registrocontable.getEstado(),
						operacionregistrocontable.getModificacionUsuario(),
						operacionregistrocontable.getModificacionUsuario()
						);          				
            }					
			
			exitoso = true;
			
		} 			
		
		// Obtener operación contable de retorno
		if (exitoso == true)
		{
			OperacionContable operacioncontable = consultar(operacionregistrocontable.getEmpresa(), operacionregistrocontable.getEjerciciocontable(), operacionregistrocontable.getOperacioncontable());
			return operacioncontable;
		}
		else
		{
			return new OperacionContable();
		}		
		//System.out.println(out.get("idopenum"));
		//System.out.println(listaregistrocontable);
	}
	
	@Override
	public void insertar(OperacionContable operacioncontable) {
		jdbcTemplate.update("insert into T_CON_OPERACION (" +
				" IDEMPCOD, IDEJECONTABLE, IDOPENUM, IDPERCOD, IDTIPOOPERACION " +
				",OPEFECHAOPERACION, OPEFECHAEMISION, OPEFECHAVENCIMIENTO, ENTCOD, ENTRUC " +
				",ENTCODBCO, CBACOD, TDOCOD, SUBCOD, IDADUANA " +
				",OPESERCOMPROBANTE, OPENUMCOMPROBANTE, OPENUMCOMPROBANTEFIN, IDMONEDA, IDTIPOCAMBIO " +
				",OPETIPOCAMBIOSOL, OPETIPOCAMBIODOL, OPEBASEIMPONIBLE, OPEINAFECTO, OPEADICIONALNODOM " +
				",OPEINDIMPUESTO, OPEPORCENTAJEIGV, OPEIMPUESTO, OPEIMPUESTOVAR, OPEDEDCOSTONODOM, OPEPRECIOORI " +
				",OPEPRECIOSOL, OPEPRECIODOL, IDMEDIOPAGO, IDFLUJOEFECTIVO, IDBIENSERVICIO, DETCOD " +
				",OPETASADETRACCION, OPENUMDETRACCION, OPEFECHADETRACCION, OPEBASEDETRACCION, OPEDETRACCION " +
				",TDOCODREF, IDEJECONTABLEREF, IDOPENUMREF, OPEGIRADO, OPEGLOSA " +
				",TDOCODNODOM, OPESERCOMPROBANTENODOM, OPEANOCOMPROBANTENODOM, OPENUMCOMPROBANTENODOM, ENTCODNODOM, ENTRUCNODOM " +
				",IDVINCULOECONODOM, OPETASARETENCIONNODOM, OPEIMPUESTORETNODOM, IDCONVENIOTRIBNODOM, IDEXONERACIONNODOM " +
				",IDTIPORENTANODOM, IDMODALIDADSERVNODOM, OPEINDARTICULONODOM, OPEREGCOMREF, OPEREGCOMIGV " +
				",OPEESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
				" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				operacioncontable.getEmpresa(),
				operacioncontable.getEjerciciocontable(),
				operacioncontable.getOperacioncontable(),
				operacioncontable.getPeriodocontable(),
				operacioncontable.getTipooperacion(),
				operacioncontable.getFechaoperacion(),
				operacioncontable.getFechaemision(),
				operacioncontable.getFechavencimiento(),
				operacioncontable.getEntidad(),
				operacioncontable.getRuc(),
				operacioncontable.getBanco(),
				operacioncontable.getCuentabancaria(),
				operacioncontable.getTipodocumento(),
				operacioncontable.getSubdiario(),
				operacioncontable.getAduana(),
				operacioncontable.getNumeroserie(),
				operacioncontable.getNumerocomprobanteini(),
				operacioncontable.getNumerocomprobantefin(),
				operacioncontable.getMoneda(),
				operacioncontable.getTipocambio(),
				operacioncontable.getTipocambiosoles(),
				operacioncontable.getTipocambiodolares(),
				operacioncontable.getBaseimponible(),
				operacioncontable.getInafecto(),
				operacioncontable.getAdicionalnodom(),
				operacioncontable.getIndimpuestorenta(),
				operacioncontable.getPorcentajeimpuesto(),
				operacioncontable.getImpuesto(),
				operacioncontable.getImpuestovar(),
				operacioncontable.getDeduccionnodom(),
				operacioncontable.getPrecioorigen(),
				operacioncontable.getPreciosoles(),
				operacioncontable.getPreciodolares(),
				operacioncontable.getMediopago(),
				operacioncontable.getFlujoefectivo(),
				operacioncontable.getBienservicio(),
				operacioncontable.getDetraccion(),
				operacioncontable.getTasadetraccion(),
				operacioncontable.getNumerodetraccion(),
				operacioncontable.getFechadetraccion(),
				operacioncontable.getBasedetraccion(),
				operacioncontable.getMontodetraccion(),
				operacioncontable.getTipodocumentoref(),
				operacioncontable.getEjerciciocontableref(),
				operacioncontable.getOperacioncontableref(),
				operacioncontable.getGirado(),
				operacioncontable.getGlosa(),
				operacioncontable.getTipodocumentonodom(),
				operacioncontable.getNumeroserienodom(),
				operacioncontable.getAnnocomprobantenodom(),
				operacioncontable.getNumerocomprobantenodom(),
				operacioncontable.getEntidadnodom(),
				operacioncontable.getRucnodom(),
				operacioncontable.getVinculoeconomiconodom(),
				operacioncontable.getTasaretencionnodom(),
				operacioncontable.getImpuestoretenidonodom(),
				operacioncontable.getConvenionodom(),
				operacioncontable.getExoneracionnodom(),
				operacioncontable.getTiporentanodom(),
				operacioncontable.getModalidadnodom(),
				operacioncontable.getArticulonodom(),
				operacioncontable.getColumnabaseimponible(),
				operacioncontable.getColumnaimpuesto(),
				operacioncontable.getEstado(),
				operacioncontable.getCreacionUsuario(),
				operacioncontable.getModificacionUsuario()
				);
	}

	@Override
	public void editar(OperacionContable operacioncontable) {
		jdbcTemplate.update("update T_CON_OPERACION set " +
				" IDPERCOD = ?, IDTIPOOPERACION = ? " +
				",OPEFECHAOPERACION = ?, OPEFECHAEMISION = ?, OPEFECHAVENCIMIENTO = ?, ENTCOD = ?, ENTRUC = ? " +
				",ENTCODBCO = ?, CBACOD = ?, TDOCOD = ?, SUBCOD = ?, IDADUANA = ? " +
				",OPESERCOMPROBANTE = ?, OPENUMCOMPROBANTE = ?, OPENUMCOMPROBANTEFIN = ?, IDMONEDA = ?, IDTIPOCAMBIO = ? " +
				",OPETIPOCAMBIOSOL = ?, OPETIPOCAMBIODOL = ?, OPEBASEIMPONIBLE = ?, OPEINAFECTO = ?, OPEADICIONALNODOM = ? " +
				",OPEINDIMPUESTO = ?, OPEPORCENTAJEIGV = ?, OPEIMPUESTO = ?, OPEIMPUESTOVAR = ?, OPEDEDCOSTONODOM = ?, OPEPRECIOORI = ? " +
				",OPEPRECIOSOL = ?, OPEPRECIODOL = ?, IDMEDIOPAGO = ?, IDFLUJOEFECTIVO = ?, IDBIENSERVICIO = ?, DETCOD = ? " +
				",OPETASADETRACCION = ?, OPENUMDETRACCION = ?, OPEFECHADETRACCION = ?, OPEBASEDETRACCION = ?, OPEDETRACCION = ? " +
				",TDOCODREF = ?, IDEJECONTABLEREF = ?, IDOPENUMREF = ?, OPEGIRADO = ?, OPEGLOSA = ? " +
				",TDOCODNODOM = ?, OPESERCOMPROBANTENODOM = ?, OPEANOCOMPROBANTENODOM = ?, OPENUMCOMPROBANTENODOM = ?, " +
				",ENTCODNODOM = ?, ENTRUCNODOM = ? " +
				",IDVINCULOECONODOM = ?, OPETASARETENCIONNODOM = ?, OPEIMPUESTORETNODOM = ?, IDCONVENIOTRIBNODOM = ?, IDEXONERACIONNODOM = ? " +
				",IDTIPORENTANODOM = ?, IDMODALIDADSERVNODOM = ?, OPEINDARTICULONODOM = ?, OPEREGCOMREF = ?, OPEREGCOMIGV = ? " +
				",OPEESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"	where IDEMPCOD = ? and IDEJECONTABLE = ? and IDOPENUM = ?  ",					
				operacioncontable.getPeriodocontable(),
				operacioncontable.getTipooperacion(),
				operacioncontable.getFechaoperacion(),
				operacioncontable.getFechaemision(),
				operacioncontable.getFechavencimiento(),
				operacioncontable.getEntidad(),
				operacioncontable.getRuc(),
				operacioncontable.getBanco(),
				operacioncontable.getCuentabancaria(),
				operacioncontable.getTipodocumento(),
				operacioncontable.getSubdiario(),
				operacioncontable.getAduana(),
				operacioncontable.getNumeroserie(),
				operacioncontable.getNumerocomprobanteini(),
				operacioncontable.getNumerocomprobantefin(),
				operacioncontable.getMoneda(),
				operacioncontable.getTipocambio(),
				operacioncontable.getTipocambiosoles(),
				operacioncontable.getTipocambiodolares(),
				operacioncontable.getBaseimponible(),
				operacioncontable.getInafecto(),
				operacioncontable.getAdicionalnodom(),
				operacioncontable.getIndimpuestorenta(),
				operacioncontable.getPorcentajeimpuesto(),
				operacioncontable.getImpuesto(),
				operacioncontable.getImpuestovar(),
				operacioncontable.getDeduccionnodom(),
				operacioncontable.getPrecioorigen(),
				operacioncontable.getPreciosoles(),
				operacioncontable.getPreciodolares(),
				operacioncontable.getMediopago(),
				operacioncontable.getFlujoefectivo(),
				operacioncontable.getBienservicio(),
				operacioncontable.getDetraccion(),
				operacioncontable.getTasadetraccion(),
				operacioncontable.getNumerodetraccion(),
				operacioncontable.getFechadetraccion(),
				operacioncontable.getBasedetraccion(),
				operacioncontable.getMontodetraccion(),
				operacioncontable.getTipodocumentoref(),
				operacioncontable.getEjerciciocontableref(),
				operacioncontable.getOperacioncontableref(),
				operacioncontable.getGirado(),
				operacioncontable.getGlosa(),
				operacioncontable.getTipodocumentonodom(),
				operacioncontable.getNumeroserienodom(),
				operacioncontable.getAnnocomprobantenodom(),
				operacioncontable.getNumerocomprobantenodom(),
				operacioncontable.getEntidadnodom(),
				operacioncontable.getRucnodom(),
				operacioncontable.getVinculoeconomiconodom(),
				operacioncontable.getTasaretencionnodom(),
				operacioncontable.getImpuestoretenidonodom(),
				operacioncontable.getConvenionodom(),
				operacioncontable.getExoneracionnodom(),
				operacioncontable.getTiporentanodom(),
				operacioncontable.getModalidadnodom(),
				operacioncontable.getArticulonodom(),
				operacioncontable.getColumnabaseimponible(),
				operacioncontable.getColumnaimpuesto(),
				operacioncontable.getEstado(),
				operacioncontable.getModificacionUsuario(),
				operacioncontable.getEmpresa(),
				operacioncontable.getEjerciciocontable(),
				operacioncontable.getOperacioncontable()
				);	
		
	}

	@Override
	public int eliminar(OperacionContable operacioncontable) {
		return jdbcTemplate.update("delete from T_CON_OPERACION " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and IDOPENUM = ? ", 
				operacioncontable.getEmpresa(),
				operacioncontable.getEjerciciocontable(),
				operacioncontable.getOperacioncontable());	
	}

	@Override
	public OperacionContable consultar(Integer empresa, Integer ejerciciocontable, String operacioncontable) {
		
		return jdbcTemplate.query(
				"select  " +
					"   t1.IDEMPCOD, t1.IDEJECONTABLE, t1.IDOPENUM " +
					" , t1.IDPERCOD, '' as descripcionperiodocontable " +
					" , t1.IDTIPOOPERACION, '' as descripciontipooperacion " +
					" , CONVERT(DATE,t1.OPEFECHAOPERACION), CONVERT(DATE,t1.OPEFECHAEMISION), CONVERT(DATE,t1.OPEFECHAVENCIMIENTO) " +
					" , t1.ENTCOD, '' as nombreentidad, t1.ENTRUC " +
					" , t1.ENTCODBCO, '' as nombrebanco, t1.CBACOD, t1.TDOCOD, t6.TDODESCRIP as descripciontipodocumento " +
					" , t1.SUBCOD, t1.SUBCOD + '-' + t7.SUBDESCRIP as descripcionsubdiario, t1.IDADUANA, '' as descripcionaduana " +
					" , t1.OPESERCOMPROBANTE, t1.OPENUMCOMPROBANTE, t1.OPENUMCOMPROBANTEFIN, t1.IDMONEDA, '' as descripcionmoneda " +
					" , t1.IDTIPOCAMBIO, '' as descripciontipocambio, t1.OPETIPOCAMBIOSOL, t1.OPETIPOCAMBIODOL " +
					" , t1.OPEBASEIMPONIBLE, t1.OPEINAFECTO, t1.OPEADICIONALNODOM " +
					" , t1.OPEINDIMPUESTO, t1.OPEPORCENTAJEIGV, t1.OPEIMPUESTO, t1.OPEIMPUESTOVAR, t1.OPEDEDCOSTONODOM, t1.OPEPRECIOORI  " +
					" , t1.OPEPRECIOSOL, t1.OPEPRECIODOL, t1.IDMEDIOPAGO, '' as descripcionmediopago " +
					" , t1.IDFLUJOEFECTIVO, '' as descripcionflujoefectivo " +
					" , t1.IDBIENSERVICIO, '' as descripcionbienservicio, t1.DETCOD, '' as descripciondetraccion " +
					" , t1.OPETASADETRACCION, t1.OPENUMDETRACCION, CONVERT(DATE,t1.OPEFECHADETRACCION), t1.OPEBASEDETRACCION, t1.OPEDETRACCION  " +
					" , t1.TDOCODREF, t14.TDODESCRIP as descripciontipodocumentoref, t1.IDEJECONTABLEREF, t1.IDOPENUMREF " +
					" , T2.OPESERCOMPROBANTE as OPESERCOMPROBANTEREF, T2.OPENUMCOMPROBANTE as OPENUMCOMPROBANTEREF " + 
					" , CONVERT(DATE,t2.OPEFECHAEMISION) as OPEFECHAEMISIONREF, T2.OPETIPOCAMBIOSOL as OPETIPOCAMBIOSOLREF " +
					" , T2.OPEBASEIMPONIBLE as OPEBASEIMPONIBLEREF, T2.OPEINAFECTO as OPEINAFECTOREF " +
					" , T2.OPEIMPUESTO as OPEIMPUESTOREF, T2.OPEPRECIOORI as OPEPRECIOORIREF " +
					" , t1.OPEGIRADO, t1.OPEGLOSA  " +
					" , t1.TDOCODNODOM, '' as descripciontipodocumentonodom, t1.OPESERCOMPROBANTENODOM, t1.OPEANOCOMPROBANTENODOM " +
					" , t1.OPENUMCOMPROBANTENODOM, t1.ENTCODNODOM, '' as nombreentidadnodom, t1.ENTRUCNODOM " +
					" , t1.IDVINCULOECONODOM, '' as descripcionvinculoeconomiconodom " +
					" , t1.OPETASARETENCIONNODOM, t1.OPEIMPUESTORETNODOM, t1.IDCONVENIOTRIBNODOM, '' as descripcionconvenionodom " +
					" , t1.IDEXONERACIONNODOM, '' as descripcionexoneracionnodom, t1.IDTIPORENTANODOM, '' as descripciontiporentanodom " +
					" , t1.IDMODALIDADSERVNODOM, '' as descripcionmodalidadnodom, t1.OPEINDARTICULONODOM, t1.OPEREGCOMREF, t1.OPEREGCOMIGV  " +
					" , t1.OPEESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +
				"from T_CON_OPERACION t1 " +
					" LEFT OUTER JOIN T_CON_OPERACION T2 " +
					" ON t1.IDEMPCOD = T2.IDEMPCOD " +
					" AND t1.IDEJECONTABLEREF = T2.IDEJECONTABLE " +
					" AND t1.IDOPENUMREF = T2.IDOPENUM " +
					" left outer join T_M_TIPODOCUMENTO t6 " +
					" on  t1.IDEMPCOD = t6.IDEMPCOD " +
					" and t1.TDOCOD = t6.TDOCOD " +
					" left outer join T_CON_SUBDIARIO t7 " +
					" on  t1.IDEMPCOD = t7.IDEMPCOD " +
					" and t1.SUBCOD = t7.SUBCOD " +
					" left outer join T_M_TIPODOCUMENTO t14 " +
					" on  t1.IDEMPCOD = t14.IDEMPCOD " +
					" and t1.TDOCODREF = t14.TDOCOD " +
				"where t1.IDEMPCOD = ?  and t1.IDEJECONTABLE = ? and t1.IDOPENUM = ? ", 
					new OperacionContableExtractor(), empresa, ejerciciocontable, operacioncontable);
	}
	
	@Override
	public List<OperacionContable> consultarLista(Integer empresa, Integer ejerciciocontable, String tipooperacion) {
	
		return jdbcTemplate.query(
			"select  " +
				"   t1.IDEMPCOD, t1.IDEJECONTABLE, t1.IDOPENUM " +
				" , t1.IDPERCOD, t2.DESCRIPCION as descripcionperiodocontable " +
				" , t1.IDTIPOOPERACION, t3.DESCRIPCION as descripciontipooperacion " +
				" , t1.OPEFECHAOPERACION, t1.OPEFECHAEMISION, t1.OPEFECHAVENCIMIENTO " +
				" , t1.ENTCOD, case when t4.IDTIPOPERSONA = 'N' then t4.ENTPATERNO + ' ' + t4.ENTMATERNO + ', ' + t4.ENTNOMBRES ELSE t4.ENTNOMBRES end as nombreentidad " +
				" , t1.ENTRUC, t1.ENTCODBCO, t5.ENTNOMBRES as nombrebanco, t1.CBACOD " +
				" , t1.TDOCOD, t6.TDODESCRIP as descripciontipodocumento " + 
				" , t1.SUBCOD, t7.SUBDESCRIP as descripcionsubdiario, t1.IDADUANA, T8.DESCRIPCION as descripcionaduana " +
				" , t1.OPESERCOMPROBANTE, t1.OPENUMCOMPROBANTE, t1.OPENUMCOMPROBANTEFIN, t1.IDMONEDA, t9.DESCRIPCION as descripcionmoneda " +
				" , t1.IDTIPOCAMBIO, t10.DESCRIPCION as descripciontipocambio, t1.OPETIPOCAMBIOSOL, t1.OPETIPOCAMBIODOL " +
				" , t1.OPEBASEIMPONIBLE, t1.OPEINAFECTO, t1.OPEADICIONALNODOM " +
				" , t1.OPEINDIMPUESTO, t1.OPEPORCENTAJEIGV, t1.OPEIMPUESTO, t1.OPEIMPUESTOVAR, t1.OPEDEDCOSTONODOM, t1.OPEPRECIOORI " + 
				" , t1.OPEPRECIOSOL, t1.OPEPRECIODOL, t1.IDMEDIOPAGO, t11.DESCRIPCION as descripcionmediopago " +
				" , t1.IDFLUJOEFECTIVO,  t24.DESCRIPCION as descripcionflujoefectivo " +
				" , t1.IDBIENSERVICIO, t12.DESCRIPCION as descripcionbienservicio, t1.DETCOD, t13.DETDEFINICION as descripcionproducto " +
				" , t1.OPETASADETRACCION, t1.OPENUMDETRACCION, t1.OPEFECHADETRACCION, t1.OPEBASEDETRACCION, t1.OPEDETRACCION " +
				" , t1.TDOCODREF, t14.TDODESCRIP as descripciontipodocumentoref, t1.IDEJECONTABLEREF, t1.IDOPENUMREF " +
				" , T23.OPESERCOMPROBANTE as OPESERCOMPROBANTEREF, T23.OPENUMCOMPROBANTE as OPENUMCOMPROBANTEREF " + 
				" , T23.OPEFECHAEMISION as OPEFECHAEMISIONREF, T23.OPETIPOCAMBIOSOL as OPETIPOCAMBIOSOLREF " +
				" , T23.OPEBASEIMPONIBLE as OPEBASEIMPONIBLEREF, T23.OPEINAFECTO as OPEINAFECTOREF " +
				" , T23.OPEIMPUESTO as OPEIMPUESTOREF, T23.OPEPRECIOORI as OPEPRECIOORIREF " +
				" , t1.OPEGIRADO, t1.OPEGLOSA " + 
				" , t1.TDOCODNODOM, t15.TDODESCRIP as descripciontipodocumentonodom, t1.OPESERCOMPROBANTENODOM, t1.OPEANOCOMPROBANTENODOM " +
				" , t1.OPENUMCOMPROBANTENODOM " +
				" , t1.ENTCODNODOM, case when t16.IDTIPOPERSONA = 'N' then t16.ENTPATERNO + ' ' + t16.ENTMATERNO + ', ' + t16.ENTNOMBRES ELSE t16.ENTNOMBRES end as nombreentidadnodom " +
				" , t1.ENTRUCNODOM, t1.IDVINCULOECONODOM, t17.DESCRIPCION as descripcionvinculoeconomiconodom " +
				" , t1.OPETASARETENCIONNODOM, t1.OPEIMPUESTORETNODOM, t1.IDCONVENIOTRIBNODOM, t18.DESCRIPCION as descripcionconvenionodom " +
				" , t1.IDEXONERACIONNODOM, t19.DESCRIPCION as descripcionexoneracionnodom, t1.IDTIPORENTANODOM, t20.DESCRIPCION as descripciontiporentanodom " +
				" , t1.IDMODALIDADSERVNODOM, t21.DESCRIPCION as descripcionmodalidadnodom, t1.OPEINDARTICULONODOM, t1.OPEREGCOMREF, t1.OPEREGCOMIGV " +
				" , t1.OPEESTADO, t22.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_OPERACION t1 " +
				" left outer join V_T_MES t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDPERCOD = t2.IDMES " + 
				" left outer join V_T_TIPOOPERACION t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.IDTIPOOPERACION = t3.IDTIPOOPERACION " +
				" left outer join T_M_ENTE t4 " +
				" on  t1.IDEMPCOD = t4.IDEMPCOD " +
				" and t1.ENTCOD = t4.ENTCOD " +
				" left outer join T_M_ENTE t5 " +
				" on  t1.IDEMPCOD = t5.IDEMPCOD " +
				" and t1.ENTCODBCO = t5.ENTCOD " +
				" left outer join T_M_TIPODOCUMENTO t6 " +
				" on  t1.IDEMPCOD = t6.IDEMPCOD " +
				" and t1.TDOCOD = t6.TDOCOD " +
				" left outer join T_CON_SUBDIARIO t7 " +
				" on  t1.IDEMPCOD = t7.IDEMPCOD " +
				" and t1.SUBCOD = t7.SUBCOD " +
				" left outer join V_T_ADUANA t8 " +
				" on  t1.IDEMPCOD = t8.IDEMPCOD " +
				" and t1.IDADUANA = t8.IDADUANA " +
				" left outer join V_T_MONEDA t9 " +
				" on  t1.IDEMPCOD = t9.IDEMPCOD " +
				" and t1.IDMONEDA = t9.IDMONEDA " +
				" left outer join V_T_TIPOCAMBIO t10 " +
				" on  t1.IDEMPCOD = t10.IDEMPCOD " +
				" and t1.IDTIPOCAMBIO = t10.IDTIPOCAMBIO " +
				" left outer join V_T_MEDIOPAGO t11 " +
				" on  t1.IDEMPCOD = t11.IDEMPCOD " +
				" and t1.IDMEDIOPAGO = t11.IDMEDIOPAGO " +
				" left outer join V_T_BIENSERVICIO t12 " +
				" on  t1.IDEMPCOD = t12.IDEMPCOD " +
				" and t1.IDBIENSERVICIO = t12.IDBIENSERVICIO " +
				" left outer join T_CON_DETRACCION t13 " +
				" on  t1.IDEMPCOD = t13.IDEMPCOD " +
				" and t1.DETCOD = t13.DETCOD " +
				" left outer join T_M_TIPODOCUMENTO t14 " +
				" on  t1.IDEMPCOD = t14.IDEMPCOD " +
				" and t1.TDOCODREF = t14.TDOCOD " +
				" left outer join T_M_TIPODOCUMENTO t15 " +
				" on  t1.IDEMPCOD = t15.IDEMPCOD " +
				" and t1.TDOCODNODOM = t15.TDOCOD " +
				" left outer join T_M_ENTE t16 " +
				" on  t1.IDEMPCOD = t16.IDEMPCOD " +
				" and t1.ENTCODNODOM = t16.ENTCOD " +
				" left outer join V_T_VINCULOECONODOM t17 " +
				" on  t1.IDEMPCOD = t17.IDEMPCOD " +
				" and t1.IDVINCULOECONODOM = t17.IDVINCULOECONODOM " +
				" left outer join V_T_CONVENIOTRIBNODOM t18 " +
				" on  t1.IDEMPCOD = t18.IDEMPCOD " +
				" and t1.IDCONVENIOTRIBNODOM = t18.IDCONVENIOTRIBNODOM " +
				" left outer join V_T_EXONERACIONNODOM t19 " +
				" on  t1.IDEMPCOD = t19.IDEMPCOD " +
				" and t1.IDEXONERACIONNODOM = t19.IDEXONERACIONNODOM " +
				" left outer join V_T_TIPORENTANODOM t20 " +
				" on  t1.IDEMPCOD = t20.IDEMPCOD " +
				" and t1.IDTIPORENTANODOM = t20.IDTIPORENTANODOM " +
				" left outer join V_T_MODALIDADSERVNODOM t21 " +
				" on  t1.IDEMPCOD = t21.IDEMPCOD " +
				" and t1.IDMODALIDADSERVNODOM = t21.IDMODALIDADSERVNODOM " +
				" left outer join V_T_ESTADOOPE t22 " +
				" on  t1.IDEMPCOD = t22.IDEMPCOD " +
				" and t1.OPEESTADO = t22.OPEESTADO " +
				" LEFT OUTER JOIN T_CON_OPERACION T23 " +
				" ON t1.IDEMPCOD = T23.IDEMPCOD " +
				" AND t1.IDEJECONTABLEREF = T23.IDEJECONTABLE " +
				" AND t1.IDOPENUMREF = T23.IDOPENUM " +
				" left outer join V_T_FLUJOEFECTIVO t24 " +
				" on  t1.IDEMPCOD = t24.IDEMPCOD " +
				" and t1.IDFLUJOEFECTIVO = t24.IDFLUJOEFECTIVO " +
			"where t1.IDEMPCOD = ?  and t1.IDEJECONTABLE = ? and t1.IDTIPOOPERACION = ? ", 
				new OperacionContableRowMapper(), new Object[] {empresa, ejerciciocontable, tipooperacion});
		
	}
	
	@Override
	public List<OperacionContable> consultarVista(Integer empresa, Integer ejerciciocontable, String tipooperacion) {
	
		return jdbcTemplate.query(
			"select  " +
				"   t1.IDEMPCOD, t1.IDEJECONTABLE, t1.IDOPENUM " +
				" , t1.IDPERCOD, '' as descripcionperiodocontable " +
				" , t1.IDTIPOOPERACION, '' as descripciontipooperacion " +
				" , CONVERT(VARCHAR,t1.OPEFECHAOPERACION,103), t1.OPEFECHAEMISION, t1.OPEFECHAVENCIMIENTO " +
				" , t1.ENTCOD, case when t4.IDTIPOPERSONA = 'N' then t4.ENTPATERNO + ' ' + t4.ENTMATERNO + ', ' + t4.ENTNOMBRES ELSE t4.ENTNOMBRES end as nombreentidad " +
				" , t1.ENTRUC, t1.ENTCODBCO, t5.ENTNOMBRES as nombrebanco, t1.CBACOD " +
				" , t1.TDOCOD, t1.TDOCOD + '-' + t6.TDODESCRIP as descripciontipodocumento " + 
				" , t1.SUBCOD, t1.SUBCOD + '-' + t7.SUBDESCRIP as descripcionsubdiario, t1.IDADUANA, '' as descripcionaduana " +
				" , t1.OPESERCOMPROBANTE, t1.OPENUMCOMPROBANTE, t1.OPENUMCOMPROBANTEFIN, t1.IDMONEDA, '' as descripcionmoneda " +
				" , t1.IDTIPOCAMBIO, '' as descripciontipocambio, t1.OPETIPOCAMBIOSOL, t1.OPETIPOCAMBIODOL " +
				" , t1.OPEBASEIMPONIBLE, t1.OPEINAFECTO, t1.OPEADICIONALNODOM " +
				" , t1.OPEINDIMPUESTO, t1.OPEPORCENTAJEIGV, t1.OPEIMPUESTO, t1.OPEIMPUESTOVAR, t1.OPEDEDCOSTONODOM, t1.OPEPRECIOORI " + 
				" , t1.OPEPRECIOSOL, t1.OPEPRECIODOL, t1.IDMEDIOPAGO, '' as descripcionmediopago " +
				" , t1.IDFLUJOEFECTIVO,  '' as descripcionflujoefectivo " +
				" , t1.IDBIENSERVICIO, '' as descripcionbienservicio, t1.DETCOD, '' as descripcionproducto " +
				" , t1.OPETASADETRACCION, t1.OPENUMDETRACCION, t1.OPEFECHADETRACCION, t1.OPEBASEDETRACCION, t1.OPEDETRACCION " +
				" , t1.TDOCODREF, '' as descripciontipodocumentoref, t1.IDEJECONTABLEREF, t1.IDOPENUMREF " + 
				" , '' as OPESERCOMPROBANTEREF, '' as OPENUMCOMPROBANTEREF " + 
				" , '' as OPEFECHAEMISIONREF, 0 as OPETIPOCAMBIOSOLREF " +
				" , 0 as OPEBASEIMPONIBLEREF, 0 as OPEINAFECTOREF " +
				" , 0 as OPEIMPUESTOREF, 0 as OPEPRECIOORIREF " +
				" , t1.OPEGIRADO, t1.OPEGLOSA " + 
				" , t1.TDOCODNODOM, '' as descripciontipodocumentonodom, t1.OPESERCOMPROBANTENODOM, t1.OPEANOCOMPROBANTENODOM " +
				" , t1.OPENUMCOMPROBANTENODOM " +
				" , t1.ENTCODNODOM, '' as nombreentidadnodom, t1.ENTRUCNODOM " +
				" , t1.IDVINCULOECONODOM, '' as descripcionvinculoeconomiconodom " +
				" , t1.OPETASARETENCIONNODOM, t1.OPEIMPUESTORETNODOM, t1.IDCONVENIOTRIBNODOM, '' as descripcionconvenionodom " +
				" , t1.IDEXONERACIONNODOM, '' as descripcionexoneracionnodom, t1.IDTIPORENTANODOM, '' as descripciontiporentanodom " +
				" , t1.IDMODALIDADSERVNODOM, '' as descripcionmodalidadnodom, t1.OPEINDARTICULONODOM, t1.OPEREGCOMREF, t1.OPEREGCOMIGV " +
				" , t1.OPEESTADO, t22.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_OPERACION t1 " +
				" left outer join T_M_ENTE t4 " +
				" on  t1.IDEMPCOD = t4.IDEMPCOD " +
				" and t1.ENTCOD = t4.ENTCOD " +
				" left outer join T_M_ENTE t5 " +
				" on  t1.IDEMPCOD = t5.IDEMPCOD " +
				" and t1.ENTCODBCO = t5.ENTCOD " +
				" left outer join T_M_TIPODOCUMENTO t6 " +
				" on  t1.IDEMPCOD = t6.IDEMPCOD " +
				" and t1.TDOCOD = t6.TDOCOD " +
				" left outer join T_CON_SUBDIARIO t7 " +
				" on  t1.IDEMPCOD = t7.IDEMPCOD " +
				" and t1.SUBCOD = t7.SUBCOD " +
				" left outer join V_T_ESTADOOPE t22 " +
				" on  t1.IDEMPCOD = t22.IDEMPCOD " +
				" and t1.OPEESTADO = t22.OPEESTADO " +
			"where t1.IDEMPCOD = ?  and t1.IDEJECONTABLE = ? and t1.IDTIPOOPERACION = ? ", 
				new OperacionContableRowMapper(), new Object[] {empresa, ejerciciocontable, tipooperacion});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String operacioncontable) {

		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_OPERACION " + 
				"where IDEMPCOD = ?  and IDEJECONTABLE = ? and IDOPENUM = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable, operacioncontable);
	}
	
	@Override
	public ValorContador contadorRegistrosComprobante(Integer empresa, String entidad, String numeroserie, String numerocomprobanteini) {

		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_OPERACION " + 
				"where IDEMPCOD = ?  and ENTCOD = ? and OPESERCOMPROBANTE = ? and OPENUMCOMPROBANTE = ? ",
					new ValorContadorExtractor(), empresa, entidad, numeroserie, numerocomprobanteini);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(OperacionContable operacioncontable){
		
		return jdbcTemplate.query(
				"select t1.IDOPENUM as codigo, t2.DESCRIPCION as descripcion, t3.DESCRIPCION as detalle " +
				"from T_CON_OPERACION t1 " +
				"left outer join V_T_TIPOOPERACION t2 " +
					"on t1.IDEMPCOD = T2.IDEMPCOD " +
					"and t1.IDTIPOOPERACION = t2.IDTIPOOPERACION " +
				"left outer join V_T_ESTADOOPE t3 " +
					"on t1.IDEMPCOD = t3.IDEMPCOD " +
					"and t1.OPEESTADO = t3.OPEESTADO " +
				"where t1.IDEMPCOD = ?  and t1.IDEJECONTABLE = ? and t1.IDTIPOOPERACION like ? " +
				"order by t1.IDOPENUM ",
					new CatalogoRowMapper(), 
					new Object[] {operacioncontable.getEmpresa(), operacioncontable.getEjerciciocontable(), operacioncontable.getTipooperacion()});
	}
	
	public List<Catalogo> consultarCatalogoRef(OperacionContable operacioncontable){
		
		return jdbcTemplate.query(
				"select t1.IDOPENUM as codigo, " +
				"isnull(t2.DESCRIPCION,'') + ' | ' + isnull(t3.TDODESCRIP,'') + ' | ' + isnull(t1.OPESERCOMPROBANTE,'') + ' | ' + isnull(t1.OPENUMCOMPROBANTE,'') + ' | ' + ltrim(convert(varchar(10), convert(DEC(20,2),t1.OPEPRECIOORI))) as descripcion, " +
				"CONVERT(VARCHAR,t1.OPEFECHAEMISION,103) as detalle " +
				"from T_CON_OPERACION t1 " +
				"left outer join V_T_TIPOOPERACION t2 " +
					"on t1.IDEMPCOD = T2.IDEMPCOD " +
					"and t1.IDTIPOOPERACION = t2.IDTIPOOPERACION " +
				"left outer join T_M_TIPODOCUMENTO t3 " +
					"on t1.IDEMPCOD = t3.IDEMPCOD " +
					"and t1.TDOCOD = t3.TDOCOD " +
				"where t1.IDEMPCOD = ?  and t1.IDEJECONTABLE = ? and t1.IDOPENUM <> ? and t1.ENTCOD = ? " +
					" and (t1.IDOPENUM like ? or t1.OPENUMCOMPROBANTE like ? or t1.OPENUMCOMPROBANTE like ? )" +
				"order by t1.IDOPENUM ",
					new CatalogoRowMapper(), 
					new Object[] 
							{
									operacioncontable.getEmpresa(), 
									operacioncontable.getEjerciciocontable(), 
									operacioncontable.getOperacioncontable(),
									operacioncontable.getEntidad(),
									operacioncontable.getGlosa(),
									operacioncontable.getGlosa(),
									operacioncontable.getGlosa()
							}
				);
		
	}
	
	public List<ConsultaString> consultarSaldo(RegistroContable registrocontable)
	{
		
		if (registrocontable.getNumerocomprobanteini() != null &&  registrocontable.getNumerocomprobanteini() != "")
		{
			// System.out.println(registrocontable.getEmpresa());

			return jdbcTemplate.query(
					"SELECT " +
					"(SELECT top 1 convert(varchar, TCO.OPEFECHAEMISION, 103) " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS OPEFECHAEMISION	 " +
					",(SELECT top 1 TCO.SUBCOD  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS SUBCOD		  " +
					",(SELECT top 1 TCO.IDOPENUM  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS IDOPENUM		 " +
					",TX.TDOCOD, TX.REGSERCOMPROBANTE, TX.REGNUMCOMPROBANTE " +
					",(SELECT top 1 TCO.OPEGLOSA  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS OPEGLOSA		 " +
					", TX.MONEDA, convert(decimal(10,2) , TX.SALDO_MO), convert(decimal(10,2) , TX.SALDO_MN), convert(decimal(10,2) , TX.SALDO_MD) " +
					" , '12', '13','14','15','16','17','18','19','20' " +
					"FROM " +
					"( " +
					"	SELECT IDEMPCOD, IDEJECONTABLE, CTACOD, ENTCOD, TDOCOD, REGSERCOMPROBANTE, REGNUMCOMPROBANTE, MONEDA,  " +
					"		ROUND(SUM(DEBE_MO),2) - ROUND(SUM(HABER_MO),2) AS SALDO_MO, " +
					"		ROUND(SUM(DEBE_MN),2) - ROUND(SUM(HABER_MN),2) AS SALDO_MN, " +
					"		ROUND(SUM(DEBE_MD),2) - ROUND(SUM(HABER_MD),2) AS SALDO_MD " +
					"	FROM " +
					"		(  " +
					"		SELECT 	 T_CON_REGISTROCONTABLE.IDEMPCOD, T_CON_REGISTROCONTABLE.IDEJECONTABLE, T_CON_REGISTROCONTABLE.CTACOD, T_CON_REGISTROCONTABLE.ENTCOD " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.TDOCOD,'') AS TDOCOD " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.REGSERCOMPROBANTE,'') AS REGSERCOMPROBANTE " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.REGNUMCOMPROBANTE,'') AS REGNUMCOMPROBANTE " +
					"				,V_T_MONEDA.ABREVIATURA AS MONEDA " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTOORIGEN  ELSE 0 END AS DEBE_MO " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTOORIGEN  ELSE 0 END AS HABER_MO	 " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTONACIONAL ELSE 0 END AS DEBE_MN " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTONACIONAL ELSE 0 END AS HABER_MN " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTODOLARES  ELSE 0 END AS DEBE_MD " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTODOLARES  ELSE 0 END AS HABER_MD		" +			 
					"		  FROM T_CON_OPERACION " +
					"			LEFT OUTER JOIN T_CON_ASIENTOCONTABLE " +
					"				ON T_CON_OPERACION.IDEMPCOD = T_CON_ASIENTOCONTABLE.IDEMPCOD " +
					"				AND T_CON_OPERACION.IDEJECONTABLE = T_CON_ASIENTOCONTABLE.IDEJECONTABLE " +
					"				AND T_CON_OPERACION.IDOPENUM = T_CON_ASIENTOCONTABLE.IDOPENUM " +
					"			LEFT OUTER JOIN T_CON_REGISTROCONTABLE " +
					"				ON T_CON_ASIENTOCONTABLE.IDEMPCOD = T_CON_REGISTROCONTABLE.IDEMPCOD " +
					"				AND T_CON_ASIENTOCONTABLE.IDASINUM = T_CON_REGISTROCONTABLE.IDASINUM	 " +
					"			LEFT OUTER JOIN V_T_MONEDA " +
					"				ON T_CON_OPERACION.IDEMPCOD = V_T_MONEDA.IDEMPCOD " +
					"				AND T_CON_OPERACION.IDMONEDA = V_T_MONEDA.IDMONEDA			 " +
					"		  WHERE T_CON_REGISTROCONTABLE.IDEMPCOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.IDEJECONTABLE = ? " +
					"				AND T_CON_REGISTROCONTABLE.CTACOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.ENTCOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.TDOCOD = ? " +
					"				AND T_CON_OPERACION.OPEESTADO = 'C' " +
					"				AND ISNULL(T_CON_REGISTROCONTABLE.TDOCOD,'') = ISNULL(?,'') " +
					"				AND ISNULL(T_CON_REGISTROCONTABLE.REGSERCOMPROBANTE,'') = ISNULL(?,'') " +
					"				AND T_CON_REGISTROCONTABLE.REGNUMCOMPROBANTE = ? " +
					"		) T1 " +
					"	GROUP BY IDEMPCOD, IDEJECONTABLE, CTACOD, ENTCOD, TDOCOD, REGSERCOMPROBANTE, REGNUMCOMPROBANTE, MONEDA " +
					"	having  ROUND(SUM(DEBE_MO),2) - ROUND(SUM(HABER_MO),2) <> 0 OR  " +
					"			ROUND(SUM(DEBE_MN),2) - ROUND(SUM(HABER_MN),2) <> 0 OR  " +
					"			ROUND(SUM(DEBE_MD),2) - ROUND(SUM(HABER_MD),2) <> 0 " +
					") TX " ,
						new ConsultaStringRowMapper(), 
						new Object[] 
							{
								registrocontable.getEmpresa(), 
						 		registrocontable.getEjerciciocontable(), 
						 		registrocontable.getCuentacontable(),
						 		registrocontable.getEntidad(),
						 		registrocontable.getTipodocumento(),
						 		registrocontable.getTipodocumento(),
						 		registrocontable.getNumeroserie(),
						 		registrocontable.getNumerocomprobanteini()
							}
					);
		}
		else
		{
			return jdbcTemplate.query(
					"SELECT " +
					"(SELECT top 1 convert(varchar, TCO.OPEFECHAEMISION, 103)  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS OPEFECHAEMISION	 " +
					",(SELECT top 1 TCO.SUBCOD  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS SUBCOD		  " +
					",(SELECT top 1 TCO.IDOPENUM  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS IDOPENUM		 " +
					",TX.TDOCOD, TX.REGSERCOMPROBANTE, TX.REGNUMCOMPROBANTE " +
					",(SELECT top 1 TCO.OPEGLOSA  " +
					"	FROM T_CON_OPERACION TCO " +
					"		LEFT OUTER JOIN T_CON_ASIENTOCONTABLE TCC " +
					"			ON TCO.IDEMPCOD = TCC.IDEMPCOD " +
					"			AND TCO.IDEJECONTABLE = TCC.IDEJECONTABLE " +
					"			AND TCO.IDOPENUM = TCC.IDOPENUM " +
					"		LEFT OUTER JOIN T_CON_REGISTROCONTABLE TCD " +
					"			ON TCC.IDEMPCOD = TCD.IDEMPCOD " +
					"			AND TCC.IDASINUM = TCD.IDASINUM " +
					"	WHERE   TCD.IDEMPCOD = TX.IDEMPCOD " +
					"		AND TCD.IDEJECONTABLE = TX.IDEJECONTABLE " +
					"		AND TCD.CTACOD = TX.CTACOD " +
					"		AND TCD.ENTCOD = TX.ENTCOD " +
					"		AND ISNULL(TCD.TDOCOD,'') = ISNULL(TX.TDOCOD,'') " +
					"		AND ISNULL(TCD.REGSERCOMPROBANTE,'') = ISNULL(TX.REGSERCOMPROBANTE,'') " +
					"		AND ISNULL(TCD.REGNUMCOMPROBANTE,'') = ISNULL(TX.REGNUMCOMPROBANTE,'') " +
					"		AND TCO.OPEESTADO = 'C' " +
					"	ORDER BY TCO.OPEFECHAOPERACION, TCD.IDASINUM " +
					" )  " +
					" AS OPEGLOSA		 " +
					", TX.MONEDA, convert(decimal(10,2) , TX.SALDO_MO), convert(decimal(10,2) , TX.SALDO_MN), convert(decimal(10,2) , TX.SALDO_MD) " +
					" , '12', '13','14','15','16','17','18','19','20' " +
					"FROM " +
					"( " +
					"	SELECT IDEMPCOD, IDEJECONTABLE, CTACOD, ENTCOD, TDOCOD, REGSERCOMPROBANTE, REGNUMCOMPROBANTE, MONEDA,  " +
					"		ROUND(SUM(DEBE_MO),2) - ROUND(SUM(HABER_MO),2) AS SALDO_MO, " +
					"		ROUND(SUM(DEBE_MN),2) - ROUND(SUM(HABER_MN),2) AS SALDO_MN, " +
					"		ROUND(SUM(DEBE_MD),2) - ROUND(SUM(HABER_MD),2) AS SALDO_MD " +
					"	FROM " +
					"		(  " +
					"		SELECT 	 T_CON_REGISTROCONTABLE.IDEMPCOD, T_CON_REGISTROCONTABLE.IDEJECONTABLE, T_CON_REGISTROCONTABLE.CTACOD, T_CON_REGISTROCONTABLE.ENTCOD " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.TDOCOD,'') AS TDOCOD " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.REGSERCOMPROBANTE,'') AS REGSERCOMPROBANTE " +
					"				,ISNULL(T_CON_REGISTROCONTABLE.REGNUMCOMPROBANTE,'') AS REGNUMCOMPROBANTE " +
					"				,V_T_MONEDA.ABREVIATURA AS MONEDA " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTOORIGEN  ELSE 0 END AS DEBE_MO " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTOORIGEN  ELSE 0 END AS HABER_MO	 " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTONACIONAL ELSE 0 END AS DEBE_MN " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTONACIONAL ELSE 0 END AS HABER_MN " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'D' THEN T_CON_REGISTROCONTABLE.REGMTODOLARES  ELSE 0 END AS DEBE_MD " +
					"				,CASE T_CON_REGISTROCONTABLE.IDTIPOANOTACION WHEN 'H' THEN T_CON_REGISTROCONTABLE.REGMTODOLARES  ELSE 0 END AS HABER_MD		" +			 
					"		  FROM T_CON_OPERACION " +
					"			LEFT OUTER JOIN T_CON_ASIENTOCONTABLE " +
					"				ON T_CON_OPERACION.IDEMPCOD = T_CON_ASIENTOCONTABLE.IDEMPCOD " +
					"				AND T_CON_OPERACION.IDEJECONTABLE = T_CON_ASIENTOCONTABLE.IDEJECONTABLE " +
					"				AND T_CON_OPERACION.IDOPENUM = T_CON_ASIENTOCONTABLE.IDOPENUM " +
					"			LEFT OUTER JOIN T_CON_REGISTROCONTABLE " +
					"				ON T_CON_ASIENTOCONTABLE.IDEMPCOD = T_CON_REGISTROCONTABLE.IDEMPCOD " +
					"				AND T_CON_ASIENTOCONTABLE.IDASINUM = T_CON_REGISTROCONTABLE.IDASINUM	 " +
					"			LEFT OUTER JOIN V_T_MONEDA " +
					"				ON T_CON_OPERACION.IDEMPCOD = V_T_MONEDA.IDEMPCOD " +
					"				AND T_CON_OPERACION.IDMONEDA = V_T_MONEDA.IDMONEDA			 " +
					"		  WHERE T_CON_REGISTROCONTABLE.IDEMPCOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.IDEJECONTABLE = ? " +
					"				AND T_CON_REGISTROCONTABLE.CTACOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.ENTCOD = ? " +
					"				AND T_CON_REGISTROCONTABLE.TDOCOD = ? " +
					"				AND T_CON_OPERACION.OPEESTADO = 'C' " +
					"		) T1 " +
					"	GROUP BY IDEMPCOD, IDEJECONTABLE, CTACOD, ENTCOD, TDOCOD, REGSERCOMPROBANTE, REGNUMCOMPROBANTE, MONEDA " +
					"	having  ROUND(SUM(DEBE_MO),2) - ROUND(SUM(HABER_MO),2) <> 0 OR  " +
					"			ROUND(SUM(DEBE_MN),2) - ROUND(SUM(HABER_MN),2) <> 0 OR  " +
					"			ROUND(SUM(DEBE_MD),2) - ROUND(SUM(HABER_MD),2) <> 0 " +
					") TX " ,
						new ConsultaStringRowMapper(), 
						new Object[] 
							{
								registrocontable.getEmpresa(), 
						 		registrocontable.getEjerciciocontable(), 
						 		registrocontable.getCuentacontable(),
						 		registrocontable.getEntidad(),
						 		registrocontable.getTipodocumento()
							}
					);			
		}		
		
	}
	
}




