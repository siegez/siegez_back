package com.siegez.daos.implementation;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.siegez.daos.interfaces.ValorFechaDao;
import com.siegez.models.ValorFecha;
import com.siegez.rowmapper.extractors.ValorFechaExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.ValorFechaRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.Valor;

@Repository
public class ValorFechaDaoImp implements ValorFechaDao {

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall valorfechaSp;
	private SimpleJdbcCall resultadoSp;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.valorfechaSp = new SimpleJdbcCall(dataSource);
		this.resultadoSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public ResultadoSP registrarValorFecha(ValorFecha valorfecha) {
		this.resultadoSp.withSchemaName("dbo").withProcedureName("sp_con_registrarvalorfecha").declareParameters(
				new SqlParameter("idempcod", Types.INTEGER),
				new SqlParameter("valcod", Types.VARCHAR),
				new SqlParameter("valvez", Types.INTEGER),
				new SqlParameter("valfechainicio", Types.VARCHAR),
				new SqlParameter("valfechafin", Types.VARCHAR),
				new SqlParameter("valvalor", Types.DOUBLE),
				new SqlParameter("estado", Types.VARCHAR),
				new SqlParameter("username", Types.VARCHAR),				
				new SqlOutParameter("mensaje", Types.VARCHAR),
				new SqlOutParameter("indexito", Types.VARCHAR));
				SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", valorfecha.getEmpresa(), Types.INTEGER)
																			.addValue("valcod", valorfecha.getCodigo(), Types.VARCHAR)
																			.addValue("valvez", valorfecha.getVez(), Types.INTEGER)
																			.addValue("valfechainicio", valorfecha.getFechainicio(), Types.VARCHAR)
																			.addValue("valfechafin", valorfecha.getFechafin(), Types.VARCHAR)
																			.addValue("valvalor", valorfecha.getValor(), Types.DOUBLE)
																			.addValue("estado", valorfecha.getEstado(), Types.VARCHAR)
																			.addValue("username", valorfecha.getModificacionUsuario(), Types.VARCHAR);
				Map<String, Object> out = resultadoSp.execute(parameters);
				return new ResultadoSP(0, "", (String) out.get("mensaje"), (String) out.get("indexito"));					
	}
	
	
	@Override
	public void insertar(ValorFecha valorfecha) {
		jdbcTemplate.update("insert into T_M_VALORFECHA (IDEMPCOD, VALCOD, VALVEZ ,VALFECHAINICIO, VALFECHAFIN, VALVALOR " + 
				" , VALESTADO " + 
				" , MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				valorfecha.getEmpresa(),
				valorfecha.getCodigo(),
				valorfecha.getVez(),
				valorfecha.getFechainicio(),
				valorfecha.getFechafin(),
				valorfecha.getValor(),
				valorfecha.getEstado(),
				valorfecha.getCreacionUsuario(),
				valorfecha.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(ValorFecha valorfecha) {
		jdbcTemplate.update("update T_M_VALORFECHA set VALFECHAINICIO = ?, VALFECHAFIN = ?, VALVALOR = ?, VALESTADO = ?, "				
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and VALCOD = ? and VALVEZ = ? ",				
				valorfecha.getFechainicio(),
				valorfecha.getFechafin(),
				valorfecha.getValor(),
				valorfecha.getEstado(),
				valorfecha.getModificacionUsuario(),
				valorfecha.getEmpresa(),
				valorfecha.getCodigo(),
				valorfecha.getVez()
				);	
		
	}
	
	@Override
	public int eliminar(ValorFecha valorfecha) {
		return jdbcTemplate.update("delete from T_M_VALORFECHA " 
				+ " where IDEMPCOD = ? and VALCOD = ? and VALVEZ = ? ", 
				valorfecha.getEmpresa(),
				valorfecha.getCodigo(),
				valorfecha.getVez());	
	}
	
	@Override
	public ValorFecha consultar(Integer empresa, String codigo, Integer vez) {
		
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.VALCOD, t1.VALVEZ, CONVERT(DATE,t1.VALFECHAINICIO), CONVERT(DATE,t1.VALFECHAFIN), t1.VALVALOR " +
					", t1.VALESTADO, '' as descripcionestado " +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
				"from T_M_VALORFECHA t1 " +					
				"where t1.IDEMPCOD = ?  " + 
					"and t1.VALCOD = ? " +
					"and t1.VALVEZ = ? ", 
					new ValorFechaExtractor(), empresa, codigo, vez);
	}			
	
	@Override
	public Valor consultarValorPorFecha(Integer empresa, String codigo, String fecha) {
		
		this.valorfechaSp.withSchemaName("dbo").withProcedureName("sp_sha_getvalorfecha").declareParameters(
		new SqlParameter("idempcod", Types.INTEGER),
		new SqlParameter("valcod", Types.VARCHAR),
		new SqlParameter("fecha", Types.VARCHAR),
		new SqlOutParameter("valor", Types.DOUBLE));
		//new SqlOutParameter("valor", Types.DOUBLE)).returningResultSet("RESULT", new ValorRowMapper());		
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", empresa, Types.INTEGER)
																	.addValue("valcod", codigo, Types.VARCHAR)															
																	.addValue("fecha", fecha , Types.VARCHAR);
		Map<String, Object> out = valorfechaSp.execute(parameters);
		// Map<String, Object> out = valorfechaSp.execute(empresa, codigo);
		
		//System.out.println(out.values());						
		//System.out.println(out.get("RESULT"));
		//return null;		
		return new Valor((Double) out.get("valor"));
		
	}
	
	/*
	@Override
	public Valor consultarValorPorFecha(Integer empresa, String codigo, String fecha) {
	return jdbcTemplate.query(
			"select t1.VALVALOR " +					
			"from T_M_VALORFECHA t1 " +					
			"where t1.IDEMPCOD = ?  " + 
				"and t1.VALCOD = ? " +
				"and  " +
				"( " +
				"	(CONVERT(DATE, ?) between convert(date, valfechainicio)  and convert(date, valfechafin) ) " +
				"	or " +
				"	(CONVERT(DATE, ?) >=  convert(date, valfechainicio) and valfechafin is null) " +
				") "+
				"and t1.VALESTADO = 'A' ", 
				new ValorExtractor(), empresa, codigo, fecha, fecha);
	}
	*/
	
	@Override
	public List<ValorFecha> consultarLista(Integer empresa, String codigo) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.VALCOD, t1.VALVEZ, CONVERT(VARCHAR,t1.VALFECHAINICIO,103), CONVERT(VARCHAR,t1.VALFECHAFIN, 103), t1.VALVALOR " +
				" , t1.VALESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_VALORFECHA t1 " +
				" left outer join V_T_ESTADOOPE t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.VALESTADO = t2.OPEESTADO " +
			"where t1.IDEMPCOD = ? and t1.VALCOD = ?  " +
			"order by t1.VALFECHAINICIO ", 
				new ValorFechaRowMapper(), new Object[] {empresa, codigo});		
	}	
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, String fecha){
		
		return jdbcTemplate.query(
				"select t1.VALFECHAINICIO as codigo, t1.VALFECHAFIN as descripcion, t1.TCAVENTA as VALVALOR " +
				"from T_M_VALORFECHA t1 " +				
				"where t1.IDEMPCOD = ?  and t1.VALFECHAINICIO like ?, and t1.VALFECHAFIN like ? " +
				"order by t1.VALFECHAINICIO ",
					new CatalogoRowMapper(), new Object[] {empresa, fecha, fecha});
	}
}
