package com.siegez.daos.implementation;


//import java.sql.Types;
//import java.util.Date;
import java.util.List;
//import java.util.Map;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.SqlParameter;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.SqlParameterSource;
//import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.UsuarioDao;
import com.siegez.models.Usuario;
import com.siegez.rowmapper.extractors.UsuarioExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.UsuarioRowMapper;
import com.siegez.views.ValorContador;

//obs: you don't worry about try catch, remember that jdbcTemplate is an improvement of the classic jdbc
//spring implement it, Spring takes care of try catch

@Repository
public class UsuarioDaoImp implements UsuarioDao {
	
	private JdbcTemplate jdbcTemplate;
//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public void insertar(Usuario usuario) {	
		jdbcTemplate.update("insert into t_m_usuario(username,userfullname, userpass, useremail,usertelefono, userfecing, userinddb, persexo,"
				+ "userfoto, idliccod, userestado, mgbcreauser, mgbcreadate, mgbmodiuser,mgbmodidate) values (?,?,?,?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",
				usuario.getUsername(), 
				usuario.getNombre(),
				usuario.getPassword(),
				usuario.getEmail(),
				usuario.getTelefono(),
				usuario.getFechaUltimoIngreso(),
				usuario.getIdUserDB(),
				usuario.getSexo(),
				usuario.getFoto(),
				usuario.getCodLicencia(),
				usuario.getEstado(),
				usuario.getCreacionUsuario(),
				usuario.getModificacionUsuario());
	}

	@Override
	public void editar(Usuario usuario) {
		jdbcTemplate.update("update t_m_usuario set userfullname = ?, userpass = ?, useremail = ?, usertelefono = ?, userfecing = ?, userinddb = ?, persexo = ?, "
				+ "									userfoto = ?, idliccod = ?, userestado = ?, mgbmodiuser = ?, mgbmodidate = dateadd(hour, -5, getdate()) "
				+ "									where username like ?",
				usuario.getNombre(),
				usuario.getPassword(),
				usuario.getEmail(),
				usuario.getTelefono(),
				usuario.getFechaUltimoIngreso(),
				usuario.getIdUserDB(),
				usuario.getSexo(),
				usuario.getFoto(),
				usuario.getCodLicencia(),
				usuario.getEstado(),				
				usuario.getModificacionUsuario(),				
				usuario.getUsername());
	}

	@Override
	public Usuario consultar(String username) {
		
		return jdbcTemplate.query("select username,userfullname, userpass, useremail,usertelefono, userfecing, userinddb, persexo, userfoto, idliccod, userestado, mgbcreauser, mgbcreadate, mgbmodiuser, mgbmodidate from t_m_usuario where username like ?", new UsuarioExtractor(), username);
		
		// Obs: if we want to work with stored procedures (First Way)
	/*	this.usuarioSp.withSchemaName("MI_SCHEMA")
						.withProcedureName("SP_GET_USUARIO_BY_USERNAME");
		
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("@username", username);
	    Map<String, Object> out = usuarioSp.execute(parameters);
	    return new Usuario(out.get("username").toString(),out.get("userfullname").toString(), out.get("userpass").toString(),out.get("useremail").toString(), out.get("usertelefono").toString(),
	    		(Date) out.get("userfecing"), out.get("userinddb").toString(),out.get("persexo").toString(), out.get("userfoto").toString(),
	    		out.get("idliccod").toString(), out.get("userestado").toString(),out.get("mgbcreauser").toString(), (Date) out.get("mgbcreadate"),
	    		out.get("mgbmodiuser").toString(), (Date) out.get("mgbmodidate")); 
	    		
	  */
	 
	    // Obs: if we want to work with stored procedures (Second Way)
	  /*  this.usuarioSp.withSchemaName("MI_SCHEMA")
				.withProcedureName("SP_GET_USUARIO_BY_USERNAME")
				.declareParameters(
		                 new SqlParameter("@username", Types.VARCHAR))
		         .returningResultSet("RESULT", new UsuarioRowMapper()); //it will store it in a resultSet called "RESULT" mapping with UsuarioRowMapper

	    Map<String, Object> out = usuarioSp.execute(username);
	    return (Usuario) out.get("RESULT");
	    
	    */
	   
	}

	@Override
	public int eliminar(String username) {		
		return jdbcTemplate.update("delete from t_m_usuario where username like ?", username);	
	}

	@Override
	public List<Usuario> consultarLista() {
		return jdbcTemplate.query("select username,userfullname, userpass, useremail,usertelefono, userfecing, userinddb, persexo, userfoto, idliccod, userestado, mgbcreauser, mgbcreadate, mgbmodiuser, mgbmodidate from t_m_usuario", new UsuarioRowMapper());
	}
	
	@Override
	public List<Usuario> consultarListaPorLicencia(String licencia){
		return jdbcTemplate.query("select username,userfullname, userpass, useremail,usertelefono, userfecing, userinddb, persexo, userfoto, idliccod, userestado, mgbcreauser, mgbcreadate, mgbmodiuser, mgbmodidate from t_m_usuario where idliccod = ? ", new UsuarioRowMapper(), licencia);
	}
	
	@Override
	public ValorContador contadorRegistros(Usuario usuario) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from t_m_usuario " +
				"where username = ?  " , 
					new ValorContadorExtractor(), 
					new Object[] {usuario.getUsername()});
	}
	
	
}
