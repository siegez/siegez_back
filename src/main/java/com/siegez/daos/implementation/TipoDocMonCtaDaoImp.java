package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.TipoDocMonCtaDao;
import com.siegez.models.TipoDocMonCta;
import com.siegez.rowmapper.extractors.TipoDocMonCtaExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.TipoDocMonCtaRowMapper;
import com.siegez.views.ValorContador;

@Repository
public class TipoDocMonCtaDaoImp implements TipoDocMonCtaDao {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(TipoDocMonCta tipodocmoncta) {
		jdbcTemplate.update("insert into T_CON_TIPODOCMON_CUENTA (" +
				" IDEMPCOD, IDEJECONTABLE, TDOCOD, IDMONEDA, " +	
				" CTACODCOMPRA, CTACODCOMPRAREL, CTACODHONORARIO, CTACODVENTA, " +	
				" CTACODVENTAREL, CTACODCAJA, CTACODEGRESO, CTACODINGRESO, " +	
				" CTACODDIARIO, CTACODIMPORTACION, CTACODNODOMICILIADO, CTACODNODOMICILIADOREL," +	
				" TMCESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				tipodocmoncta.getEmpresa(),
				tipodocmoncta.getEjerciciocontable(),				
				tipodocmoncta.getTipodocumento(),
				tipodocmoncta.getMoneda(),
				tipodocmoncta.getCuentacompra(),
				tipodocmoncta.getCuentacomprarel(),
				tipodocmoncta.getCuentahonorario(),
				tipodocmoncta.getCuentaventa(),
				tipodocmoncta.getCuentaventarel(),
				tipodocmoncta.getCuentacaja(),
				tipodocmoncta.getCuentaegreso(),
				tipodocmoncta.getCuentaingreso(),
				tipodocmoncta.getCuentadiario(),
				tipodocmoncta.getCuentaimportacion(),
				tipodocmoncta.getCuentanodomiciliado(),	
				tipodocmoncta.getCuentanodomiciliadorel(),
				tipodocmoncta.getEstado(),
				tipodocmoncta.getCreacionUsuario(),
				tipodocmoncta.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(TipoDocMonCta tipodocmoncta) {
		jdbcTemplate.update("update T_CON_TIPODOCMON_CUENTA set " +
				" CTACODCOMPRA = ?, CTACODCOMPRAREL = ?, CTACODHONORARIO = ?, CTACODVENTA = ?, " +		
				" CTACODVENTAREL = ?, CTACODCAJA = ?, CTACODEGRESO = ?, CTACODINGRESO = ?, " +	
				" CTACODDIARIO = ?, CTACODIMPORTACION = ?, CTACODNODOMICILIADO = ?, CTACODNODOMICILIADOREL = ?, " +	
				" TMCESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and IDEJECONTABLE = ? and TDOCOD = ? and IDMONEDA = ? ",					
				tipodocmoncta.getCuentacompra(),
				tipodocmoncta.getCuentacomprarel(),
				tipodocmoncta.getCuentahonorario(),
				tipodocmoncta.getCuentaventa(),
				tipodocmoncta.getCuentaventarel(),
				tipodocmoncta.getCuentacaja(),
				tipodocmoncta.getCuentaegreso(),
				tipodocmoncta.getCuentaingreso(),
				tipodocmoncta.getCuentadiario(),
				tipodocmoncta.getCuentaimportacion(),
				tipodocmoncta.getCuentanodomiciliado(),	
				tipodocmoncta.getCuentanodomiciliadorel(),
				tipodocmoncta.getEstado(),
				tipodocmoncta.getModificacionUsuario(),
				tipodocmoncta.getEmpresa(),
				tipodocmoncta.getEjerciciocontable(),				
				tipodocmoncta.getTipodocumento(),
				tipodocmoncta.getMoneda()
				);	
		
	}	
	
	@Override
	public int eliminar(TipoDocMonCta tipodocmoncta) {
		return jdbcTemplate.update("delete from T_CON_TIPODOCMON_CUENTA " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and TDOCOD = ? and IDMONEDA = ? ", 
				tipodocmoncta.getEmpresa(),
				tipodocmoncta.getEjerciciocontable(),				
				tipodocmoncta.getTipodocumento(),
				tipodocmoncta.getMoneda());	
	}
	
	@Override
	public TipoDocMonCta consultar(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, tipodocumento, moneda);
		
		if (valorcontador.getContador() == 1)
		{		
			return jdbcTemplate.query(
									"select  " +
										" t1.IDEMPCOD, t1.IDEJECONTABLE, t1.TDOCOD, t2.TDODESCRIP as descripciontipodocumento, t1.IDMONEDA, t3.DESCRIPCION as descripcionmoneda, " +	
										" t1.CTACODCOMPRA, t1.CTACODCOMPRAREL, t1.CTACODHONORARIO, t1.CTACODVENTA, " +	
										" t1.CTACODVENTAREL, t1.CTACODCAJA, t1.CTACODEGRESO, t1.CTACODINGRESO, " +	
										" t1.CTACODDIARIO, t1.CTACODIMPORTACION, t1.CTACODNODOMICILIADO, t1.CTACODNODOMICILIADOREL, " +	
										" t1.TMCESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
									"from T_CON_TIPODOCMON_CUENTA t1 " +
										" left outer join T_M_TIPODOCUMENTO t2 " +
										" on  t1.IDEMPCOD = t2.IDEMPCOD " +
										" and t1.TDOCOD = t2.TDOCOD " +
										" left outer join V_T_MONEDA t3 " +
										" on  t1.IDEMPCOD = t3.IDEMPCOD " +
										" and t1.IDMONEDA = t3.IDMONEDA " +	
									"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ? and t1.TDOCOD = ? and t1.IDMONEDA = ?  ", 
									new TipoDocMonCtaExtractor(), 
									new Object[] {empresa, ejerciciocontable, tipodocumento, moneda});
		} else {
			return new TipoDocMonCta();
		} 
		
	}
	
	@Override
	public List<TipoDocMonCta> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.IDEJECONTABLE, t1.TDOCOD, t1.TDOCOD + '-' + t2.TDODESCRIP as descripciontipodocumento, t1.IDMONEDA, t3.DESCRIPCION as descripcionmoneda, " +	
				" isnull(t1.CTACODCOMPRA,''), isnull(t1.CTACODCOMPRAREL,''), isnull(t1.CTACODHONORARIO,''), isnull(t1.CTACODVENTA,''), " +	
				" isnull(t1.CTACODVENTAREL,''), isnull(t1.CTACODCAJA,''), isnull(t1.CTACODEGRESO,''), isnull(t1.CTACODINGRESO,''), " +	
				" isnull(t1.CTACODDIARIO,''), isnull(t1.CTACODIMPORTACION,''), isnull(t1.CTACODNODOMICILIADO,''), isnull(t1.CTACODNODOMICILIADOREL,''), " +
				" t1.TMCESTADO, t4.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_TIPODOCMON_CUENTA t1 " +	
				" left outer join T_M_TIPODOCUMENTO t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.TDOCOD = t2.TDOCOD " +
				" left outer join V_T_MONEDA t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.IDMONEDA = t3.IDMONEDA " +	
				" left outer join V_T_ESTADOREG t4 " +
				" on  t1.IDEMPCOD = t4.IDEMPCOD " +
				" and t1.TMCESTADO = t4.IDESTADO " +
			"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ?  ", 
				new TipoDocMonCtaRowMapper(), new Object[] {empresa, ejerciciocontable});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_TIPODOCMON_CUENTA " + 
				"where IDEMPCOD = ? and IDEJECONTABLE = ? and TDOCOD = ? and IDMONEDA = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable, tipodocumento, moneda);
	}
	
}
