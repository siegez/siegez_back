package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.CentroCostoDao;
import com.siegez.models.CentroCosto;
import com.siegez.rowmapper.extractors.CentroCostoExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.CentroCostoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class CentroCostoDaoImp implements CentroCostoDao {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(CentroCosto centrocosto) {
		jdbcTemplate.update("insert into T_CON_CENTROCOSTO (IDEMPCOD, IDEJECONTABLE, CCOCOD " 
				+ ", CCONOMBRE, CTACODCARGO, CTACODABONO, CCOPORCENTAJE, CCOESTADO " 
				+ ", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " 
				+ " values (?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				centrocosto.getEmpresa(),
				centrocosto.getEjerciciocontable(),
				centrocosto.getCentrocosto(),
				centrocosto.getNombrecentrocosto(),
				centrocosto.getCuentacontablecargo(),
				centrocosto.getCuentacontableabono(),
				centrocosto.getPorcentaje(),
				centrocosto.getEstado(),
				centrocosto.getCreacionUsuario(),
				centrocosto.getModificacionUsuario()
				);
	}

	@Override
	public void editar(CentroCosto centrocosto) {
		jdbcTemplate.update("update T_CON_CENTROCOSTO set CCONOMBRE = ?, CTACODCARGO = ?, CTACODABONO = ?, CCOPORCENTAJE = ?, "
				+ " CCOESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDEJECONTABLE = ? and CCOCOD = ?  ",				
				centrocosto.getNombrecentrocosto(),
				centrocosto.getCuentacontablecargo(),
				centrocosto.getCuentacontableabono(),
				centrocosto.getPorcentaje(),
				centrocosto.getEstado(),
				centrocosto.getModificacionUsuario(),
				centrocosto.getEmpresa(),
				centrocosto.getEjerciciocontable(),
				centrocosto.getCentrocosto()
				);	
		
	}

	@Override
	public int eliminar(CentroCosto centrocosto) {
		return jdbcTemplate.update("delete from T_CON_CENTROCOSTO " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and CCOCOD = ? ", 
				centrocosto.getEmpresa(),
				centrocosto.getEjerciciocontable(),
				centrocosto.getCentrocosto());	
	}

	@Override
	public CentroCosto consultar(Integer empresa, Integer ejerciciocontable, String centrocosto) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, centrocosto);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.CCOCOD, t1.CCONOMBRE " +
						", t1.CTACODCARGO, t1.CTACODABONO, t1.CCOPORCENTAJE, t1.CCOESTADO " +
						", t2.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_CENTROCOSTO t1 " +	
						"left outer join V_T_ESTADOREG t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.CCOESTADO = t2.IDESTADO " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? " +
						"and t1.ccocod = ? ", 
						new CentroCostoExtractor(), 
						new Object[] {empresa, ejerciciocontable, centrocosto});
		} else {
			return new CentroCosto();
		} 		
		
		
	}
	
	@Override
	public List<CentroCosto> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.CCOCOD, t1.CCONOMBRE " +
						", ISNULL(t1.CTACODCARGO,'') + '-' + ISNULL(t3.CTADESCRIP,'') AS CARGO " + 
						", ISNULL(t1.CTACODABONO,'') + '-' + ISNULL(t4.CTADESCRIP,'') AS ABONO " + 
						", t1.CCOPORCENTAJE, t1.CCOESTADO " +
						", t2.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_CENTROCOSTO t1 " +	
						"left outer join V_T_ESTADOREG t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.CCOESTADO = t2.IDESTADO " +
						"left outer join T_CON_PLANCONTABLE t3 " +
						"on t1.IDEMPCOD = t3.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t3.IDEJECONTABLE " +
						"and t1.CTACODCARGO = t3.CTACOD " +
						"left outer join T_CON_PLANCONTABLE t4 " +
						"on t1.IDEMPCOD = t4.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t4.IDEJECONTABLE " +
						"and t1.CTACODABONO = t4.CTACOD " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? ", 
					new CentroCostoRowMapper(), new Object[] {empresa, ejerciciocontable});
				
	}
		
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String centrocosto) {
		
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_CON_CENTROCOSTO " +
				"where idempcod = ?  " + 
					"and idejecontable = ? " +
					"and ccocod = ? ", 
					new ValorContadorExtractor(), 
					new Object[] {empresa, ejerciciocontable, centrocosto});
	}
	
	@Override
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable){
		
		return jdbcTemplate.query(
				"select t1.CCOCOD as codigo, t1.CCOCOD + '-' + t1.CCONOMBRE as descripcion " +
					", t2.DESCRIPCION as estado " +
					"from T_CON_CENTROCOSTO t1 " +
					"left outer join V_T_ESTADOREG t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.CCOESTADO = t2.IDESTADO " +		
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and t1.CCOESTADO = 'A' " +
				"order by t1.CCOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable});
	}	
	
	@Override
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable, String centrocosto){
		
		return jdbcTemplate.query(
				"select t1.CCOCOD as codigo, t1.CCOCOD + '-' + t1.CCONOMBRE as descripcion " +
				", t2.DESCRIPCION as estado " +
				"from T_CON_CENTROCOSTO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.CCOESTADO = t2.IDESTADO " +
				"where t1.idempcod = ?  " + 
					"and t1.idejecontable = ? " +
					"and (t1.CCOCOD like ? or t1.CCONOMBRE like ?) " +
				"order by t1.CCOCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable, centrocosto, centrocosto});
	}
	@Override
	public List<Catalogo> listarNotInCatalogo(CentroCosto centrocosto){
		String 	excluir;
		excluir = centrocosto.getDescripcionestado();
		excluir = excluir.trim();
		if (excluir == null || excluir == "")
		{
			return jdbcTemplate.query(
					"select t1.CCOCOD as codigo, t1.CCOCOD + '-' + t1.CCONOMBRE as descripcion " +
						", t2.DESCRIPCION as estado " +
						"from T_CON_CENTROCOSTO t1 " +
						"left outer join V_T_ESTADOREG t2 " +
							"on t1.IDEMPCOD = t2.IDEMPCOD " +
							"and t1.CCOESTADO = t2.IDESTADO " +		
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? " +
						"and t1.CCOESTADO = 'A' " +
					"order by t1.CCOCOD ",
						new CatalogoRowMapper(), 
						new Object[] {centrocosto.getEmpresa(), centrocosto.getEjerciciocontable()});
		}
		else
		{
			return jdbcTemplate.query(
					"select t1.CCOCOD as codigo, t1.CCOCOD + '-' + t1.CCONOMBRE as descripcion " +
						", t2.DESCRIPCION as estado " +
						"from T_CON_CENTROCOSTO t1 " +
						"left outer join V_T_ESTADOREG t2 " +
							"on t1.IDEMPCOD = t2.IDEMPCOD " +
							"and t1.CCOESTADO = t2.IDESTADO " +		
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? " +
						"and t1.CCOESTADO = 'A' " +
						"and t1.CCOCOD not in (" + excluir + ")" + 
					"order by t1.CCOCOD ",
						new CatalogoRowMapper(), 
						new Object[] {centrocosto.getEmpresa(), centrocosto.getEjerciciocontable()});
		}
	}
	
}
