package com.siegez.daos.implementation;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.ConsultaDao;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.extractors.ValoresInicialesExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.ValoresInicialesRowMapper;
import com.siegez.rowmapper.mappers.VistaCentroCostoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;
import com.siegez.views.ValoresIniciales;
import com.siegez.views.VistaCentroCosto;

@Repository
public class ConsultaDaoImp implements ConsultaDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public ValoresIniciales findValoresIniciales(String username) {
		return jdbcTemplate.query("select top 1 t1.IDLICCOD, t1.USERNAME, t1.USERFULLNAME, " 
				+ " t3.IDEMPCOD, t3.EMPDESCRIP, t3.EMPRUC, t4.IDPEFCOD, t4.PEFDESCRIP, t5.PARVALOR "
				+ " from t_m_usuario t1 "
				+ " left outer join T_M_EMPRESAPERFILUSUARIO t2 "
				+ " on t1.USERNAME = t2.USERNAME "
				+ " and t2.EXUDEFECTO = 'A' "
				+ " left outer join T_M_EMPRESA T3 "
				+ " on t2.IDEMPCOD = t3.IDEMPCOD "
				+ " left outer join T_M_PERFIL t4 "
				+ " on t2.IDPEFCOD = t4.IDPEFCOD "
				+ " left outer join T_M_PARAMETRO t5 "
				+ " on t2.IDEMPCOD = t5.IDEMPCOD "
				+ " and t5.IDPARCOD = 'ANNO' "
				+ " where t1.username = ? ", new ValoresInicialesExtractor(), username);
				
	}
	
	@Override
	public List<ValoresIniciales> findAllValoresIniciales(String username) {
		return jdbcTemplate.query("select t1.IDLICCOD, t1.USERNAME, t1.USERFULLNAME, t3.IDEMPCOD, t3.EMPDESCRIP, "
				+ " t3.EMPRUC, t4.IDPEFCOD, t4.PEFDESCRIP, t5.PARVALOR "
				+ " from t_m_usuario t1 "
				+ " left outer join T_M_EMPRESAPERFILUSUARIO t2 "
				+ " on t1.USERNAME = t2.USERNAME "
				+ " and t2.EXUDEFECTO = 'A' "
				+ " left outer join T_M_EMPRESA T3 "
				+ " on t2.IDEMPCOD = t3.IDEMPCOD "
				+ " left outer join T_M_PERFIL t4 "
				+ " on t2.IDPEFCOD = t4.IDPEFCOD "
				+ " left outer join T_M_PARAMETRO t5 "
				+ " on t2.IDEMPCOD = t5.IDEMPCOD "
				+ " and t5.IDPARCOD = 'ANNO' "
				+ " where t1.username = ? "
				, new ValoresInicialesRowMapper()
				, username);
				
	}

	@Override
	public ValorContador permitirDocumentoReferencia(Integer empresa, Integer ejerciciocontable, String tipoodocumento, String moneda) {
		return jdbcTemplate.query("select count(1) "
				+ " from T_CON_TIPODOCMON_CUENTA t1 "
				+ " left outer join T_CON_PLANCONTABLE t2 "
				+ " on t1.IDEMPCOD = t2.IDEMPCOD "
				+ " and t1.IDEJECONTABLE = t2.IDEJECONTABLE "
				+ " and t1.CTACODCOMPRA = t2.CTACOD "
				+ " where t1.IDEMPCOD = ? "
				+ " and t1.IDEJECONTABLE = ? "
				+ " and t1.TDOCOD = ? "
				+ " and T1.IDMONEDA = ? "
				+ " and t2.CTAINDDOC = 'S' ", new ValorContadorExtractor(), empresa, ejerciciocontable, tipoodocumento, moneda);		
				
	}	
	
	@Override
	public List<VistaCentroCosto> listarCentroCosto(Integer empresa, Integer ejerciciocontable){
		return jdbcTemplate.query("select IDEMPCOD, IDEJECONTABLE, CODIGO, DESCRIPCION, TIPO, DESCRIPCIONTIPO " +
				" from V_M_CENTROCOSTO " + 
				" where IDEMPCOD = ? and IDEJECONTABLE = ? "
				, new VistaCentroCostoRowMapper() 
				, new Object[] {empresa, ejerciciocontable});
				
	}
	
	@Override
	public List<Catalogo> listarUbicacionGeografica(){
		
		return jdbcTemplate.query(
				" select t1.UBIGEOCOD as codigo, " +
				" t1.UBIDISTRITO + '-' + t1.UBIPROVINCIA + '-' + t1.UBIDEPARTAMENTO as descripcion, " + 
				" 'Activo' as estado " +
				" from T_M_UBIGEO t1 " +					
				" order by t1.UBIDISTRITO, t1.UBIPROVINCIA, t1.UBIDEPARTAMENTO ",
					new CatalogoRowMapper(), 
					new Object[] {});
	}

}
