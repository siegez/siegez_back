package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.GrupoCentroCostoDetalleDao;
import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.rowmapper.extractors.GrupoCentroCostoDetalleExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.GrupoCentroCostoDetalleRowMapper;
import com.siegez.views.ValorContador;

@Repository
public class GrupoCentroCostoDetalleDaoImp implements GrupoCentroCostoDetalleDao {
	
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		jdbcTemplate.update("insert into T_CON_GRUPOCENTROCOSTO_DET (IDEMPCOD, IDEJECONTABLE, GCCCOD " 
				+ ", CCOCOD, DGCPORCENTAJE, DGCESTADO "
				+ ", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " 
				+ " values (?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				grupocentrocostodetalle.getEmpresa(),
				grupocentrocostodetalle.getEjerciciocontable(),
				grupocentrocostodetalle.getGrupocentrocosto(),
				grupocentrocostodetalle.getCentrocosto(),
				grupocentrocostodetalle.getPorcentaje(),
				grupocentrocostodetalle.getEstado(),
				grupocentrocostodetalle.getCreacionUsuario(),
				grupocentrocostodetalle.getModificacionUsuario()
				);
	}

	@Override
	public void editar(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		jdbcTemplate.update("update T_CON_GRUPOCENTROCOSTO_DET set "
				+ " DGCESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ? and CCOCOD= ? ",				
				grupocentrocostodetalle.getEstado(),
				grupocentrocostodetalle.getModificacionUsuario(),
				grupocentrocostodetalle.getEmpresa(),
				grupocentrocostodetalle.getEjerciciocontable(),
				grupocentrocostodetalle.getGrupocentrocosto(),
				grupocentrocostodetalle.getCentrocosto()
				);	
		
	}

	@Override
	public int eliminar(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		return jdbcTemplate.update("delete from T_CON_GRUPOCENTROCOSTO_DET " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and GCCCOD = ? and CCOCOD= ? ", 
				grupocentrocostodetalle.getEmpresa(),
				grupocentrocostodetalle.getEjerciciocontable(),
				grupocentrocostodetalle.getGrupocentrocosto(),
				grupocentrocostodetalle.getCentrocosto());	
	}
	
	@Override
	public GrupoCentroCostoDetalle consultar(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto) {
		
		ValorContador valorcontador = contadorRegistros(empresa, ejerciciocontable, grupocentrocosto, centrocosto);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query(
					"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.GCCCOD, t2.GCCNOMBRE, t1.CCOCOD, t3.CCONOMBRE " +
						", t3.CTACODCARGO, t3.CTACODABONO " +
						", t1.DGCPORCENTAJE, t1.DGCESTADO " +
						", t4.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_GRUPOCENTROCOSTO_DET t1 " +	
						"left outer join T_CON_GRUPOCENTROCOSTO t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t2.IDEJECONTABLE " +
						"and t1.GCCCOD = t2.GCCCOD " +
						"left outer join T_CON_CENTROCOSTO t3 " +
						"on t1.IDEMPCOD = t3.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t3.IDEJECONTABLE " +
						"and t1.CCOCOD = t3.CCOCOD " +
						"left outer join V_T_ESTADOREG t4 " +
						"on t1.IDEMPCOD = t4.IDEMPCOD " +
						"and t1.DGCESTADO = t4.IDESTADO " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? " +
						"and t1.gcccod = ? and t1.ccocod = ? ", 
						new GrupoCentroCostoDetalleExtractor(), 
						new Object[] {empresa, ejerciciocontable, grupocentrocosto, centrocosto});
		} else {
			return new GrupoCentroCostoDetalle();
		} 				
		
	}
	
	@Override
	public List<GrupoCentroCostoDetalle> consultarLista(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.IDEJECONTABLE, t1.GCCCOD, t2.GCCNOMBRE, t1.CCOCOD, t1.CCOCOD + '-' + t3.CCONOMBRE as CCONOMBRE " +
						", t3.CTACODCARGO, t3.CTACODABONO " +
						", t1.DGCPORCENTAJE, t1.DGCESTADO " +
						", t4.DESCRIPCION as descripcionestado " +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_CON_GRUPOCENTROCOSTO_DET t1 " +	
						"left outer join T_CON_GRUPOCENTROCOSTO t2 " +
						"on t1.IDEMPCOD = t2.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t2.IDEJECONTABLE " +
						"and t1.GCCCOD = t2.GCCCOD " +
						"left outer join T_CON_CENTROCOSTO t3 " +
						"on t1.IDEMPCOD = t3.IDEMPCOD " +
						"and t1.IDEJECONTABLE = t3.IDEJECONTABLE " +
						"and t1.CCOCOD = t3.CCOCOD " +
						"left outer join V_T_ESTADOREG t4 " +
						"on t1.IDEMPCOD = t4.IDEMPCOD " +
						"and t1.DGCESTADO = t4.IDESTADO " +
					"where t1.idempcod = ?  " + 
						"and t1.idejecontable = ? and t1.gcccod = ? ", 
					new GrupoCentroCostoDetalleRowMapper(), 
					new Object[] {empresa, ejerciciocontable, grupocentrocosto});
		}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto){
		
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_CON_GRUPOCENTROCOSTO_DET " +
				"where idempcod = ?  " + 
					"and idejecontable = ? " +
					"and gcccod = ? and ccocod = ? ", 
					new ValorContadorExtractor(), 
					new Object[] {empresa, ejerciciocontable, grupocentrocosto, centrocosto});
	}
	
}
