package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.siegez.daos.interfaces.EnteSocioNegocioDao;
import com.siegez.models.EnteSocioNegocio;
import com.siegez.rowmapper.extractors.EnteSocioNegocioExtractor;
import com.siegez.rowmapper.mappers.EnteSocioNegocioRowMapper;

@Repository
public class EnteSocioNegocioDaoImp implements EnteSocioNegocioDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertar(EnteSocioNegocio entesocionegocio) {
		jdbcTemplate.update("insert into T_M_ENTE_SOCIONEGOCIO (" +
				" IDEMPCOD, ENTCOD, ESNVEZ, IDSOCIONEGOCIO " +				
				", ESNESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				entesocionegocio.getEmpresa(),	
				entesocionegocio.getEntidad(),
				entesocionegocio.getVez(),
				entesocionegocio.getSocionegocio(),
				entesocionegocio.getEstado(),
				entesocionegocio.getCreacionUsuario(),
				entesocionegocio.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(EnteSocioNegocio entesocionegocio) {
		jdbcTemplate.update("update T_M_ENTE_SOCIONEGOCIO set " +
				"  IDSOCIONEGOCIO = ? " +				
				", ESNESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate())) " +
				"  where IDEMPCOD = ? and ENTCOD = ? and ESNVEZ = ?  ",
				entesocionegocio.getSocionegocio(),
				entesocionegocio.getEstado(),
				entesocionegocio.getModificacionUsuario(),
				entesocionegocio.getEmpresa(),	
				entesocionegocio.getEntidad(),
				entesocionegocio.getVez()
				);	
		
	}	
	
	@Override
	public int eliminar(EnteSocioNegocio entesocionegocio) {
		return jdbcTemplate.update("delete from T_M_ENTE_SOCIONEGOCIO " 
				+ " where IDEMPCOD = ? and ENTCOD = ? and ESNVEZ = ? ", 
				entesocionegocio.getEmpresa(),	
				entesocionegocio.getEntidad(),
				entesocionegocio.getVez());	
	}
	
	@Override
	public EnteSocioNegocio consultar(Integer empresa, String entidad, Integer vez) {
		
		return jdbcTemplate.query(
				"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.ESNVEZ, t1.IDSOCIONEGOCIO, '' as descripcionsocionegocio " +
				", t1.ESNESTADO, '' as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +  
				"from T_M_ENTE_SOCIONEGOCIO t1 " +
				"where t1.IDEMPCOD = ? and t1.ENTCOD = ? and t1.ESNVEZ = ? ", 
				new EnteSocioNegocioExtractor(), empresa, entidad, vez);
	}
	
	@Override
	public List<EnteSocioNegocio> consultarLista(Integer empresa, String entidad) {
	
		return jdbcTemplate.query(
			"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.ESNVEZ, t1.IDSOCIONEGOCIO, t2.DESCRIPCION as descripcionsocionegocio " +
				", t1.ESNESTADO, t3.DESCRIPCION as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_ENTE_SOCIONEGOCIO t1 " +	
				" left outer join V_T_SOCIONEGOCIO t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDSOCIONEGOCIO = t2.IDSOCIONEGOCIO " +
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.ESNESTADO = t3.IDESTADO " +				
			"where t1.IDEMPCOD = ? and t1.ENTCOD = ?  ", 
				new EnteSocioNegocioRowMapper(), new Object[] {empresa, entidad});
		
	}
	
	
}
