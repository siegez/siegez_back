package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.EnteDocumentoIdentidadDao;
import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.rowmapper.extractors.EnteDocumentoIdentidadExtractor;
import com.siegez.rowmapper.mappers.EnteDocumentoIdentidadRowMapper;

@Repository
public class EnteDocumentoIdentidadDaoImp implements EnteDocumentoIdentidadDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertar(EnteDocumentoIdentidad entedocumentoidentidad) {
		jdbcTemplate.update("insert into T_M_ENTE_DOCIDENTIDAD (" +
				" IDEMPCOD, ENTCOD, EDIVEZ, IDDOCIDENTIDAD, EDINUMERO " +				
				", EDIESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				entedocumentoidentidad.getEmpresa(),	
				entedocumentoidentidad.getEntidad(),
				entedocumentoidentidad.getVez(),
				entedocumentoidentidad.getDocumentoidentidad(),
				entedocumentoidentidad.getNumerodocumento(),
				entedocumentoidentidad.getEstado(),
				entedocumentoidentidad.getCreacionUsuario(),
				entedocumentoidentidad.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(EnteDocumentoIdentidad entedocumentoidentidad) {
		jdbcTemplate.update("update T_M_ENTE_DOCIDENTIDAD set " +
				"  IDDOCIDENTIDAD = ?, EDINUMERO = ? " +				
				", EDIESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate())) " +
				"  where IDEMPCOD = ? and ENTCOD = ? and EDIVEZ = ?  ",
				entedocumentoidentidad.getDocumentoidentidad(),
				entedocumentoidentidad.getNumerodocumento(),
				entedocumentoidentidad.getEstado(),
				entedocumentoidentidad.getModificacionUsuario(),
				entedocumentoidentidad.getEmpresa(),	
				entedocumentoidentidad.getEntidad(),
				entedocumentoidentidad.getVez()
				);	
		
	}	
	
	@Override
	public int eliminar(EnteDocumentoIdentidad entedocumentoidentidad) {
		return jdbcTemplate.update("delete from T_M_ENTE_DOCIDENTIDAD " 
				+ " where IDEMPCOD = ? and ENTCOD = ? and EDIVEZ = ? ", 
				entedocumentoidentidad.getEmpresa(),	
				entedocumentoidentidad.getEntidad(),
				entedocumentoidentidad.getVez());	
	}
	
	@Override
	public EnteDocumentoIdentidad consultar(Integer empresa, String entidad, Integer vez) {
		
		return jdbcTemplate.query(
				"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.EDIVEZ, t1.IDDOCIDENTIDAD, '' as descripciontipodocumento " +
				", t1.EDINUMERO " +
				", t1.EDIESTADO, '' as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +  
				"from T_M_ENTE_DOCIDENTIDAD t1 " +
				"where t1.IDEMPCOD = ? and t1.ENTCOD = ? and t1.EDIVEZ = ? ", 
				new EnteDocumentoIdentidadExtractor(), empresa, entidad, vez);
	}
	
	@Override
	public List<EnteDocumentoIdentidad> consultarLista(Integer empresa, String entidad) {
	
		return jdbcTemplate.query(
			"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.EDIVEZ, t1.IDDOCIDENTIDAD, t2.DESCRIPCION as descripciontipodocumento " +
				", t1.EDINUMERO " +
				", t1.EDIESTADO, t3.DESCRIPCION as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_ENTE_DOCIDENTIDAD t1 " +	
				" left outer join V_T_TIPODOCUMENTOIDENTIDAD t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDDOCIDENTIDAD = t2.IDDOCIDENTIDAD " +
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.EDIESTADO = t3.IDESTADO " +				
			"where t1.IDEMPCOD = ? and t1.ENTCOD = ?  ", 
				new EnteDocumentoIdentidadRowMapper(), new Object[] {empresa, entidad});
		
	}
}
