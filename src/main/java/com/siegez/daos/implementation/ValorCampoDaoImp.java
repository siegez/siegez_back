package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.ValorCampoDao;
import com.siegez.models.ValorCampo;
import com.siegez.rowmapper.mappers.ValorCampoRowMapper;

@Repository
public class ValorCampoDaoImp implements ValorCampoDao{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void save(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValorCampo find(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int delete(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ValorCampo> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ValorCampo> findAllValorDescripcion(ValorCampo valorcampo) {
		return jdbcTemplate.query("select t1.IDEMPCOD, t1.IDVALFUN, t1.IDVALCOD, t1.VALVALOR, substring(t1.VALDESCRIP, 1,80) as VALDESCRIP, " +
				" t1.VALABREVIA, t1.VALREFCOD, t1.VALESTADO, " + 
				"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 			
				"				from T_M_VALORCAMPO t1 " + 
				"				where t1.IDEMPCOD = ? and t1.IDVALFUN = ? and t1.IDVALCOD = ? " +
				"				order by t1.VALDESCRIP ", 
				new ValorCampoRowMapper(), 
				new Object[] {valorcampo.getEmpresa(), valorcampo.getAmbito(), valorcampo.getCampo()});
	}
	
	@Override
	public List<ValorCampo> findNotInValorDescripcion(ValorCampo valorcampo) {
		
		String 	excluir;
		excluir = valorcampo.getValor();
		excluir = excluir.trim();
		if (excluir == null || excluir == "")
		{
			return jdbcTemplate.query("select t1.IDEMPCOD, t1.IDVALFUN, t1.IDVALCOD, t1.VALVALOR, substring(t1.VALDESCRIP, 1,80) as VALDESCRIP, t1.VALABREVIA, t1.VALREFCOD, t1.VALESTADO, " + 
					"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 			
					"				from T_M_VALORCAMPO t1 " + 
					"				where t1.IDEMPCOD = ? and t1.IDVALFUN = ? and t1.IDVALCOD = ? " + 
					"				order by t1.VALDESCRIP ", 
					new ValorCampoRowMapper(), 
					new Object[] {valorcampo.getEmpresa(), valorcampo.getAmbito(), valorcampo.getCampo()});
		}
		else
		{
			return jdbcTemplate.query("select t1.IDEMPCOD, t1.IDVALFUN, t1.IDVALCOD, t1.VALVALOR, substring(t1.VALDESCRIP, 1,80) as VALDESCRIP, t1.VALABREVIA, t1.VALREFCOD, t1.VALESTADO, " + 
					"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 			
					"				from T_M_VALORCAMPO t1 " + 
					"				where t1.IDEMPCOD = ? and t1.IDVALFUN = ? and t1.IDVALCOD = ? and t1.VALVALOR not in (" + excluir + ")" + 
					"				order by t1.VALDESCRIP ", 
					new ValorCampoRowMapper(), 
					new Object[] {valorcampo.getEmpresa(), valorcampo.getAmbito(), valorcampo.getCampo()});
		}
	}
	
	@Override
	public List<ValorCampo> findAllValorCodigo(ValorCampo valorcampo) {
		return jdbcTemplate.query("select t1.IDEMPCOD, t1.IDVALFUN, t1.IDVALCOD, t1.VALVALOR, substring(t1.VALDESCRIP, 1,80) as VALDESCRIP, t1.VALABREVIA, t1.VALREFCOD, t1.VALESTADO, " + 
				"				t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 			
				"				from T_M_VALORCAMPO t1 " + 
				"				where t1.IDEMPCOD = ? and t1.IDVALFUN = ? and t1.IDVALCOD = ? " +
				"				order by t1.VALVALOR ", 
				new ValorCampoRowMapper(), 
				new Object[] {valorcampo.getEmpresa(), valorcampo.getAmbito(), valorcampo.getCampo()});
	}	

}
