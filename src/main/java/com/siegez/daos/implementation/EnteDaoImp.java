package com.siegez.daos.implementation;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.siegez.daos.interfaces.EnteDao;
import com.siegez.models.Ente;
import com.siegez.models.EnteDireccion;
import com.siegez.models.EnteRegistro;
import com.siegez.models.EnteSocioNegocio;
import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.rowmapper.extractors.EnteExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.EnteRowMapper;
import com.siegez.rowmapper.mappers.SocioNegocioRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.SocioNegocio;
import com.siegez.views.ValorContador;

@Repository
public class EnteDaoImp implements EnteDao {
	
	private JdbcTemplate jdbcTemplate;
	//	private SimpleJdbcCall usuarioSp; //if we want to work with stored procedures
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	//	this.usuarioSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	@Transactional
	public ResultadoSP registrarEnte(EnteRegistro enteregistro) {
		
		String 	codigoentidad;
		String 	mensaje;
		
		codigoentidad = null;
		mensaje = "No Exitoso, revisar datos";
		
		//System.out.println(out.get("idopenum"));
		//System.out.println(listaregistrocontable);
		
		// Transacción		
		// Asignar lista de socio de negocio
		List<EnteSocioNegocio> listasocionegocio = (List<EnteSocioNegocio>) enteregistro.getEntesocionegocio();
		List<EnteDocumentoIdentidad> listadocumentoidentidad = (List<EnteDocumentoIdentidad>) enteregistro.getEntedocumentoidentidad();
		List<EnteDireccion> listadireccion = (List<EnteDireccion>) enteregistro.getEntedireccion();
		
		// Validar nuevo o edición -- || OR; && AND

		if (enteregistro.getEntidad() == null || enteregistro.getEntidad() == "") {
			// NUEVA ENTIDAD	
			// obtener código			
			codigoentidad = jdbcTemplate.queryForObject(" SELECT replicate('0', (8 - len(isnull(max(entcod),0) + 1))) + convert(varchar,isnull(max(entcod),0) + 1) FROM T_M_ENTE " + 
															 " WHERE IDEMPCOD = ? ", 
															 new Object[]{enteregistro.getEmpresa()}, String.class); 
			enteregistro.setEntidad(codigoentidad);
			
			jdbcTemplate.update("insert into T_M_ENTE (IDEMPCOD, ENTCOD, ENTPATERNO, ENTMATERNO, ENTNOMBRES " + 
					", IDTIPOPERSONA, ENTRUC, ENTINDNODOM, IDPAIS, ENTESTADO " + 
					", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
					+ " values (?,?,?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					enteregistro.getEmpresa(),
					enteregistro.getEntidad(),
					enteregistro.getApellidopaterno(),
					enteregistro.getApellidomaterno(),
					enteregistro.getNombres(),
					enteregistro.getTipopersona(),
					enteregistro.getRuc(),
					enteregistro.getNodomiciliado(),
					enteregistro.getPais(),				
					enteregistro.getEstado(),
					enteregistro.getCreacionUsuario(),
					enteregistro.getModificacionUsuario()
					);
			
		}
		else
		{
			jdbcTemplate.update("update T_M_ENTE set ENTPATERNO = ?, ENTMATERNO = ?, ENTNOMBRES = ?, "
					+ "IDTIPOPERSONA = ?, ENTRUC = ?, ENTINDNODOM = ?, IDPAIS = ?, ENTESTADO = ?, "
					+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
					+ "	where IDEMPCOD = ? and ENTCOD = ? ",				
					enteregistro.getApellidopaterno(),
					enteregistro.getApellidomaterno(),
					enteregistro.getNombres(),
					enteregistro.getTipopersona(),
					enteregistro.getRuc(),
					enteregistro.getNodomiciliado(),
					enteregistro.getPais(),				
					enteregistro.getEstado(),
					enteregistro.getModificacionUsuario(),
					enteregistro.getEmpresa(),
					enteregistro.getEntidad()
					);
		}
		// eliminar socio de negocio 
		jdbcTemplate.update("delete from T_M_ENTE_SOCIONEGOCIO " 
				+ " where IDEMPCOD = ? and ENTCOD = ? ", 
				enteregistro.getEmpresa(),
				enteregistro.getEntidad());	
		
		// insertar socio de negocio
		for(int i=0; i<listasocionegocio.size(); i++)
		{
			
			EnteSocioNegocio entesocionegocio = listasocionegocio.get(i);
			
			jdbcTemplate.update("insert into T_M_ENTE_SOCIONEGOCIO (" +
					" IDEMPCOD, ENTCOD, ESNVEZ, IDSOCIONEGOCIO " +				
					", ESNESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
					" values (?, ?, ?, ?, " +
					" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					entesocionegocio.getEmpresa(),	
					entesocionegocio.getEntidad(),
					i+1,
					entesocionegocio.getSocionegocio(),
					entesocionegocio.getEstado(),
					enteregistro.getModificacionUsuario(),
					enteregistro.getModificacionUsuario()
					);      				
        }				
				
		// eliminar documento de identidad 
		jdbcTemplate.update("delete from T_M_ENTE_DOCIDENTIDAD " 
				+ " where IDEMPCOD = ? and ENTCOD = ? ", 
				enteregistro.getEmpresa(),
				enteregistro.getEntidad());	
		
		// insertar documento de identidad 
		for(int i=0; i<listadocumentoidentidad.size(); i++)
		{
			
			EnteDocumentoIdentidad entedocumentoidentidad = listadocumentoidentidad.get(i);
			
			jdbcTemplate.update("insert into T_M_ENTE_DOCIDENTIDAD (" +
					" IDEMPCOD, ENTCOD, EDIVEZ, IDDOCIDENTIDAD, EDINUMERO " +				
					", EDIESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
					" values (?, ?, ?, ?, ?, " +
					" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					entedocumentoidentidad.getEmpresa(),	
					entedocumentoidentidad.getEntidad(),
					i+1,
					entedocumentoidentidad.getDocumentoidentidad(),
					entedocumentoidentidad.getNumerodocumento(),
					entedocumentoidentidad.getEstado(),
					enteregistro.getModificacionUsuario(),
					enteregistro.getModificacionUsuario()
					);     				
        }	
		
		// eliminar dirección 
		jdbcTemplate.update("delete from T_M_ENTE_DIRECCION " 
				+ " where IDEMPCOD = ? and ENTCOD = ? ", 
				enteregistro.getEmpresa(),
				enteregistro.getEntidad());	
		
		// insertar dirección 
		for(int i=0; i<listadireccion.size(); i++)
		{
			
			EnteDireccion entedireccion = listadireccion.get(i);
			jdbcTemplate.update("insert into T_M_ENTE_DIRECCION (" +
					" IDEMPCOD, ENTCOD, ENDVEZ, IDTIPODOMICILIO, PAISCOD, UBIGEOCOD, DIRECCION " +				
					", ENDESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
					" values (?, ?, ?, ?, ?, ?, ?, " +
					" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
					entedireccion.getEmpresa(),	
					entedireccion.getEntidad(),
					i+1,
					entedireccion.getTipodomicilio(),
					entedireccion.getPais(),
					entedireccion.getUbigeo(),
					entedireccion.getDireccion(),
					entedireccion.getEstado(),
					enteregistro.getModificacionUsuario(),
					enteregistro.getModificacionUsuario()
					);     				
        }

		mensaje = "Grabación exitosa";
		return new ResultadoSP(0, enteregistro.getEntidad(), mensaje, "S");
	}
	
	@Override
	public void insertar(Ente ente) {
		jdbcTemplate.update("insert into T_M_ENTE (IDEMPCOD, ENTCOD, ENTPATERNO, ENTMATERNO, ENTNOMBRES " + 
				", IDTIPOPERSONA, ENTRUC, ENTINDNODOM, IDPAIS, ENTESTADO " + 
				", MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) "
				+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				ente.getEmpresa(),
				ente.getEntidad(),
				ente.getApellidopaterno(),
				ente.getApellidomaterno(),
				ente.getNombres(),
				ente.getTipopersona(),
				ente.getRuc(),
				ente.getNodomiciliado(),
				ente.getPais(),				
				ente.getEstado(),
				ente.getCreacionUsuario(),
				ente.getModificacionUsuario()
				);
	}

	@Override
	public void editar(Ente ente) {
		jdbcTemplate.update("update T_M_ENTE set ENTPATERNO = ?, ENTMATERNO = ?, ENTNOMBRES = ?, "
				+ "IDTIPOPERSONA = ?, ENTRUC = ?, ENTINDNODOM = ?, IDPAIS = ?, ENTESTADO = ?, "
				+ "	MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) "
				+ "	where IDEMPCOD = ? and ENTCOD = ? ",				
				ente.getApellidopaterno(),
				ente.getApellidomaterno(),
				ente.getNombres(),
				ente.getTipopersona(),
				ente.getRuc(),
				ente.getNodomiciliado(),
				ente.getPais(),				
				ente.getEstado(),
				ente.getCreacionUsuario(),
				ente.getModificacionUsuario(),
				ente.getEmpresa(),
				ente.getEntidad()
				);	
		
	}
	
	@Override
	public int eliminar(Ente ente) {
		return jdbcTemplate.update("delete from T_M_ENTE " 
				+ " where IDEMPCOD = ? and ENTCOD = ? ", 
				ente.getEmpresa(),
				ente.getEntidad());	
	}
	
	@Override
	public Ente consultar(Integer empresa, String entidad) {
		

		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.ENTCOD, t1.ENTPATERNO, t1.ENTMATERNO, t1.ENTNOMBRES " +
					", t1.IDTIPOPERSONA, t2.VALDESCRIP as descripciontipopersona, t1.ENTRUC, t1.ENTINDNODOM, '' " +
					", t1.IDPAIS, t3.DETDESCRIP as nombrepais " +
					", t1.ENTESTADO, t4.VALDESCRIP as descripcionestado" +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
				"from T_M_ENTE t1 " +
					" left outer join T_M_VALORCAMPO t2 " +
					"	on t1.IDEMPCOD = t2.IDEMPCOD " +
					"	and t1.IDTIPOPERSONA = t2.VALVALOR " +
					"	and t2.IDVALFUN = 'SHA' " +
					"	and t2.IDVALCOD = 'IDTIPOPERSONA'	 " +	
					" left outer join T_M_TABLADETALLE t3 " +
					"	on t1.IDEMPCOD = t3.IDEMPCOD " +
					"	and t3.IDTABCOD = 1 " +
					"	and t1.IDPAIS = t3.DETVALOR " +
					" left outer join T_M_VALORCAMPO t4 " +
					"	on t1.IDEMPCOD = t4.IDEMPCOD  " +
					"	and t1.ENTESTADO = t4.VALVALOR " +
					"	and t4.IDVALFUN = 'SHA' " +
					"	and t4.IDVALCOD = 'ESTADO' " +
				"where t1.idempcod = ? and t1.ENTCOD = ?", 
				new EnteExtractor(), empresa, entidad);
	}

	@Override
	public Ente consultarRucAnexo(Integer empresa, String ruc, String anexo) {
		
		String 	cadena;
		int		contador;
		
		cadena = "select Count(1) from T_M_ENTE t1 " ;

		switch (anexo) {
		  case "P":
			cadena += " left outer join V_M_PROVEEDORACTIVO t5 ";
			break;
		  case "C":
			cadena += " left outer join V_M_CLIENTEACTIVO t5 ";
			break;
		  case "N":
			cadena += " left outer join V_M_PROVEEDORNODOMACTIVO t5 ";
			break;
		  default:
			cadena += " left outer join V_M_SOCIONEGOCIOACTIVO t5 ";
			break;
		}
		
		cadena += 	"	on t1.IDEMPCOD = t5.IDEMPCOD  " +
					"	and t1.ENTCOD = t5.ENTCOD " +
					" where t1.idempcod = ? and t1.ENTRUC = ? and not t5.ENTCOD is null";
		
		contador =  jdbcTemplate.queryForObject(cadena, new Object[] {empresa, ruc}, Integer.class);
		
		if (contador == 1)
		{		
			cadena = "select t1.IDEMPCOD, t1.ENTCOD, t1.ENTPATERNO, t1.ENTMATERNO, t1.ENTNOMBRES " +
					", t1.IDTIPOPERSONA, t2.VALDESCRIP as descripciontipopersona, t1.ENTRUC, t1.ENTINDNODOM, ''   " +
					", t1.IDPAIS, t3.DETDESCRIP as nombrepais " +
					", t1.ENTESTADO, t4.VALDESCRIP as descripcionestado" +
					", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_M_ENTE t1 " +
						" left outer join T_M_VALORCAMPO t2 " +
						"	on t1.IDEMPCOD = t2.IDEMPCOD " +
						"	and t1.IDTIPOPERSONA = t2.VALVALOR " +
						"	and t2.IDVALFUN = 'SHA' " +
						"	and t2.IDVALCOD = 'IDTIPOPERSONA'	 " +	
						" left outer join T_M_TABLADETALLE t3 " +
						"	on t1.IDEMPCOD = t3.IDEMPCOD " +
						"	and t3.IDTABCOD = 1 " +
						"	and t1.IDPAIS = t3.DETVALOR " +
						" left outer join T_M_VALORCAMPO t4 " +
						"	on t1.IDEMPCOD = t4.IDEMPCOD  " +
						"	and t1.ENTESTADO = t4.VALVALOR " +
						"	and t4.IDVALFUN = 'SHA' " +
						"	and t4.IDVALCOD = 'ESTADO' ";
	
			switch (anexo) {
			  case "P":
				cadena += " left outer join V_M_PROVEEDORACTIVO t5 ";
				break;
			  case "C":
				cadena += " left outer join V_M_CLIENTEACTIVO t5 ";
				break;
			  case "N":
				cadena += " left outer join V_M_PROVEEDORNODOMACTIVO t5 ";
				break;
			  default:
				cadena += " left outer join V_M_SOCIONEGOCIOACTIVO t5 ";
				break;
			}
			
			cadena += 	"	on t1.IDEMPCOD = t5.IDEMPCOD  " +
						"	and t1.ENTCOD = t5.ENTCOD " +
						" where t1.idempcod = ? and t1.ENTRUC = ? and not t5.ENTCOD is null";
			
			return jdbcTemplate.query(cadena, new EnteExtractor(), empresa, ruc);
		}
		else
		{
			return new Ente();
		}
		
	}
	
	@Override
	public List<Ente> consultarLista(Integer empresa) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.ENTCOD, t1.ENTPATERNO, t1.ENTMATERNO " + 
						", case when t1.IDTIPOPERSONA = 'N' then isnull(t1.ENTPATERNO,'') + ' ' + isnull(t1.ENTMATERNO,'') + ', '  + t1.ENTNOMBRES " + 
						"        else t1.ENTNOMBRES end as ENTNOMBRES" +
						", t1.IDTIPOPERSONA, t2.VALDESCRIP as descripciontipopersona, t1.ENTRUC " +
						", t1.ENTINDNODOM  " +
						", case when t1.ENTINDNODOM = 'S' then 'Si' when t1.ENTINDNODOM = 'N' then 'No' else '' end as descripcionnodomiciliado " +
						", t1.IDPAIS, t3.DETDESCRIP as nombrepais " +
						", t1.ENTESTADO, t4.VALDESCRIP as descripcionestado" +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_M_ENTE t1 " +
						" left outer join T_M_VALORCAMPO t2 " +
						"	on t1.IDEMPCOD = t2.IDEMPCOD " +
						"	and t1.IDTIPOPERSONA = t2.VALVALOR " +
						"	and t2.IDVALFUN = 'SHA' " +
						"	and t2.IDVALCOD = 'IDTIPOPERSONA'	 " +	
						" left outer join T_M_TABLADETALLE t3 " +
						"	on t1.IDEMPCOD = t3.IDEMPCOD " +
						"	and t3.IDTABCOD = 1 " +
						"	and t1.IDPAIS = t3.DETVALOR " +
						" left outer join T_M_VALORCAMPO t4 " +
						"	on t1.IDEMPCOD = t4.IDEMPCOD  " +
						"	and t1.ENTESTADO = t4.VALVALOR " +
						"	and t4.IDVALFUN = 'SHA' " +
						"	and t4.IDVALCOD = 'ESTADO' " +
					"where t1.idempcod = ? ", new EnteRowMapper(), new Object[] {empresa});
	}	
	
	@Override
	public List<SocioNegocio> consultarProveedor(Integer empresa){
	
		return jdbcTemplate.query(
				"select IDEMPCOD, ENTCOD, isnull(ENTRUC,''), PROVEEDOR  " +
					"from V_M_PROVEEDORACTIVO " +
					"where IDEMPCOD = ? " +
					"order by PROVEEDOR ", new SocioNegocioRowMapper(), new Object[] {empresa});
	}		
	
	@Override
	public List<Catalogo> consultarProveedor(Integer empresa, String criterio){
		
		String	criteriobusqueda;
		
		criteriobusqueda = '%' + criterio + '%';
		
		return jdbcTemplate.query(
				"select ENTRUC as codigo, PROVEEDOR as descripcion, ENTCOD as estado   " +
					"from V_M_PROVEEDORACTIVO " +
					"where IDEMPCOD = ? " +
					" and ( ENTRUC like ? or PROVEEDOR like ? or ENTCOD like ? ) " +
					"order by PROVEEDOR ", new CatalogoRowMapper(), new Object[] {empresa, criteriobusqueda, criteriobusqueda, criteriobusqueda});
	}
		
	@Override
	public ValorContador contadorRegistros(Ente ente) {
		
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_M_ENTE " +
				"where idempcod = ?  " + 
					"and entcod = ? " , 
					new ValorContadorExtractor(), 
					new Object[] {ente.getEmpresa(), ente.getEntidad()});
	}
	
	@Override
	public ValorContador contadorRuc(Ente ente) {
		return jdbcTemplate.query(
				"select Count(1) " +
				"from T_M_ENTE " +
				"where idempcod = ?  " + 
					"and entruc = ? " , 
					new ValorContadorExtractor(), 
					new Object[] {ente.getEmpresa(), ente.getRuc()});
	}
	
	@Override
	public ValorContador contadorProveedorRel(Ente ente) {
		return jdbcTemplate.query(
				"select Count(1) " +
				"from V_M_PROVEEDORRELACIONADO " +
				"where idempcod = ?  " + 
					"and entcod = ? " , 
					new ValorContadorExtractor(), 
					new Object[] {ente.getEmpresa(), ente.getEntidad()});
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Ente ente){
		
		return jdbcTemplate.query(
				"select t1.ENTCOD as codigo " +
				", t1.ENTRUC + '-' + case when t1.IDTIPOPERSONA = 'N' then t1.ENTPATERNO + ' '+ t1.ENTMATERNO + ', ' + t1.ENTNOMBRES else t1.ENTNOMBRES end as descripcion " +
				", t2.VALDESCRIP as estado " +
				"from T_M_ENTE t1 " +
				"left outer join T_M_VALORCAMPO t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t2.IDVALFUN = 'SHA' " + 
					"and t2.IDVALCOD = 'ESTADO' " +
					"and t1.ENTESTADO = t2.VALVALOR " +
				"where t1.idempcod = ?  " +
				"order by t1.ENTRUC, t1.ENTPATERNO, t1.ENTMATERNO, t1.ENTNOMBRES ",
					new CatalogoRowMapper(), 
					new Object[] {ente.getEmpresa()});
	}
	
	@Override
	public List<SocioNegocio> consultarCliente(Integer empresa){
	
		return jdbcTemplate.query(
				"select IDEMPCOD, ENTCOD, ENTRUC, CLIENTE  " +
					"from V_M_CLIENTEACTIVO " +
					"where IDEMPCOD = ? " +
					"order by CLIENTE ", new SocioNegocioRowMapper(), new Object[] {empresa});
	}	
	
	@Override
	public List<Catalogo> consultarCliente(Integer empresa, String criterio){
		
		String	criteriobusqueda;
		
		criteriobusqueda = '%' + criterio + '%';
		
		return jdbcTemplate.query(
				"select ENTRUC as codigo, CLIENTE as descripcion, ENTCOD as estado   " +
					"from V_M_CLIENTEACTIVO " +
					"where IDEMPCOD = ? " +
					" and ( ENTRUC like ? or CLIENTE like ? or ENTCOD like ? ) " +
					"order by CLIENTE ", new CatalogoRowMapper(), new Object[] {empresa, criteriobusqueda, criteriobusqueda, criteriobusqueda});
	}
	
	@Override
	public ValorContador contadorClienteRel(Ente ente) {
		return jdbcTemplate.query(
				"select Count(1) " +
				"from V_M_CLIENTERELACIONADO " +
				"where idempcod = ?  " + 
					"and entcod = ? " , 
					new ValorContadorExtractor(), 
					new Object[] {ente.getEmpresa(), ente.getEntidad()});
	}
	
	@Override
	public List<SocioNegocio> consultarProveedorNoDom(Integer empresa){
	
		return jdbcTemplate.query(
				"select IDEMPCOD, ENTCOD, ENTRUC, PROVEEDOR  " +
					"from V_M_PROVEEDORNODOMACTIVO " +
					"where IDEMPCOD = ? " +
					"order by PROVEEDOR ", new SocioNegocioRowMapper(), new Object[] {empresa});
	}	
	
	@Override
	public List<Catalogo> consultarProveedorNoDom(Integer empresa, String criterio){
		
		String	criteriobusqueda;
		
		criteriobusqueda = '%' + criterio + '%';
		
		return jdbcTemplate.query(
				"select ENTRUC as codigo, PROVEEDOR as descripcion, ENTCOD as estado   " +
					"from V_M_PROVEEDORNODOMACTIVO " +
					"where IDEMPCOD = ? " +
					" and ( ENTRUC like ? or PROVEEDOR like ? or ENTCOD like ? ) " +
					"order by PROVEEDOR ", new CatalogoRowMapper(), new Object[] {empresa, criteriobusqueda, criteriobusqueda, criteriobusqueda});
	}
	
	@Override
	public ValorContador contadorProveedorNoDomRel(Ente ente) {
		return jdbcTemplate.query(
				"select Count(1) " +
				"from V_M_PROVEEDORNODOMRELACIONADO " +
				"where idempcod = ?  " + 
					"and entcod = ? " , 
					new ValorContadorExtractor(), 
					new Object[] {ente.getEmpresa(), ente.getEntidad()});
	}
	
	@Override
	public List<SocioNegocio> consultarBanco(Integer empresa){	
		return jdbcTemplate.query(
				"select IDEMPCOD, ENTCOD, ENTRUC, BANCO  " +
					"from V_M_BANCOACTIVO " +
					"where IDEMPCOD = ? " +
					"order by BANCO ", new SocioNegocioRowMapper(), new Object[] {empresa});
	}
	
	@Override
	public List<SocioNegocio> consultarSocioNegocio(Integer empresa){	
		return jdbcTemplate.query(
				"select IDEMPCOD, ENTCOD, isnull(ENTRUC,''), SOCIONEGOCIO  " +
					"from V_M_SOCIONEGOCIOACTIVO " +
					"where IDEMPCOD = ? " +
					"order by SOCIONEGOCIO ", new SocioNegocioRowMapper(), new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarSocioNegocio(Integer empresa, String criterio){
		
		String	criteriobusqueda;
		
		criteriobusqueda = '%' + criterio + '%';
		
		return jdbcTemplate.query(
				"select ENTRUC as codigo, SOCIONEGOCIO as descripcion, ENTCOD as estado   " +
					"from V_M_SOCIONEGOCIOACTIVO " +
					"where IDEMPCOD = ? " +
					" and ( ENTRUC like ? or SOCIONEGOCIO like ? or ENTCOD like ? ) " +
					"order by SOCIONEGOCIO ", new CatalogoRowMapper(), new Object[] {empresa, criteriobusqueda, criteriobusqueda, criteriobusqueda});
	}
	
	@Override
	public List<Ente> consultarListaBanco(Integer empresa) {
	
		return jdbcTemplate.query(
				"select t1.IDEMPCOD, t1.ENTCOD, t1.ENTPATERNO, t1.ENTMATERNO " + 
						", case when t1.IDTIPOPERSONA = 'N' then isnull(t1.ENTPATERNO,'') + ' ' + isnull(t1.ENTMATERNO,'') + ', '  + t1.ENTNOMBRES " + 
						"        else t1.ENTNOMBRES end as ENTNOMBRES" +
						", t1.IDTIPOPERSONA, '', t1.ENTRUC " +
						", t1.ENTINDNODOM  " +
						", '' as descripcionnodomiciliado " +
						", t1.IDPAIS, '' as nombrepais " +
						", t1.ENTESTADO, t4.VALDESCRIP as descripcionestado" +
						", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE  " +
					"from T_M_ENTE t1 " +
						" left outer join T_M_ENTE_SOCIONEGOCIO t2 " +
						"   on t1.IDEMPCOD = t2.IDEMPCOD " +
						"   and t1.ENTCOD = t2.ENTCOD " +								
						" left outer join T_M_VALORCAMPO t4 " +
						"	on t1.IDEMPCOD = t4.IDEMPCOD  " +
						"	and t1.ENTESTADO = t4.VALVALOR " +
						"	and t4.IDVALFUN = 'SHA' " +
						"	and t4.IDVALCOD = 'ESTADO' " +
					"where t1.idempcod = ? and t2.IDSOCIONEGOCIO = '04' ", new EnteRowMapper(), new Object[] {empresa});
	}	
}
