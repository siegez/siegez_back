package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.SubDiarioDao;
import com.siegez.models.SubDiario;
import com.siegez.rowmapper.extractors.SubDiarioExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.SubDiarioRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class SubDiarioDaoImp implements SubDiarioDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insertar(SubDiario subdiario) {
		jdbcTemplate.update("insert into T_CON_SUBDIARIO (" +
				" IDEMPCOD, SUBCOD, SUBDESCRIP, SUBINDCOMPRA, SUBCOLCOMPRA, SUBCOLIGVCOMPRA " +
				", SUBINDHONORARIO, SUBCOLHONORARIO, SUBCOLIGVHONORARIO " +
				", SUBINDVENTA, SUBCOLVENTA, SUBCOLIGVVENTA, SUBINDCAJA, SUBCOLCAJA, SUBCOLIGVCAJA " +
				", SUBINDEGRESO, SUBCOLEGRESO, SUBCOLIGVEGRESO, SUBINDINGRESO, SUBCOLINGRESO, SUBCOLIGVINGRESO " +
				", SUBINDDIARIO, SUBCOLDIARIO, SUBCOLIGVDIARIO, SUBINDIMPORTACION, SUBCOLIMPORTACION, SUBCOLIGVIMPORTACION " +
				", SUBINDNODOMICILIADO, SUBCOLNODOMICILIADO, SUBCOLIGVNODOMICILIADO " +
				", SUBESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				subdiario.getEmpresa(),
				subdiario.getSubdiario(),
				subdiario.getDescripcion(),
				subdiario.getIndicadorcompra(),
				subdiario.getColumnacompra(),
				subdiario.getColumnaigvcompra(),				
				subdiario.getIndicadorhonorario(),
				subdiario.getColumnahonorario(),
				subdiario.getColumnaigvhonorario(),				
				subdiario.getIndicadorventa(),
				subdiario.getColumnaventa(),
				subdiario.getColumnaigvventa(),				
				subdiario.getIndicadorcaja(),
				subdiario.getColumnacaja(),
				subdiario.getColumnaigvcaja(),
				subdiario.getIndicadoregreso(),
				subdiario.getColumnaegreso(),
				subdiario.getColumnaigvegreso(),
				subdiario.getIndicadoringreso(),
				subdiario.getColumnaingreso(),
				subdiario.getColumnaigvingreso(),				
				subdiario.getIndicadordiario(),
				subdiario.getColumnadiario(),
				subdiario.getColumnaigvdiario(),
				subdiario.getIndicadorimportacion(),
				subdiario.getColumnaimportacion(),
				subdiario.getColumnaigvimportacion(),
				subdiario.getIndicadornodomiciliado(),
				subdiario.getColumnanodomiciliado(),
				subdiario.getColumnaigvnodomiciliado(),
				subdiario.getEstado(),
				subdiario.getCreacionUsuario(),
				subdiario.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(SubDiario subdiario) {
		jdbcTemplate.update("update T_CON_SUBDIARIO set " +
				"  SUBDESCRIP = ?, SUBINDCOMPRA = ?, SUBCOLCOMPRA = ?, SUBCOLIGVCOMPRA = ? " +
				", SUBINDHONORARIO = ?, SUBCOLHONORARIO = ?, SUBCOLIGVHONORARIO = ?  " +
				", SUBINDVENTA = ?, SUBCOLVENTA = ?, SUBCOLIGVVENTA = ?, SUBINDCAJA = ?, SUBCOLCAJA = ?, SUBCOLIGVCAJA = ? " +
				", SUBINDEGRESO = ?, SUBCOLEGRESO = ?, SUBCOLIGVEGRESO = ?, SUBINDINGRESO = ?, SUBCOLINGRESO = ?, SUBCOLIGVINGRESO = ? " +
				", SUBINDDIARIO = ?, SUBCOLDIARIO = ?, SUBCOLIGVDIARIO = ?, SUBINDIMPORTACION = ?, SUBCOLIMPORTACION = ?, SUBCOLIGVIMPORTACION = ? " +
				", SUBINDNODOMICILIADO = ?, SUBCOLNODOMICILIADO = ?, SUBCOLIGVNODOMICILIADO = ? " +
				", SUBESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and SUBCOD = ? ",					
				subdiario.getDescripcion(),
				subdiario.getIndicadorcompra(),
				subdiario.getColumnacompra(),
				subdiario.getColumnaigvcompra(),				
				subdiario.getIndicadorhonorario(),
				subdiario.getColumnahonorario(),
				subdiario.getColumnaigvhonorario(),				
				subdiario.getIndicadorventa(),
				subdiario.getColumnaventa(),
				subdiario.getColumnaigvventa(),				
				subdiario.getIndicadorcaja(),
				subdiario.getColumnacaja(),
				subdiario.getColumnaigvcaja(),
				subdiario.getIndicadoregreso(),
				subdiario.getColumnaegreso(),
				subdiario.getColumnaigvegreso(),
				subdiario.getIndicadoringreso(),
				subdiario.getColumnaingreso(),
				subdiario.getColumnaigvingreso(),				
				subdiario.getIndicadordiario(),
				subdiario.getColumnadiario(),
				subdiario.getColumnaigvdiario(),
				subdiario.getIndicadorimportacion(),
				subdiario.getColumnaimportacion(),
				subdiario.getColumnaigvimportacion(),
				subdiario.getIndicadornodomiciliado(),
				subdiario.getColumnanodomiciliado(),
				subdiario.getColumnaigvnodomiciliado(),
				subdiario.getEstado(),
				subdiario.getModificacionUsuario(),
				subdiario.getEmpresa(),
				subdiario.getSubdiario()
				);	
		
	}	
	
	@Override
	public int eliminar(SubDiario subdiario) {
		return jdbcTemplate.update("delete from T_CON_SUBDIARIO " 
				+ " where IDEMPCOD = ? and SUBCOD = ? ", 
				subdiario.getEmpresa(),
				subdiario.getSubdiario());	
	}
	
	@Override
	public SubDiario consultar(Integer empresa, String subdiario) {
		
		return jdbcTemplate.query(
				"select  " +
					" t1.IDEMPCOD, t1.SUBCOD, SUBDESCRIP " +
					" , t1.SUBINDCOMPRA, t1.SUBCOLCOMPRA, t1.SUBCOLIGVCOMPRA " +
					" , t1.SUBINDHONORARIO, t1.SUBCOLHONORARIO, t1.SUBCOLIGVHONORARIO " +
					" , t1.SUBINDVENTA, t1.SUBCOLVENTA, t1.SUBCOLIGVVENTA " +
					" , t1.SUBINDCAJA, t1.SUBCOLCAJA, t1.SUBCOLIGVCAJA " +
					" , t1.SUBINDEGRESO, t1.SUBCOLEGRESO, t1.SUBCOLIGVEGRESO " +
					" , t1.SUBINDINGRESO, t1.SUBCOLINGRESO, t1.SUBCOLIGVINGRESO " +
					" , t1.SUBINDDIARIO, t1.SUBCOLDIARIO, t1.SUBCOLIGVDIARIO " +
					" , t1.SUBINDIMPORTACION, t1.SUBCOLIMPORTACION, t1.SUBCOLIGVIMPORTACION " +
					" , t1.SUBINDNODOMICILIADO, t1.SUBCOLNODOMICILIADO, t1.SUBCOLIGVNODOMICILIADO " +
					" , t1.SUBESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_CON_SUBDIARIO t1 " +
				"where t1.IDEMPCOD = ?  and t1.SUBCOD = ?  ", 
				new SubDiarioExtractor(), empresa, subdiario);
	}
	
	@Override
	public List<SubDiario> consultarLista(Integer empresa) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.SUBCOD, SUBDESCRIP " +
				" , case when t1.SUBINDCOMPRA = 'S' then 'Si' else 'No' end, t1.SUBCOLCOMPRA, t1.SUBCOLIGVCOMPRA " +
				" , case when t1.SUBINDHONORARIO = 'S' then 'Si' else 'No' end, t1.SUBCOLHONORARIO, t1.SUBCOLIGVHONORARIO " +
				" , case when t1.SUBINDVENTA = 'S' then 'Si' else 'No' end, t1.SUBCOLVENTA, t1.SUBCOLIGVVENTA " +
				" , case when t1.SUBINDCAJA = 'S' then 'Si' else 'No' end, t1.SUBCOLCAJA, t1.SUBCOLIGVCAJA " +
				" , case when t1.SUBINDEGRESO = 'S' then 'Si' else 'No' end, t1.SUBCOLEGRESO, t1.SUBCOLIGVEGRESO " +
				" , case when t1.SUBINDINGRESO = 'S' then 'Si' else 'No' end, t1.SUBCOLINGRESO, t1.SUBCOLIGVINGRESO " +
				" , case when t1.SUBINDDIARIO = 'S' then 'Si' else 'No' end, t1.SUBCOLDIARIO, t1.SUBCOLIGVDIARIO " +
				" , case when t1.SUBINDIMPORTACION = 'S' then 'Si' else 'No' end, t1.SUBCOLIMPORTACION, t1.SUBCOLIGVIMPORTACION " +
				" , case when t1.SUBINDNODOMICILIADO = 'S' then 'Si' else 'No' end, t1.SUBCOLNODOMICILIADO, t1.SUBCOLIGVNODOMICILIADO " +
				", t1.SUBESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_SUBDIARIO t1 " +				
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.SUBESTADO = t2.IDESTADO " +				
			"where t1.IDEMPCOD = ?  ", 
				new SubDiarioRowMapper(), new Object[] {empresa});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, String subdiario) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_SUBDIARIO " + 
				"where IDEMPCOD = ?  and SUBCOD = ? ",
					new ValorContadorExtractor(), empresa, subdiario);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ?  " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoCompras(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDCOMPRA = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoVentas(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDVENTA = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoHonorarios(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDHONORARIO = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoNoDomiciliados(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDNODOMICILIADO = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoImportaciones(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDIMPORTACION = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoEgresos(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDEGRESO = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoIngresos(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDINGRESO = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
	@Override
	public List<Catalogo> consultarCatalogoDiarios(Integer empresa){
		
		return jdbcTemplate.query(
				"select t1.SUBCOD as codigo, t1.SUBDESCRIP as descripcion, t2.DESCRIPCION as estado " +
				"from T_CON_SUBDIARIO t1 " +
				"left outer join V_T_ESTADOREG t2 " +
					"on t1.IDEMPCOD = t2.IDEMPCOD " +
					"and t1.SUBESTADO = t2.IDESTADO " +
				"where t1.IDEMPCOD = ? and t1.SUBINDDIARIO = 'S' " +
				"order by t1.SUBCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
	
}
