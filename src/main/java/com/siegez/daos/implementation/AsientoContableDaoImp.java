package com.siegez.daos.implementation;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.AsientoContableDao;
import com.siegez.models.AsientoContable;
import com.siegez.rowmapper.extractors.AsientoContableExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.AsientoContableRowMapper;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@Repository
public class AsientoContableDaoImp implements AsientoContableDao {

	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall asientocontableSp;	
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.asientocontableSp = new SimpleJdbcCall(dataSource);
	}
	
	@Override
	public ResultadoSP insertar(AsientoContable asientocontable) {
		
		this.asientocontableSp.withSchemaName("dbo").withProcedureName("sp_con_insertasientocontable").declareParameters(
		new SqlParameter("idempcod", Types.INTEGER),
		new SqlParameter("idejecontable", Types.INTEGER),
		new SqlParameter("idopenum", Types.VARCHAR),
		new SqlParameter("idtipoasiento", Types.VARCHAR),
		new SqlParameter("estado", Types.VARCHAR),
		new SqlParameter("username", Types.VARCHAR),
		new SqlOutParameter("idasinum", Types.INTEGER),
		new SqlOutParameter("mensaje", Types.VARCHAR),
		new SqlOutParameter("indexito", Types.VARCHAR));
		SqlParameterSource parameters = new MapSqlParameterSource().addValue("idempcod", asientocontable.getEmpresa(), Types.INTEGER)
																	.addValue("idejecontable", asientocontable.getEjerciciocontable(), Types.INTEGER)	
																	.addValue("idopenum", asientocontable.getOperacioncontable(), Types.VARCHAR)
																	.addValue("idtipoasiento", asientocontable.getTipoasiento() , Types.VARCHAR)
																	.addValue("estado", asientocontable.getEstado() , Types.VARCHAR)
																	.addValue("username", asientocontable.getCreacionUsuario() , Types.VARCHAR);
		Map<String, Object> out = asientocontableSp.execute(parameters);
		return new ResultadoSP((Integer) out.get("idasinum"), "", out.get("mensaje").toString(), out.get("indexito").toString());	
		
	}
	
	
	@Override
	public void editar(AsientoContable asientocontable) {
		jdbcTemplate.update("update T_CON_ASIENTOCONTABLE set " +
				"  IDOPENUM = ?, IDTIPOASIENTO = ? " +				
				", ASIESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate())) " +
				"  where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? ",					
				asientocontable.getOperacioncontable(),
				asientocontable.getTipoasiento(),
				asientocontable.getEstado(),
				asientocontable.getModificacionUsuario(),
				asientocontable.getEmpresa(),
				asientocontable.getEjerciciocontable(),
				asientocontable.getAsientocontable()
				);	
		
	}
	
	@Override
	public int eliminar(AsientoContable asientocontable) {
		return jdbcTemplate.update("delete from T_CON_ASIENTOCONTABLE " 
				+ " where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? ", 
				asientocontable.getEmpresa(),
				asientocontable.getEjerciciocontable(),
				asientocontable.getAsientocontable());	
	}
	
	@Override
	public AsientoContable consultar(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		
		return jdbcTemplate.query(
				"select  " +
					"  t1.IDEMPCOD, t1.IDEJECONTABLE, , t1.IDASINUM, t1.IDOPENUM, t1.IDTIPOASIENTO, '' as descripciontipoasiento " +
					", t1.ASIESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
				"from T_CON_ASIENTOCONTABLE t1 " +
				"where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ?  ", 
				new AsientoContableExtractor(), empresa, ejerciciocontable, asientocontable);
	}
	
	@Override
	public List<AsientoContable> consultarLista(Integer empresa, Integer ejerciciocontable) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.IDEJECONTABLE, , t1.IDASINUM, t1.IDOPENUM, t1.IDTIPOASIENTO, T2.DESCRIPCION as descripciontipoasiento " +
				", t1.ASIESTADO, t3.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_CON_ASIENTOCONTABLE t1 " +	
				" left outer join V_T_TIPOASIENTO t2 " +				
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDTIPOASIENTO = t2.IDTIPOASIENTO " +
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.ASIESTADO = t3.IDESTADO " +				
			"where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ?  ", 
				new AsientoContableRowMapper(), new Object[] {empresa, ejerciciocontable});
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_CON_ASIENTOCONTABLE " + 
				"where IDEMPCOD = ? and IDEJECONTABLE = ? and IDASINUM = ? ",
					new ValorContadorExtractor(), empresa, ejerciciocontable, asientocontable);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable){
		
		return jdbcTemplate.query(
				" select t1.IDASINUM as codigo, " +
				" t2.DESCRIPCION as descripcion, " + 
				" t3.DESCRIPCION as estado " +
				" from T_CON_ASIENTOCONTABLE t1 " +
					" left outer join V_T_TIPOASIENTO t2 " +				
					" on  t1.IDEMPCOD = t2.IDEMPCOD " +
					" and t1.IDTIPOASIENTO = t2.IDTIPOASIENTO " +
					" left outer join V_T_ESTADOREG t3 " +
					" on  t1.IDEMPCOD = t3.IDEMPCOD " +
					" and t1.ASIESTADO = t3.IDESTADO " +
				" where t1.IDEMPCOD = ? and t1.IDEJECONTABLE = ?  " +
				" order by t1.IDASINUM ",
					new CatalogoRowMapper(), 
					new Object[] {empresa, ejerciciocontable});
	}
	
}
