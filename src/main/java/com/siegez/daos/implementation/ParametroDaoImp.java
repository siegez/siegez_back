package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.ParametroDao;
import com.siegez.models.Parametro;
import com.siegez.rowmapper.extractors.ParametroExtractor;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.rowmapper.mappers.CatalogoRowMapper;
import com.siegez.rowmapper.mappers.ParametroRowMapper;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Repository
public class ParametroDaoImp implements ParametroDao {
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertar(Parametro parametro) {
		jdbcTemplate.update("insert into T_M_PARAMETRO (" +
				" IDEMPCOD, IDPARFUN, IDPARCOD, PARDESCRIP, PARVALOR, PARSUBFUN " +				
				", PARESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?,  " +
				" ?, ?, dateadd(hour, -5, getdate()),? ,dateadd(hour, -5, getdate()))",				
				parametro.getEmpresa(),
				parametro.getModulo(),				
				parametro.getParametro(),
				parametro.getDescripcion(),
				parametro.getValor(),
				parametro.getSubmodulo(),
				parametro.getEstado(),
				parametro.getCreacionUsuario(),
				parametro.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(Parametro parametro) {
		jdbcTemplate.update("update T_M_PARAMETRO set " +
				"  PARVALOR = ? " +				
				", PARESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate()) " +
				"  where IDEMPCOD = ? and IDPARFUN = ? and IDPARCOD = ? ",					
				parametro.getValor(),
				parametro.getEstado(),
				parametro.getModificacionUsuario(),
				parametro.getEmpresa(),
				parametro.getModulo(),
				parametro.getParametro()
				);	
		
	}	
	
	@Override
	public int eliminar(Parametro parametro) {
		return jdbcTemplate.update("delete from T_M_PARAMETRO " 
				+ " IDEMPCOD = ? and IDPARFUN = ? and IDPARCOD = ? ", 
				parametro.getEmpresa(),
				parametro.getModulo(),
				parametro.getParametro());	
	}
	
	@Override
	public Parametro consultar(Integer empresa, String modulo, String parametro) {
		
		ValorContador valorcontador = contadorRegistros(empresa, modulo, parametro);
		
		if (valorcontador.getContador() == 1)
		{	
			return jdbcTemplate.query( 
					"select  " +
						" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, '', T1.PARSUBFUN" +
						", t1.PARESTADO, '' as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
					"from T_M_PARAMETRO t1 " +
					"where t1.IDEMPCOD = ?  and t1.IDPARFUN = ? and t1.IDPARCOD = ? ", 
					new ParametroExtractor(), empresa, modulo, parametro);
		} else {
			return new Parametro();
		} 
	}
	
	@Override
	public List<Parametro> consultarLista(Integer empresa) {
	
		return jdbcTemplate.query(
			"select  " +
				" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, '' as descripcionvalor, T1.PARSUBFUN " +
				", t1.PARESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_PARAMETRO t1 " +					
				" left outer join V_T_ESTADOREG t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.PARESTADO = t2.IDESTADO " +				
			"where t1.IDEMPCOD = ?  ", 
				new ParametroRowMapper(), new Object[] {empresa});
		
	}
	
	@Override
	public List<Parametro> consultarListaSubModulo(Integer empresa, Integer ejerciciocontable, String modulo, String submodulo) {
		
		switch (submodulo) {
		  case "CCA":
			  return jdbcTemplate.query(
						"select  " +
							" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, t1.PARVALOR + '-' + isnull(t3.CTADESCRIP,'') as descripcionvalor, T1.PARSUBFUN " +
							", t1.PARESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
						"from T_M_PARAMETRO t1 " +					
							" left outer join V_T_ESTADOREG t2 " +
							" on  t1.IDEMPCOD = t2.IDEMPCOD " +
							" and t1.PARESTADO = t2.IDESTADO " +
							" left outer join T_CON_PLANCONTABLE t3 " +
							" on  t1.IDEMPCOD = t3.IDEMPCOD " +
							" and t3.IDEJECONTABLE = ? " +
							" and t1.PARVALOR = t3.CTACOD " +
						"where t1.IDEMPCOD = ? and t1.IDPARFUN = ? and t1.PARSUBFUN = ? " + 
						"order by t1.PARDESCRIP ", 
							new ParametroRowMapper(), new Object[] {ejerciciocontable, empresa, modulo, submodulo});
		  case "TDO":
			  return jdbcTemplate.query(
						"select  " +
							" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, t1.PARVALOR + '-' + isnull(t3.TDODESCRIP,'') as descripcionvalor, T1.PARSUBFUN " +
							", t1.PARESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
						"from T_M_PARAMETRO t1 " +					
							" left outer join V_T_ESTADOREG t2 " +
							" on  t1.IDEMPCOD = t2.IDEMPCOD " +
							" and t1.PARESTADO = t2.IDESTADO " +
							" left outer join T_M_TIPODOCUMENTO t3 " +
							" on  t1.IDEMPCOD = t3.IDEMPCOD " +
							" and t1.PARVALOR = t3.TDOCOD " +
						"where t1.IDEMPCOD = ? and t1.IDPARFUN = ? and t1.PARSUBFUN = ? " +
						"order by t1.PARDESCRIP ",
							new ParametroRowMapper(), new Object[] {empresa, modulo, submodulo});
		  case "SUO":
			  return jdbcTemplate.query(
						"select  " +
							" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, t1.PARVALOR + '-' + isnull(t3.SUBDESCRIP,'') as descripcionvalor, T1.PARSUBFUN " +
							", t1.PARESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
						"from T_M_PARAMETRO t1 " +					
							" left outer join V_T_ESTADOREG t2 " +
							" on  t1.IDEMPCOD = t2.IDEMPCOD " +
							" and t1.PARESTADO = t2.IDESTADO " +
							" left outer join T_CON_SUBDIARIO t3 " +
							" on  t1.IDEMPCOD = t3.IDEMPCOD " +
							" and t1.PARVALOR = t3.SUBCOD " +
						"where t1.IDEMPCOD = ? and t1.IDPARFUN = ? and t1.PARSUBFUN = ? " + 
						"order by t1.PARDESCRIP ", 
							new ParametroRowMapper(), new Object[] {empresa, modulo, submodulo});
		  default:
			return jdbcTemplate.query(
						"select  " +
							" t1.IDEMPCOD, t1.IDPARFUN, t1.IDPARCOD, t1.PARDESCRIP, t1.PARVALOR, '' as descripcionvalor, T1.PARSUBFUN " +
							", t1.PARESTADO, t2.DESCRIPCION as descripcionestado, t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
						"from T_M_PARAMETRO t1 " +					
							" left outer join V_T_ESTADOREG t2 " +
							" on  t1.IDEMPCOD = t2.IDEMPCOD " +
							" and t1.PARESTADO = t2.IDESTADO " +				
						"where t1.IDEMPCOD = ? and t1.IDPARFUN = ? and t1.PARSUBFUN = ? " + 
						"order by t1.PARDESCRIP ", 
							new ParametroRowMapper(), new Object[] {empresa, modulo, submodulo});
		}	
		
	}
	
	@Override
	public ValorContador contadorRegistros(Integer empresa, String modulo, String parametro) {
		
		return jdbcTemplate.query(
				"select count(1) " +
				"from T_M_PARAMETRO " + 
				"where IDEMPCOD = ?  and IDPARFUN = ? and IDPARCOD = ?  ",
					new ValorContadorExtractor(), empresa, modulo, parametro);
	}
	
	@Override
	public List<Catalogo> consultarCatalogo(Integer empresa){
		
		return jdbcTemplate.query(
				" select t1.IDPARCOD as codigo, " +
				" t1.PARDESCRIP as descripcion, " + 
				" t2.DESCRIPCION as estado " +
				" from T_M_PARAMETRO t1 " +					
					" left outer join V_T_ESTADOREG t2 " +
					" on  t1.IDEMPCOD = t2.IDEMPCOD " +
					" and t1.DETESTADO = t2.IDESTADO " +
				" where t1.IDEMPCOD = ?  " +
				" order by t1.IDPARCOD ",
					new CatalogoRowMapper(), 
					new Object[] {empresa});
	}
}
