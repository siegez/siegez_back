package com.siegez.daos.implementation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.EnteDireccionDao;
import com.siegez.models.EnteDireccion;
import com.siegez.rowmapper.extractors.EnteDireccionExtractor;
import com.siegez.rowmapper.mappers.EnteDireccionRowMapper;

@Repository
public class EnteDireccionDaoImp implements EnteDireccionDao{
	
private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void insertar(EnteDireccion entedireccion) {
		jdbcTemplate.update("insert into T_M_ENTE_DIRECCION (" +
				" IDEMPCOD, ENTCOD, ENDVEZ, IDTIPODOMICILIO, PAISCOD, UBIGEOCOD, DIRECCION,  " +				
				", ENDESTADO, MGBCREAUSER, MGBCREADATE, MGBMODIUSER, MGBMODIDATE) " +
				" values (?, ?, ?, ?, ?, ?, ?, " +
				" ?, ?, dateadd(hour, -5, getdate()),?,dateadd(hour, -5, getdate()))",				
				entedireccion.getEmpresa(),	
				entedireccion.getEntidad(),
				entedireccion.getVez(),
				entedireccion.getTipodomicilio(),
				entedireccion.getPais(),
				entedireccion.getUbigeo(),
				entedireccion.getDireccion(),
				entedireccion.getEstado(),
				entedireccion.getCreacionUsuario(),
				entedireccion.getModificacionUsuario()
				);
	}
	
	@Override
	public void editar(EnteDireccion entedireccion) {
		jdbcTemplate.update("update T_M_ENTE_DIRECCION set " +
				"  IDTIPODOMICILIO = ?, PAISCOD = ?, UBIGEOCOD = ?, DIRECCION = ?, " +				
				", ENDESTADO = ?, MGBMODIUSER = ?, MGBMODIDATE = dateadd(hour, -5, getdate())) " +
				"  where IDEMPCOD = ? and ENTCOD = ? and ENDVEZ = ?  ",
				entedireccion.getTipodomicilio(),
				entedireccion.getPais(),
				entedireccion.getUbigeo(),
				entedireccion.getDireccion(),
				entedireccion.getEstado(),
				entedireccion.getModificacionUsuario(),
				entedireccion.getEmpresa(),	
				entedireccion.getEntidad(),
				entedireccion.getVez()
				);	
		
	}	
	
	@Override
	public int eliminar(EnteDireccion entedireccion) {
		return jdbcTemplate.update("delete from T_M_ENTE_DIRECCION " 
				+ " where IDEMPCOD = ? and ENTCOD = ? and ENDVEZ = ? ", 
				entedireccion.getEmpresa(),	
				entedireccion.getEntidad(),
				entedireccion.getVez());	
	}
	
	@Override
	public EnteDireccion consultar(Integer empresa, String entidad, Integer vez) {
		
		return jdbcTemplate.query(
				"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.ENDVEZ, t1.IDTIPODOMICILIO, '' as descripciontipodocomicio " +
				", t1.PAISCOD, '' as nombrepais " +
				", t1.UBIGEOCOD, '' as descripcionubigeo " + 
				", t1.DIRECCION " +
				", t1.ENDESTADO, '' as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " +  
				"from T_M_ENTE_DIRECCION t1 " +
				"where t1.IDEMPCOD = ? and t1.ENTCOD = ? and t1.ENDVEZ = ? ", 
				new EnteDireccionExtractor(), empresa, entidad, vez);
	}
	
	@Override
	public List<EnteDireccion> consultarLista(Integer empresa, String entidad) {
	
		return jdbcTemplate.query(
			"select  " +
				"  t1.IDEMPCOD, t1.ENTCOD, t1.ENDVEZ, t1.IDTIPODOMICILIO, t2.DESCRIPCION as descripciontipodomicilio " +
				", t1.PAISCOD, t4.DESCRIPCION as nombrepais " +
				", t1.UBIGEOCOD, t5.UBIDISTRITO + '-' + t5.UBIPROVINCIA + '-' + t5.UBIDEPARTAMENTO as descripcionubigeo " + 
				", t1.DIRECCION " +
				", t1.ENDESTADO, t3.DESCRIPCION as descripcionestado " +
				", t1.MGBCREAUSER, t1.MGBCREADATE, t1.MGBMODIUSER, t1.MGBMODIDATE " + 
			"from T_M_ENTE_DIRECCION t1 " +	
				" left outer join V_T_TIPODOMICILIO t2 " +
				" on  t1.IDEMPCOD = t2.IDEMPCOD " +
				" and t1.IDTIPODOMICILIO = t2.IDTIPODOMICILIO " +
				" left outer join V_T_PAIS t4 " +
				" on  t1.IDEMPCOD = t4.IDEMPCOD " +
				" and t1.PAISCOD = t4.PAISCOD " +
				" left outer join T_M_UBIGEO t5 " +
				" on t1.UBIGEOCOD = t5.UBIGEOCOD " +
				" left outer join V_T_ESTADOREG t3 " +
				" on  t1.IDEMPCOD = t3.IDEMPCOD " +
				" and t1.ENDESTADO = t3.IDESTADO " +				
			"where t1.IDEMPCOD = ? and t1.ENTCOD = ?  ", 
				new EnteDireccionRowMapper(), new Object[] {empresa, entidad});
		
	}
}
