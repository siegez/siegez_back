package com.siegez.daos.implementation;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.siegez.daos.interfaces.JwtUsuarioDao;
import com.siegez.models.JwtUsuario;
import com.siegez.rowmapper.extractors.JwtUsuarioExtractor;

@Repository
public class JwtUsuarioDaoImp implements JwtUsuarioDao {

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public JwtUsuario find(String username) {
		/*
		return jdbcTemplate.query("select username, userpass " 
				+ "from T_M_USUARIO t1 left outer join T_M_LICENCIA t2 " 
				+ "on t1.IDLICCOD = t2.IDLICCOD "
				+ "where t1.USERESTADO = '1' and t2.LICESTADO = '1' and t1.username like ?", new JwtUsuarioExtractor(), username);
		*/
		return jdbcTemplate.query("select t1.USERNAME, t1.USERPASS, T1.USERESTADO, T2.LXMESTADO, "
				+ "t2.LXMFECINICIO, T2.LXMFECCADUCA "
				+ "from T_M_USUARIO t1 left outer join T_M_LICENCIAMODULO t2 " 
				+ "on t1.IDLICCOD = t2.IDLICCOD " 
				+ "where t1.username like ?", new JwtUsuarioExtractor(), username);
	}

}
