package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Licencia;

public interface LicenciaDao {

	public Licencia find(String	licenciaCodigo);
	public List<Licencia> findAll();
	
}
