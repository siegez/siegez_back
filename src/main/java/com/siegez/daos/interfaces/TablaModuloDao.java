package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.TablaModulo;

public interface TablaModuloDao {

	public List<TablaModulo> findAll(String modulo);
}
