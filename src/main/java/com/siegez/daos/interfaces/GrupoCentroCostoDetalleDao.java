package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.views.ValorContador;

public interface GrupoCentroCostoDetalleDao {

	public void insertar(GrupoCentroCostoDetalle grupocentrocostodetalle);
	public void editar(GrupoCentroCostoDetalle grupocentrocostodetalle);	
	public int eliminar(GrupoCentroCostoDetalle grupocentrocostodetalle);
	public GrupoCentroCostoDetalle consultar(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto);	
	public List<GrupoCentroCostoDetalle> consultarLista(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto);

}
