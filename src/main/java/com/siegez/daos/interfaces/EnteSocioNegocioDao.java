package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.EnteSocioNegocio;

public interface EnteSocioNegocioDao {
	
	public void insertar(EnteSocioNegocio entesocionegocio);
	public void editar(EnteSocioNegocio entesocionegocio);	
	public int eliminar(EnteSocioNegocio entesocionegocio);
	public EnteSocioNegocio consultar(Integer empresa, String entidad, Integer vez);	
	public List<EnteSocioNegocio> consultarLista(Integer empresa, String entidad);

}
