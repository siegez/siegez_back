package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.EmpresaPerfilUsuario;

public interface EmpresaPerfilUsuarioDao {

	
	public void insertar(EmpresaPerfilUsuario empresaperfilusuario);
	public void editar(EmpresaPerfilUsuario empresaperfilusuario);
	public EmpresaPerfilUsuario consultar(EmpresaPerfilUsuario empresaperfilusuario);	
	public int eliminar(EmpresaPerfilUsuario empresaperfilusuario);
	public List<EmpresaPerfilUsuario> consultarLista(String username);
	
}
