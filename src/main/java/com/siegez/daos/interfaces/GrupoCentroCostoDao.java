package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.GrupoCentroCosto;
import com.siegez.models.GrupoCentroCostoRegistro;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

public interface GrupoCentroCostoDao {
	
	public ResultadoSP registrarGrupoCentroCosto(GrupoCentroCostoRegistro grupocentrocostoregistro);
	public void insertar(GrupoCentroCosto grupocentrocosto);
	public void editar(GrupoCentroCosto grupocentrocosto);	
	public int eliminar(GrupoCentroCosto grupocentrocosto);
	public GrupoCentroCosto consultar(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);	
	public List<GrupoCentroCosto> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
}
