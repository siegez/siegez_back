package com.siegez.daos.interfaces;

import com.siegez.models.JwtUsuario;

public interface JwtUsuarioDao {
	public JwtUsuario find(String username);
}
