package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.TipoDocumento;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface TipoDocumentoDao {

	public void insertar(TipoDocumento tipodocumento);
	public void editar(TipoDocumento tipodocumento);	
	public int eliminar(TipoDocumento tipodocumento);
	public TipoDocumento consultar(Integer empresa, String tipodocumento);	
	public List<TipoDocumento> consultarLista(Integer empresa);
	public ValorContador contadorRegistros(Integer empresa, String tipodocumento);
	public List<Catalogo> consultarCatalogo(Integer empresa);
	public List<Catalogo> consultarCatalogoCompras(Integer empresa);
	public List<Catalogo> consultarCatalogoVentas(Integer empresa);
	public List<Catalogo> consultarCatalogoHonorarios(Integer empresa);
	public List<Catalogo> consultarCatalogoNoDomiciliados(Integer empresa);
	public List<Catalogo> consultarCatalogoNoDomSustentos(Integer empresa);
	public List<Catalogo> consultarCatalogoImportaciones(Integer empresa);
	public List<Catalogo> consultarCatalogoEgresos(Integer empresa);
	public List<Catalogo> consultarCatalogoIngresos(Integer empresa);
	public List<Catalogo> consultarCatalogoDiarios(Integer empresa);
	
}
