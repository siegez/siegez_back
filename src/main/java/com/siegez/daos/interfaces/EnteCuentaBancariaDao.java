package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.EnteCuentaBancaria;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface EnteCuentaBancariaDao {
	public void insertar(EnteCuentaBancaria entecuentabancaria);
	public void editar(EnteCuentaBancaria entecuentabancaria);	
	public int eliminar(EnteCuentaBancaria entecuentabancaria);
	public EnteCuentaBancaria consultar(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria);	
	public List<EnteCuentaBancaria> consultarLista(Integer empresa, Integer ejerciciocontable, String entidad);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria);
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, String entidad);
}
