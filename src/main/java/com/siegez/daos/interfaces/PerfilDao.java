package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Perfil;

public interface PerfilDao {

	public void save(Perfil perfil);
	public void update(Perfil perfil);
	public Perfil find(Integer perfil);	
	public int delete(Integer perfil);
	public List<Perfil> findAll(Integer empresa);
}
