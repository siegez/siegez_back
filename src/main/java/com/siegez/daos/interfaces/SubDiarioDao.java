package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.SubDiario;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface SubDiarioDao {

	public void insertar(SubDiario subdiario);
	public void editar(SubDiario subdiario);	
	public int eliminar(SubDiario subdiario);
	public SubDiario consultar(Integer empresa, String subdiario);	
	public List<SubDiario> consultarLista(Integer empresa);
	public ValorContador contadorRegistros(Integer empresa, String subdiario);
	public List<Catalogo> consultarCatalogo(Integer empresa);
	public List<Catalogo> consultarCatalogoCompras(Integer empresa);
	public List<Catalogo> consultarCatalogoVentas(Integer empresa);
	public List<Catalogo> consultarCatalogoHonorarios(Integer empresa);
	public List<Catalogo> consultarCatalogoNoDomiciliados(Integer empresa);
	public List<Catalogo> consultarCatalogoImportaciones(Integer empresa);
	public List<Catalogo> consultarCatalogoEgresos(Integer empresa);
	public List<Catalogo> consultarCatalogoIngresos(Integer empresa);
	public List<Catalogo> consultarCatalogoDiarios(Integer empresa);
}
