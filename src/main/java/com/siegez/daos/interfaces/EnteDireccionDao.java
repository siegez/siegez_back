package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.EnteDireccion;

public interface EnteDireccionDao {
	public void insertar(EnteDireccion entedireccion);
	public void editar(EnteDireccion entedireccion);	
	public int eliminar(EnteDireccion entedireccion);
	public EnteDireccion consultar(Integer empresa, String entidad, Integer vez);	
	public List<EnteDireccion> consultarLista(Integer empresa, String entidad);

}
