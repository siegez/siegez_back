package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.Periodo;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface PeriodoDao {
	public void insertar(Periodo periodo);
	public void editar(Periodo periodo);
	public int eliminar(Periodo periodo);
	public Periodo consultar(Integer empresa, Integer ejerciciocontable, String periococontable);
	public List<Periodo> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String periococontable);
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable);	
}
