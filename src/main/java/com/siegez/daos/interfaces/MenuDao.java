package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Menu;
import com.siegez.models.MenuOpcionesConsulta;

public interface MenuDao {
	
	public void save(Menu menu);
	public void update(Menu menu);
	public Menu find(Integer codigo);	
	public int delete(Integer codigo);
	public List<Menu> findAll();
	public List<MenuOpcionesConsulta> findAllMenuOpciones(String username);
	public List<MenuOpcionesConsulta> findAllMenuEmpresa(String username, Integer idempcod);
	public List<MenuOpcionesConsulta> findAllMenuPerfilNuevo(String licencia);
	public List<MenuOpcionesConsulta> findAllMenuPerfilModifica(String perfil, String licencia);
}
