package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.reports.BalanceComprobacion;
import com.siegez.reports.CajaBanco;
import com.siegez.reports.EstadoResultado;
import com.siegez.reports.LibroDiario;
import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.reports.LibroMayor;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.reports.PLELibroDiario;
import com.siegez.reports.PLERegistroCompra;
import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.reports.PLERegistroVenta;
import com.siegez.reports.RegistroCompra;
import com.siegez.reports.RegistroVenta;
import com.siegez.reports.SaldoCuenta;
import com.siegez.reports.SituacionFinanciera;

import net.sf.jasperreports.engine.JasperPrint;

public interface ReporteDao {
	

	public List<PLERegistroCompra> consultarPLERegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLERegistroCompraNoDomiciliado> consultarPLERegistroCompraNoDom(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLERegistroVenta> consultarPLERegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLAMEPrestadorServicio> consultarPLAMEPreSer(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			String ruc, String nombreempresa, String moneda, String orden);	
	public List<PLELibroDiario> consultarPLELibroDiario(Integer empresa, Integer ejerciciocontable, String periodocontable);
	
	public JasperPrint exportarPDFRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
												String nombreempresa, String moneda, String orden);
	public List<RegistroCompra> consultarRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, 
												String ruc, String nombreempresa, String moneda, String orden);
	public JasperPrint exportarPDFRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
												String nombreempresa, String moneda, String orden);
	public List<RegistroVenta> consultarRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, 
												String ruc, String nombreempresa, String moneda, String orden);
	public JasperPrint exportarPDFDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String glosa);
	public List<LibroDiario> consultarDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String glosa);
	public JasperPrint exportarPDFDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String categoria);
	public List<LibroDiarioSimplificado> consultarDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String categoria);
	public JasperPrint exportarPDFMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public List<LibroMayor> consultarMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public JasperPrint exportarPDFMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public List<LibroMayorBiMoneda> consultarMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public JasperPrint exportarPDFCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public List<CajaBanco> consultarCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
											String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public JasperPrint exportarPDFBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public List<BalanceComprobacion> consultarBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public JasperPrint exportarPDFSaldoCtaEntDoc(Integer empresa, Integer ejerciciocontable,
											String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha);
	public List<SaldoCuenta> consultarSaldoCuenta(Integer empresa, Integer ejerciciocontable,
											String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha);
	public JasperPrint exportarPDFSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public JasperPrint exportarPDFSitFin(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public List<SituacionFinanciera> consultarSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public JasperPrint exportarPDFEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public List<EstadoResultado> consultarEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public JasperPrint exportarPDFEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
	public List<EstadoResultado> consultarEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
											String ruc, String nombreempresa, String moneda);
}
