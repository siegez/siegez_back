package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.views.ValorContador;
import com.siegez.views.ValoresIniciales;
import com.siegez.views.VistaCentroCosto;
import com.siegez.views.Catalogo;

public interface ConsultaDao {

	public ValoresIniciales findValoresIniciales(String username);
	
	public List<ValoresIniciales> findAllValoresIniciales(String username);
	
	public ValorContador permitirDocumentoReferencia(Integer empresa, Integer ejerciciocontable, String tipoodocumento, String moneda);
	
	public List<VistaCentroCosto> listarCentroCosto(Integer empresa, Integer ejerciciocontable);
	
	public List<Catalogo> listarUbicacionGeografica();

}
