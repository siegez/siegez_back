package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.EnteDocumentoIdentidad;

public interface EnteDocumentoIdentidadDao {
	
	public void insertar(EnteDocumentoIdentidad entedocumentoidentidad);
	public void editar(EnteDocumentoIdentidad entedocumentoidentidad);	
	public int eliminar(EnteDocumentoIdentidad entedocumentoidentidad);
	public EnteDocumentoIdentidad consultar(Integer empresa, String entidad, Integer vez);	
	public List<EnteDocumentoIdentidad> consultarLista(Integer empresa, String entidad);
	
}
