package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.views.ResultadoSP;
import com.siegez.models.TipoCambio;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface TipoCambioDao {
	public ResultadoSP registrarTipoCambio(TipoCambio tipocambio);
	public void insertar(TipoCambio tipocambio);
	public void editar(TipoCambio tipocambio);	
	public int eliminar(TipoCambio tipocambio);
	public TipoCambio consultar(Integer empresa, Integer vez);	
	public TipoCambio consultarDia(Integer empresa, String fecha, String moneda, String indicadorcierre);	
	public List<TipoCambio> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, String fecha, String moneda, String indicadorcierre);
	public List<Catalogo> consultarCatalogo(Integer empresa, String fecha);
}
