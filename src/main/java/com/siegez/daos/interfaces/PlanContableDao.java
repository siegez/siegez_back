package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.PlanContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;
import com.siegez.views.VistaPlanContable;

public interface PlanContableDao {

	public void insertar(PlanContable plancontable);
	public void editar(PlanContable plancontable);	
	public int eliminar(PlanContable plancontable);
	public PlanContable consultar(Integer empresa, Integer ejerciciocontable, String cuentacontable);	
	public List<PlanContable> consultarLista(Integer empresa, Integer ejerciciocontable);
	public List<VistaPlanContable> consultarVista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String cuentacontable);
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, String cuentacontable);
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable);
	public ConsultaString consultarCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable);
}

