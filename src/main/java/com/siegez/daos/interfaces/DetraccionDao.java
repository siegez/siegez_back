package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Detraccion;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface DetraccionDao {
	
	public void insertar(Detraccion detraccion);
	public void editar(Detraccion detraccion);	
	public int eliminar(Detraccion detraccion);
	public Detraccion consultar(Integer empresa, String detraccion);	
	public List<Detraccion> consultarLista(Integer empresa);
	public ValorContador contadorRegistros(Integer empresa, String detraccion);
	public List<Catalogo> consultarCatalogo(Integer empresa);
	
}
