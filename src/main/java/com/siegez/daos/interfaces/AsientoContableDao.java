package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.AsientoContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

public interface AsientoContableDao {

	public ResultadoSP insertar(AsientoContable asientocontable);
	public void editar(AsientoContable asientocontable);	
	public int eliminar(AsientoContable asientocontable);
	public AsientoContable consultar(Integer empresa, Integer ejerciciocontable, Integer asientocontable);	
	public List<AsientoContable> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable);
}
