package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.Ejercicio;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface EjercicioDao {

	public void insertar(Ejercicio ejercicio);
	public void editar(Ejercicio ejercicio);
	public int eliminar(Ejercicio ejercicio);
	public Ejercicio consultar(Integer empresa, Integer ejerciciocontable);
	public List<Ejercicio> consultarLista(Integer empresa);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> consultarCatalogo(Integer empresa);
	
}

