package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.TablaDetalle;


public interface TablaDetalleDao {


	public void insertar(TablaDetalle tabladetalle);
	public void editar(TablaDetalle tabladetalle);
	public int eliminar(TablaDetalle tabladetalle);
	public TablaDetalle consultar(TablaDetalle tabladetalle);	
	public List<TablaDetalle> consultarListaDescripcion(Integer empresa, Integer tabla);
	public List<TablaDetalle> consultarListaCodigo(Integer empresa, Integer tabla);
	
}
