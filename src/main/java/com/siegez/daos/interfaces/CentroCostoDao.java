package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.CentroCosto;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface CentroCostoDao {

	public void insertar(CentroCosto centrocosto);
	public void editar(CentroCosto centrocosto);	
	public int eliminar(CentroCosto centrocosto);
	public CentroCosto consultar(Integer empresa, Integer ejerciciocontable, String centrocosto);	
	public List<CentroCosto> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String centrocosto);
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> listarCatalogo(Integer empresa, Integer ejerciciocontable, String centrocosto);
	public List<Catalogo> listarNotInCatalogo(CentroCosto centrocosto);
}
