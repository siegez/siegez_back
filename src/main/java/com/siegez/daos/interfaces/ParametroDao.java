package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.Parametro;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface ParametroDao {
	public void insertar(Parametro parametro);
	public void editar(Parametro parametro);	
	public int eliminar(Parametro parametro);
	public Parametro consultar(Integer empresa, String modulo, String parametro);	
	public List<Parametro> consultarLista(Integer empresa);
	public List<Parametro> consultarListaSubModulo(Integer empresa, Integer ejerciciocontable, String modulo, String submodulo);
	public ValorContador contadorRegistros(Integer empresa, String modulo, String parametro);
	public List<Catalogo> consultarCatalogo(Integer empresa);
}
