package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.ValorCampo;

public interface ValorCampoDao {

	public void save(ValorCampo valorcampo);
	public void update(ValorCampo valorcampo);
	public ValorCampo find(ValorCampo valorcampo);	
	public int delete(ValorCampo valorcampo);
	public List<ValorCampo> findAll();
	public List<ValorCampo> findAllValorDescripcion(ValorCampo valorcampo);
	public List<ValorCampo> findNotInValorDescripcion(ValorCampo valorcampo);
	public List<ValorCampo> findAllValorCodigo(ValorCampo valorcampo);
}
