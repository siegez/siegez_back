package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.TipoDocMonCta;
import com.siegez.views.ValorContador;

public interface TipoDocMonCtaDao {
	
	public void insertar(TipoDocMonCta tipodocmoncta);
	public void editar(TipoDocMonCta tipodocmoncta);	
	public int eliminar(TipoDocMonCta tipodocmoncta);
	public TipoDocMonCta consultar(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda);	
	public List<TipoDocMonCta> consultarLista(Integer empresa, Integer ejerciciocontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda);

}
