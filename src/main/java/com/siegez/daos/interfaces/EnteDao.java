package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Ente;
import com.siegez.models.EnteRegistro;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.SocioNegocio;
import com.siegez.views.ValorContador;

public interface EnteDao {
	
	public ResultadoSP registrarEnte(EnteRegistro enteregistro);
	public void insertar(Ente ente);
	public void editar(Ente ente);	
	public int eliminar(Ente ente);
	public Ente consultar(Integer empresa, String entidad);	
	public Ente consultarRucAnexo(Integer empresa, String ruc, String anexo);	
	public List<Ente> consultarLista(Integer empresa);	
	public List<SocioNegocio> consultarProveedor(Integer empresa);
	public List<Catalogo> consultarProveedor(Integer empresa, String criterio);
	public ValorContador contadorRegistros(Ente ente);
	public ValorContador contadorRuc(Ente ente);
	public ValorContador contadorProveedorRel(Ente ente);
	public List<Catalogo> consultarCatalogo(Ente ente);
	public List<SocioNegocio> consultarCliente(Integer empresa);
	public List<Catalogo> consultarCliente(Integer empresa, String criterio);
	public ValorContador contadorClienteRel(Ente ente);
	public List<SocioNegocio> consultarProveedorNoDom(Integer empresa);
	public List<Catalogo> consultarProveedorNoDom(Integer empresa, String criterio);
	public ValorContador contadorProveedorNoDomRel(Ente ente);
	public List<SocioNegocio> consultarBanco(Integer empresa);
	public List<SocioNegocio> consultarSocioNegocio(Integer empresa);
	public List<Catalogo> consultarSocioNegocio(Integer empresa, String criterio);
	public List<Ente> consultarListaBanco(Integer empresa);
	
}
