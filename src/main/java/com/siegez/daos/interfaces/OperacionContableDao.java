package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.OperacionContable;
import com.siegez.models.OperacionRegistroContable;
import com.siegez.models.RegistroContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;

public interface OperacionContableDao {
	
	public OperacionContable registrarOperacion(OperacionRegistroContable operacionregistrocontable);
	public void insertar(OperacionContable operacioncontable);
	public void editar(OperacionContable operacioncontable);	
	public int eliminar(OperacionContable operacioncontable);
	public OperacionContable consultar(Integer empresa, Integer ejerciciocontable, String operacioncontable);	
	public List<OperacionContable> consultarLista(Integer empresa, Integer ejerciciocontable, String tipooperacion);
	public List<OperacionContable> consultarVista(Integer empresa, Integer ejerciciocontable, String tipooperacion);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, String operacioncontable);
	public ValorContador contadorRegistrosComprobante(Integer empresa, String entidad, String numeroserie, String numerocomprobanteini);
	public List<Catalogo> consultarCatalogo(OperacionContable operacioncontable);
	public List<Catalogo> consultarCatalogoRef(OperacionContable operacioncontable);
	public List<ConsultaString> consultarSaldo(RegistroContable registrocontable);
}
