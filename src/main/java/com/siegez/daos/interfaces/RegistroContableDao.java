package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.RegistroContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface RegistroContableDao {

	public void insertar(RegistroContable registrocontable);
	public void editar(RegistroContable registrocontable);	
	public int eliminar(RegistroContable registrocontable);
	public int eliminarOperacion(RegistroContable registrocontable);
	public RegistroContable consultar(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable);	
	public List<RegistroContable> consultarLista(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
	public List<RegistroContable> consultarListaOperacion(Integer empresa, Integer ejerciciocontable, String operacioncontable);
	public ValorContador contadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable);
	public List<Catalogo> consultarCatalogo(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
}
