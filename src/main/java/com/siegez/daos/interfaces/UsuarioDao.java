package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.Usuario;
import com.siegez.views.ValorContador;

public interface UsuarioDao {
	
	public void insertar(Usuario usuario);
	public void editar(Usuario usuario);
	public Usuario consultar(String username);	
	public int eliminar(String username);
	public List<Usuario> consultarLista();
	public List<Usuario> consultarListaPorLicencia(String licencia);
	public ValorContador contadorRegistros(Usuario usuario);

}