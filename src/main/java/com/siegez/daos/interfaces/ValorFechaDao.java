package com.siegez.daos.interfaces;

import java.util.List;
import com.siegez.models.ValorFecha;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.Valor;

public interface ValorFechaDao {
	public ResultadoSP registrarValorFecha(ValorFecha valorfecha);
	public void insertar(ValorFecha valorfecha);
	public void editar(ValorFecha valorfecha);	
	public int eliminar(ValorFecha valorfecha);
	public ValorFecha consultar(Integer empresa, String codigo, Integer vez);	
	public Valor consultarValorPorFecha(Integer empresa, String codigo, String fecha);
	public List<ValorFecha> consultarLista(Integer empresa, String codigo);
	public List<Catalogo> consultarCatalogo(Integer empresa, String fecha);
}
