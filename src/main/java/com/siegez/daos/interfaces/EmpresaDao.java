package com.siegez.daos.interfaces;

import java.util.List;

import com.siegez.models.Empresa;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;

public interface EmpresaDao {

	public ResultadoSP registrarEmpresa(Empresa empresa);
	public int save(Empresa empresa);
	public void update(Empresa empresa);
	public Empresa find(Integer	empresaCodigo);	
	public int delete(Integer empresaCodigo);
	public List<Empresa> findAll();
	public List<Empresa> findAllUsuarioLicencia(String licencia);
	public List<Catalogo> consultarCatalogo(String licencia);
	public List<Catalogo> consultarCatalogoUsuario(String username);
	public ResultadoSP replicarEmpresa(Integer empresaorigen, Integer ejercicioorigen, Integer empresadestino, Integer ejerciciodestino);
}
