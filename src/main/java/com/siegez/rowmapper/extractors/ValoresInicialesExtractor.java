package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.views.ValoresIniciales;

public class ValoresInicialesExtractor implements ResultSetExtractor<ValoresIniciales> {
	
	@Override
	public ValoresIniciales extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new ValoresIniciales(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getString(5),
						   rs.getString(6),rs.getString(7),rs.getString(8),rs.getInt(9));
	}
}
