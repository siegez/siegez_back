package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.EstadoResultado;

public class EstadoResultadoExtractor implements ResultSetExtractor<EstadoResultado> {
	@Override
	public EstadoResultado extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new EstadoResultado( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getInt(6),rs.getInt(7),rs.getString(8),rs.getDouble(9),rs.getDouble(10));
	}
}
