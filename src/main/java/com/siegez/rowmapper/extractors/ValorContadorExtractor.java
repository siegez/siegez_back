package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.views.ValorContador;

public class ValorContadorExtractor implements ResultSetExtractor<ValorContador> {

	@Override
	public ValorContador extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new ValorContador(rs.getInt(1));
	}
}
