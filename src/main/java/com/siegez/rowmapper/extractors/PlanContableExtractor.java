package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.PlanContable;

public class PlanContableExtractor implements ResultSetExtractor<PlanContable> {
	@Override
	public PlanContable extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new PlanContable(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5),
						   rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
						   rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),
						   rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19),rs.getString(20),
						   rs.getString(21),rs.getString(22),rs.getString(23),rs.getString(24),rs.getString(25),						   
						   rs.getString(26),rs.getString(27),rs.getString(28),rs.getString(29),rs.getString(30),
						   rs.getString(31),rs.getString(32),rs.getString(33),rs.getString(34),rs.getString(35),
						   rs.getString(36),rs.getString(37),rs.getString(38),rs.getString(39),rs.getString(40),
						   rs.getString(41),rs.getString(42),rs.getString(43),rs.getString(44));
	}
}
