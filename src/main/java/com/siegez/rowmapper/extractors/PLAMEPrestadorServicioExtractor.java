package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.PLAMEPrestadorServicio;

public class PLAMEPrestadorServicioExtractor implements ResultSetExtractor<PLAMEPrestadorServicio> {
	@Override
	public PLAMEPrestadorServicio extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new PLAMEPrestadorServicio( 
				rs.getString(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5),
				rs.getInt(6),rs.getInt(7),rs.getString(8),rs.getString(9),rs.getString(10),
				rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),
				rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19),rs.getDouble(20),
				rs.getDouble(21),rs.getDouble(22),rs.getDouble(23),rs.getString(24),rs.getString(25),
				rs.getString(26),rs.getDouble(27),rs.getString(28),rs.getString(29),rs.getString(30),
				rs.getString(31));
	}
}
