package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.RegistroVenta;

public class RegistroVentaExtractor implements ResultSetExtractor<RegistroVenta>{
	@Override
	public RegistroVenta extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new RegistroVenta( 
				rs.getString(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
				rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getDouble(15),
				rs.getDouble(16),rs.getDouble(17),rs.getDouble(18),rs.getDouble(19),rs.getDouble(20),
				rs.getDouble(21),rs.getDouble(22),rs.getString(23),rs.getString(24),rs.getString(25),
				rs.getString(26),rs.getString(27),rs.getString(28),rs.getString(29));
	}
}
