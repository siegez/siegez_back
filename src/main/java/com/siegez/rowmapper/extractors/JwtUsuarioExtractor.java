package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.JwtUsuario;

public class JwtUsuarioExtractor implements ResultSetExtractor<JwtUsuario> {

	@Override
	public JwtUsuario extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
	
		return new JwtUsuario(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
				   rs.getDate(5),rs.getDate(5));
	}

}
