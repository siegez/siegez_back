package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.reports.SaldoCuenta;

public class SaldoCuentaExtractor implements ResultSetExtractor<SaldoCuenta>{
	@Override
	public SaldoCuenta extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new SaldoCuenta( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
				rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),
				rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19),rs.getString(20),
				rs.getString(21),rs.getString(22),rs.getDouble(23),rs.getDouble(24),rs.getDouble(25),
				rs.getDouble(26),rs.getDouble(27),rs.getDouble(28));
	}
}
