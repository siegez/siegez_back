package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.views.VistaCentroCosto;

public class VistaCentroCostoExtractor implements ResultSetExtractor<VistaCentroCosto> {
	@Override
	public VistaCentroCosto extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new VistaCentroCosto(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
	}
}
