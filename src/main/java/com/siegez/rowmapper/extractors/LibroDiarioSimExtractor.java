package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.LibroDiarioSimplificado;

public class LibroDiarioSimExtractor implements ResultSetExtractor<LibroDiarioSimplificado>{
	@Override
	public LibroDiarioSimplificado extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new LibroDiarioSimplificado( 
				rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getDouble(7));
	}
}
