package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.EnteDireccion;

public class EnteDireccionExtractor implements ResultSetExtractor<EnteDireccion> {
	@Override
	public EnteDireccion extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new EnteDireccion(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5),
						   rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
						   rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15),
						   rs.getString(16));
	}
}
