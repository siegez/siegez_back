package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.models.OperacionContable;

public class OperacionContableExtractor implements ResultSetExtractor<OperacionContable>{

	@Override
	public OperacionContable extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new OperacionContable(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5),
						   rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10),
						   rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),
						   rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19),rs.getString(20),
						   rs.getString(21),rs.getString(22),rs.getString(23),rs.getString(24),rs.getString(25),
						   rs.getString(26),rs.getString(27),rs.getString(28),rs.getString(29),rs.getDouble(30),
						   rs.getDouble(31),rs.getDouble(32),rs.getDouble(33),rs.getDouble(34),rs.getString(35),
						   rs.getDouble(36),rs.getDouble(37),rs.getDouble(38),rs.getDouble(39),rs.getDouble(40),
						   rs.getDouble(41),rs.getDouble(42),rs.getString(43),rs.getString(44),rs.getString(45),
						   rs.getString(46),rs.getString(47),rs.getString(48),rs.getString(49),rs.getString(50),
						   rs.getDouble(51),rs.getString(52),rs.getString(53),rs.getDouble(54),rs.getDouble(55),
						   rs.getString(56),rs.getString(57),rs.getInt(58),rs.getString(59),rs.getString(60),
						   rs.getString(61),rs.getString(62),rs.getDouble(63),rs.getDouble(64),rs.getDouble(65),
						   rs.getDouble(66),rs.getDouble(67),rs.getString(68),rs.getString(69),rs.getString(70),
						   rs.getString(71),rs.getString(72),rs.getInt(73),rs.getString(74),rs.getString(75),
						   rs.getString(76),rs.getString(77),rs.getString(78),rs.getString(79),rs.getDouble(80),
						   rs.getDouble(81),rs.getString(82),rs.getString(83),rs.getString(84),rs.getString(85),
						   rs.getString(86),rs.getString(87),rs.getString(88),rs.getString(89),rs.getString(90),
						   rs.getInt(91),rs.getInt(92),rs.getString(93),rs.getString(94),rs.getString(95),
						   rs.getString(96),rs.getString(97),rs.getString(98));
	}		

}
