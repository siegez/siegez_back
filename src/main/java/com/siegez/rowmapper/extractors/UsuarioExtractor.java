package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.Usuario;


public class UsuarioExtractor implements ResultSetExtractor<Usuario> {
	@Override
	public Usuario extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new Usuario(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
						   rs.getDate(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
						   rs.getString(11),rs.getString(12),rs.getDate(13),rs.getString(14),rs.getDate(15));
	}

}
