package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.views.SocioNegocio;

public class SocioNegocioExtractor implements ResultSetExtractor<SocioNegocio> {

	@Override
	public SocioNegocio extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new SocioNegocio(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
	}
	
}
