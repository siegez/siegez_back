package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.MenuOpcionesConsulta;

public class MenuOpcionesExtractor implements ResultSetExtractor<MenuOpcionesConsulta> {

	@Override
	public MenuOpcionesConsulta extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new MenuOpcionesConsulta(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getInt(5),
						   rs.getString(6),rs.getString(7),rs.getString(8));
		
		
	}
}
