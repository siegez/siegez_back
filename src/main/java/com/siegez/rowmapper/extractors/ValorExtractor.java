package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.views.Valor;

public class ValorExtractor implements ResultSetExtractor<Valor> {
	@Override
	public Valor extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new Valor(rs.getDouble(1));
	}
}
