package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.BalanceComprobacion;

public class BalanceComprobacionExtractor implements ResultSetExtractor<BalanceComprobacion> {
	@Override
	public BalanceComprobacion extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new BalanceComprobacion( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getString(7),rs.getDouble(8),rs.getDouble(9),rs.getDouble(10),
				rs.getDouble(11),rs.getDouble(12),rs.getDouble(13),rs.getDouble(14),rs.getDouble(15),
				rs.getDouble(16),rs.getDouble(17));
	}
}
