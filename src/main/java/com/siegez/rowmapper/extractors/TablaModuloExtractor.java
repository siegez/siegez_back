package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.models.TablaModulo;

public class TablaModuloExtractor implements ResultSetExtractor<TablaModulo>{

	
	@Override
	public TablaModulo extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new TablaModulo(rs.getString(1),rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getString(5),rs.getString(6),
				rs.getString(7),rs.getDate(8),rs.getString(9),rs.getDate(10));
	}
	
}

