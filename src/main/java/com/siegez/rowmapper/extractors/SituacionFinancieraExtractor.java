package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.SituacionFinanciera;

public class SituacionFinancieraExtractor implements ResultSetExtractor<SituacionFinanciera> {
	@Override
	public SituacionFinanciera extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new SituacionFinanciera( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getInt(6),rs.getString(7),rs.getInt(8),rs.getString(9),rs.getInt(10),
				rs.getString(11),rs.getString(12),rs.getDouble(13),rs.getString(14));
	}
}
