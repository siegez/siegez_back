package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.reports.PLERegistroCompra;

public class PLERegistroCompraExtractor  implements ResultSetExtractor<PLERegistroCompra> {
	@Override
	public PLERegistroCompra extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new PLERegistroCompra( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),
				rs.getString(11),rs.getString(12),rs.getString(13),rs.getDouble(14),rs.getDouble(15),
				rs.getDouble(16),rs.getDouble(17),rs.getDouble(18),rs.getDouble(19),rs.getDouble(20),
				rs.getDouble(21),rs.getDouble(22),rs.getDouble(23),rs.getDouble(24),rs.getString(25),
				rs.getString(26),rs.getString(27),rs.getString(28),rs.getString(29),rs.getString(30),
				rs.getString(31),rs.getString(32),rs.getString(33),rs.getString(34),rs.getString(35),
				rs.getString(36),rs.getString(37),rs.getString(38),rs.getString(39),rs.getString(40),
				rs.getString(41), rs.getString(42));
	}
}
