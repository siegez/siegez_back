package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.models.Detraccion;


public class DetraccionExtractor implements ResultSetExtractor<Detraccion>  {

	@Override
	public Detraccion extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new Detraccion(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
						   rs.getDouble(5),rs.getDouble(6),rs.getString(7),rs.getString(8),
						   rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12));
	}
}
