package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.Perfil;


public class PerfilExtractor implements ResultSetExtractor<Perfil>{

	
	@Override
	public Perfil extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		/*return new Perfil(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getString(5),
						   rs.getDate(6),rs.getString(7),rs.getDate(8));
						   */
		return new Perfil(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5), rs.getString(6),
				   rs.getDate(7),rs.getString(8),rs.getDate(9));
	}
}
