package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.siegez.views.Catalogo;

public class CatalogoExtractor implements ResultSetExtractor<Catalogo>{

	@Override
	public Catalogo extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new Catalogo(rs.getString(1),rs.getString(2),rs.getString(3));
	}
}
