package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.Empresa;

public class EmpresaExtractor implements ResultSetExtractor<Empresa>{

	@Override
	public Empresa extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new Empresa(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),
				rs.getString(8),rs.getDate(9),rs.getString(10),rs.getDate(11));
	}

}
