package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.TipoCambio;

public class TipoCambioExtractor implements ResultSetExtractor<TipoCambio> {

	@Override
	public TipoCambio extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new TipoCambio(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),
						   rs.getString(5),rs.getDouble(6),rs.getDouble(7),rs.getString(8),rs.getString(9),rs.getString(10),
						   rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16));
	}
}
