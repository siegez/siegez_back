package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.models.SubDiario;

public class SubDiarioExtractor implements ResultSetExtractor<SubDiario> {
	
	@Override
	public SubDiario extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new SubDiario(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
						   rs.getInt(5),rs.getInt(6),rs.getString(7),rs.getInt(8),rs.getInt(9),rs.getString(10),
						   rs.getInt(11),rs.getInt(12),rs.getString(13),rs.getInt(14),rs.getInt(15),rs.getString(16),
						   rs.getInt(17),rs.getInt(18),rs.getString(19),rs.getInt(20),rs.getInt(21),rs.getString(22),
						   rs.getInt(23),rs.getInt(24),rs.getString(25),rs.getInt(26),rs.getInt(27),rs.getString(28),
						   rs.getInt(29),rs.getInt(30),rs.getString(31),rs.getString(32),
						   rs.getString(33),rs.getString(34),rs.getString(36),rs.getString(36));
	}
}
