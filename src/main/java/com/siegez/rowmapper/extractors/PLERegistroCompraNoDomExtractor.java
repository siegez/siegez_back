package com.siegez.rowmapper.extractors;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.siegez.reports.PLERegistroCompraNoDomiciliado;

public class PLERegistroCompraNoDomExtractor implements ResultSetExtractor<PLERegistroCompraNoDomiciliado> {
	@Override
	public PLERegistroCompraNoDomiciliado extractData(ResultSet rs) throws SQLException, DataAccessException {
		if(rs.isBeforeFirst()) rs.next();
		return new PLERegistroCompraNoDomiciliado( 
				rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
				rs.getString(6),rs.getString(7),rs.getDouble(8),rs.getDouble(9),rs.getDouble(10),
				rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getDouble(15),
				rs.getString(16),rs.getString(17),rs.getString(18),rs.getString(19),rs.getString(20),
				rs.getString(21),rs.getString(22),rs.getString(23),rs.getString(24),rs.getString(25),
				rs.getDouble(26),rs.getDouble(27),rs.getDouble(28),rs.getDouble(29),rs.getDouble(30),
				rs.getString(31),rs.getString(32),rs.getString(33),rs.getString(34),rs.getString(35),
				rs.getString(36));
	}
}
