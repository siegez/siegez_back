package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Menu;
import com.siegez.rowmapper.extractors.MenuExtractor;

public class MenuRowMapper implements RowMapper<Menu> {

	@Override
	public Menu mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new MenuExtractor().extractData(rs);
		
	}
}
