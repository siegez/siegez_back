package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.views.VistaPlanContable;
import com.siegez.rowmapper.extractors.VistaPlanContableExtractor;

public class VistaPlanContableRowMapper implements RowMapper<VistaPlanContable> {
	@Override
	public VistaPlanContable mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new VistaPlanContableExtractor().extractData(rs);
		
	}
}
