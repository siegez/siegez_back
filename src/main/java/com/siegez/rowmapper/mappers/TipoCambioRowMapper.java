package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.TipoCambio;
import com.siegez.rowmapper.extractors.TipoCambioExtractor;

public class TipoCambioRowMapper implements RowMapper<TipoCambio> {
	@Override
	public TipoCambio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TipoCambioExtractor().extractData(rs);
	}
}
