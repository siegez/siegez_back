package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.LibroDiario;
import com.siegez.rowmapper.extractors.LibroDiarioExtractor;

public class LibroDiarioRowMapper implements RowMapper<LibroDiario>{
	@Override
	public LibroDiario mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LibroDiarioExtractor().extractData(rs);
		
	}
}
