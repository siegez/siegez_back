package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.TablaModulo;
import com.siegez.rowmapper.extractors.TablaModuloExtractor;

public class TablaModuloRowMapper implements RowMapper<TablaModulo>{
	
	
	@Override
	public TablaModulo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TablaModuloExtractor().extractData(rs);
		
	}
}
