package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Licencia;
import com.siegez.rowmapper.extractors.LicenciaExtractor;

public class LicenciaRowMapper implements RowMapper<Licencia>{

	@Override
	public Licencia mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LicenciaExtractor().extractData(rs);
	}

}
