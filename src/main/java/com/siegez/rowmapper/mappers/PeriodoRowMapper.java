package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Periodo;
import com.siegez.rowmapper.extractors.PeriodoExtractor;

public class PeriodoRowMapper implements RowMapper<Periodo>{

	@Override
	public Periodo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PeriodoExtractor().extractData(rs);
	}

}
