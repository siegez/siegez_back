package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.PlanContable;
import com.siegez.rowmapper.extractors.PlanContableExtractor;

public class PlanContableRowMapper implements RowMapper<PlanContable> {

	@Override
	public PlanContable mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PlanContableExtractor().extractData(rs);
		
	}
}
