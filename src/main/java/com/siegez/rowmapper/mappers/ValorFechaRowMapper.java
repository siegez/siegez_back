package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.ValorFecha;
import com.siegez.rowmapper.extractors.ValorFechaExtractor;

public class ValorFechaRowMapper implements RowMapper<ValorFecha> {
	@Override
	public ValorFecha mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ValorFechaExtractor().extractData(rs);
	}
}
