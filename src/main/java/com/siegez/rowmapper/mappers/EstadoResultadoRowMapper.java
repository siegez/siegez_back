package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.EstadoResultado;
import com.siegez.rowmapper.extractors.EstadoResultadoExtractor;

public class EstadoResultadoRowMapper implements RowMapper<EstadoResultado>{
	@Override
	public EstadoResultado mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EstadoResultadoExtractor().extractData(rs);
		
	}
}
