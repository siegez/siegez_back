package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.TipoDocMonCta;
import com.siegez.rowmapper.extractors.TipoDocMonCtaExtractor;

public class TipoDocMonCtaRowMapper implements RowMapper<TipoDocMonCta> {
	@Override
	public TipoDocMonCta mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TipoDocMonCtaExtractor().extractData(rs);
		
	}
}
