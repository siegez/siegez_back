package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.EnteCuentaBancaria;
import com.siegez.rowmapper.extractors.EnteCuentaBancariaExtractor;

public class EnteCuentaBancariaRowMapper implements RowMapper<EnteCuentaBancaria>{
	@Override
	public EnteCuentaBancaria mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EnteCuentaBancariaExtractor().extractData(rs);
		
	}
}
