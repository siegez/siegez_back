package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.siegez.rowmapper.extractors.ValorExtractor;
import com.siegez.views.Valor;

public class ValorRowMapper implements RowMapper<Valor>  {
	
	@Override
	public Valor mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ValorExtractor().extractData(rs);
		
	}
}
