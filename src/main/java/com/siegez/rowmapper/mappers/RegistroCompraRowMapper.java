package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.RegistroCompra;
import com.siegez.rowmapper.extractors.RegistroCompraExtractor;

public class RegistroCompraRowMapper implements RowMapper<RegistroCompra> {
	@Override
	public RegistroCompra mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new RegistroCompraExtractor().extractData(rs);
		
	}
}
