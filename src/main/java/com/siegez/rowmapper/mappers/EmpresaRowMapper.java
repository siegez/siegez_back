package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Empresa;
import com.siegez.rowmapper.extractors.EmpresaExtractor;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EmpresaExtractor().extractData(rs);
	}

}
