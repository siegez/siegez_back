package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.MenuOpcionesConsulta;
import com.siegez.rowmapper.extractors.MenuOpcionesExtractor;

public class MenuOpcionesRowMapper implements RowMapper<MenuOpcionesConsulta> {

	@Override
	public MenuOpcionesConsulta mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new MenuOpcionesExtractor().extractData(rs);
		
	}
}
