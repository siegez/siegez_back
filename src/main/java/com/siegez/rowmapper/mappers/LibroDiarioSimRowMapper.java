package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.rowmapper.extractors.LibroDiarioSimExtractor;

public class LibroDiarioSimRowMapper implements RowMapper<LibroDiarioSimplificado> {
	@Override
	public LibroDiarioSimplificado mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LibroDiarioSimExtractor().extractData(rs);
		
	}
}
