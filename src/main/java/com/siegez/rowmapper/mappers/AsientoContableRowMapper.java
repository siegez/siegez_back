package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.AsientoContable;
import com.siegez.rowmapper.extractors.AsientoContableExtractor;

public class AsientoContableRowMapper implements RowMapper<AsientoContable> {
	@Override
	public AsientoContable mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new AsientoContableExtractor().extractData(rs);
		
	}
}
