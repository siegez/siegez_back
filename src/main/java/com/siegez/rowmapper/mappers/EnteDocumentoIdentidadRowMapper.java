package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.rowmapper.extractors.EnteDocumentoIdentidadExtractor;

public class EnteDocumentoIdentidadRowMapper implements RowMapper<EnteDocumentoIdentidad> {
	@Override
	public EnteDocumentoIdentidad mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EnteDocumentoIdentidadExtractor().extractData(rs);
		
	}
}
