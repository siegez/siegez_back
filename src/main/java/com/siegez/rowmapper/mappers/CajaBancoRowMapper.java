package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.CajaBanco;
import com.siegez.rowmapper.extractors.CajaBancoExtractor;

public class CajaBancoRowMapper implements RowMapper<CajaBanco> {
	@Override
	public CajaBanco mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new CajaBancoExtractor().extractData(rs);
		
	}
}
