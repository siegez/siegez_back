package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.rowmapper.extractors.PLERegistroCompraNoDomExtractor;

public class PLERegistroCompraNoDomRowMapper implements RowMapper<PLERegistroCompraNoDomiciliado>{
	@Override
	public PLERegistroCompraNoDomiciliado mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PLERegistroCompraNoDomExtractor().extractData(rs);
		
	}
}
