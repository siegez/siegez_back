package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.CentroCosto;
import com.siegez.rowmapper.extractors.CentroCostoExtractor;

public class CentroCostoRowMapper implements RowMapper<CentroCosto>{
	@Override
	public CentroCosto mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new CentroCostoExtractor().extractData(rs);
		
	}
}
