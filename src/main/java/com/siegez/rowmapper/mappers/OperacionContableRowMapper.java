package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.models.OperacionContable;
import com.siegez.rowmapper.extractors.OperacionContableExtractor;

public class OperacionContableRowMapper implements RowMapper<OperacionContable> {
	@Override
	public OperacionContable mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new OperacionContableExtractor().extractData(rs);
		
	}
}
