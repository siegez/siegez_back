package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.GrupoCentroCosto;
import com.siegez.rowmapper.extractors.GrupoCentroCostoExtractor;

public class GrupoCentroCostoRowMapper implements RowMapper<GrupoCentroCosto> {
	@Override
	public GrupoCentroCosto mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new GrupoCentroCostoExtractor().extractData(rs);
		
	}
}
