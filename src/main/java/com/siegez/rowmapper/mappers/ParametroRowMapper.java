package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Parametro;
import com.siegez.rowmapper.extractors.ParametroExtractor;

public class ParametroRowMapper implements RowMapper<Parametro>  {
	@Override
	public Parametro mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ParametroExtractor().extractData(rs);
		
	}
}
