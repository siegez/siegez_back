package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Ejercicio;
import com.siegez.rowmapper.extractors.EjercicioExtractor;

public class EjercicioRowMapper implements RowMapper<Ejercicio>{

	@Override
	public Ejercicio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EjercicioExtractor().extractData(rs);
	}

}
