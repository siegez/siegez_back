package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.rowmapper.extractors.PLAMEPrestadorServicioExtractor;

public class PLAMEPrestadorServicioRowMapper implements RowMapper<PLAMEPrestadorServicio> {
	@Override
	public PLAMEPrestadorServicio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PLAMEPrestadorServicioExtractor().extractData(rs);
		
	}
}
