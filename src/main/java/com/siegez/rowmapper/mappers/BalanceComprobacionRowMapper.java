package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.BalanceComprobacion;
import com.siegez.rowmapper.extractors.BalanceComprobacionExtractor;

public class BalanceComprobacionRowMapper implements RowMapper<BalanceComprobacion> {
	@Override
	public BalanceComprobacion mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new BalanceComprobacionExtractor().extractData(rs);
		
	}
}
