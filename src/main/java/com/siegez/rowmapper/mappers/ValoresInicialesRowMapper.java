package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.views.ValoresIniciales;
import com.siegez.rowmapper.extractors.ValoresInicialesExtractor;

public class ValoresInicialesRowMapper implements RowMapper<ValoresIniciales> {

	@Override
	public ValoresIniciales mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ValoresInicialesExtractor().extractData(rs);
		
	}
}
