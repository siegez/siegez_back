package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.models.TipoDocumento;
import com.siegez.rowmapper.extractors.TipoDocumentoExtractor;

public class TipoDocumentoRowMapper implements RowMapper<TipoDocumento> {

	@Override
	public TipoDocumento mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TipoDocumentoExtractor().extractData(rs);
		
	}
}
