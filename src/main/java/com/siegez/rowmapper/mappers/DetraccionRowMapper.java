package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.models.Detraccion;
import com.siegez.rowmapper.extractors.DetraccionExtractor;

public class DetraccionRowMapper implements RowMapper<Detraccion>{

	@Override
	public Detraccion mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new DetraccionExtractor().extractData(rs);
		
	}
}
