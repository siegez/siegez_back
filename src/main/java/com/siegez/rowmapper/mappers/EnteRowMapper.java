package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Ente;
import com.siegez.rowmapper.extractors.EnteExtractor;

public class EnteRowMapper implements RowMapper<Ente>{
	@Override
	public Ente mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EnteExtractor().extractData(rs);
		
	}
}
