package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.rowmapper.extractors.CatalogoExtractor;
import com.siegez.views.Catalogo;


public class CatalogoRowMapper implements RowMapper<Catalogo> {
	@Override
	public Catalogo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new CatalogoExtractor().extractData(rs);
		
	}
}
