package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.SubDiario;
import com.siegez.rowmapper.extractors.SubDiarioExtractor;

public class SubDiarioRowMapper implements RowMapper<SubDiario> {

	@Override
	public SubDiario mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new SubDiarioExtractor().extractData(rs);
		
	}
}
