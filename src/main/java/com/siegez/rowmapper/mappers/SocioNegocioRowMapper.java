package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.views.SocioNegocio;
import com.siegez.rowmapper.extractors.SocioNegocioExtractor;

public class SocioNegocioRowMapper implements RowMapper<SocioNegocio> {
	@Override
	public SocioNegocio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new SocioNegocioExtractor().extractData(rs);
		
	}
}
