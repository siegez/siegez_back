package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.PLERegistroVenta;
import com.siegez.rowmapper.extractors.PLERegistroVentaExtractor;

public class PLERegistroVentaRowMapper implements RowMapper<PLERegistroVenta>{
	@Override
	public PLERegistroVenta mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PLERegistroVentaExtractor().extractData(rs);
		
	}
}
