package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.siegez.reports.SaldoCuenta;
import com.siegez.rowmapper.extractors.SaldoCuentaExtractor;

public class SaldoCuentaRowMapper implements RowMapper<SaldoCuenta>{
	@Override
	public SaldoCuenta mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new SaldoCuentaExtractor().extractData(rs);
		
	}

}
