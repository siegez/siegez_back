package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Usuario;
import com.siegez.rowmapper.extractors.UsuarioExtractor;

public class UsuarioRowMapper implements RowMapper<Usuario>  {

	@Override
	public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new UsuarioExtractor().extractData(rs);
		
	}

}
