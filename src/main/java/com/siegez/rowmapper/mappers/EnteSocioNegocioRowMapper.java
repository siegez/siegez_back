package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.EnteSocioNegocio;
import com.siegez.rowmapper.extractors.EnteSocioNegocioExtractor;

public class EnteSocioNegocioRowMapper implements RowMapper<EnteSocioNegocio> {
	@Override
	public EnteSocioNegocio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EnteSocioNegocioExtractor().extractData(rs);
		
	}
}
