package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.RegistroContable;
import com.siegez.rowmapper.extractors.RegistroContableExtractor;

public class RegistroContableRowMapper implements RowMapper<RegistroContable> {
	@Override
	public RegistroContable mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new RegistroContableExtractor().extractData(rs);
		
	}
}
