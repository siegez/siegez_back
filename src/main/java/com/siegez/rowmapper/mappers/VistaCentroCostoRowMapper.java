package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.views.VistaCentroCosto;
import com.siegez.rowmapper.extractors.VistaCentroCostoExtractor;

public class VistaCentroCostoRowMapper implements RowMapper<VistaCentroCosto>{
	
	@Override
	public VistaCentroCosto mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new VistaCentroCostoExtractor().extractData(rs);
		
	}
}
