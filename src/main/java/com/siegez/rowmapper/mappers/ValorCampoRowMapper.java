package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.ValorCampo;
import com.siegez.rowmapper.extractors.ValorCampoExtractor;

public class ValorCampoRowMapper implements RowMapper<ValorCampo> {

	@Override
	public ValorCampo mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ValorCampoExtractor().extractData(rs);
		
	}
}
