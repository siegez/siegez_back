package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.RegistroVenta;
import com.siegez.rowmapper.extractors.RegistroVentaExtractor;

public class RegistroVentaRowMapper implements RowMapper<RegistroVenta> {
	@Override
	public RegistroVenta mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new RegistroVentaExtractor().extractData(rs);
		
	}
}
