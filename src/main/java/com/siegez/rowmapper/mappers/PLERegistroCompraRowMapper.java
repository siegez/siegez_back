package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.rowmapper.extractors.PLERegistroCompraExtractor;
import com.siegez.reports.PLERegistroCompra;

public class PLERegistroCompraRowMapper implements RowMapper<PLERegistroCompra>{
	@Override
	public PLERegistroCompra mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PLERegistroCompraExtractor().extractData(rs);
		
	}
}
