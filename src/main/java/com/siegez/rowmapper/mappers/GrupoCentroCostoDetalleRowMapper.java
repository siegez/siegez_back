package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.rowmapper.extractors.GrupoCentroCostoDetalleExtractor;

public class GrupoCentroCostoDetalleRowMapper implements RowMapper<GrupoCentroCostoDetalle> {
	@Override
	public GrupoCentroCostoDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new GrupoCentroCostoDetalleExtractor().extractData(rs);
		
	}
}
