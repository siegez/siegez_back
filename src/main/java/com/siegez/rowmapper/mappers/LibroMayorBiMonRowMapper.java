package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.rowmapper.extractors.LibroMayorBiMonExtractor;

public class LibroMayorBiMonRowMapper implements RowMapper<LibroMayorBiMoneda>{
	@Override
	public LibroMayorBiMoneda mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LibroMayorBiMonExtractor().extractData(rs);
		
	}
}
