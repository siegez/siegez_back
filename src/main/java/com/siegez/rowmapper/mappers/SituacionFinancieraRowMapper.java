package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.SituacionFinanciera;
import com.siegez.rowmapper.extractors.SituacionFinancieraExtractor;

public class SituacionFinancieraRowMapper implements RowMapper<SituacionFinanciera>{
	@Override
	public SituacionFinanciera mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new SituacionFinancieraExtractor().extractData(rs);
		
	}
}
