package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.EmpresaPerfilUsuario;
import com.siegez.rowmapper.extractors.EmpresaPerfilUsuarioExtractor;

public class EmpresaPerfilUsuarioRowMapper implements RowMapper<EmpresaPerfilUsuario>{

	@Override
	public EmpresaPerfilUsuario mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EmpresaPerfilUsuarioExtractor().extractData(rs);
	}
}
