package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.EnteDireccion;
import com.siegez.rowmapper.extractors.EnteDireccionExtractor;

public class EnteDireccionRowMapper implements RowMapper<EnteDireccion> {
	@Override
	public EnteDireccion mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EnteDireccionExtractor().extractData(rs);
		
	}
}
