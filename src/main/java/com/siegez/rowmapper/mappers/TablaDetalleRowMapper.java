package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.TablaDetalle;
import com.siegez.rowmapper.extractors.TablaDetalleExtractor;

public class TablaDetalleRowMapper implements RowMapper<TablaDetalle>{

	
	@Override
	public TablaDetalle mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new TablaDetalleExtractor().extractData(rs);
		
	}
}
