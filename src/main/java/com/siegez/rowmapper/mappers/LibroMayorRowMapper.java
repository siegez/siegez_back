package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.reports.LibroMayor;
import com.siegez.rowmapper.extractors.LibroMayorExtractor;

public class LibroMayorRowMapper implements RowMapper<LibroMayor>{
	@Override
	public LibroMayor mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LibroMayorExtractor().extractData(rs);
		
	}
}
