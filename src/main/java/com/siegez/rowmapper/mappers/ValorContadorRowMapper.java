package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.siegez.rowmapper.extractors.ValorContadorExtractor;
import com.siegez.views.ValorContador;


public class ValorContadorRowMapper implements RowMapper<ValorContador>{
	@Override
	public ValorContador mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ValorContadorExtractor().extractData(rs);
		
	}
}
