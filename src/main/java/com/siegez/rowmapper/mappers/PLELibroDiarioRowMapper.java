package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.reports.PLELibroDiario;
import com.siegez.rowmapper.extractors.PLELibroDiarioExtractor;

public class PLELibroDiarioRowMapper implements RowMapper<PLELibroDiario>{
	@Override
	public PLELibroDiario mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PLELibroDiarioExtractor().extractData(rs);
		
	}
}
