package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.models.Perfil;
import com.siegez.rowmapper.extractors.PerfilExtractor;

public class PerfilRowMapper implements RowMapper<Perfil> {

	@Override
	public Perfil mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PerfilExtractor().extractData(rs);
		
	}
}
