package com.siegez.rowmapper.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.siegez.views.ConsultaString;
import com.siegez.rowmapper.extractors.ConsultaStringExtractor;

public class ConsultaStringRowMapper implements RowMapper<ConsultaString>{
	@Override
	public ConsultaString mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new ConsultaStringExtractor().extractData(rs);
		
	}
}
