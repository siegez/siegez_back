package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;
import com.siegez.views.ValoresIniciales;
import com.siegez.views.VistaCentroCosto;


public interface ConsultaService {
	
	public ValoresIniciales getValoresIniciales(String username);
	public List<ValoresIniciales> getAllValoresIniciales(String username);
	public ValorContador getpermitirDocumentoReferencia(Integer empresa, Integer ejerciciocontable, String tipoodocumento, String moneda);
	public List<VistaCentroCosto> getListaCentroCosto(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> getListaUbicacionGeografica();
	
}
