package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.GrupoCentroCosto;
import com.siegez.models.GrupoCentroCostoRegistro;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

public interface GrupoCentroCostoService {
	
	public ResultadoSP postRegistrarGrupoCentroCosto(GrupoCentroCostoRegistro grupocentrocostoregistro);
	public void postGrupoCentroCosto(GrupoCentroCosto grupocentrocosto);
	public void putGrupoCentroCosto(GrupoCentroCosto grupocentrocosto);
	public int deleteGrupoCentroCosto(GrupoCentroCosto grupocentrocosto);
	public GrupoCentroCosto getGrupoCentroCosto(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
	public List<GrupoCentroCosto> getListaGrupoCentroCosto(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
}
