package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.EnteDocumentoIdentidad;

public interface EnteDocumentoIdentidadService {
	public void postEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad);
	public void putEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad);
	public int deleteEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad);
	public EnteDocumentoIdentidad getEnteDocumentoIdentidad(Integer empresa, String entidad, Integer vez);
	public List<EnteDocumentoIdentidad> getListaEnteDocumentoIdentidad(Integer empresa, String entidad);
}
