package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.TipoDocMonCta;
import com.siegez.views.ValorContador;

public interface TipoDocMonCtaService {
	public void postTipoDocMonCta(TipoDocMonCta tipodocmoncta);
	public void putTipoDocMonCta(TipoDocMonCta tipodocmoncta);
	public int deleteTipoDocMonCta(TipoDocMonCta tipodocmoncta);
	public TipoDocMonCta getTipoDocMonCta(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda);
	public List<TipoDocMonCta> getListaTipoDocMonCta(Integer empresa, Integer ejerciciocontable);
	public ValorContador postContadorRegistros(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda);

}
