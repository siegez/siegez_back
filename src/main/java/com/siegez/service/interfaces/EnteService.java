package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Ente;
import com.siegez.models.EnteRegistro;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.SocioNegocio;
import com.siegez.views.ValorContador;

public interface EnteService {
	
	public ResultadoSP postRegistrarEnte(EnteRegistro enteregistro);
	public void postEnte(Ente ente);
	public void putEnte(Ente ente);
	public int deleteEnte(Ente ente);
	public Ente getEnte(Integer empresa, String entidad);
	public Ente getRucAnexo(Integer empresa, String ruc, String anexo);	
	public List<Ente> getListaEnte(Integer empresa);
	public List<SocioNegocio> getListaProveedor(Integer empresa);	
	public List<Catalogo> getListaProveedor(Integer empresa, String criterio);
	public ValorContador postContadorRegistros(Ente ente);
	public ValorContador postContadorRuc(Ente ente);
	public ValorContador postContadorProveedorRel(Ente ente);
	public List<Catalogo> postListaCatalogo(Ente ente);
	public List<SocioNegocio> getListaCliente(Integer empresa);	
	public List<Catalogo> getListaCliente(Integer empresa, String criterio);
	public ValorContador postContadorClienteRel(Ente ente);
	public List<SocioNegocio> getListaProveedorNoDom(Integer empresa);	
	public List<Catalogo> getListaProveedorNoDom(Integer empresa, String criterio);
	public ValorContador postContadorProveedorNoDomRel(Ente ente);
	public List<SocioNegocio> getListaBanco(Integer empresa);
	public List<SocioNegocio> getListaSocioNegocio(Integer empresa);
	public List<Catalogo> getListaSocioNegocio(Integer empresa, String criterio);
	public List<Ente> getListaEnteBanco(Integer empresa);
}

