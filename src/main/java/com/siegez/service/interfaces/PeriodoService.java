package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.Periodo;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface PeriodoService {

	public void postPeriodo(Periodo periodo);
	public void putPeriodo(Periodo periodo);
	public int deletePeriodo(Periodo periodo);
	public Periodo getPeriodo(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<Periodo> getListaPeriodo(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable);
}
