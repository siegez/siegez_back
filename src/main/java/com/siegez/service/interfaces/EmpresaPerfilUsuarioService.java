package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.EmpresaPerfilUsuario;

public interface EmpresaPerfilUsuarioService {

	public void postEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario);
	public void putEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario);
	public int deleteEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario);
	public EmpresaPerfilUsuario getEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario);
	public List<EmpresaPerfilUsuario> getListaEmpresaPerfilUsuario(String username);
	

}
