package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.views.ValorContador;

public interface GrupoCentroCostoDetalleService {
	
	public void postGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle);
	public void putGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle);
	public int deleteGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle);
	public GrupoCentroCostoDetalle getGrupoCentroCostoDetalle(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto);
	public List<GrupoCentroCostoDetalle> getListaGrupoCentroCostoDetalle(Integer empresa, Integer ejerciciocontable, String grupocentrocosto);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto);

}
