package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.AsientoContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface AsientoContableService {

	public void postAsientoContable(AsientoContable asientocontable);
	public void putAsientoContable(AsientoContable asientocontable);
	public int deleteAsientoContable(AsientoContable asientocontable);
	public AsientoContable getAsientoContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
	public List<AsientoContable> getListaAsientoContable(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable);

}
