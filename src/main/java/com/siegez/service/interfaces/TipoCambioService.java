package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.TipoCambio;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

public interface TipoCambioService {
	
	public ResultadoSP postRegistrarTipoCambio(TipoCambio tipocambio);
	public void postTipoCambio(TipoCambio tipocambio);
	public void putTipoCambio(TipoCambio tipocambio);
	public int deleteTipoCambio(TipoCambio tipocambio);
	public TipoCambio getTipoCambio(Integer empresa, Integer vez);
	public TipoCambio getTipoCambioDia(Integer empresa, String fecha, String moneda, String indicadorcierre);
	public List<TipoCambio> getListaTipoCambio(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, String fecha, String moneda, String indicadorcierre);
	public List<Catalogo> getListaCatalogo(Integer empresa, String fecha);

}
