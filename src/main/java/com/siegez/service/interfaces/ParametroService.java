package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Parametro;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface ParametroService {
	public void postParametro(Parametro parametro);
	public void putParametro(Parametro parametro);
	public int deleteParametro(Parametro parametro);
	public Parametro getParametro(Integer empresa, String modulo, String parametro);
	public List<Parametro> getListaParametro(Integer empresa);
	public List<Parametro> getListaSubModulo(Integer empresa, Integer ejerciciocontable, String modulo, String submodulo);
	public ValorContador postContadorRegistros(Integer empresa, String modulo, String parametro);
	public List<Catalogo> postListaCatalogo(Integer empresa);
}
