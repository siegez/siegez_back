package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.EnteCuentaBancaria;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface EnteCuentaBancariaService {
	public void postEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria);
	public void putEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria);
	public int deleteEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria);
	public EnteCuentaBancaria getEnteCuentaBancaria(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria);
	public List<EnteCuentaBancaria> getListaEnteCuentaBancaria(Integer empresa, Integer ejerciciocontable, String entidad);
	public ValorContador postContadorRegistros(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria);
	public List<Catalogo> postListaCatalogo(Integer empresa, Integer ejerciciocontable, String entidad);
}
