package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.SubDiario;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface SubDiarioService {
	
	public void postSubDiario(SubDiario subdiario);
	public void putSubDiario(SubDiario subdiario);
	public int deleteSubDiario(SubDiario subdiario);
	public SubDiario getSubDiario(Integer empresa, String subdiario);
	public List<SubDiario> getListaSubDiario(Integer empresa);
	public ValorContador postContadorRegistros(Integer empresa, String subdiario);
	public List<Catalogo> postListaCatalogo(Integer empresa);
	public List<Catalogo> postListaCatalogoCompras(Integer empresa);
	public List<Catalogo> postListaCatalogoVentas(Integer empresa);
	public List<Catalogo> postListaCatalogoHonorarios(Integer empresa);
	public List<Catalogo> postListaCatalogoNoDomiciliados(Integer empresa);
	public List<Catalogo> postListaCatalogoImportaciones(Integer empresa);
	public List<Catalogo> postListaCatalogoEgresos(Integer empresa);
	public List<Catalogo> postListaCatalogoIngresos(Integer empresa);
	public List<Catalogo> postListaCatalogoDiarios(Integer empresa);
}
