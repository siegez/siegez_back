package com.siegez.service.interfaces;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtUsuarioService {

	public UserDetails loadUserByUsername(String username); 
	
}
