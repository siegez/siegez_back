package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Menu;
import com.siegez.models.MenuOpcionesConsulta;

public interface MenuService {

	public void postMenu(Menu menu);
	public void putMenu(Menu menu);
	public Menu getMenu(Integer codigo);	
	public int deleteMenu(Integer codigo);
	public List<Menu> getAllMenu();
	public List<MenuOpcionesConsulta> getAllMenuOpciones(String username);
	public List<MenuOpcionesConsulta> getAllMenuEmpresa(String username, Integer idempcod);
	public List<MenuOpcionesConsulta> getAllMenuPerfilNuevo(String licencia);
	public List<MenuOpcionesConsulta> getAllMenuPerfilModifica(String perfil, String licencia);
}
