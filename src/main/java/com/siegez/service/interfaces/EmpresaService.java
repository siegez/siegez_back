package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Empresa;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;

public interface EmpresaService {
	public ResultadoSP postRegistrarEmpresa(Empresa empresa);
	public int postEmpresa(Empresa empresa);
	public void putEmpresa(Empresa empresa);
	public Empresa getEmpresa(Integer empresaCodigo);	
	public int deleteEmpresa(Integer empresaCodigo);
	public List<Empresa> getAllEmpresa();
	public List<Empresa> getAllEmpresaLicencia(String licencia);
	public List<Catalogo> getListaCatalogo(String licencia);
	public List<Catalogo> getListaCatalogoUsuario(String username);
	public ResultadoSP postReplicarEmpresa(Integer empresaorigen, Integer ejercicioorigen, Integer empresadestino, Integer ejerciciodestino);
}
