package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.TipoDocumento;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface TipoDocumentoService {

	public void postTipoDocumento(TipoDocumento tipodocumento);
	public void putTipoDocumento(TipoDocumento tipodocumento);
	public int deleteTipoDocumento(TipoDocumento tipodocumento);
	public TipoDocumento getTipoDocumento(Integer empresa, String tipodocumento);
	public List<TipoDocumento> getListaTipoDocumento(Integer empresa);
	public ValorContador postContadorRegistros(Integer empresa, String tipodocumento);
	public List<Catalogo> postListaCatalogo(Integer empresa);
	public List<Catalogo> postListaCatalogoCompras(Integer empresa);
	public List<Catalogo> postListaCatalogoVentas(Integer empresa);
	public List<Catalogo> postListaCatalogoHonorarios(Integer empresa);
	public List<Catalogo> postListaCatalogoNoDomiciliados(Integer empresa);
	public List<Catalogo> postListaCatalogoNoDomSustentos(Integer empresa);
	public List<Catalogo> postListaCatalogoImportaciones(Integer empresa);
	public List<Catalogo> postListaCatalogoEgresos(Integer empresa);
	public List<Catalogo> postListaCatalogoIngresos(Integer empresa);
	public List<Catalogo> postListaCatalogoDiarios(Integer empresa);
	
}
