package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.EnteDireccion;

public interface EnteDireccionService {
	
	public void postEnteDireccion(EnteDireccion entedireccion);
	public void putEnteDireccion(EnteDireccion entedireccion);
	public int deleteEnteDireccion(EnteDireccion entedireccion);
	public EnteDireccion getEnteDireccion(Integer empresa, String entidad, Integer vez);
	public List<EnteDireccion> getListaEnteDireccion(Integer empresa, String entidad);

}
