package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.EnteSocioNegocio;

public interface EnteSocioNegocioService {
	
	public void postEnteSocioNegocio(EnteSocioNegocio entesocionegocio);
	public void putEnteSocioNegocio(EnteSocioNegocio entesocionegocio);
	public int deleteEnteSocioNegocio(EnteSocioNegocio entesocionegocio);
	public EnteSocioNegocio getEnteSocioNegocio(Integer empresa, String entidad, Integer vez);
	public List<EnteSocioNegocio> getListaEnteSocioNegocio(Integer empresa, String entidad);

}

