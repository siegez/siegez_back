package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.PlanContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;
import com.siegez.views.VistaPlanContable;

public interface PlanContableService {
	
	public void postPlanContable(PlanContable plancontable);
	public void putPlanContable(PlanContable plancontable);
	public int deletePlanContable(PlanContable plancontable);
	public PlanContable getCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable);
	public List<PlanContable> getListaPlanContable(Integer empresa, Integer ejerciciocontable);
	public List<VistaPlanContable> getVistaPlanContable(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String cuentacontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String cuentacontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable);
	public ConsultaString getconsultarCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable);
}
