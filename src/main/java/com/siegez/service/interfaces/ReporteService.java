package com.siegez.service.interfaces;

import java.io.OutputStream;
import java.util.List;

import com.siegez.reports.BalanceComprobacion;
import com.siegez.reports.CajaBanco;
import com.siegez.reports.EstadoResultado;
import com.siegez.reports.LibroDiario;
import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.reports.LibroMayor;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.reports.PLELibroDiario;
import com.siegez.reports.PLERegistroCompra;
import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.reports.PLERegistroVenta;
import com.siegez.reports.RegistroCompra;
import com.siegez.reports.RegistroVenta;
import com.siegez.reports.SaldoCuenta;
import com.siegez.reports.SituacionFinanciera;

import net.sf.jasperreports.engine.JRException;


public interface ReporteService {
	
	
	public List<PLERegistroCompra> getListaPLERegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLERegistroCompraNoDomiciliado> getListaPLERegistroCompraNoDom(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLERegistroVenta> getListaPLERegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public List<PLAMEPrestadorServicio> getListaPLAMEPreSer(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   String ruc, String nombreempresa, String moneda, String orden);
	public List<PLELibroDiario> getListaPLELibroDiario(Integer empresa, Integer ejerciciocontable, String periodocontable);
	public void getPDFRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
									String nombreempresa, String moneda, String orden, OutputStream outputStream) throws JRException;
	public List<RegistroCompra> getListaRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   String ruc, String nombreempresa, String moneda, String orden);
	public void getPDFRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
									String nombreempresa, String moneda, String orden, OutputStream outputStream) throws JRException;
	public List<RegistroVenta> getListaRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   String ruc, String nombreempresa, String moneda, String orden);
	public void getPDFDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
									String subfin, String ruc, String nombreempresa, String moneda, String glosa,
									OutputStream outputStream) throws JRException;
	public List<LibroDiario> getListaDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
				String subfin, String ruc, String nombreempresa, String moneda, String glosa);
	public void getPDFDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
				String subfin, String ruc, String nombreempresa, String moneda, String categoria, OutputStream outputStream) throws JRException;
	public List<LibroDiarioSimplificado> getListaDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
				String subfin, String ruc, String nombreempresa, String moneda, String categoria);
	public void getPDFMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException;
	public List<LibroMayor> getListaMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public void getPDFMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException;
	public List<LibroMayorBiMoneda> getListaMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public void getPDFCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException;
	public List<CajaBanco> getListaCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa);
	public void getPDFBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException;
	public List<BalanceComprobacion> getListaBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda);
	public void getPDFSaldoCtaEntDoc(Integer empresa, Integer ejerciciocontable,
			String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha, OutputStream outputStream) throws JRException;
	public List<SaldoCuenta> getListaSaldoCuenta(Integer empresa, Integer ejerciciocontable,
			String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha);
	public void getPDFSitFin(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException;
	public void getPDFSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException;
	public List<SituacionFinanciera> getListaSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda);
	public void getPDFEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException;
	public List<EstadoResultado> getListaEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda);
	public void getPDFEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException;
	public List<EstadoResultado> getListaEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda);

	
}
