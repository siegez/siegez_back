package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.OperacionContable;
import com.siegez.models.OperacionRegistroContable;
import com.siegez.models.RegistroContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;

public interface OperacionContableService {

	public OperacionContable postRegistrarOperacion(OperacionRegistroContable operacionregistrocontable);
	public void postOperacionContable(OperacionContable operacioncontable);
	public void putOperacionContable(OperacionContable operacioncontable);
	public int deleteOperacionContable(OperacionContable operacioncontable);
	public OperacionContable getOperacionContable(Integer empresa, Integer ejerciciocontable, String operacioncontable);
	public List<OperacionContable> getListaOperacionContable(Integer empresa, Integer ejerciciocontable, String tipooperacion);
	public List<OperacionContable> getVistaOperacionContable(Integer empresa, Integer ejerciciocontable, String tipooperacion);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String operacioncontable);
	public ValorContador getContadorRegistrosComprobante(Integer empresa, String entidad, String numeroserie, String numerocomprobanteini);
	public List<Catalogo> postListaCatalogo(OperacionContable operacioncontable);
	public List<Catalogo> postListaCatalogoRef(OperacionContable operacioncontable);
	public List<ConsultaString> postListaSaldo(RegistroContable registrocontable);
}
