package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Perfil;

public interface PerfilService {
	public void postPerfil(Perfil perfil);
	public void putPerfil(Perfil perfil);
	public int deletePerfil(Integer codigo);
	public Perfil getPerfil(Integer codigo);	
	public List<Perfil> getAllPerfil(Integer empresa);

}
