package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.ValorFecha;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.Valor;

public interface ValorFechaService {
	
	public ResultadoSP postRegistrarValorFecha(ValorFecha valorfecha);
	public void postValorFecha(ValorFecha valorfecha);
	public void putValorFecha(ValorFecha valorfecha);
	public int deleteValorFecha(ValorFecha valorfecha);
	public ValorFecha getValorFecha(Integer empresa, String codigo, Integer vez);
	public Valor getValorPorFecha(Integer empresa, String codigo, String fecha);
	public List<ValorFecha> getListaValorFecha(Integer empresa, String codigo);
	public List<Catalogo> getListaCatalogo(Integer empresa, String fecha);
	
}
