package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.CentroCosto;

import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface CentroCostoService {
	
	public void postCentroCosto(CentroCosto centrocosto);
	public void putCentroCosto(CentroCosto centrocosto);
	public int deleteCentroCosto(CentroCosto centrocosto);
	public CentroCosto getCentroCosto(Integer empresa, Integer ejerciciocontable, String centrocosto);
	public List<CentroCosto> getListaCentroCosto(Integer empresa, Integer ejerciciocontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String centrocosto);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String centrocosto);
	public List<Catalogo> getNotInCatalogo(CentroCosto centrocosto);

}
