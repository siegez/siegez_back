package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.Usuario;
import com.siegez.views.ValorContador;

public interface UsuarioService {

	public void postUsuario(Usuario usuario);
	public void putUsuario(Usuario usuario);
	public Usuario getUsuario(String username);	
	public int deleteUsuario(String username);
	public List<Usuario> getListaUsuario();
	public List<Usuario> getListaUsuarioLicencia(String licencia);
	public ValorContador postContadorRegistros(Usuario usuario);
}

