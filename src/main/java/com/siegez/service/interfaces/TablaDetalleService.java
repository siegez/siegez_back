package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.TablaDetalle;

public interface TablaDetalleService {
	public void postTablaDetalle(TablaDetalle tabladetalle);
	public void putTablaDetalle(TablaDetalle tabladetalle);
	public int deleteTablaDetalle(TablaDetalle tabladetalle);
	public TablaDetalle getTablaDetalle(TablaDetalle tabladetalle);
	public List<TablaDetalle> getListaTablaDetalleDescripcion(Integer empresa, Integer tabla);
	public List<TablaDetalle> getListaTablaDetalleCodigo(Integer empresa, Integer tabla);
}
