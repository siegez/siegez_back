package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Detraccion;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface DetraccionService {
	
	public void postDetraccion(Detraccion detraccion);
	public void putDetraccion(Detraccion detraccion);
	public int deleteDetraccion(Detraccion detraccion);
	public Detraccion getDetraccion(Integer empresa, String detraccion);
	public List<Detraccion> getListaDetraccion(Integer empresa);
	public ValorContador postContadorRegistros(Integer empresa, String detraccion);
	public List<Catalogo> postListaCatalogo(Integer empresa);

}
