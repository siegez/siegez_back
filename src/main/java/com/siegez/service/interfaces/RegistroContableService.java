package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.RegistroContable;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

public interface RegistroContableService {
	public void postRegistroContable(RegistroContable registrocontable);
	public void putRegistroContable(RegistroContable registrocontable);
	public int deleteRegistroContable(RegistroContable registrocontable);
	public int deleteOperacionContable(RegistroContable registrocontable);
	public RegistroContable getRegistroContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable);
	public List<RegistroContable> getListaRegistroContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
	public List<RegistroContable> getListaOperacionContable(Integer empresa, Integer ejerciciocontable, String operacioncontable);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, Integer asientocontable);
}
