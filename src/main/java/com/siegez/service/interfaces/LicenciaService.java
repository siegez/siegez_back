package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.Licencia;

public interface LicenciaService {

	public Licencia getEmpresaPerfilUsuario(String licenciaCodigo);
	public List<Licencia> getAllEmpresaPerfilUsuario();
	
}
