package com.siegez.service.interfaces;

import java.util.List;

import com.siegez.models.TablaModulo;

public interface TablaModuloService {

	public List<TablaModulo> getAllTablaModulo(String modulo);
	
}
