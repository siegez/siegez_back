package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.Ejercicio;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;


public interface EjercicioService {

	public void postEjercicio(Ejercicio ejercicio);
	public void putEjercicio(Ejercicio ejercicio);
	public int deleteEjercicio(Ejercicio ejercicio);
	public Ejercicio getEjercicio(Integer empresa, Integer ejerciciocontable);
	public List<Ejercicio> getListaEjercicio(Integer empresa);
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable);
	public List<Catalogo> getListaCatalogo(Integer empresa);
	
}
