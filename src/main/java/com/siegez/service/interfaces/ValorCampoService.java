package com.siegez.service.interfaces;

import java.util.List;
import com.siegez.models.ValorCampo;

public interface ValorCampoService {

	
	public void postValorCampo(ValorCampo valorcampo);
	public void putValorCampo(ValorCampo valorcampo);
	public ValorCampo getValorCampo(ValorCampo valorcampo);	
	public int deleteValorCampo(ValorCampo valorcampo);
	public List<ValorCampo> getAll();
	public List<ValorCampo> getAllValorDescripcion(ValorCampo valorcampo);
	public List<ValorCampo> getNotInValorDescripcion(ValorCampo valorcampo);
	public List<ValorCampo> getAllValorCodigo(ValorCampo valorcampo);
}
