package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.OperacionContableDao;
import com.siegez.models.OperacionContable;
import com.siegez.models.OperacionRegistroContable;
import com.siegez.models.RegistroContable;
import com.siegez.service.interfaces.OperacionContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;

@Service
public class OperacionContableServiceImp implements OperacionContableService {

	@Autowired
	private OperacionContableDao operacioncontableDao;
	
	@Override
	public OperacionContable postRegistrarOperacion(OperacionRegistroContable operacionregistrocontable) {
		return this.operacioncontableDao.registrarOperacion(operacionregistrocontable);
	}
	
	@Override
	public void postOperacionContable(OperacionContable operacioncontable) {
		this.operacioncontableDao.insertar(operacioncontable);
		
	}

	@Override
	public void putOperacionContable(OperacionContable operacioncontable) {
		this.operacioncontableDao.editar(operacioncontable);
		
	}

	@Override
	public int deleteOperacionContable(OperacionContable operacioncontable) {
		return this.operacioncontableDao.eliminar(operacioncontable);
	}

	@Override
	public OperacionContable getOperacionContable(Integer empresa, Integer ejerciciocontable, String operacioncontable) {
		return this.operacioncontableDao.consultar(empresa, ejerciciocontable, operacioncontable);
	}

	@Override
	public List<OperacionContable> getListaOperacionContable(Integer empresa, Integer ejerciciocontable, String tipooperacion) {
		return this.operacioncontableDao.consultarLista(empresa, ejerciciocontable, tipooperacion);
	}
	
	@Override
	public List<OperacionContable> getVistaOperacionContable(Integer empresa, Integer ejerciciocontable, String tipooperacion) {
		return this.operacioncontableDao.consultarVista(empresa, ejerciciocontable, tipooperacion);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String operacioncontable) {
		return this.operacioncontableDao.contadorRegistros(empresa, ejerciciocontable, operacioncontable);
	}
	
	@Override
	public ValorContador getContadorRegistrosComprobante(Integer empresa, String entidad, String numeroserie, String numerocomprobanteini) {
		return this.operacioncontableDao.contadorRegistrosComprobante(empresa, entidad, numeroserie, numerocomprobanteini);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(OperacionContable operacioncontable) {
		return this.operacioncontableDao.consultarCatalogo(operacioncontable);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoRef(OperacionContable operacioncontable) {
		return this.operacioncontableDao.consultarCatalogoRef(operacioncontable);
	}
	
	@Override
	public List<ConsultaString> postListaSaldo(RegistroContable registrocontable) {
		return this.operacioncontableDao.consultarSaldo(registrocontable);
	}
}
