package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EnteCuentaBancariaDao;
import com.siegez.models.EnteCuentaBancaria;
import com.siegez.service.interfaces.EnteCuentaBancariaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class EnteCuentaBancariaServiceImp implements EnteCuentaBancariaService {
	@Autowired
	private EnteCuentaBancariaDao entecuentabancariaDao;
	
	@Override
	public void postEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria) {
		this.entecuentabancariaDao.insertar(entecuentabancaria);
		
	}

	@Override
	public void putEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria) {
		this.entecuentabancariaDao.editar(entecuentabancaria);
		
	}

	@Override
	public int deleteEnteCuentaBancaria(EnteCuentaBancaria entecuentabancaria) {
		return this.entecuentabancariaDao.eliminar(entecuentabancaria);
	}

	@Override
	public EnteCuentaBancaria getEnteCuentaBancaria(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria) {
		return this.entecuentabancariaDao.consultar(empresa, ejerciciocontable, entidad, cuentabancaria);
	}

	@Override
	public List<EnteCuentaBancaria> getListaEnteCuentaBancaria(Integer empresa, Integer ejerciciocontable, String entidad) {
		return this.entecuentabancariaDao.consultarLista(empresa, ejerciciocontable, entidad);
	}
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, Integer ejerciciocontable, String entidad, String cuentabancaria) {
		return this.entecuentabancariaDao.contadorRegistros(empresa, ejerciciocontable, entidad, cuentabancaria);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Integer empresa, Integer ejerciciocontable, String entidad) {
		return this.entecuentabancariaDao.consultarCatalogo(empresa, ejerciciocontable, entidad);
	}
}
