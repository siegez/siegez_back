package com.siegez.service.implementation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.SubDiarioDao;
import com.siegez.models.SubDiario;
import com.siegez.service.interfaces.SubDiarioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;
@Service
public class SubDiarioServiceImp implements SubDiarioService {

	@Autowired
	private SubDiarioDao subdiarioDao;
	
	@Override
	public void postSubDiario(SubDiario subdiario) {
		this.subdiarioDao.insertar(subdiario);
		
	}

	@Override
	public void putSubDiario(SubDiario subdiario) {
		this.subdiarioDao.editar(subdiario);
		
	}

	@Override
	public int deleteSubDiario(SubDiario subdiario) {
		return this.subdiarioDao.eliminar(subdiario);
	}

	@Override
	public SubDiario getSubDiario(Integer empresa, String subdiario) {
		return this.subdiarioDao.consultar(empresa, subdiario);
	}

	@Override
	public List<SubDiario> getListaSubDiario(Integer empresa) {
		return this.subdiarioDao.consultarLista(empresa);
	}
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, String subdiario) {
		return this.subdiarioDao.contadorRegistros(empresa, subdiario);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Integer empresa) {
		return this.subdiarioDao.consultarCatalogo(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoCompras(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoCompras(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoVentas(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoVentas(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoHonorarios(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoHonorarios(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoNoDomiciliados(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoNoDomiciliados(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoImportaciones(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoImportaciones(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoEgresos(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoEgresos(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoIngresos(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoIngresos(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoDiarios(Integer empresa) {
		return this.subdiarioDao.consultarCatalogoDiarios(empresa);
	}
}
