package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.siegez.daos.interfaces.TablaModuloDao;
import com.siegez.models.TablaModulo;
import com.siegez.service.interfaces.TablaModuloService;


@Service
public class TablaModuloServiceImp implements TablaModuloService{
	
	@Autowired
	private TablaModuloDao tablamoduloDao;
	
	@Override
	public List<TablaModulo> getAllTablaModulo(String modulo) {
		return this.tablamoduloDao.findAll(modulo);
	}

}
