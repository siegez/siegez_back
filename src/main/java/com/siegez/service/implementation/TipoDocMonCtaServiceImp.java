package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.TipoDocMonCtaDao;
import com.siegez.models.TipoDocMonCta;
import com.siegez.service.interfaces.TipoDocMonCtaService;
import com.siegez.views.ValorContador;

@Service
public class TipoDocMonCtaServiceImp implements TipoDocMonCtaService {

	@Autowired
	private TipoDocMonCtaDao tipodocmonctaDao;
	
	@Override
	public void postTipoDocMonCta(TipoDocMonCta tipodocmoncta) {
		this.tipodocmonctaDao.insertar(tipodocmoncta);
		
	}

	@Override
	public void putTipoDocMonCta(TipoDocMonCta tipodocmoncta) {
		this.tipodocmonctaDao.editar(tipodocmoncta);
		
	}

	@Override
	public int deleteTipoDocMonCta(TipoDocMonCta tipodocmoncta) {
		return this.tipodocmonctaDao.eliminar(tipodocmoncta);
	}

	@Override
	public TipoDocMonCta getTipoDocMonCta(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda) {
		return this.tipodocmonctaDao.consultar(empresa, ejerciciocontable, tipodocumento, moneda);
	}

	@Override
	public List<TipoDocMonCta> getListaTipoDocMonCta(Integer empresa, Integer ejerciciocontable) {
		return this.tipodocmonctaDao.consultarLista(empresa, ejerciciocontable);
	}
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, Integer ejerciciocontable, String tipodocumento, String moneda) {
		return this.tipodocmonctaDao.contadorRegistros(empresa, ejerciciocontable, tipodocumento, moneda);
	}
	

}
