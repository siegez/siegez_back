package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.PerfilDao;
import com.siegez.models.Perfil;
import com.siegez.service.interfaces.PerfilService;

@Service
public class PerfilServiceImp implements PerfilService {
	@Autowired
	private PerfilDao perfilDao;
	
	@Override
	public void postPerfil(Perfil perfil) {
		this.perfilDao.save(perfil);
	}

	@Override
	public void putPerfil(Perfil perfil) {
		this.perfilDao.update(perfil);
	}
	
	@Override
	public int deletePerfil(Integer codigo) {
		return this.perfilDao.delete(codigo);
	}
	
	@Override
	public Perfil getPerfil(Integer codigo) {
		return this.perfilDao.find(codigo);
	}

	@Override
	public List<Perfil> getAllPerfil(Integer empresa) {
		return this.perfilDao.findAll(empresa);
	}

	
}
