package com.siegez.service.implementation;

import java.io.OutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.siegez.daos.interfaces.ReporteDao;
import com.siegez.reports.BalanceComprobacion;
import com.siegez.reports.CajaBanco;
import com.siegez.reports.EstadoResultado;
import com.siegez.reports.LibroDiario;
import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.reports.LibroMayor;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.reports.PLELibroDiario;
import com.siegez.reports.PLERegistroCompra;
import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.reports.PLERegistroVenta;
import com.siegez.reports.RegistroCompra;
import com.siegez.reports.RegistroVenta;
import com.siegez.reports.SaldoCuenta;
import com.siegez.reports.SituacionFinanciera;
import com.siegez.service.interfaces.ReporteService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class ReporteServiceImp implements ReporteService {
	
	@Autowired
	private ReporteDao reporteDao;
	
	
	
	@Override
	public List<PLERegistroCompra> getListaPLERegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable) {
		return this.reporteDao.consultarPLERegistroCompra(empresa, ejerciciocontable, periodocontable);
	}
	
	@Override
	public List<PLERegistroCompraNoDomiciliado> getListaPLERegistroCompraNoDom(Integer empresa, Integer ejerciciocontable, String periodocontable) {
		return this.reporteDao.consultarPLERegistroCompraNoDom(empresa, ejerciciocontable, periodocontable);
	}
	
	@Override
	public List<PLERegistroVenta> getListaPLERegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable) {
		return this.reporteDao.consultarPLERegistroVenta(empresa, ejerciciocontable, periodocontable);
	}
	
	@Override
	public List<PLAMEPrestadorServicio> getListaPLAMEPreSer(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   										   String ruc, String nombreempresa, String moneda, String orden) {
		return this.reporteDao.consultarPLAMEPreSer(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden);
	}
	
	@Override
	public List<PLELibroDiario> getListaPLELibroDiario(Integer empresa, Integer ejerciciocontable, String periodocontable){
		return this.reporteDao.consultarPLELibroDiario(empresa, ejerciciocontable, periodocontable);
	}	
	
	@Override
	public void getPDFRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
			String nombreempresa, String moneda, String orden, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFRegistroCompra(empresa, ejerciciocontable, periodocontable,  ruc, nombreempresa, moneda, orden);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<RegistroCompra> getListaRegistroCompra(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   										   String ruc, String nombreempresa, String moneda, String orden) {
		return this.reporteDao.consultarRegistroCompra(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden);
	}
	
	@Override
	public void getPDFRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, String ruc, 
				String nombreempresa, String moneda, String orden, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFRegistroVenta(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<RegistroVenta> getListaRegistroVenta(Integer empresa, Integer ejerciciocontable, String periodocontable, 
			   										   String ruc, String nombreempresa, String moneda, String orden) {
		return this.reporteDao.consultarRegistroVenta(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden);
	}

	@Override
	public void getPDFDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
			 String subfin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException {
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFDiario(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, glosa);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<LibroDiario> getListaDiario(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
												String subfin, String ruc, String nombreempresa, String moneda, String glosa){
		return this.reporteDao.consultarDiario(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, glosa);
	}
	
	@Override
	public void getPDFDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
			String subfin, String ruc, String nombreempresa, String moneda, String categoria, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFDiarioSim(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, categoria);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<LibroDiarioSimplificado> getListaDiarioSim(Integer empresa, Integer ejerciciocontable, String periodocontable, String subini,
			String subfin, String ruc, String nombreempresa, String moneda, String categoria){
		return this.reporteDao.consultarDiarioSim(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, categoria);
	}
	
	@Override
	public void getPDFMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFMayor(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<LibroMayor> getListaMayor(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		return this.reporteDao.consultarMayor(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
	}
	
	@Override
	public void getPDFMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFMayorBiMon(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<LibroMayorBiMoneda> getListaMayorBiMon(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		return this.reporteDao.consultarMayorBiMon(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
	}
	
	@Override
	public void getPDFCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFCajaBanco(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<CajaBanco> getListaCajaBanco(Integer empresa, Integer ejerciciocontable, String periodocontable, String ctaini,
			String ctafin, String ruc, String nombreempresa, String moneda, String glosa){
		return this.reporteDao.consultarCajaBanco(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa);
	}
	
	@Override
	public void getPDFBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFBalanceComprobacion(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<BalanceComprobacion> getListaBalanceComprobacion(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda){
		return this.reporteDao.consultarBalanceComprobacion(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
	}
	
	@Override
	public void getPDFSaldoCtaEntDoc(Integer empresa, Integer ejerciciocontable,
			String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFSaldoCtaEntDoc(empresa, ejerciciocontable, tiposaldo, ctaini, ctafin, ruc, nombreempresa, fecha);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}	
	
	@Override
	public List<SaldoCuenta> getListaSaldoCuenta(Integer empresa, Integer ejerciciocontable,
			String tiposaldo, String ctaini, String ctafin, String ruc, String nombreempresa, String fecha){
		return this.reporteDao.consultarSaldoCuenta(empresa, ejerciciocontable, tiposaldo, ctaini, ctafin, ruc, nombreempresa, fecha);
	}
	
	@Override
	public void getPDFSitFin(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFSitFin(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public void getPDFSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFSitFinVer(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<SituacionFinanciera> getListaSitFinVer(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda){
		return this.reporteDao.consultarSitFinVer(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
	}
	
	@Override
	public void getPDFEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFEstResNat(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<EstadoResultado> getListaEstResNat(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda){
		return this.reporteDao.consultarEstResNat(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
	}
	
	@Override
	public void getPDFEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda, OutputStream outputStream) throws JRException{
		
		JasperPrint jasperPrint = this.reporteDao.exportarPDFEstResFun(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
		
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);	  
	}
	
	@Override
	public List<EstadoResultado> getListaEstResFun(Integer empresa, Integer ejerciciocontable, String periodocontable,
			String ruc, String nombreempresa, String moneda){
		return this.reporteDao.consultarEstResFun(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda);
	}
	
}
