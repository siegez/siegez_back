package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.ParametroDao;
import com.siegez.models.Parametro;
import com.siegez.service.interfaces.ParametroService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class ParametroServiceImp implements ParametroService {
	@Autowired
	private ParametroDao parametroDao;
	
	@Override
	public void postParametro(Parametro parametro) {
		this.parametroDao.insertar(parametro);
		
	}

	@Override
	public void putParametro(Parametro parametro) {
		this.parametroDao.editar(parametro);
		
	}

	@Override
	public int deleteParametro(Parametro parametro) {
		return this.parametroDao.eliminar(parametro);
	}

	@Override
	public Parametro getParametro(Integer empresa, String modulo, String parametro) {
		return this.parametroDao.consultar(empresa, modulo, parametro);
	}

	@Override
	public List<Parametro> getListaParametro(Integer empresa) {
		return this.parametroDao.consultarLista(empresa);
	}
	
	@Override
	public List<Parametro> getListaSubModulo(Integer empresa, Integer ejerciciocontable, String modulo, String submodulo){
		return this.parametroDao.consultarListaSubModulo(empresa, ejerciciocontable, modulo, submodulo);
	}
	
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, String modulo, String parametro) {
		return this.parametroDao.contadorRegistros(empresa, modulo, parametro);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Integer empresa) {
		return this.parametroDao.consultarCatalogo(empresa);
	}
}
