package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.DetraccionDao;
import com.siegez.models.Detraccion;
import com.siegez.service.interfaces.DetraccionService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class DetraccionServiceImp implements DetraccionService {

	@Autowired
	private DetraccionDao detraccionDao;
	
	@Override
	public void postDetraccion(Detraccion detraccion) {
		this.detraccionDao.insertar(detraccion);
		
	}

	@Override
	public void putDetraccion(Detraccion detraccion) {
		this.detraccionDao.editar(detraccion);
		
	}

	@Override
	public int deleteDetraccion(Detraccion detraccion) {
		return this.detraccionDao.eliminar(detraccion);
	}

	@Override
	public Detraccion getDetraccion(Integer empresa, String detraccion) {
		return this.detraccionDao.consultar(empresa, detraccion);
	}

	@Override
	public List<Detraccion> getListaDetraccion(Integer empresa) {
		return this.detraccionDao.consultarLista(empresa);
	}
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, String detraccion) {
		return this.detraccionDao.contadorRegistros(empresa, detraccion);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Integer empresa) {
		return this.detraccionDao.consultarCatalogo(empresa);
	}
}
