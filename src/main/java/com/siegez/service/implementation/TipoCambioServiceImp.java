package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.TipoCambioDao;
import com.siegez.models.TipoCambio;
import com.siegez.service.interfaces.TipoCambioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@Service
public class TipoCambioServiceImp implements TipoCambioService {
	
	@Autowired
	private TipoCambioDao tipocambioDao;
	
	@Override
	public ResultadoSP postRegistrarTipoCambio(TipoCambio tipocambio) {
		return this.tipocambioDao.registrarTipoCambio(tipocambio);
	}
	
	@Override
	public void postTipoCambio(TipoCambio tipocambio) {
		this.tipocambioDao.insertar(tipocambio);		
	}

	@Override
	public void putTipoCambio(TipoCambio tipocambio) {
		this.tipocambioDao.editar(tipocambio);		
	}

	@Override
	public int deleteTipoCambio(TipoCambio tipocambio) {
		return this.tipocambioDao.eliminar(tipocambio);
	}

	@Override
	public TipoCambio getTipoCambio(Integer empresa, Integer vez) {
		return this.tipocambioDao.consultar(empresa, vez);
	}
	
	@Override
	public TipoCambio getTipoCambioDia(Integer empresa, String fecha, String moneda, String indicadorcierre) {
		return this.tipocambioDao.consultarDia(empresa, fecha, moneda, indicadorcierre);
	}

	@Override
	public List<TipoCambio> getListaTipoCambio(Integer empresa, Integer ejerciciocontable) {
		return this.tipocambioDao.consultarLista(empresa, ejerciciocontable);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, String fecha, String moneda, String indicadorcierre) {
		return this.tipocambioDao.contadorRegistros(empresa, fecha, moneda, indicadorcierre);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, String fecha) {
		return this.tipocambioDao.consultarCatalogo(empresa, fecha);
	}	

}
