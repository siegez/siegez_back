package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.GrupoCentroCostoDao;
import com.siegez.models.GrupoCentroCosto;
import com.siegez.models.GrupoCentroCostoRegistro;
import com.siegez.service.interfaces.GrupoCentroCostoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@Service
public class GrupoCentroCostoServiceImp implements GrupoCentroCostoService {


	@Autowired
	private GrupoCentroCostoDao grupocentrocostoDao;
	
	@Override
	public ResultadoSP postRegistrarGrupoCentroCosto(GrupoCentroCostoRegistro grupocentrocostoregistro) {
		return this.grupocentrocostoDao.registrarGrupoCentroCosto(grupocentrocostoregistro);
	}
	
	@Override
	public void postGrupoCentroCosto(GrupoCentroCosto grupocentrocosto) {
		this.grupocentrocostoDao.insertar(grupocentrocosto);		
	}

	@Override
	public void putGrupoCentroCosto(GrupoCentroCosto grupocentrocosto) {
		this.grupocentrocostoDao.editar(grupocentrocosto);		
	}

	@Override
	public int deleteGrupoCentroCosto(GrupoCentroCosto grupocentrocosto) {
		return this.grupocentrocostoDao.eliminar(grupocentrocosto);
	}

	@Override
	public GrupoCentroCosto getGrupoCentroCosto(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		return this.grupocentrocostoDao.consultar(empresa, ejerciciocontable, grupocentrocosto);
	}

	@Override
	public List<GrupoCentroCosto> getListaGrupoCentroCosto(Integer empresa, Integer ejerciciocontable) {
		return this.grupocentrocostoDao.consultarLista(empresa, ejerciciocontable );
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		return this.grupocentrocostoDao.contadorRegistros(empresa, ejerciciocontable, grupocentrocosto);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable) {
		return this.grupocentrocostoDao.listarCatalogo(empresa, ejerciciocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		return this.grupocentrocostoDao.listarCatalogo(empresa, ejerciciocontable, grupocentrocosto);
	}

		
}
