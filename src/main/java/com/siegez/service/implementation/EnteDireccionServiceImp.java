package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EnteDireccionDao;
import com.siegez.models.EnteDireccion;
import com.siegez.service.interfaces.EnteDireccionService;

@Service
public class EnteDireccionServiceImp implements EnteDireccionService {

	@Autowired
	private EnteDireccionDao entedireccionDao;
	
	@Override
	public void postEnteDireccion(EnteDireccion entedireccion) {
		this.entedireccionDao.insertar(entedireccion);
		
	}

	@Override
	public void putEnteDireccion(EnteDireccion entedireccion) {
		this.entedireccionDao.editar(entedireccion);
		
	}

	@Override
	public int deleteEnteDireccion(EnteDireccion entedireccion) {
		return this.entedireccionDao.eliminar(entedireccion);
	}

	@Override
	public EnteDireccion getEnteDireccion(Integer empresa, String entidad, Integer vez) {
		return this.entedireccionDao.consultar(empresa, entidad, vez);
	}
	
	@Override
	public List<EnteDireccion> getListaEnteDireccion(Integer empresa, String entidad) {
		return this.entedireccionDao.consultarLista(empresa, entidad);
	}	
}
