package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.TipoDocumentoDao;
import com.siegez.models.TipoDocumento;
import com.siegez.service.interfaces.TipoDocumentoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class TipoDocumentoServiceImp implements TipoDocumentoService{

	@Autowired
	private TipoDocumentoDao tipodocumentoDao;
	
	@Override
	public void postTipoDocumento(TipoDocumento tipodocumento) {
		this.tipodocumentoDao.insertar(tipodocumento);
		
	}

	@Override
	public void putTipoDocumento(TipoDocumento tipodocumento) {
		this.tipodocumentoDao.editar(tipodocumento);
		
	}

	@Override
	public int deleteTipoDocumento(TipoDocumento tipodocumento) {
		return this.tipodocumentoDao.eliminar(tipodocumento);
	}

	@Override
	public TipoDocumento getTipoDocumento(Integer empresa, String tipodocumento) {
		return this.tipodocumentoDao.consultar(empresa, tipodocumento);
	}

	@Override
	public List<TipoDocumento> getListaTipoDocumento(Integer empresa) {
		return this.tipodocumentoDao.consultarLista(empresa);
	}
	
	@Override
	public ValorContador postContadorRegistros(Integer empresa, String tipodocumento) {
		return this.tipodocumentoDao.contadorRegistros(empresa, tipodocumento);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogo(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoCompras(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoCompras(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoVentas(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoVentas(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoHonorarios(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoHonorarios(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoNoDomiciliados(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoNoDomiciliados(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoNoDomSustentos(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoNoDomSustentos(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoImportaciones(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoImportaciones(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoEgresos(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoEgresos(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoIngresos(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoIngresos(empresa);
	}
	
	@Override
	public List<Catalogo> postListaCatalogoDiarios(Integer empresa) {
		return this.tipodocumentoDao.consultarCatalogoDiarios(empresa);
	}
	
	
}
