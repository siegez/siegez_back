package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.siegez.daos.interfaces.EmpresaDao;
import com.siegez.models.Empresa;
import com.siegez.service.interfaces.EmpresaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;

@Service
public class EmpresaServiceImp  implements EmpresaService{

	@Autowired
	 private EmpresaDao empresaDao;
	
	@Override
	public ResultadoSP postRegistrarEmpresa(Empresa empresa) {
		return this.empresaDao.registrarEmpresa(empresa);
	}
	
	@Override
	public int postEmpresa(Empresa empresa) {
		// TODO Auto-generated method stub
		return this.empresaDao.save(empresa);
	}

	@Override
	public void putEmpresa(Empresa empresa) {
		// TODO Auto-generated method stub
		this.empresaDao.update(empresa);
	}

	@Override
	public Empresa getEmpresa(Integer empresaCodigo) {
		return this.empresaDao.find(empresaCodigo);
	}

	@Override
	public int deleteEmpresa(Integer empresaCodigo) {
		return this.empresaDao.delete(empresaCodigo);
	}

	@Override
	public List<Empresa> getAllEmpresa() {
		return this.empresaDao.findAll();
	}

	@Override
	public List<Empresa> getAllEmpresaLicencia(String licencia) {
		return this.empresaDao.findAllUsuarioLicencia(licencia);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(String licencia) {
		return this.empresaDao.consultarCatalogo(licencia);
	}
	
	@Override
	public List<Catalogo> getListaCatalogoUsuario(String username) {
		return this.empresaDao.consultarCatalogoUsuario(username);
	}
	
	@Override	
	public ResultadoSP postReplicarEmpresa(Integer empresaorigen, Integer ejercicioorigen, Integer empresadestino, Integer ejerciciodestino) {
		return this.empresaDao.replicarEmpresa(empresaorigen, ejercicioorigen, empresadestino, ejerciciodestino);
	}
}
