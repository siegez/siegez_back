package com.siegez.service.implementation;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.siegez.daos.interfaces.ValorCampoDao;
import com.siegez.models.ValorCampo;
import com.siegez.service.interfaces.ValorCampoService;

@Service
public class ValorCampoServiceImp implements ValorCampoService {

	@Autowired
	private ValorCampoDao valorcampoDao;
	
	@Override
	public void postValorCampo(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void putValorCampo(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ValorCampo getValorCampo(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteValorCampo(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<ValorCampo> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ValorCampo> getAllValorDescripcion(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return this.valorcampoDao.findAllValorDescripcion(valorcampo);
	}
	
	@Override
	public List<ValorCampo> getNotInValorDescripcion(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return this.valorcampoDao.findNotInValorDescripcion(valorcampo);
	}

	@Override
	public List<ValorCampo> getAllValorCodigo(ValorCampo valorcampo) {
		// TODO Auto-generated method stub
		return this.valorcampoDao.findAllValorCodigo(valorcampo);
	}
}
