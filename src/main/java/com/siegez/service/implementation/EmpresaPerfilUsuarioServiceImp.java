package com.siegez.service.implementation;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EmpresaPerfilUsuarioDao;
import com.siegez.models.EmpresaPerfilUsuario;
import com.siegez.service.interfaces.EmpresaPerfilUsuarioService;

@Service
public class EmpresaPerfilUsuarioServiceImp implements EmpresaPerfilUsuarioService{

	@Autowired
	private EmpresaPerfilUsuarioDao empresaperfilusuarioDao;
	
	@Override
	public void postEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario) {
		this.empresaperfilusuarioDao.insertar(empresaperfilusuario);
	}
	
	@Override
	public void putEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario) {
		this.empresaperfilusuarioDao.editar(empresaperfilusuario);
	}
	
	@Override
	public int deleteEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario) {
		return this.empresaperfilusuarioDao.eliminar(empresaperfilusuario);
	}
	
	@Override
	public EmpresaPerfilUsuario getEmpresaPerfilUsuario(EmpresaPerfilUsuario empresaperfilusuario) {
		return this.empresaperfilusuarioDao.consultar(empresaperfilusuario);
	}
	
	@Override
	public List<EmpresaPerfilUsuario> getListaEmpresaPerfilUsuario(String username) {
		return this.empresaperfilusuarioDao.consultarLista(username);
	}
	
}
