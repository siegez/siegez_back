package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EnteDocumentoIdentidadDao;
import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.service.interfaces.EnteDocumentoIdentidadService;

@Service
public class EnteDocumentoIdentidadServiceImp  implements EnteDocumentoIdentidadService {

	
	@Autowired
	private EnteDocumentoIdentidadDao entedocumentoidentidadDao;
	
	@Override
	public void postEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad) {
		this.entedocumentoidentidadDao.insertar(entedocumentoidentidad);
		
	}

	@Override
	public void putEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad) {
		this.entedocumentoidentidadDao.editar(entedocumentoidentidad);
		
	}

	@Override
	public int deleteEnteDocumentoIdentidad(EnteDocumentoIdentidad entedocumentoidentidad) {
		return this.entedocumentoidentidadDao.eliminar(entedocumentoidentidad);
	}

	@Override
	public EnteDocumentoIdentidad getEnteDocumentoIdentidad(Integer empresa, String entidad, Integer vez) {
		return this.entedocumentoidentidadDao.consultar(empresa, entidad, vez);
	}
	
	@Override
	public List<EnteDocumentoIdentidad> getListaEnteDocumentoIdentidad(Integer empresa, String entidad) {
		return this.entedocumentoidentidadDao.consultarLista(empresa, entidad);
	}
}
