package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.AsientoContableDao;
import com.siegez.models.AsientoContable;
import com.siegez.service.interfaces.AsientoContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class AsientoContableServiceImp implements AsientoContableService {

	@Autowired
	private AsientoContableDao asientocontableDao;
	
	@Override
	public void postAsientoContable(AsientoContable asientocontable) {
		this.asientocontableDao.insertar(asientocontable);
		
	}

	@Override
	public void putAsientoContable(AsientoContable asientocontable) {
		this.asientocontableDao.editar(asientocontable);
		
	}

	@Override
	public int deleteAsientoContable(AsientoContable asientocontable) {
		return this.asientocontableDao.eliminar(asientocontable);
	}

	@Override
	public AsientoContable getAsientoContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		return this.asientocontableDao.consultar(empresa, ejerciciocontable, asientocontable);
	}

	@Override
	public List<AsientoContable> getListaAsientoContable(Integer empresa, Integer ejerciciocontable) {
		return this.asientocontableDao.consultarLista(empresa, ejerciciocontable);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		return this.asientocontableDao.contadorRegistros(empresa, ejerciciocontable, asientocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable) {
		return this.asientocontableDao.consultarCatalogo(empresa, ejerciciocontable);
	}
}
