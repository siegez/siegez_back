package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.PeriodoDao;
import com.siegez.models.Periodo;
import com.siegez.service.interfaces.PeriodoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class PeriodoServiceImp implements PeriodoService{

	@Autowired
	private PeriodoDao periodoDao;
	
	@Override
	public void postPeriodo(Periodo periodo) {
		periodoDao.insertar(periodo);;
	}

	@Override
	public void putPeriodo(Periodo periodo) {
		periodoDao.editar(periodo);
	}

	@Override
	public int deletePeriodo(Periodo periodo) {
		return periodoDao.eliminar(periodo);
	}
	
	@Override
	public Periodo getPeriodo(Integer empresa, Integer ejerciciocontable, String periodocontable) {
		return this.periodoDao.consultar(empresa, ejerciciocontable, periodocontable);
	}

	@Override
	public List<Periodo> getListaPeriodo(Integer empresa, Integer ejerciciocontable) {
		return this.periodoDao.consultarLista(empresa, ejerciciocontable);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String periodocontable) {
		return this.periodoDao.contadorRegistros(empresa, ejerciciocontable, periodocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable) {
		return this.periodoDao.consultarCatalogo(empresa, ejerciciocontable);
	}

}
