package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.LicenciaDao;
import com.siegez.models.Licencia;
import com.siegez.service.interfaces.LicenciaService;

@Service
public class LicenciaServiceImp implements LicenciaService{

	@Autowired
	private LicenciaDao licenciaDao;

	@Override
	public Licencia getEmpresaPerfilUsuario(String licenciaCodigo) {
		return this.licenciaDao.find(licenciaCodigo);
	}

	@Override
	public List<Licencia> getAllEmpresaPerfilUsuario() {
		return this.licenciaDao.findAll();
	}
	
	
}
