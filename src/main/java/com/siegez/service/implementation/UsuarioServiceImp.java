package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.siegez.daos.interfaces.UsuarioDao;
import com.siegez.models.Usuario;
import com.siegez.service.interfaces.UsuarioService;
import com.siegez.views.ValorContador;

@Service
public class UsuarioServiceImp implements UsuarioService {

	//here we must implement the business logic therefore we may need to use transactions
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Override
	public void postUsuario(Usuario usuario) {
		this.usuarioDao.insertar(usuario);
	}

	@Override
	public void putUsuario(Usuario usuario) {
		this.usuarioDao.editar(usuario);
	}

	@Override
	public Usuario getUsuario(String username) {
		return this.usuarioDao.consultar(username);
	}

	@Override
	public int deleteUsuario(String username) {
		return this.usuarioDao.eliminar(username);
	}

	@Override
	public List<Usuario> getListaUsuario() {
		return this.usuarioDao.consultarLista();
	}
	
	@Override
	public List<Usuario> getListaUsuarioLicencia(String licencia){
		return this.usuarioDao.consultarListaPorLicencia(licencia);
	}
	
	/*
	 * Si un método anotado como @Transaccional lanza una excepción que herede de RuntimeException se producirá 
	 * un rollback. En caso contrario, un commit.
	 */
	
	@Transactional
	public Usuario miPrimerMetodoTransaccional(String username) {
		//como la PROPAGATION_REQUIERED es la opcion por defecto, si vas a usar eso no necesitas marcar el metodo a llamar como @Transacional
		String temp = miSegundoMetodoTx(username);
		return this.usuarioDao.consultar(temp);
	}
	

	/*
	 * Si dentro de un metodo Transaccional se llama a otro metodo transaccional 
	 * 
    PROPAGATION_REQUIRED - Es la que viene por defecto, así que no es necesaria especificarla. Si existe transacción la aprovecha y sino la crea
    REQUIRES_NEW - Abre una transacción nueva y pone en suspenso la anterior. Una vez el método marcado como REQUIRES_NEW termina se vuelve a la transacción anterior.
    PROPAGATION_SUPPORTS - Si existe transacción la aprovecha, sino no crea ninguna.
    PROPAGATION_MANDATORY - Si no existe una transacción abierta se lanza una excepción. Hay gente que anota sus DAO con esta opción.
    PROPAGATION_NEVER - Si existe una transacción abierta se lanza una excepción. No se me ocurre ningún ejemplo donde esto sea necesario pero seguro que alguno hay.
    PROPAGATION_NOT_SUPPORTED - Si existe una transacción la pone en suspenso, la transacción se reactiva al salir del método.
	 */
	
	//@Transactional( propagation = Propagation.REQUIRES_NEW )
	public String miSegundoMetodoTx(String username) {
		return username;
	}
	
	/*Obs: Controlar las excepciones que hagan commit o rollback
	 * 
	 * @Transactional(noRollbackFor={NumberFormatException.class,ArithmeticException.class})
	 * @Transactional(rollbackFor={FileNotFoundException.class})
	
	 */

	//Normalmente comento en Ingles, que es lo más standar, pero estas anotaciones merecían ser en español
	
	@Override
	public ValorContador postContadorRegistros(Usuario usuario) {
		return this.usuarioDao.contadorRegistros(usuario);
	}

}
