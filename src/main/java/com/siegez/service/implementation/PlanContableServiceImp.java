package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.PlanContableDao;
import com.siegez.models.PlanContable;
import com.siegez.service.interfaces.PlanContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;
import com.siegez.views.VistaPlanContable;

@Service
public class PlanContableServiceImp implements PlanContableService {

	@Autowired
	private PlanContableDao plancontableDao;
	
	@Override
	public void postPlanContable(PlanContable plancontable) {
		this.plancontableDao.insertar(plancontable);		
	}

	@Override
	public void putPlanContable(PlanContable plancontable) {
		this.plancontableDao.editar(plancontable);		
	}

	@Override
	public int deletePlanContable(PlanContable plancontable) {
		return this.plancontableDao.eliminar(plancontable);
	}

	@Override
	public PlanContable getCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		return this.plancontableDao.consultar(empresa, ejerciciocontable, cuentacontable);
	}

	@Override
	public List<PlanContable> getListaPlanContable(Integer empresa, Integer ejerciciocontable) {
		return this.plancontableDao.consultarLista(empresa, ejerciciocontable );
	}
	
	@Override
	public List<VistaPlanContable> getVistaPlanContable(Integer empresa, Integer ejerciciocontable) {
		return this.plancontableDao.consultarVista(empresa, ejerciciocontable );
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		return this.plancontableDao.contadorRegistros(empresa, ejerciciocontable, cuentacontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		return this.plancontableDao.consultarCatalogo(empresa, ejerciciocontable, cuentacontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable) {
		return this.plancontableDao.listarCatalogo(empresa, ejerciciocontable);
	}
	
	@Override
	public ConsultaString getconsultarCuentaContable(Integer empresa, Integer ejerciciocontable, String cuentacontable) {
		return this.plancontableDao.consultarCuentaContable(empresa, ejerciciocontable, cuentacontable);		
	}

}
