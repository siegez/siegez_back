package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EjercicioDao;
import com.siegez.models.Ejercicio;
import com.siegez.service.interfaces.EjercicioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class EjercicioServiceImp  implements  EjercicioService{

	@Autowired
	private EjercicioDao ejercicioDao;
	
	@Override
	public void postEjercicio(Ejercicio ejercicio) {
		ejercicioDao.insertar(ejercicio);
	}

	@Override
	public void putEjercicio(Ejercicio ejercicio) {
		ejercicioDao.editar(ejercicio);;
	}

	@Override
	public int deleteEjercicio(Ejercicio ejercicio) {
		return ejercicioDao.eliminar(ejercicio);
	}

	@Override
	public Ejercicio getEjercicio(Integer empresa, Integer ejerciciocontable) {
		return this.ejercicioDao.consultar(empresa, ejerciciocontable);
	}

	@Override
	public List<Ejercicio> getListaEjercicio(Integer empresa) {
		return this.ejercicioDao.consultarLista(empresa);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable) {
		return this.ejercicioDao.contadorRegistros(empresa, ejerciciocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa) {
		return this.ejercicioDao.consultarCatalogo(empresa);
	}
}
