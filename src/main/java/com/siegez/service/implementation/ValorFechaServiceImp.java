package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.ValorFechaDao;
import com.siegez.models.ValorFecha;
import com.siegez.service.interfaces.ValorFechaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.Valor;

@Service
public class ValorFechaServiceImp implements ValorFechaService {
	
	@Autowired
	private ValorFechaDao valorfechaDao;
	
	@Override
	public ResultadoSP postRegistrarValorFecha(ValorFecha valorfecha) {
		return this.valorfechaDao.registrarValorFecha(valorfecha);
	}
	
	@Override
	public void postValorFecha(ValorFecha valorfecha) {
		this.valorfechaDao.insertar(valorfecha);		
	}

	@Override
	public void putValorFecha(ValorFecha valorfecha) {
		this.valorfechaDao.editar(valorfecha);		
	}

	@Override
	public int deleteValorFecha(ValorFecha valorfecha) {
		return this.valorfechaDao.eliminar(valorfecha);
	}

	@Override
	public ValorFecha getValorFecha(Integer empresa, String codigo, Integer vez) {
		return this.valorfechaDao.consultar(empresa, codigo, vez);
	}
	
	@Override
	public Valor getValorPorFecha(Integer empresa, String codigo, String fecha) {
		return this.valorfechaDao.consultarValorPorFecha(empresa, codigo, fecha);
	}
	
	@Override
	public List<ValorFecha> getListaValorFecha(Integer empresa, String codigo) {
		return this.valorfechaDao.consultarLista(empresa, codigo);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, String fecha) {
		return this.valorfechaDao.consultarCatalogo(empresa, fecha);
	}
}
