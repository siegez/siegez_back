package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.CentroCostoDao;
import com.siegez.models.CentroCosto;
import com.siegez.service.interfaces.CentroCostoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class CentroCostoServiceImp implements CentroCostoService{


	@Autowired
	private CentroCostoDao centrocostoDao;
	
	@Override
	public void postCentroCosto(CentroCosto centrocosto) {
		this.centrocostoDao.insertar(centrocosto);		
	}

	@Override
	public void putCentroCosto(CentroCosto centrocosto) {
		this.centrocostoDao.editar(centrocosto);		
	}

	@Override
	public int deleteCentroCosto(CentroCosto centrocosto) {
		return this.centrocostoDao.eliminar(centrocosto);
	}

	@Override
	public CentroCosto getCentroCosto(Integer empresa, Integer ejerciciocontable, String centrocosto) {
		return this.centrocostoDao.consultar(empresa, ejerciciocontable, centrocosto);
	}

	@Override
	public List<CentroCosto> getListaCentroCosto(Integer empresa, Integer ejerciciocontable) {
		return this.centrocostoDao.consultarLista(empresa, ejerciciocontable );
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String centrocosto) {
		return this.centrocostoDao.contadorRegistros(empresa, ejerciciocontable, centrocosto);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable) {
		return this.centrocostoDao.listarCatalogo(empresa, ejerciciocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, String centrocosto) {
		return this.centrocostoDao.listarCatalogo(empresa, ejerciciocontable, centrocosto);
	}
	
	@Override
	public List<Catalogo> getNotInCatalogo(CentroCosto centrocosto){
		return this.centrocostoDao.listarNotInCatalogo(centrocosto);
	}
	
	
}
