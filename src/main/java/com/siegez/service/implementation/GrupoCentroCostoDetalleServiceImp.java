package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.GrupoCentroCostoDetalleDao;
import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.service.interfaces.GrupoCentroCostoDetalleService;
import com.siegez.views.ValorContador;

@Service
public class GrupoCentroCostoDetalleServiceImp implements GrupoCentroCostoDetalleService {


	@Autowired
	private GrupoCentroCostoDetalleDao grupocentrocostodetalleDao;
	
	@Override
	public void postGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		this.grupocentrocostodetalleDao.insertar(grupocentrocostodetalle);		
	}

	@Override
	public void putGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		this.grupocentrocostodetalleDao.editar(grupocentrocostodetalle);		
	}

	@Override
	public int deleteGrupoCentroCostoDetalle(GrupoCentroCostoDetalle grupocentrocostodetalle) {
		return this.grupocentrocostodetalleDao.eliminar(grupocentrocostodetalle);
	}

	@Override
	public GrupoCentroCostoDetalle getGrupoCentroCostoDetalle(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto) {
		return this.grupocentrocostodetalleDao.consultar(empresa, ejerciciocontable, grupocentrocosto, centrocosto);
	}

	@Override
	public List<GrupoCentroCostoDetalle> getListaGrupoCentroCostoDetalle(Integer empresa, Integer ejerciciocontable, String grupocentrocosto) {
		return this.grupocentrocostodetalleDao.consultarLista(empresa, ejerciciocontable, grupocentrocosto);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, String grupocentrocosto, String centrocosto) {
		return this.grupocentrocostodetalleDao.contadorRegistros(empresa, ejerciciocontable, grupocentrocosto, centrocosto);
	}
	
}
