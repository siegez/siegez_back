package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.RegistroContableDao;
import com.siegez.models.RegistroContable;
import com.siegez.service.interfaces.RegistroContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@Service
public class RegistroContableServiceImp implements RegistroContableService {
	
	@Autowired
	private RegistroContableDao registrocontableDao;
	
	@Override
	public void postRegistroContable(RegistroContable registrocontable) {
		this.registrocontableDao.insertar(registrocontable);
		
	}

	@Override
	public void putRegistroContable(RegistroContable registrocontable) {
		this.registrocontableDao.editar(registrocontable);
		
	}

	@Override
	public int deleteRegistroContable(RegistroContable registrocontable) {
		return this.registrocontableDao.eliminar(registrocontable);
	}
	
	@Override
	public int deleteOperacionContable(RegistroContable registrocontable) {
		return this.registrocontableDao.eliminarOperacion(registrocontable);
	}

	@Override
	public RegistroContable getRegistroContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable) {
		return this.registrocontableDao.consultar(empresa, ejerciciocontable, asientocontable, registrocontable);
	}

	@Override
	public List<RegistroContable> getListaRegistroContable(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		return this.registrocontableDao.consultarLista(empresa, ejerciciocontable, asientocontable);
	}
	
	@Override
	public List<RegistroContable> getListaOperacionContable(Integer empresa, Integer ejerciciocontable, String operacioncontable) {
		return this.registrocontableDao.consultarListaOperacion(empresa, ejerciciocontable, operacioncontable);
	}
	
	@Override
	public ValorContador getContadorRegistros(Integer empresa, Integer ejerciciocontable, Integer asientocontable, Integer registrocontable) {
		return this.registrocontableDao.contadorRegistros(empresa, ejerciciocontable, asientocontable, registrocontable);
	}
	
	@Override
	public List<Catalogo> getListaCatalogo(Integer empresa, Integer ejerciciocontable, Integer asientocontable) {
		return this.registrocontableDao.consultarCatalogo(empresa, ejerciciocontable, asientocontable);
	}
	
}
