package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EnteSocioNegocioDao;
import com.siegez.models.EnteSocioNegocio;
import com.siegez.service.interfaces.EnteSocioNegocioService;

@Service
public class EnteSocioNegocioServiceImp implements EnteSocioNegocioService {

	
	@Autowired
	private EnteSocioNegocioDao entesocionegocioDao;
	
	@Override
	public void postEnteSocioNegocio(EnteSocioNegocio entesocionegocio) {
		this.entesocionegocioDao.insertar(entesocionegocio);
		
	}

	@Override
	public void putEnteSocioNegocio(EnteSocioNegocio entesocionegocio) {
		this.entesocionegocioDao.editar(entesocionegocio);
		
	}

	@Override
	public int deleteEnteSocioNegocio(EnteSocioNegocio entesocionegocio) {
		return this.entesocionegocioDao.eliminar(entesocionegocio);
	}

	@Override
	public EnteSocioNegocio getEnteSocioNegocio(Integer empresa, String entidad, Integer vez) {
		return this.entesocionegocioDao.consultar(empresa, entidad, vez);
	}
	
	@Override
	public List<EnteSocioNegocio> getListaEnteSocioNegocio(Integer empresa, String entidad) {
		return this.entesocionegocioDao.consultarLista(empresa, entidad);
	}	
	
}
