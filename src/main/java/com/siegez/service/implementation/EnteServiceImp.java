package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.EnteDao;
import com.siegez.models.Ente;
import com.siegez.models.EnteRegistro;
import com.siegez.service.interfaces.EnteService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.SocioNegocio;
import com.siegez.views.ValorContador;

@Service
public class EnteServiceImp implements EnteService {
	
	@Autowired
	private EnteDao enteDao;
	
	@Override
	public ResultadoSP postRegistrarEnte(EnteRegistro enteregistro) {
		return this.enteDao.registrarEnte(enteregistro);
	}
	

	@Override
	public void postEnte(Ente ente) {
		this.enteDao.insertar(ente);
		
	}

	@Override
	public void putEnte(Ente ente) {
		this.enteDao.editar(ente);
		
	}

	@Override
	public int deleteEnte(Ente ente) {
		return this.enteDao.eliminar(ente);
	}

	@Override
	public Ente getEnte(Integer empresa, String entidad) {
		return this.enteDao.consultar(empresa, entidad);
	}
	
	@Override
	public Ente getRucAnexo(Integer empresa, String ruc, String anexo) {
		return this.enteDao.consultarRucAnexo(empresa, ruc, anexo);
	}

	@Override
	public List<Ente> getListaEnte(Integer empresa) {
		return this.enteDao.consultarLista(empresa);
	}
	
	@Override
	public List<SocioNegocio> getListaProveedor(Integer empresa) {
		return this.enteDao.consultarProveedor(empresa);
	}
	
	@Override
	public List<Catalogo> getListaProveedor(Integer empresa, String criterio) {
		return this.enteDao.consultarProveedor(empresa, criterio);
	}
	
	@Override
	public ValorContador postContadorRegistros(Ente ente) {
		return this.enteDao.contadorRegistros(ente);
	}
	
	@Override
	public ValorContador postContadorRuc(Ente ente) {
		return this.enteDao.contadorRuc(ente);
	}
	
	@Override
	public ValorContador postContadorProveedorRel(Ente ente) {
		return this.enteDao.contadorProveedorRel(ente);
	}
	
	@Override
	public List<Catalogo> postListaCatalogo(Ente ente) {
		return this.enteDao.consultarCatalogo(ente);
	}	
	
	@Override
	public List<SocioNegocio> getListaCliente(Integer empresa) {
		return this.enteDao.consultarCliente(empresa);
	}
	
	@Override
	public List<Catalogo> getListaCliente(Integer empresa, String criterio) {
		return this.enteDao.consultarCliente(empresa, criterio);
	}
	
	@Override
	public ValorContador postContadorClienteRel(Ente ente) {
		return this.enteDao.contadorClienteRel(ente);
	}
	
	@Override
	public List<SocioNegocio> getListaProveedorNoDom(Integer empresa) {
		return this.enteDao.consultarProveedorNoDom(empresa);
	}
	
	@Override
	public List<Catalogo> getListaProveedorNoDom(Integer empresa, String criterio) {
		return this.enteDao.consultarProveedorNoDom(empresa, criterio);
	}
	
	@Override
	public ValorContador postContadorProveedorNoDomRel(Ente ente) {
		return this.enteDao.contadorProveedorNoDomRel(ente);
	}
	
	@Override
	public List<SocioNegocio> getListaBanco(Integer empresa) {
		return this.enteDao.consultarBanco(empresa);
	}
	
	@Override
	public List<SocioNegocio> getListaSocioNegocio(Integer empresa) {
		return this.enteDao.consultarSocioNegocio(empresa);
	}
	
	@Override
	public List<Catalogo> getListaSocioNegocio(Integer empresa, String criterio) {
		return this.enteDao.consultarSocioNegocio(empresa, criterio);
	}
	
	@Override
	public List<Ente> getListaEnteBanco(Integer empresa) {
		return this.enteDao.consultarListaBanco(empresa);
	}
}
