package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.siegez.daos.interfaces.MenuDao;
import com.siegez.models.Menu;
import com.siegez.models.MenuOpcionesConsulta;
import com.siegez.service.interfaces.MenuService;

@Service
public class MenuServiceImp implements MenuService {

	@Autowired
	private MenuDao menuDao;
	
	@Override
	public void postMenu(Menu menu) {
		this.menuDao.save(menu);
	}

	@Override
	public void putMenu(Menu menu) {
		this.menuDao.update(menu);
	}

	@Override
	public Menu getMenu(Integer codigo) {
		return this.menuDao.find(codigo);
	}

	@Override
	public int deleteMenu(Integer codigo) {
		return this.menuDao.delete(codigo);
	}

	@Override
	public List<Menu> getAllMenu() {
		return this.menuDao.findAll();
	}

	@Override
	public List<MenuOpcionesConsulta> getAllMenuOpciones(String username) {
		return this.menuDao.findAllMenuOpciones(username);
	}
	
	@Override
	public List<MenuOpcionesConsulta> getAllMenuEmpresa(String username, Integer idempcod) {
		return this.menuDao.findAllMenuEmpresa(username, idempcod);
	}

	@Override
	public List<MenuOpcionesConsulta> getAllMenuPerfilNuevo(String licencia){
		return this.menuDao.findAllMenuPerfilNuevo(licencia);
	} 
	
	@Override
	public List<MenuOpcionesConsulta> getAllMenuPerfilModifica(String perfil, String licencia){
		return this.menuDao.findAllMenuPerfilModifica(perfil, licencia);
	}
	
	@Transactional
	public Menu miPrimerMetodoTransaccional(Integer codigo) {
		//como la PROPAGATION_REQUIERED es la opcion por defecto, si vas a usar eso no necesitas marcar el metodo a llamar como @Transacional
		Integer temp = miSegundoMetodoTx(codigo);
		return this.menuDao.find(temp);
	}
	

	//@Transactional( propagation = Propagation.REQUIRES_NEW )
	public Integer miSegundoMetodoTx(Integer codigo) {
		return codigo;
	}

}
