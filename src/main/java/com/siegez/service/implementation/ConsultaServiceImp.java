package com.siegez.service.implementation;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.siegez.daos.interfaces.ConsultaDao;
import com.siegez.service.interfaces.ConsultaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;
import com.siegez.views.ValoresIniciales;
import com.siegez.views.VistaCentroCosto;

@Service
public class ConsultaServiceImp implements ConsultaService {

	
	@Autowired
	private ConsultaDao consultaDao;
	
	@Override
	public ValoresIniciales getValoresIniciales(String username) {
		return this.consultaDao.findValoresIniciales(username);

	}
	@Override
	public List<ValoresIniciales> getAllValoresIniciales(String username) {
		return this.consultaDao.findAllValoresIniciales(username);

	}
	
	@Override
	public ValorContador getpermitirDocumentoReferencia(Integer empresa, Integer ejerciciocontable, String tipoodocumento, String moneda) {
		return this.consultaDao.permitirDocumentoReferencia(empresa, ejerciciocontable, tipoodocumento, moneda);
	}
	
	@Override
	public List<VistaCentroCosto> getListaCentroCosto(Integer empresa, Integer ejerciciocontable) {
		return this.consultaDao.listarCentroCosto(empresa, ejerciciocontable);

	}
	
	@Override
	public List<Catalogo> getListaUbicacionGeografica() {
		return this.consultaDao.listarUbicacionGeografica();

	}

}
