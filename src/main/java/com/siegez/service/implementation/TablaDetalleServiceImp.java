package com.siegez.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siegez.daos.interfaces.TablaDetalleDao;
import com.siegez.models.TablaDetalle;
import com.siegez.service.interfaces.TablaDetalleService;

@Service
public class TablaDetalleServiceImp implements TablaDetalleService{
	
	@Autowired
	private TablaDetalleDao tabladetalleDao;
	
	@Override
	public void postTablaDetalle(TablaDetalle tabladetalle) {
		this.tabladetalleDao.insertar(tabladetalle);
	}
	
	@Override
	public void putTablaDetalle(TablaDetalle tabladetalle) {
		this.tabladetalleDao.editar(tabladetalle);
	}
	
	@Override
	public int deleteTablaDetalle(TablaDetalle tabladetalle) {
		return this.tabladetalleDao.eliminar(tabladetalle);
	}
	
	@Override
	public TablaDetalle getTablaDetalle(TablaDetalle tabladetalle) {
		return this.tabladetalleDao.consultar(tabladetalle);
	}
	
	@Override
	public List<TablaDetalle> getListaTablaDetalleDescripcion(Integer empresa, Integer tabla) {
		return this.tabladetalleDao.consultarListaDescripcion(empresa, tabla);
	}
	
	@Override
	public List<TablaDetalle> getListaTablaDetalleCodigo(Integer empresa, Integer tabla) {
		return this.tabladetalleDao.consultarListaCodigo(empresa, tabla);
	}
}
