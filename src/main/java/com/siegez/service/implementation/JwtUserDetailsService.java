package com.siegez.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.siegez.daos.interfaces.JwtUsuarioDao;
import com.siegez.models.JwtUsuario;
import com.siegez.service.interfaces.JwtUsuarioService;

@Component
public class JwtUserDetailsService implements JwtUsuarioService, UserDetailsService {
  
    private String [] authorities= {"ADMIN","USER"};
    private JwtUsuario jwtUsuario;

    @Autowired
	private JwtUsuarioDao jwtUsuarioDao;
    
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		this.jwtUsuario = this.jwtUsuarioDao.find(username);
		return new User(jwtUsuario.getUsername(),jwtUsuario.getPassword() , AuthorityUtils.createAuthorityList(authorities));	
	}

}
