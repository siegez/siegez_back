package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.TipoDocumento;
import com.siegez.service.interfaces.TipoDocumentoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class TipoDocumentoController {

	
	@Autowired
	TipoDocumentoService tipodocumentoService;
	
	// Nuevo tipo documento
	@RequestMapping(value = "/tipodocumentonuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody TipoDocumento tipodocumento) {
		tipodocumentoService.postTipoDocumento(tipodocumento);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar tipo documento
	@RequestMapping(value = "/tipodocumento", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody TipoDocumento tipodocumento) {
		TipoDocumento _tipodocumento = tipodocumentoService.getTipoDocumento(tipodocumento.getEmpresa(), tipodocumento.getTipodocumento());
		if (_tipodocumento != null) {
			tipodocumentoService.putTipoDocumento(tipodocumento);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar tipo documento
	@DeleteMapping(value = "/tipodocumento")
	public ResponseEntity<String> elimina(@RequestBody TipoDocumento tipodocumento) {
		tipodocumentoService.deleteTipoDocumento(tipodocumento);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta tipo documento
	@GetMapping(value = "/tipodocumento/{empresa}/{tipodocumento}")
	public TipoDocumento consulta(@PathVariable("empresa") Integer empresa, @PathVariable("tipodocumento") String tipodocumento) {
		TipoDocumento _tipodocumento = tipodocumentoService.getTipoDocumento(empresa, tipodocumento);
		return _tipodocumento;
	}
	
	// Listado de tipo documento
	@GetMapping(value = "/tipodocumentolista/{empresa}")
	public List<TipoDocumento> lista(@PathVariable("empresa") Integer empresa){
		List<TipoDocumento> tipodocumento = new ArrayList<>();
		tipodocumentoService.getListaTipoDocumento(empresa).forEach(tipodocumento::add);
		return tipodocumento;
	}	
	
	// Contador de tipo documento
	@GetMapping(value = "/tipodocumentocontador/{empresa}/{tipodocumento}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("tipodocumento") String tipodocumento) {
		ValorContador _valorcontador = tipodocumentoService.postContadorRegistros(empresa, tipodocumento);
		return _valorcontador;	
	}
	
	// Catálogo tipo documento
	@GetMapping(value = "/tipodocumentocatalogo/{empresa}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogo(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo tipo documento - Compras
	@GetMapping(value = "/tipodocumentocompras/{empresa}")
	public List<Catalogo> catalogocompras(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoCompras(empresa).forEach(catalogo::add);
		return catalogo;
	}		
	
	// Catálogo tipo documento - Ventas
	@GetMapping(value = "/tipodocumentoventas/{empresa}")
	public List<Catalogo> catalogoventas(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoVentas(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - honorarios
	@GetMapping(value = "/tipodocumentohonorarios/{empresa}")
	public List<Catalogo> catalogohonorarios(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoHonorarios(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - No Domiciliado
	@GetMapping(value = "/tipodocumentonodomiciliados/{empresa}")
	public List<Catalogo> catalogonodomiciliados(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoNoDomiciliados(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - No Domiciliado - Sustentos
	@GetMapping(value = "/tipodocumentonodomsustentos/{empresa}")
	public List<Catalogo> catalogonodomsustentos(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoNoDomSustentos(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - Importación
	@GetMapping(value = "/tipodocumentoimportaciones/{empresa}")
	public List<Catalogo> catalogoimportacion(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoImportaciones(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - Egresos
	@GetMapping(value = "/tipodocumentoegresos/{empresa}")
	public List<Catalogo> catalogoegresos(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoEgresos(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - Ingresos
	@GetMapping(value = "/tipodocumentoingresos/{empresa}")
	public List<Catalogo> catalogoingresos(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoIngresos(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo tipo documento - Diarios
	@GetMapping(value = "/tipodocumentodiarios/{empresa}")
	public List<Catalogo> catalogodiarios(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		tipodocumentoService.postListaCatalogoDiarios(empresa).forEach(catalogo::add);
		return catalogo;
	}
	
}


