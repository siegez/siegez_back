package com.siegez.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.jwt.JwtTokenUtil;
import com.siegez.models.JwtResponse;
import com.siegez.models.JwtUsuario;
import com.siegez.service.interfaces.JwtUsuarioService;



@RestController
@CrossOrigin(origins = "*")
public class JwtAuthenticationController {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUsuarioService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtUsuario authenticationRequest) throws Exception {

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		
		if(!userDetails.getPassword().equals(authenticationRequest.getPassword()) || !userDetails.getUsername().equals(authenticationRequest.getUsername())) {
			return (ResponseEntity<?>) ResponseEntity.notFound();
		}
		
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new JwtResponse(token));
	}
}