package com.siegez.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.Periodo;
import com.siegez.service.interfaces.PeriodoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class PeriodoController {

	@Autowired
	PeriodoService periodoService;
	
	@RequestMapping(value = "/periodo", method = RequestMethod.POST)
	public ResponseEntity<String> nuevo(@RequestBody Periodo periodo) {
		periodoService.postPeriodo(periodo);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/periodo", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Periodo periodo) {	
		// Periodo _periodo = periodoService.getPeriodo(periodo.getEmpresa(), periodo.getEjerciciocontable(), periodo.getPeriodocontable());
		// if (_periodo != null) {
			periodoService.putPeriodo(periodo);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		// } else {
			// return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		// }
		
	}	
	
	@RequestMapping(value = "/periodo",method = RequestMethod.DELETE)
	public ResponseEntity<String> elimina(@RequestBody Periodo periodo) {
		periodoService.deletePeriodo(periodo);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@GetMapping(value = "periodo/{empresa}/{ejerciciocontable}/{periodocontable}")
	public Periodo consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable){
		Periodo _periodo = periodoService.getPeriodo(empresa, ejerciciocontable, periodocontable);
		return _periodo;
	}
	
	@GetMapping(value = "/periodolista/{empresa}/{ejerciciocontable}")
	public List<Periodo> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Periodo> periodo = new ArrayList<>();
		periodoService.getListaPeriodo(empresa, ejerciciocontable).forEach(periodo::add);
		return periodo;
	}
	
	@GetMapping(value = "/periodocontador/{empresa}/{ejerciciocontable}/{periodocontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable) {
		ValorContador _valorcontador = periodoService.getContadorRegistros(empresa, ejerciciocontable, periodocontable);
		return _valorcontador;	
	}
	
	// Catálogo
	@GetMapping(value = "/periodocatalogo/{empresa}/{ejerciciocontable}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		periodoService.getListaCatalogo(empresa, ejerciciocontable).forEach(catalogo::add);
		return catalogo;
	}	
	

}
