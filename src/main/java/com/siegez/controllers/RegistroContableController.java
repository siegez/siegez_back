package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.RegistroContable;
import com.siegez.service.interfaces.RegistroContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class RegistroContableController {

	@Autowired
	RegistroContableService registrocontableService;
	
	// Nuevo registro contable
	@RequestMapping(value = "/registrocontablenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody RegistroContable registrocontable) {
		registrocontableService.postRegistroContable(registrocontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar registro contable
	@RequestMapping(value = "/registrocontable", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody RegistroContable registrocontable) {
		RegistroContable _registrocontable = registrocontableService.getRegistroContable(registrocontable.getEmpresa(), registrocontable.getEjerciciocontable(), registrocontable.getAsientocontable(), registrocontable.getRegistrocontable());
		if (_registrocontable != null) {
			registrocontableService.putRegistroContable(registrocontable);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar registro contable
	@DeleteMapping(value = "/registrocontable")
	public ResponseEntity<String> elimina(@RequestBody RegistroContable registrocontable) {
		registrocontableService.deleteRegistroContable(registrocontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Eliminar registros contables de una operación 
	@DeleteMapping(value = "/registrocontableoperacion")
	public ResponseEntity<String> eliminaoperacion(@RequestBody RegistroContable registrocontable) {
		registrocontableService.deleteOperacionContable(registrocontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta registro contable
	@GetMapping(value = "/registrocontable/{empresa}/{ejerciciocontable}/{asientocontable}/{registrocontable}")
	public RegistroContable consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable, @PathVariable("registrocontable") Integer registrocontable) {
		RegistroContable _registrocontable = registrocontableService.getRegistroContable(empresa, ejerciciocontable, asientocontable, registrocontable);
		return _registrocontable;
	}
	
	// Listado de registro contables
	@GetMapping(value = "/registrocontablelista/{empresa}/{ejerciciocontable}/{asientocontable}")
	public List<RegistroContable> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable){
		List<RegistroContable> registrocontable = new ArrayList<>();
		registrocontableService.getListaRegistroContable(empresa, ejerciciocontable, asientocontable).forEach(registrocontable::add);
		return registrocontable;
	}
	
	// Listado de registro contables
	@GetMapping(value = "/registrocontablelistaoperacion/{empresa}/{ejerciciocontable}/{operacioncontable}")
	public List<RegistroContable> listaoperacion(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("operacioncontable") String operacioncontable){
		List<RegistroContable> registrocontable = new ArrayList<>();
		registrocontableService.getListaOperacionContable(empresa, ejerciciocontable, operacioncontable).forEach(registrocontable::add);
		return registrocontable;
	}
	
	// Contador registro contables
	@GetMapping(value = "/registrocontablecontador/{empresa}/{ejerciciocontable}/{asientocontable}/{registrocontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable, @PathVariable("registrocontable") Integer registrocontable) {
		ValorContador _valorcontador = registrocontableService.getContadorRegistros(empresa, ejerciciocontable, asientocontable, registrocontable);
		return _valorcontador;	
	}
	
	// Catálogo registro contable
	@GetMapping(value = "/registrocontablecatalogo/{empresa}/{ejerciciocontable}/{asientocontable}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		registrocontableService.getListaCatalogo(empresa, ejerciciocontable, asientocontable).forEach(catalogo::add);
		return catalogo;
	}	
	
}
