package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Parametro;
import com.siegez.service.interfaces.ParametroService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class ParametroController {
	
	@Autowired
	ParametroService parametroService;	
	
	// Nuevo parámetro
	@RequestMapping(value = "/parametronuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody Parametro parametro) {
		parametroService.postParametro(parametro);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar parámetro
	@RequestMapping(value = "/parametro", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Parametro parametro) {
		Parametro _parametro = parametroService.getParametro(parametro.getEmpresa(), parametro.getModulo(), parametro.getParametro());
		if (_parametro.getEmpresa() != null) {
			parametroService.putParametro(parametro);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar parámetro
	@DeleteMapping(value = "/parametro")
	public ResponseEntity<String> elimina(@RequestBody Parametro parametro) {
		parametroService.deleteParametro(parametro);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta parámetro
	@GetMapping(value = "/parametro/{empresa}/{modulo}/{parametro}")
	public Parametro consulta(@PathVariable("empresa") Integer empresa, @PathVariable("modulo") String modulo, @PathVariable("parametro") String parametro) {
		Parametro _parametro = parametroService.getParametro(empresa, modulo, parametro);
		return _parametro;
	}
	
	// Listado parámetro
	@GetMapping(value = "/parametrolista/{empresa}")
	public List<Parametro> lista(@PathVariable("empresa") Integer empresa){
		List<Parametro> parametro = new ArrayList<>();
		parametroService.getListaParametro(empresa).forEach(parametro::add);
		return parametro;
	}	
	
	// Listado parámetro
	@GetMapping(value = "/parametrolistasubmodulo/{empresa}/{ejerciciocontable}/{modulo}/{submodulo}")
	public List<Parametro> listasubmodulo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("modulo") String modulo, @PathVariable("submodulo") String submodulo){
		List<Parametro> parametro = new ArrayList<>();
		parametroService.getListaSubModulo(empresa, ejerciciocontable, modulo, submodulo).forEach(parametro::add);
		return parametro;
	}
	
	// Contador parámetro
	@GetMapping(value = "/parametrocontador/{empresa}/{modulo}/{parametro}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("modulo") String modulo, @PathVariable("parametro") String parametro) {
		ValorContador _valorcontador = parametroService.postContadorRegistros(empresa, modulo, parametro);
		return _valorcontador;	
	}
	
	// Catálogo parámetro
	@GetMapping(value = "/parametrocatalogo/{empresa}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		parametroService.postListaCatalogo(empresa).forEach(catalogo::add);
		return catalogo;
	}	
}
