package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.TipoCambio;
import com.siegez.service.interfaces.TipoCambioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class TipoCambioController {

	@Autowired
	TipoCambioService tipocambioService;
	
	// Registar tipo cambio
	@RequestMapping(value = "/tipocambioregistro", method = RequestMethod.POST)
	public ResultadoSP registrar(@RequestBody TipoCambio tipocambio) {
		ResultadoSP _resultadoSP = tipocambioService.postRegistrarTipoCambio(tipocambio);
		return _resultadoSP;
	}	
	
	// Nuevo tipo de cambio
	@RequestMapping(value = "/tipocambionuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody TipoCambio tipocambio) {
		tipocambioService.postTipoCambio(tipocambio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar tipo de cambio
	@RequestMapping(value = "/tipocambio", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody TipoCambio tipocambio) {
		TipoCambio _tipocambio = tipocambioService.getTipoCambio(tipocambio.getEmpresa(), tipocambio.getVez());
		if (_tipocambio != null) {
			tipocambioService.putTipoCambio(tipocambio);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar tipo de cambio
	@DeleteMapping(value = "/tipocambio")
	public ResponseEntity<String> elimina(@RequestBody TipoCambio tipocambio) {
		tipocambioService.deleteTipoCambio(tipocambio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta tipo de cambio 
	@GetMapping(value = "/tipocambio/{empresa}/{vez}")
	public TipoCambio consulta(@PathVariable("empresa") Integer empresa, @PathVariable("vez") Integer vez) {
		TipoCambio _tipocambio = tipocambioService.getTipoCambio(empresa, vez);
		return _tipocambio;
	}
	
	// Consulta tipo de cambio día
	@GetMapping(value = "/tipocambiodia/{empresa}/{fecha}/{moneda}/{indicadorcierre}")
	public TipoCambio consultaDia(@PathVariable("empresa") Integer empresa, @PathVariable("fecha") String fecha, @PathVariable("moneda") String moneda, @PathVariable("indicadorcierre") String indicadorcierre) {
		TipoCambio _tipocambio = tipocambioService.getTipoCambioDia(empresa, fecha, moneda, indicadorcierre);
		return _tipocambio;
	}
	
	// Listado tipo de cambio
	@GetMapping(value = "/tipocambiolista/{empresa}/{ejerciciocontable}")
	public List<TipoCambio> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<TipoCambio> tipocambio = new ArrayList<>();
		tipocambioService.getListaTipoCambio(empresa, ejerciciocontable).forEach(tipocambio::add);
		return tipocambio;
	}

	// Contador tipo de cambio
	@GetMapping(value = "/tipocambiocontador/{empresa}/{fecha}/{moneda}/{indicadorcierre}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("fecha") String fecha, @PathVariable("moneda") String moneda, @PathVariable("indicadorcierre") String indicadorcierre) {
		ValorContador _valorcontador = tipocambioService.getContadorRegistros(empresa, fecha, moneda, indicadorcierre);
		return _valorcontador;	
	}
	
	// Catálogo
	@GetMapping(value = "/tipocambiocatalogo/{empresa}/{fecha}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("fecha") String fecha){
		List<Catalogo> catalogo = new ArrayList<>();
		tipocambioService.getListaCatalogo(empresa, fecha).forEach(catalogo::add);
		return catalogo;
	}	
}
