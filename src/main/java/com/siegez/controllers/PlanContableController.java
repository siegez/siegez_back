package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.PlanContable;
import com.siegez.service.interfaces.PlanContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;
import com.siegez.views.VistaPlanContable;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class PlanContableController {

	@Autowired
	PlanContableService plancontableService;
	
	// Nueva cuenta contable
	@RequestMapping(value = "/plancontablenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody PlanContable plancontable) {
		plancontableService.postPlanContable(plancontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	// Actualizar cuenta contable
	@RequestMapping(value = "/plancontable", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody PlanContable plancontable) {
		PlanContable _plancontable = plancontableService.getCuentaContable(plancontable.getEmpresa(), plancontable.getEjerciciocontable(), plancontable.getCuenta());
		if (_plancontable != null) {
			plancontableService.putPlanContable(plancontable);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar cuenta contable
	@DeleteMapping(value = "/plancontable")
	public ResponseEntity<String> elimina(@RequestBody PlanContable plancontable) {
		plancontableService.deletePlanContable(plancontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta cuenta contable
	@GetMapping(value = "/plancontable/{empresa}/{ejerciciocontable}/{cuentacontable}")
	public PlanContable consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("cuentacontable") String cuentacontable) {
		PlanContable _plancontable = plancontableService.getCuentaContable(empresa, ejerciciocontable, cuentacontable);
		return _plancontable;	
	}
	
	// Listado por empresa y año
	@GetMapping(value = "/plancontablelista/{empresa}/{ejerciciocontable}")
	public List<PlanContable> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<PlanContable> plancontable = new ArrayList<>();
		plancontableService.getListaPlanContable(empresa, ejerciciocontable).forEach(plancontable::add);
		return plancontable;
	}

	// Listado por empresa y año
	@GetMapping(value = "/plancontablevista/{empresa}/{ejerciciocontable}")
	public List<VistaPlanContable> vista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<VistaPlanContable> plancontable = new ArrayList<>();
		plancontableService.getVistaPlanContable(empresa, ejerciciocontable).forEach(plancontable::add);
		return plancontable;
	}
	
	// Contador de cuenta contable
	@GetMapping(value = "/plancontablecontador/{empresa}/{ejerciciocontable}/{cuentacontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("cuentacontable") String cuentacontable) {
		ValorContador _valorcontador = plancontableService.getContadorRegistros(empresa, ejerciciocontable, cuentacontable);
		return _valorcontador;	
	}
	
	// Catálogo
	@GetMapping(value = "/plancontablecatalogo/{empresa}/{ejerciciocontable}/{cuentacontable}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("cuentacontable") String cuentacontable){
		List<Catalogo> catalogo = new ArrayList<>();
		plancontableService.getListaCatalogo(empresa, ejerciciocontable, cuentacontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo lista
	@GetMapping(value = "/plancontablecatalogolista/{empresa}/{ejerciciocontable}")
	public List<Catalogo> catalogolista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		plancontableService.getListaCatalogo(empresa, ejerciciocontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Consulta cuenta contable, por búsqueda de lista
	@GetMapping(value = "/plancontablecuentacontable/{empresa}/{ejerciciocontable}/{cuentacontable}")
	public ConsultaString consultastring(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("cuentacontable") String cuentacontable) {
		ConsultaString _consultastring = plancontableService.getconsultarCuentaContable(empresa, ejerciciocontable, cuentacontable);
		return _consultastring;		
	}
}
