package com.siegez.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Licencia;
import com.siegez.service.interfaces.LicenciaService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class LicenciaController {

	@Autowired
	LicenciaService licenciaService;
	
	@GetMapping(value = "/licencia/{licenciaCodigo}")
	public Licencia getLicencia(@PathVariable("licenciaCodigo") String licenciaCodigo){
		return licenciaService.getEmpresaPerfilUsuario(licenciaCodigo);
	}
	
	@GetMapping(value = "/licencia")
	public List<Licencia> getAllLicencia(){
		return licenciaService.getAllEmpresaPerfilUsuario();
	}
}
