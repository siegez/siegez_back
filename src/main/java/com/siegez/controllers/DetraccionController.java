package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Detraccion;
import com.siegez.service.interfaces.DetraccionService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class DetraccionController {

	@Autowired
	DetraccionService detraccionService;	
	
	// Nuevo detracción
	@RequestMapping(value = "/detraccionnuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody Detraccion detraccion) {
		detraccionService.postDetraccion(detraccion);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar detracción
	@RequestMapping(value = "/detraccion", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Detraccion detraccion) {
		Detraccion _detraccion = detraccionService.getDetraccion(detraccion.getEmpresa(), detraccion.getDetraccion());
		if (_detraccion != null) {
			detraccionService.putDetraccion(detraccion);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar detracción
	@DeleteMapping(value = "/detraccion")
	public ResponseEntity<String> elimina(@RequestBody Detraccion detraccion) {
		detraccionService.deleteDetraccion(detraccion);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta detracción
	@GetMapping(value = "/detraccion/{empresa}/{detraccion}")
	public Detraccion consulta(@PathVariable("empresa") Integer empresa, @PathVariable("detraccion") String detraccion) {
		Detraccion _detraccion = detraccionService.getDetraccion(empresa, detraccion);
		return _detraccion;
	}
	
	// Listado detracción
	@GetMapping(value = "/detraccionlista/{empresa}")
	public List<Detraccion> lista(@PathVariable("empresa") Integer empresa){
		List<Detraccion> detraccion = new ArrayList<>();
		detraccionService.getListaDetraccion(empresa).forEach(detraccion::add);
		return detraccion;
	}	
	
	// Contador detracción
	@GetMapping(value = "/detraccioncontador/{empresa}/{detraccion}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("detraccion") String detraccion) {
		ValorContador _valorcontador = detraccionService.postContadorRegistros(empresa, detraccion);
		return _valorcontador;	
	}
	
	// Catálogo detracción
	@GetMapping(value = "/detraccioncatalogo/{empresa}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		detraccionService.postListaCatalogo(empresa).forEach(catalogo::add);
		return catalogo;
	}	
}
