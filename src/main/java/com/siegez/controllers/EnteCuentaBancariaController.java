package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.EnteCuentaBancaria;
import com.siegez.service.interfaces.EnteCuentaBancariaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EnteCuentaBancariaController {
	@Autowired
	EnteCuentaBancariaService entecuentabancariaService;	
	
	// Nuevo Cuenta Bancaria
	@RequestMapping(value = "/entecuentabancarianuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody EnteCuentaBancaria entecuentabancaria) {
		entecuentabancariaService.postEnteCuentaBancaria(entecuentabancaria);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Actualizar Cuenta Bancaria
	@RequestMapping(value = "/entecuentabancaria", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody EnteCuentaBancaria entecuentabancaria) { 
		EnteCuentaBancaria _entecuentabancaria = entecuentabancariaService.getEnteCuentaBancaria(entecuentabancaria.getEmpresa(),
																								 entecuentabancaria.getEjerciciocontable(),				
																								 entecuentabancaria.getEntidad(),
																								 entecuentabancaria.getCuentabancaria());
		if (_entecuentabancaria != null) {
			entecuentabancariaService.putEnteCuentaBancaria(entecuentabancaria);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar Cuenta Bancaria
	@DeleteMapping(value = "/entecuentabancaria")
	public ResponseEntity<String> elimina(@RequestBody EnteCuentaBancaria entecuentabancaria) {
		entecuentabancariaService.deleteEnteCuentaBancaria(entecuentabancaria);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta Cuenta Bancaria
	@GetMapping(value = "/entecuentabancaria/{empresa}/{ejerciciocontable}/{entidad}/{cuentabancaria}")
	public EnteCuentaBancaria consulta( @PathVariable("empresa") Integer empresa, 
										@PathVariable("ejerciciocontable") Integer ejerciciocontable,
										@PathVariable("entidad") String entidad,
										@PathVariable("cuentabancaria") String cuentabancaria) {
		EnteCuentaBancaria _entecuentabancaria = entecuentabancariaService.getEnteCuentaBancaria(empresa, ejerciciocontable, entidad, cuentabancaria);
		return _entecuentabancaria;
	}
	
	
	// Listado Cuenta Bancaria
	@GetMapping(value = "/entecuentabancarialista/{empresa}/{ejerciciocontable}/{entidad}")
	public List<EnteCuentaBancaria> lista(  @PathVariable("empresa") Integer empresa, 
											@PathVariable("ejerciciocontable") Integer ejerciciocontable,
											@PathVariable("entidad") String entidad){
		List<EnteCuentaBancaria> entecuentabancaria = new ArrayList<>();
		entecuentabancariaService.getListaEnteCuentaBancaria(empresa, ejerciciocontable, entidad).forEach(entecuentabancaria::add);
		return entecuentabancaria;
	}	
	
	// Contador Cuenta Bancaria
	@GetMapping(value = "/entecuentabancariacontador/{empresa}/{ejerciciocontable}/{entidad}/{cuentabancaria}")
	public ValorContador contador(  @PathVariable("empresa") Integer empresa, 
									@PathVariable("ejerciciocontable") Integer ejerciciocontable,
									@PathVariable("entidad") String entidad,
									@PathVariable("cuentabancaria") String cuentabancaria) {
		ValorContador _valorcontador = entecuentabancariaService.postContadorRegistros(empresa, ejerciciocontable, entidad, cuentabancaria);
		return _valorcontador;	
	}
	
	// Catálogo Cuenta Bancaria
	@GetMapping(value = "/entecuentabancariacatalogo/{empresa}/{ejerciciocontable}/{entidad}")
	public List<Catalogo> catalogo( @PathVariable("empresa") Integer empresa, 
									@PathVariable("ejerciciocontable") Integer ejerciciocontable,
									@PathVariable("entidad") String entidad){
		List<Catalogo> catalogo = new ArrayList<>();
		entecuentabancariaService.postListaCatalogo(empresa, ejerciciocontable, entidad).forEach(catalogo::add);
		return catalogo;
	}	
}
