package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.TipoDocMonCta;
import com.siegez.service.interfaces.TipoDocMonCtaService;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class TipoDocMonCtaController {

	@Autowired
	TipoDocMonCtaService tipodocmonctaService;	
	
	// Nuevo Cuentas x TipDoc y Moneda
	@RequestMapping(value = "/tipodocmonctanuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody TipoDocMonCta tipodocmoncta) {
		tipodocmonctaService.postTipoDocMonCta(tipodocmoncta);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar Cuentas x TipDoc y Moneda
	@RequestMapping(value = "/tipodocmoncta", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody TipoDocMonCta tipodocmoncta) {
		TipoDocMonCta _tipodocmoncta = tipodocmonctaService.getTipoDocMonCta(tipodocmoncta.getEmpresa(), tipodocmoncta.getEjerciciocontable(), tipodocmoncta.getTipodocumento(), tipodocmoncta.getMoneda());
		// System.out.println(_tipodocmoncta.getEmpresa());
		if (_tipodocmoncta.getEmpresa() != null ) {
			tipodocmonctaService.putTipoDocMonCta(tipodocmoncta);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar Cuentas x TipDoc y Moneda
	@DeleteMapping(value = "/tipodocmoncta")
	public ResponseEntity<String> elimina(@RequestBody TipoDocMonCta tipodocmoncta) {
		tipodocmonctaService.deleteTipoDocMonCta(tipodocmoncta);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta Cuentas x TipDoc y Moneda
	@GetMapping(value = "/tipodocmoncta/{empresa}/{ejerciciocontable}/{tipodocumento}/{moneda}")
	public TipoDocMonCta consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, 
			@PathVariable("tipodocumento") String tipodocumento, @PathVariable("moneda") String moneda) {
		
		TipoDocMonCta _tipodocmoncta = tipodocmonctaService.getTipoDocMonCta(empresa, ejerciciocontable, tipodocumento, moneda);		
		// System.out.println(_tipodocmoncta.hashCode());ResponseEntity<String>
		// System.out.println(_tipodocmoncta.toString());
		// System.out.println(_tipodocmoncta.getClass());
		return _tipodocmoncta;
	}
	
	// Listado Cuentas x TipDoc y Moneda
	@GetMapping(value = "/tipodocmonctalista/{empresa}/{ejerciciocontable}")
	public List<TipoDocMonCta> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<TipoDocMonCta> tipodocmoncta = new ArrayList<>();
		tipodocmonctaService.getListaTipoDocMonCta(empresa, ejerciciocontable).forEach(tipodocmoncta::add);
		return tipodocmoncta;
	}	
	
	// Contador Cuentas x TipDoc y Moneda
	@GetMapping(value = "/tipodocmonctacontador/{empresa}/{ejerciciocontable}/{tipodocumento}/{moneda}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, 
			@PathVariable("tipodocumento") String tipodocumento, @PathVariable("moneda") String moneda) {
		ValorContador _valorcontador = tipodocmonctaService.postContadorRegistros(empresa, ejerciciocontable, tipodocumento, moneda);
		return _valorcontador;	
	}
}
