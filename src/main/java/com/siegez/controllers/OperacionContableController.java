package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.OperacionContable;
import com.siegez.models.OperacionRegistroContable;
import com.siegez.models.RegistroContable;
import com.siegez.service.interfaces.OperacionContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ConsultaString;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class OperacionContableController {

	@Autowired
	OperacionContableService operacioncontableService;
	
	// Registar operación contable
	@RequestMapping(value = "/operacioncontableregistro", method = RequestMethod.POST)
	public OperacionContable registrar(@RequestBody OperacionRegistroContable operacionregistrocontable) {
		OperacionContable _operacioncontable = operacioncontableService.postRegistrarOperacion(operacionregistrocontable);
		return _operacioncontable;
	}	
	
	// Nueva operación contable
	@RequestMapping(value = "/operacioncontablenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody OperacionContable operacioncontable) {
		operacioncontableService.postOperacionContable(operacioncontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar operación contable
	@RequestMapping(value = "/operacioncontable", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody OperacionContable operacioncontable) {
		OperacionContable _operacioncontable = operacioncontableService.getOperacionContable(operacioncontable.getEmpresa(), operacioncontable.getEjerciciocontable(), operacioncontable.getOperacioncontable());
		if (_operacioncontable != null) {
			operacioncontableService.putOperacionContable(operacioncontable);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar operación contable
	@DeleteMapping(value = "/operacioncontable")
	public ResponseEntity<String> elimina(@RequestBody OperacionContable operacioncontable) {
		operacioncontableService.deleteOperacionContable(operacioncontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}

	// Consulta operación contable
	@GetMapping(value = "/operacioncontable/{empresa}/{ejerciciocontable}/{operacioncontable}")
	public OperacionContable consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("operacioncontable") String operacioncontable) {
		OperacionContable _operacioncontable = operacioncontableService.getOperacionContable(empresa, ejerciciocontable, operacioncontable);
		return _operacioncontable;
	}
	
	// Listado de operaciones contables por tipo de operación
	@GetMapping(value = "/operacioncontablelista/{empresa}/{ejerciciocontable}/{tipooperacion}")
	public List<OperacionContable> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("tipooperacion") String tipooperacion){
		List<OperacionContable> operacioncontable = new ArrayList<>();
		operacioncontableService.getListaOperacionContable(empresa, ejerciciocontable, tipooperacion).forEach(operacioncontable::add);
		return operacioncontable;
	}
	
	// Vista de operaciones contables por tipo de operación
	@GetMapping(value = "/operacioncontablevista/{empresa}/{ejerciciocontable}/{tipooperacion}")
	public List<OperacionContable> vista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("tipooperacion") String tipooperacion){
		List<OperacionContable> operacioncontable = new ArrayList<>();
		operacioncontableService.getVistaOperacionContable(empresa, ejerciciocontable, tipooperacion).forEach(operacioncontable::add);
		return operacioncontable;
	}

	// Contador de operación contable
	@GetMapping(value = "/operacioncontablecontador/{empresa}/{ejerciciocontable}/{operacioncontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("operacioncontable") String operacioncontable) {
		ValorContador _valorcontador = operacioncontableService.getContadorRegistros(empresa, ejerciciocontable, operacioncontable);
		return _valorcontador;
	}
	
	// Contador de operación contable por comprobante
	@GetMapping(value = "/operacioncontablecontadorcom/{empresa}/{entidad}/{numeroserie}/{numerocomprobanteini}")
	public ValorContador contadorComprobante(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad, @PathVariable("numeroserie") String numeroserie, @PathVariable("numerocomprobanteini") String numerocomprobanteini) {
		ValorContador _valorcontador = operacioncontableService.getContadorRegistrosComprobante(empresa, entidad, numeroserie, numerocomprobanteini);
		return _valorcontador;
	}
	
	
	// Catálogo
	@RequestMapping(value = "/operacioncontablecatalogo", method = RequestMethod.POST)
	public List<Catalogo> catalogo(@RequestBody OperacionContable operacioncontable){
		List<Catalogo> catalogo = new ArrayList<>();
		operacioncontableService.postListaCatalogo(operacioncontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo como documento referencia
	@RequestMapping(value = "/operacioncontablecatalogoref", method = RequestMethod.POST)
	public List<Catalogo> catalogoref(@RequestBody OperacionContable operacioncontable){
		List<Catalogo> catalogo = new ArrayList<>();
		operacioncontableService.postListaCatalogoRef(operacioncontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Consulta de saldos
	@RequestMapping(value = "/operacioncontablesaldo", method = RequestMethod.POST)
	public List<ConsultaString> operacionsaldo(@RequestBody RegistroContable registrocontable){
		List<ConsultaString> saldo = new ArrayList<>();
		operacioncontableService.postListaSaldo(registrocontable).forEach(saldo::add);
		return saldo;
	}
		
}
