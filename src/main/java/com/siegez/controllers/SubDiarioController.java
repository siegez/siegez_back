package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.SubDiario;
import com.siegez.service.interfaces.SubDiarioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class SubDiarioController {

	
	@Autowired
	SubDiarioService subdiarioService;	
	
	// Nuevo subdiario
	@RequestMapping(value = "/subdiarionuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody SubDiario subdiario) {
		subdiarioService.postSubDiario(subdiario);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar subdiario
	@RequestMapping(value = "/subdiario", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody SubDiario subdiario) {
		SubDiario _subdiario = subdiarioService.getSubDiario(subdiario.getEmpresa(), subdiario.getSubdiario());
		if (_subdiario != null) {
			subdiarioService.putSubDiario(subdiario);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar subdiario
	@DeleteMapping(value = "/subdiario")
	public ResponseEntity<String> elimina(@RequestBody SubDiario subdiario) {
		subdiarioService.deleteSubDiario(subdiario);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta subdiario
	@GetMapping(value = "/subdiario/{empresa}/{subdiario}")
	public SubDiario consulta(@PathVariable("empresa") Integer empresa, @PathVariable("subdiario") String subdiario) {
		SubDiario _subdiario = subdiarioService.getSubDiario(empresa, subdiario);
		return _subdiario;
	}
	
	// Listado subdiario
	@GetMapping(value = "/subdiariolista/{empresa}")
	public List<SubDiario> lista(@PathVariable("empresa") Integer empresa){
		List<SubDiario> subdiario = new ArrayList<>();
		subdiarioService.getListaSubDiario(empresa).forEach(subdiario::add);
		return subdiario;
	}	
	
	// Contador subdiario
	@GetMapping(value = "/subdiariocontador/{empresa}/{subdiario}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("subdiario") String subdiario) {
		ValorContador _valorcontador = subdiarioService.postContadorRegistros(empresa, subdiario);
		return _valorcontador;	
	}
	
	// Catálogo subdiario
	@GetMapping(value = "/subdiariocatalogo/{empresa}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogo(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Compras
	@GetMapping(value = "/subdiariocompras/{empresa}")
	public List<Catalogo> catalogocompras(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoCompras(empresa).forEach(catalogo::add);
		return catalogo;
	}		
	
	// Catálogo subdiario - Ventas
	@GetMapping(value = "/subdiarioventas/{empresa}")
	public List<Catalogo> catalogoventas(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoVentas(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Honorarios
	@GetMapping(value = "/subdiariohonorarios/{empresa}")
	public List<Catalogo> catalogohonorarios(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoHonorarios(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - No Domiciliado
	@GetMapping(value = "/subdiarionodomiciliados/{empresa}")
	public List<Catalogo> catalogonodomiciliados(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoNoDomiciliados(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Importaciones
	@GetMapping(value = "/subdiarioimportaciones/{empresa}")
	public List<Catalogo> catalogoimportaciones(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoImportaciones(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Egresos
	@GetMapping(value = "/subdiarioegresos/{empresa}")
	public List<Catalogo> catalogoegresos(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoEgresos(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Ingresos
	@GetMapping(value = "/subdiarioingresos/{empresa}")
	public List<Catalogo> catalogoingresos(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoIngresos(empresa).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo subdiario - Diario
	@GetMapping(value = "/subdiariodiarios/{empresa}")
	public List<Catalogo> catalogodiarios(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		subdiarioService.postListaCatalogoDiarios(empresa).forEach(catalogo::add);
		return catalogo;
	}	

}
