package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.EmpresaPerfilUsuario;
import com.siegez.service.interfaces.EmpresaPerfilUsuarioService;


@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EmpresaPerfilUsuarioController {

	@Autowired
	EmpresaPerfilUsuarioService empresaperfilusuarioService;

	// Consulta empresa-perfil
	@RequestMapping(value = "/empresaperfilusuario", method = RequestMethod.POST)
	public EmpresaPerfilUsuario consulta(@RequestBody EmpresaPerfilUsuario empresaperfilusuario) {
		EmpresaPerfilUsuario _empresaperfilusuario = empresaperfilusuarioService.getEmpresaPerfilUsuario(empresaperfilusuario);
		return _empresaperfilusuario;	
	}	
	

	// Nueva empresa-perfil
	@RequestMapping(value = "/empresaperfilusuarionuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody EmpresaPerfilUsuario empresaperfilusuario) {
		empresaperfilusuarioService.postEmpresaPerfilUsuario(empresaperfilusuario);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	// Actualizar empresa-perfil
	@RequestMapping(value = "/empresaperfilusuario", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody EmpresaPerfilUsuario empresaperfilusuario) {
		EmpresaPerfilUsuario _empresaperfilusuario = empresaperfilusuarioService.getEmpresaPerfilUsuario(empresaperfilusuario);
		if (_empresaperfilusuario != null) {
			empresaperfilusuarioService.putEmpresaPerfilUsuario(empresaperfilusuario);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar empresa-perfil
	@DeleteMapping(value = "/empresaperfilusuario")
	public ResponseEntity<String> elimina(@RequestBody EmpresaPerfilUsuario empresaperfilusuario) {
		empresaperfilusuarioService.deleteEmpresaPerfilUsuario(empresaperfilusuario);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Listado por usuario
	@GetMapping(value = "/empresaperfilusuario/{username}")
	public List<EmpresaPerfilUsuario> lista(@PathVariable("username") String username){
		List<EmpresaPerfilUsuario> empresaperfilusuario = new ArrayList<>();
		empresaperfilusuarioService.getListaEmpresaPerfilUsuario(username).forEach(empresaperfilusuario::add);
		return empresaperfilusuario;
	}
	
}
