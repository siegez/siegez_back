package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.ValorFecha;
import com.siegez.service.interfaces.ValorFechaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.Valor;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class ValorFechaController {

	
	@Autowired
	ValorFechaService valorfechaService;
	
	// Registar  valor fecha
	@RequestMapping(value = "/valorfecharegistro", method = RequestMethod.POST)
	public ResultadoSP registrar(@RequestBody ValorFecha valorfecha) {
		ResultadoSP _resultadoSP = valorfechaService.postRegistrarValorFecha(valorfecha);
		return _resultadoSP;
	}	
	
	
	// Nuevo valor fecha
	@RequestMapping(value = "/valorfechanuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody ValorFecha valorfecha) {
		valorfechaService.postValorFecha(valorfecha);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar valor fecha
	@RequestMapping(value = "/valorfecha", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody ValorFecha valorfecha) {
		ValorFecha _valorfecha = valorfechaService.getValorFecha(valorfecha.getEmpresa(), valorfecha.getCodigo(), valorfecha.getVez());
		if (_valorfecha != null) {
			valorfechaService.putValorFecha(valorfecha);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar valor fecha
	@DeleteMapping(value = "/valorfecha")
	public ResponseEntity<String> elimina(@RequestBody ValorFecha valorfecha) {
		valorfechaService.deleteValorFecha(valorfecha);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta valor por vez
	@GetMapping(value = "/valorfecha/{empresa}/{codigo}/{vez}")
	public ValorFecha consulta(@PathVariable("empresa") Integer empresa, @PathVariable("codigo") String codigo, @PathVariable("vez") Integer vez) {
		ValorFecha _valorfecha = valorfechaService.getValorFecha(empresa, codigo, vez);
		return _valorfecha;
	}
	
	// Consulta valor por fecha
	@GetMapping(value = "/valorporfecha/{empresa}/{codigo}/{fecha}")
	public Valor consultaporfecha(@PathVariable("empresa") Integer empresa, @PathVariable("codigo") String codigo, @PathVariable("fecha") String fecha) {
		Valor _valor = valorfechaService.getValorPorFecha(empresa, codigo, fecha);
		return _valor;
	}
	
	// Listado de valor fecha
	@GetMapping(value = "/valorfechalista/{empresa}/{codigo}")
	public List<ValorFecha> lista(@PathVariable("empresa") Integer empresa, @PathVariable("codigo") String codigo){
		List<ValorFecha> valorfecha = new ArrayList<>();
		valorfechaService.getListaValorFecha(empresa, codigo).forEach(valorfecha::add);
		return valorfecha;
	}

	// Catálogo
	@GetMapping(value = "/valorfechacatalogo/{empresa}/{fecha}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("fecha") String fecha){
		List<Catalogo> catalogo = new ArrayList<>();
		valorfechaService.getListaCatalogo(empresa, fecha).forEach(catalogo::add);
		return catalogo;
	}		
}
