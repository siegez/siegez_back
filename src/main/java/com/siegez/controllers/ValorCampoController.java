package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.ValorCampo;
import com.siegez.service.interfaces.ValorCampoService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class ValorCampoController {

	@Autowired
	ValorCampoService valorcampoService;
	
	// Listado por descripción
	@RequestMapping(value = "/valorcampodescripcion", method = RequestMethod.POST)
	public List<ValorCampo> getValorDescripcion(@RequestBody ValorCampo valorcampo){
		List<ValorCampo> _valorcampo = new ArrayList<>();
		valorcampoService.getAllValorDescripcion(valorcampo).forEach(_valorcampo::add);
		return _valorcampo;
	}
	
	// Listado por descripción
	@RequestMapping(value = "/valorcampofiltro", method = RequestMethod.POST)
	public List<ValorCampo> getValorFiltro(@RequestBody ValorCampo valorcampo){
		List<ValorCampo> _valorcampo = new ArrayList<>();
		valorcampoService.getNotInValorDescripcion(valorcampo).forEach(_valorcampo::add);
		return _valorcampo;
	}
	
	// Listado por código
	@RequestMapping(value = "/valorcampocodigo", method = RequestMethod.POST)
	public List<ValorCampo> getValorCodigo(@RequestBody ValorCampo valorcampo){
		List<ValorCampo> _valorcampo = new ArrayList<>();
		valorcampoService.getAllValorCodigo(valorcampo).forEach(_valorcampo::add);
		return _valorcampo;
	}
}


