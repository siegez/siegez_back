package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Perfil;
import com.siegez.service.interfaces.PerfilService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class PerfilController {

	@Autowired
	PerfilService perfilService;
	
	@RequestMapping(value = "/perfil", method = RequestMethod.POST)
	public ResponseEntity<String> post(@RequestBody Perfil perfil) {
		perfilService.postPerfil(perfil);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	@RequestMapping(value = "/perfil", method = RequestMethod.PUT)
	public ResponseEntity<String> update(@RequestBody Perfil perfil) {
		Perfil _perfil = perfilService.getPerfil(perfil.getPerfil());
		if (_perfil != null) {
			perfilService.putPerfil(perfil);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	@DeleteMapping(value = "/perfil/{codigo}")
	public ResponseEntity<String> delete(@PathVariable("codigo") Integer codigo) {
		perfilService.deletePerfil(codigo);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@GetMapping(value = "perfil/{codigo}")
	public Perfil findByUsername(@PathVariable("codigo") Integer codigo) {
		Perfil _perfil = perfilService.getPerfil(codigo);
		return _perfil;
	}
	
	@GetMapping(value = "/perfilempresa/{empresa}")
	public List<Perfil> get(@PathVariable("empresa") Integer empresa){
		List<Perfil> perfil = new ArrayList<>();
		perfilService.getAllPerfil(empresa).forEach(perfil::add);
		return perfil;
	}

}
