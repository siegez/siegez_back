package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Menu;
import com.siegez.models.MenuOpcionesConsulta;
import com.siegez.service.interfaces.MenuService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class MenuController {

	@Autowired
	MenuService menuService;
	
	@GetMapping(value = "/menu")
	public List<Menu> get(){
		List<Menu> menus = new ArrayList<>();
		menuService.getAllMenu().forEach(menus::add);
		return menus;
	}
	
	//Lista de opciones de menú, por usuario y empresa por default
	@GetMapping(value = "/menuusuario/{username}")
	public List<MenuOpcionesConsulta> getOpciones(@PathVariable("username") String username){
		List<MenuOpcionesConsulta> menus = new ArrayList<>();
		menuService.getAllMenuOpciones(username).forEach(menus::add);
		return menus;
	}
	
	//Lista de opciones de menú, por usuario y empresa
	@GetMapping(value = "/menuusuarioempresa/{username}/{idempcod}")
	public List<MenuOpcionesConsulta> getOpcionesEmpresa(@PathVariable("username") String username, @PathVariable("idempcod") Integer idempcod){
		List<MenuOpcionesConsulta> menus = new ArrayList<>();
		menuService.getAllMenuEmpresa(username, idempcod).forEach(menus::add);
		return menus;
	}

	//Lista de opciones de menú, por licencia
	@GetMapping(value = "/menulicencia/{licencia}")
	public List<MenuOpcionesConsulta> getOpcionesMenuLicencia(@PathVariable("licencia") String licencia){
		List<MenuOpcionesConsulta> menus = new ArrayList<>();
		menuService.getAllMenuPerfilNuevo(licencia).forEach(menus::add);
		return menus;
	}
	
	//Lista de opciones de menú, por perfil y licencia, para actualizar accesos
	@GetMapping(value = "/menulicenciaperfil/{perfil}/{licencia}")
	public List<MenuOpcionesConsulta> getOpcionesMenuLicenciaPerfil(@PathVariable("perfil") String perfil, @PathVariable("licencia") String licencia){
		List<MenuOpcionesConsulta> menus = new ArrayList<>();
		menuService.getAllMenuPerfilModifica(perfil, licencia).forEach(menus::add);
		return menus;
	}
	
	@GetMapping(value = "menu/{codigo}")
	public Menu findByCodigo(@PathVariable("codigo") Integer codigo) {
		Menu _menu = menuService.getMenu(codigo);
		return _menu;
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.POST)
	public ResponseEntity<String> post(@RequestBody Menu menu) {
		menuService.postMenu(menu);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	@DeleteMapping(value = "/menu/{codigo}")
	public ResponseEntity<String> delete(@PathVariable("codigo") Integer codigo) {
		menuService.deleteMenu(codigo);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.PUT)
	public ResponseEntity<String> update(@RequestBody Menu menu) {
		Menu _menu = menuService.getMenu(menu.getCodigo());
		if (_menu != null) {
			menuService.putMenu(menu);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}	
	
}
