package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.EnteDireccion;
import com.siegez.service.interfaces.EnteDireccionService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EnteDireccionController {
	@Autowired
	EnteDireccionService entedireccionService;	
	
	// Nueva ente
	@RequestMapping(value = "/entedireccionnuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody EnteDireccion entedireccion) {
		entedireccionService.postEnteDireccion(entedireccion);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Actualizar ente
	@RequestMapping(value = "/entedireccion", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody EnteDireccion entedireccion) {
		EnteDireccion _entedireccion = entedireccionService.getEnteDireccion(entedireccion.getEmpresa(), entedireccion.getEntidad(), entedireccion.getVez());
		if (_entedireccion != null) {
			entedireccionService.putEnteDireccion(entedireccion);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}

	// Eliminar ente
	@DeleteMapping(value = "/entedireccion")
	public ResponseEntity<String> elimina(@RequestBody EnteDireccion entedireccion) {
		entedireccionService.deleteEnteDireccion(entedireccion);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta ente
	@GetMapping(value = "/entedireccion/{empresa}/{entidad}/{vez}")
	public EnteDireccion consulta(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad, @PathVariable("vez") Integer vez) {
		EnteDireccion _entedireccion = entedireccionService.getEnteDireccion(empresa, entidad, vez);
		return _entedireccion;
	}	
	
	// Listado de entes
	@GetMapping(value = "/entedireccionlista/{empresa}/{entidad}")
	public List<EnteDireccion> lista(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad){
		List<EnteDireccion> entedireccion = new ArrayList<>();
		entedireccionService.getListaEnteDireccion(empresa, entidad).forEach(entedireccion::add);
		return entedireccion;
	}	
	
	
}
