package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.siegez.service.interfaces.ConsultaService;
import com.siegez.views.ValoresIniciales;
import com.siegez.views.VistaCentroCosto;
import com.siegez.views.ValorContador;
import com.siegez.views.Catalogo;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController

public class ConsultaController {

	@Autowired
	ConsultaService consultaService;

	// Consulta por usuario
	@GetMapping(value = "valoresiniciales/{username}")
	public ValoresIniciales consultaValoresIniciales(@PathVariable("username") String username) {
		ValoresIniciales _valoresiniciales = consultaService.getValoresIniciales(username);
		return _valoresiniciales;
	}
	
	// Lista por usuario
	@GetMapping(value = "listavaloresiniciales/{username}")
	public List<ValoresIniciales> listaValoresIniciales(@PathVariable("username") String username) {
		List<ValoresIniciales> valoresiniciales = new ArrayList<>();
		consultaService.getAllValoresIniciales(username).forEach(valoresiniciales::add);
		return valoresiniciales;
	}
	
	// Consulta si esta permitido incluir documento de referencia en compras
	@GetMapping(value = "documentoreferenciacompra/{empresa}/{ejerciciocontable}/{tipoodocumento}/{moneda}")
	public ValorContador permiteDocumentoReferencia(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("tipoodocumento") String tipoodocumento, @PathVariable("moneda") String moneda) {
		ValorContador _valorcontador = consultaService.getpermitirDocumentoReferencia(empresa, ejerciciocontable, tipoodocumento, moneda);
		return _valorcontador;
	}	
	
	// Lista centro de costos
	@GetMapping(value = "listacentrocosto/{empresa}/{ejerciciocontable}")
	public List<VistaCentroCosto> listaCentroCosto(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable) {
		List<VistaCentroCosto> vistacentrocosto = new ArrayList<>();
		consultaService.getListaCentroCosto(empresa, ejerciciocontable).forEach(vistacentrocosto::add);
		return vistacentrocosto;
	}
	
	// Lista ubicación geográfica
	@GetMapping(value = "listaubicaciongeografica")
	public List<Catalogo> listaUbicacionGeografica() {
		List<Catalogo> vistaubicaciongeografica = new ArrayList<>();
		consultaService.getListaUbicacionGeografica().forEach(vistaubicaciongeografica::add);
		return vistaubicaciongeografica;
	}
	
}
