package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Usuario;
import com.siegez.service.interfaces.UsuarioService;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	// Listado general	
	@GetMapping(value = "/usuario")
	public List<Usuario> lista(){
		List<Usuario> usuarios = new ArrayList<>();
		usuarioService.getListaUsuario().forEach(usuarios::add);
		return usuarios;
	}
	
	// Listado por licencia
	@GetMapping(value = "/usuariolicencia/{licencia}")
	public List<Usuario> listaLicencia(@PathVariable("licencia") String licencia){
		List<Usuario> usuarios = new ArrayList<>();
		usuarioService.getListaUsuarioLicencia(licencia).forEach(usuarios::add);
		return usuarios;
	}

	// Consulta por usuario
	@GetMapping(value = "usuario/{username}")
	public Usuario consulta(@PathVariable("username") String username) {
		Usuario _usuario = usuarioService.getUsuario(username);
		return _usuario;
	}
	
	// Nuevo usuario
	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody Usuario usuario) {
		usuarioService.postUsuario(usuario);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Eliminar usuario
	@DeleteMapping(value = "/usuario/{username}")
	public ResponseEntity<String> elimina(@PathVariable("username") String username) {
		usuarioService.deleteUsuario(username);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Actualizar usuario
	@RequestMapping(value = "/usuario", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Usuario usuario) {
		Usuario _usuario = usuarioService.getUsuario(usuario.getUsername());
		if (_usuario != null) {
			usuarioService.putUsuario(usuario);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}	
	
	// Contador de cuenta contable
	@RequestMapping(value = "/usuariocontador", method = RequestMethod.POST)
	public ValorContador contador(@RequestBody Usuario usuario) {
		ValorContador _valorcontador = usuarioService.postContadorRegistros(usuario);
		return _valorcontador;	
	}
}
