package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Ente;
import com.siegez.models.EnteRegistro;
import com.siegez.service.interfaces.EnteService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.SocioNegocio;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EnteController {

	
	@Autowired
	EnteService enteService;
	
	// Registar Entidad
	@RequestMapping(value = "/enteregistro", method = RequestMethod.POST)
	public ResultadoSP registrar(@RequestBody EnteRegistro enteregistro) {
		ResultadoSP _resultadoSP = enteService.postRegistrarEnte(enteregistro);
		return _resultadoSP;
	}
	
	// Nueva ente
	@RequestMapping(value = "/entenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody Ente ente) {
		enteService.postEnte(ente);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Actualizar ente
	@RequestMapping(value = "/ente", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Ente ente) {
		Ente _ente = enteService.getEnte(ente.getEmpresa(), ente.getEntidad());
		if (_ente != null) {
			enteService.putEnte(ente);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}

	// Eliminar ente
	@DeleteMapping(value = "/ente")
	public ResponseEntity<String> elimina(@RequestBody Ente ente) {
		enteService.deleteEnte(ente);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta ente
	@GetMapping(value = "/ente/{empresa}/{entidad}")
	public Ente consulta(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad) {
		Ente _ente = enteService.getEnte(empresa, entidad);
		return _ente;
	}	
	
	// Consulta ente por ruc
	@GetMapping(value = "/enterucanexo/{empresa}/{ruc}/{anexo}")
	public Ente consultarucanexo(@PathVariable("empresa") Integer empresa, @PathVariable("ruc") String ruc, @PathVariable("anexo") String anexo) {
		Ente _ente = enteService.getRucAnexo(empresa, ruc, anexo);
		return _ente;
	}
	
	// Listado de entes
	@GetMapping(value = "/entelista/{empresa}")
	public List<Ente> lista(@PathVariable("empresa") Integer empresa){
		List<Ente> ente = new ArrayList<>();
		enteService.getListaEnte(empresa).forEach(ente::add);
		return ente;
	}	
	
	// Listado de proveedores
	@GetMapping(value = "/proveedorlista/{empresa}")
	public List<SocioNegocio> listaProveedor(@PathVariable("empresa") Integer empresa){
		List<SocioNegocio> socionegocio = new ArrayList<>();
		enteService.getListaProveedor(empresa).forEach(socionegocio::add);
		return socionegocio;
	}	
	
	// Listado de proveedores
	@GetMapping(value = "/proveedorlistabusqueda/{empresa}/{criterio}")
	public List<Catalogo> listaProveedorbusqueda(@PathVariable("empresa") Integer empresa, @PathVariable("criterio") String criterio){
		List<Catalogo> catalogo = new ArrayList<>();
		enteService.getListaProveedor(empresa, criterio).forEach(catalogo::add);
		return catalogo;
	}
	
	// Contador de entes
	@RequestMapping(value = "/entecontador", method = RequestMethod.POST)
	public ValorContador contador(@RequestBody Ente ente) {
		ValorContador _valorcontador = enteService.postContadorRegistros(ente);
		return _valorcontador;	
	}
	
	// Contador de ente por ruc
	@RequestMapping(value = "/enteruccontador", method = RequestMethod.POST)
	public ValorContador contadorruc(@RequestBody Ente ente) {
		ValorContador _valorcontador = enteService.postContadorRuc(ente);
		return _valorcontador;	
	}
	
	// Contador de proveedor relacionado
	@RequestMapping(value = "/enteproveedorrelcontador", method = RequestMethod.POST)
	public ValorContador contadorProveedorRel(@RequestBody Ente ente) {
		ValorContador _valorcontador = enteService.postContadorProveedorRel(ente);
		return _valorcontador;	
	}
	
	// Catálogo
	@RequestMapping(value = "/entecatalogo", method = RequestMethod.POST)
	public List<Catalogo> catalogo(@RequestBody Ente ente){
		List<Catalogo> catalogo = new ArrayList<>();
		enteService.postListaCatalogo(ente).forEach(catalogo::add);
		return catalogo;
	}
	
	// Listado de clientes
	@GetMapping(value = "/clientelista/{empresa}")
	public List<SocioNegocio> listaCliente(@PathVariable("empresa") Integer empresa){
		List<SocioNegocio> socionegocio = new ArrayList<>();
		enteService.getListaCliente(empresa).forEach(socionegocio::add);
		return socionegocio;
	}	
	
	// Listado de clientes
	@GetMapping(value = "/clientelistabusqueda/{empresa}/{criterio}")
	public List<Catalogo> listaClientebusqueda(@PathVariable("empresa") Integer empresa, @PathVariable("criterio") String criterio){
		List<Catalogo> catalogo = new ArrayList<>();
		enteService.getListaCliente(empresa, criterio).forEach(catalogo::add);
		return catalogo;
	}
	
	// Contador de cliente relacionado
	@RequestMapping(value = "/enteclienterelcontador", method = RequestMethod.POST)
	public ValorContador contadorClienteRel(@RequestBody Ente ente) {
		ValorContador _valorcontador = enteService.postContadorClienteRel(ente);
		return _valorcontador;	
	}
	
	// Listado de proveedores no domiciliado
	@GetMapping(value = "/proveedornodomlista/{empresa}")
	public List<SocioNegocio> listaProveedorNoDom(@PathVariable("empresa") Integer empresa){
		List<SocioNegocio> socionegocio = new ArrayList<>();
		enteService.getListaProveedorNoDom(empresa).forEach(socionegocio::add);
		return socionegocio;
	}	
	
	// Listado de  proveedores no domiciliado
	@GetMapping(value = "/proveedornodomlistabusqueda/{empresa}/{criterio}")
	public List<Catalogo> listaProveedorNoDombusqueda(@PathVariable("empresa") Integer empresa, @PathVariable("criterio") String criterio){
		List<Catalogo> catalogo = new ArrayList<>();
		enteService.getListaProveedorNoDom(empresa, criterio).forEach(catalogo::add);
		return catalogo;
	}
	
	// Contador de  proveedores no domiciliado relacionado
	@RequestMapping(value = "/enteproveedornodomrelcontador", method = RequestMethod.POST)
	public ValorContador contadorProveedorNoDomRel(@RequestBody Ente ente) {
		ValorContador _valorcontador = enteService.postContadorProveedorNoDomRel(ente);
		return _valorcontador;	
	}
	
	// Listado de bancos
	@GetMapping(value = "/bancolista/{empresa}")
	public List<SocioNegocio> listaBanco(@PathVariable("empresa") Integer empresa){
		List<SocioNegocio> socionegocio = new ArrayList<>();
		enteService.getListaBanco(empresa).forEach(socionegocio::add);
		return socionegocio;
	}
	
	// Listado de socios de negocio
	@GetMapping(value = "/socionegociolista/{empresa}")
	public List<SocioNegocio> listaSocioNegocio(@PathVariable("empresa") Integer empresa){
		List<SocioNegocio> socionegocio = new ArrayList<>();
		enteService.getListaSocioNegocio(empresa).forEach(socionegocio::add);
		return socionegocio;
	}
	
	// Listado de socios de negocio
	@GetMapping(value = "/socionegociolistabusqueda/{empresa}/{criterio}")
	public List<Catalogo> listaSocioNegociobusqueda(@PathVariable("empresa") Integer empresa, @PathVariable("criterio") String criterio){
		List<Catalogo> catalogo = new ArrayList<>();
		enteService.getListaSocioNegocio(empresa, criterio).forEach(catalogo::add);
		return catalogo;
	}
	
	// Listado de entes
	@GetMapping(value = "/entebancolista/{empresa}")
	public List<Ente> listaEnteBanco(@PathVariable("empresa") Integer empresa){
		List<Ente> ente = new ArrayList<>();
		enteService.getListaEnteBanco(empresa).forEach(ente::add);
		return ente;
	}
}
