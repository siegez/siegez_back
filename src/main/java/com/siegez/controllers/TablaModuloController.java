package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.TablaModulo;
import com.siegez.service.interfaces.TablaModuloService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class TablaModuloController {

	@Autowired
	TablaModuloService tablamoduloService;
	
	@GetMapping(value = "/tablamodulo/{modulo}")
	public List<TablaModulo> get(@PathVariable("modulo") String modulo){
		List<TablaModulo> tablamodulo = new ArrayList<>();
		tablamoduloService.getAllTablaModulo(modulo).forEach(tablamodulo::add);
		return tablamodulo;
	}
}
