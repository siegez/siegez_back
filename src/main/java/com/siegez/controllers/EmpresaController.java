package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.Empresa;
import com.siegez.service.interfaces.EmpresaService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EmpresaController {
	
	@Autowired
	EmpresaService empresaService;
	
	// Registar Empresa
	@RequestMapping(value = "/empresaregistro", method = RequestMethod.POST)
	public ResultadoSP registrar(@RequestBody Empresa empresa) {
		ResultadoSP _resultadoSP = empresaService.postRegistrarEmpresa(empresa);
		return _resultadoSP;
	}
		
	@GetMapping(value = "/empresa")
	public List<Empresa> get(){
		List<Empresa> empresas = new ArrayList<>();
		empresaService.getAllEmpresa().forEach(empresas::add);
		return empresas;
	}

	@GetMapping(value = "/empresalicencia/{licencia}")
	public List<Empresa> getEmpresaLicencia(@PathVariable("licencia") String licencia){
		List<Empresa> empresas = new ArrayList<>();
		empresaService.getAllEmpresaLicencia(licencia).forEach(empresas::add);
		return empresas;
	}

	
	@GetMapping(value = "empresa/{empresaCodigo}")
	public Empresa findByEmpresaCod(@PathVariable("empresaCodigo") Integer empresaCodigo) {
		Empresa empresa = empresaService.getEmpresa(empresaCodigo);
		return empresa;
	}
	
	@RequestMapping(value = "/empresa", method = RequestMethod.POST)
	public ResponseEntity<Integer> post(@RequestBody Empresa empresa) {
		int codigo= empresaService.postEmpresa(empresa);
		return new ResponseEntity<Integer>(codigo, HttpStatus.OK);	
	}
	
	@DeleteMapping(value = "/empresa/{empresaCodigo}")
	public ResponseEntity<String> delete(@PathVariable("empresaCodigo") Integer empresaCodigo) {
		empresaService.deleteEmpresa(empresaCodigo);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/empresa", method = RequestMethod.PUT)
	public ResponseEntity<String> update(@RequestBody Empresa empresa) {
		Empresa _empresa = empresaService.getEmpresa(empresa.getEmpresa());
		if (_empresa != null) {
			empresaService.putEmpresa(empresa);;
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}	
	
	// Catálogo
	@GetMapping(value = "/empresacatalogo/{licencia}")
	public List<Catalogo> catalogo(@PathVariable("licencia") String licencia){
		List<Catalogo> catalogo = new ArrayList<>();
		empresaService.getListaCatalogo(licencia).forEach(catalogo::add);
		return catalogo;
	}	
	
	// Catálogo por usuario
	@GetMapping(value = "/empresausuario/{username}")
	public List<Catalogo> catalogousuario(@PathVariable("username") String username){
		List<Catalogo> catalogo = new ArrayList<>();
		empresaService.getListaCatalogoUsuario(username).forEach(catalogo::add);
		return catalogo;
	}
	
	// Replicar Empresa
	@GetMapping(value = "/empresareplicar/{idempcodori}/{annoori}/{idempcoddes}/{annodes}")
	public ResultadoSP replicar(@PathVariable("idempcodori") Integer idempcodori, @PathVariable("annoori") Integer annoori, 
								@PathVariable("idempcoddes") Integer idempcoddes, @PathVariable("annodes") Integer annodes) {
		ResultadoSP _resultadoSP = empresaService.postReplicarEmpresa(idempcodori, annoori, idempcoddes, annodes);
		return _resultadoSP;	
	}
		
}
