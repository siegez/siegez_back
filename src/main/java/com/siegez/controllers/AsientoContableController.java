package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.AsientoContable;
import com.siegez.service.interfaces.AsientoContableService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class AsientoContableController {

	@Autowired
	AsientoContableService asientocontableService;
	
	// Nuevo asiento contable
	@RequestMapping(value = "/asientocontablenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody AsientoContable asientocontable) {
		asientocontableService.postAsientoContable(asientocontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	

	// Actualizar asiento contable
	@RequestMapping(value = "/asientocontable", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody AsientoContable asientocontable) {
		AsientoContable _asientocontable = asientocontableService.getAsientoContable(asientocontable.getEmpresa(), asientocontable.getEjerciciocontable(), asientocontable.getAsientocontable());
		if (_asientocontable != null) {
			asientocontableService.putAsientoContable(asientocontable);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar asiento contable
	@DeleteMapping(value = "/asientocontable")
	public ResponseEntity<String> elimina(@RequestBody AsientoContable asientocontable) {
		asientocontableService.deleteAsientoContable(asientocontable);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta asiento contable
	@GetMapping(value = "/asientocontable/{empresa}/{ejerciciocontable}/{asientocontable}")
	public AsientoContable consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable) {
		AsientoContable _asientocontable = asientocontableService.getAsientoContable(empresa, ejerciciocontable, asientocontable);
		return _asientocontable;
	}
	
	// Listado de asiento contables
	@GetMapping(value = "/asientocontablelista/{empresa}/{ejerciciocontable}")
	public List<AsientoContable> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<AsientoContable> asientocontable = new ArrayList<>();
		asientocontableService.getListaAsientoContable(empresa, ejerciciocontable).forEach(asientocontable::add);
		return asientocontable;
	}
	
	// Contador asientos contables
	@GetMapping(value = "/asientocontablecontador/{empresa}/{ejerciciocontable}/{asientocontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("asientocontable") Integer asientocontable) {
		ValorContador _valorcontador = asientocontableService.getContadorRegistros(empresa, ejerciciocontable, asientocontable);
		return _valorcontador;	
	}
	
	// Catálogo asiento contable
	@GetMapping(value = "/asientocontablecatalogo/{empresa}/{ejerciciocontable}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		asientocontableService.getListaCatalogo(empresa, ejerciciocontable).forEach(catalogo::add);
		return catalogo;
	}	
	
}

