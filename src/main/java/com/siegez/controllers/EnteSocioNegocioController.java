package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.EnteSocioNegocio;
import com.siegez.service.interfaces.EnteSocioNegocioService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EnteSocioNegocioController {

	@Autowired
	EnteSocioNegocioService entesocionegocioService;	
	
	// Nueva ente
	@RequestMapping(value = "/entesocionegocionuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody EnteSocioNegocio entesocionegocio) {
		entesocionegocioService.postEnteSocioNegocio(entesocionegocio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Actualizar ente
	@RequestMapping(value = "/entesocionegocio", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody EnteSocioNegocio entesocionegocio) {
		EnteSocioNegocio _entesocionegocio = entesocionegocioService.getEnteSocioNegocio(entesocionegocio.getEmpresa(), entesocionegocio.getEntidad(), entesocionegocio.getVez());
		if (_entesocionegocio != null) {
			entesocionegocioService.putEnteSocioNegocio(entesocionegocio);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}

	// Eliminar ente
	@DeleteMapping(value = "/entesocionegocio")
	public ResponseEntity<String> elimina(@RequestBody EnteSocioNegocio entesocionegocio) {
		entesocionegocioService.deleteEnteSocioNegocio(entesocionegocio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta ente
	@GetMapping(value = "/entesocionegocio/{empresa}/{entidad}/{vez}")
	public EnteSocioNegocio consulta(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad, @PathVariable("vez") Integer vez) {
		EnteSocioNegocio _entesocionegocio = entesocionegocioService.getEnteSocioNegocio(empresa, entidad, vez);
		return _entesocionegocio;
	}	
	
	// Listado de entes
	@GetMapping(value = "/entesocionegociolista/{empresa}/{entidad}")
	public List<EnteSocioNegocio> lista(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad){
		List<EnteSocioNegocio> entesocionegocio = new ArrayList<>();
		entesocionegocioService.getListaEnteSocioNegocio(empresa, entidad).forEach(entesocionegocio::add);
		return entesocionegocio;
	}		
	
}

