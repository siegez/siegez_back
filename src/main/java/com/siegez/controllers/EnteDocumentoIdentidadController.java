package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.EnteDocumentoIdentidad;
import com.siegez.service.interfaces.EnteDocumentoIdentidadService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EnteDocumentoIdentidadController {

	@Autowired
	EnteDocumentoIdentidadService entedocumentoidentidadService;	
	
	// Nueva ente
	@RequestMapping(value = "/entedocumentoidentidadnuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody EnteDocumentoIdentidad entedocumentoidentidad) {
		entedocumentoidentidadService.postEnteDocumentoIdentidad(entedocumentoidentidad);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	// Actualizar ente
	@RequestMapping(value = "/entedocumentoidentidad", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody EnteDocumentoIdentidad entedocumentoidentidad) {
		EnteDocumentoIdentidad _entedocumentoidentidad = entedocumentoidentidadService.getEnteDocumentoIdentidad(entedocumentoidentidad.getEmpresa(), entedocumentoidentidad.getEntidad(), entedocumentoidentidad.getVez());
		if (_entedocumentoidentidad != null) {
			entedocumentoidentidadService.putEnteDocumentoIdentidad(entedocumentoidentidad);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}

	// Eliminar ente
	@DeleteMapping(value = "/entedocumentoidentidad")
	public ResponseEntity<String> elimina(@RequestBody EnteDocumentoIdentidad entedocumentoidentidad) {
		entedocumentoidentidadService.deleteEnteDocumentoIdentidad(entedocumentoidentidad);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta ente
	@GetMapping(value = "/entedocumentoidentidad/{empresa}/{entidad}/{vez}")
	public EnteDocumentoIdentidad consulta(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad, @PathVariable("vez") Integer vez) {
		EnteDocumentoIdentidad _entedocumentoidentidad = entedocumentoidentidadService.getEnteDocumentoIdentidad(empresa, entidad, vez);
		return _entedocumentoidentidad;
	}	
	
	// Listado de entes
	@GetMapping(value = "/entedocumentoidentidadlista/{empresa}/{entidad}")
	public List<EnteDocumentoIdentidad> lista(@PathVariable("empresa") Integer empresa, @PathVariable("entidad") String entidad){
		List<EnteDocumentoIdentidad> entedocumentoidentidad = new ArrayList<>();
		entedocumentoidentidadService.getListaEnteDocumentoIdentidad(empresa, entidad).forEach(entedocumentoidentidad::add);
		return entedocumentoidentidad;
	}	
	
}
