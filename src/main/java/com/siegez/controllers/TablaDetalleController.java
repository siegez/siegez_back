package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.TablaDetalle;
import com.siegez.service.interfaces.TablaDetalleService;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class TablaDetalleController {
	@Autowired
	TablaDetalleService tabladetalleService;
	
	@RequestMapping(value = "/tabladetallenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody TablaDetalle tabladetalle) {
		tabladetalleService.postTablaDetalle(tabladetalle);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	@RequestMapping(value = "/tabladetalle", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody TablaDetalle tabladetalle) {
		TablaDetalle _tabladetalle = tabladetalleService.getTablaDetalle(tabladetalle);
		if (_tabladetalle != null) {
			tabladetalleService.putTablaDetalle(tabladetalle);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	@DeleteMapping(value = "/tabladetalle")
	public ResponseEntity<String> elimina(@RequestBody TablaDetalle tabladetalle) {
		tabladetalleService.deleteTablaDetalle(tabladetalle);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/tabladetalle", method = RequestMethod.POST)
	public TablaDetalle consulta(@RequestBody TablaDetalle tabladetalle) {
		TablaDetalle _tabladetalle = tabladetalleService.getTablaDetalle(tabladetalle);
		return _tabladetalle;
	}	
	
	@GetMapping(value = "/tabladetalledescripcion/{empresa}/{tabla}")
	public List<TablaDetalle> listadescripcion(@PathVariable("empresa") Integer empresa, @PathVariable("tabla") Integer tabla){
		List<TablaDetalle> tabladetalle = new ArrayList<>();
		tabladetalleService.getListaTablaDetalleDescripcion(empresa, tabla).forEach(tabladetalle::add);
		return tabladetalle;
	}
	
	@GetMapping(value = "/tabladetallecodigo/{empresa}/{tabla}")
	public List<TablaDetalle> listacodigo(@PathVariable("empresa") Integer empresa, @PathVariable("tabla") Integer tabla){
		List<TablaDetalle> tabladetalle = new ArrayList<>();
		tabladetalleService.getListaTablaDetalleCodigo(empresa, tabla).forEach(tabladetalle::add);
		return tabladetalle;
	}

}
