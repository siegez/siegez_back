package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.CentroCosto;
import com.siegez.service.interfaces.CentroCostoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class CentroCostoController {
	
	@Autowired
	CentroCostoService centrocostoService;
	
	// Nueva cuenta Centro Costo
	@RequestMapping(value = "/centrocostonuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody CentroCosto centrocosto) {
		centrocostoService.postCentroCosto(centrocosto);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	// Actualizar Centro Costo
	@RequestMapping(value = "/centrocosto", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody CentroCosto centrocosto) {
		CentroCosto _centrocosto = centrocostoService.getCentroCosto(centrocosto.getEmpresa(), centrocosto.getEjerciciocontable(), centrocosto.getCentrocosto());
		if (_centrocosto != null) {
			centrocostoService.putCentroCosto(centrocosto);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar centro costo
	@DeleteMapping(value = "/centrocosto")
	public ResponseEntity<String> elimina(@RequestBody CentroCosto centrocosto) {
		centrocostoService.deleteCentroCosto(centrocosto);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta centro costo
	@GetMapping(value = "/centrocosto/{empresa}/{ejerciciocontable}/{centrocosto}")
	public CentroCosto consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("centrocosto") String centrocosto) {
		CentroCosto _centrocosto = centrocostoService.getCentroCosto(empresa, ejerciciocontable, centrocosto);
		return _centrocosto;	
	}
	
	// Listado 
	@GetMapping(value = "/centrocostolista/{empresa}/{ejerciciocontable}")
	public List<CentroCosto> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<CentroCosto> centrocosto = new ArrayList<>();
		centrocostoService.getListaCentroCosto(empresa, ejerciciocontable).forEach(centrocosto::add);
		return centrocosto;
	}

	// Contador de centro costo
	@GetMapping(value = "/centrocostocontador/{empresa}/{ejerciciocontable}/{centrocosto}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("centrocosto") String centrocosto) {
		ValorContador _valorcontador = centrocostoService.getContadorRegistros(empresa, ejerciciocontable, centrocosto);
		return _valorcontador;	
	}
	
	// Catálogo lista
	@GetMapping(value = "/centrocostocatalogolista/{empresa}/{ejerciciocontable}")
	public List<Catalogo> catalogolista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		centrocostoService.getListaCatalogo(empresa, ejerciciocontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo filtro centro costo
	@GetMapping(value = "/centrocostocatalogo/{empresa}/{ejerciciocontable}/{centrocosto}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("centrocosto") String centrocosto){
		List<Catalogo> catalogo = new ArrayList<>();
		centrocostoService.getListaCatalogo(empresa, ejerciciocontable, centrocosto).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo filtro not in
	@RequestMapping(value = "/centrocostofiltro", method = RequestMethod.POST)
	public List<Catalogo> getValorFiltro(@RequestBody CentroCosto centrocosto){
		List<Catalogo> _catalogo = new ArrayList<>();
		centrocostoService.getNotInCatalogo(centrocosto).forEach(_catalogo::add);
		return _catalogo;
	}
	
}
