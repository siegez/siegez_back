package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.Ejercicio;
import com.siegez.service.interfaces.EjercicioService;
import com.siegez.views.Catalogo;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class EjercicioController {

	@Autowired
	EjercicioService ejercicioService;
	
	@RequestMapping(value = "/ejercicio", method = RequestMethod.POST)
	public ResponseEntity<String> nuevo(@RequestBody Ejercicio ejercicio) {
		ejercicioService.postEjercicio(ejercicio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}
	
	@RequestMapping(value = "/ejercicio", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody Ejercicio ejercicio) {	
		Ejercicio _ejercicio = ejercicioService.getEjercicio(ejercicio.getEmpresa(), ejercicio.getEjerciciocontable());
		if (_ejercicio != null) {
			ejercicioService.putEjercicio(ejercicio);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		}
		
	}	
	
	@RequestMapping(value = "/ejercicio",method = RequestMethod.DELETE)
	public ResponseEntity<String> elimina(@RequestBody Ejercicio ejercicio) {
		ejercicioService.deleteEjercicio(ejercicio);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	@GetMapping(value = "ejercicio/{empresa}/{ejerciciocontable}")
	public Ejercicio consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		Ejercicio _ejercicio = ejercicioService.getEjercicio(empresa, ejerciciocontable);
		return _ejercicio;
	}
	
	@GetMapping(value = "/ejerciciolista/{empresa}")
	public List<Ejercicio> lista(@PathVariable("empresa") Integer empresa){
		List<Ejercicio> ejercicio = new ArrayList<>();
		ejercicioService.getListaEjercicio(empresa).forEach(ejercicio::add);
		return ejercicio;
	}
	
	@GetMapping(value = "/ejerciciocontador/{empresa}/{ejerciciocontable}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable) {
		ValorContador _valorcontador = ejercicioService.getContadorRegistros(empresa, ejerciciocontable);
		return _valorcontador;	
	}
	
	// Catálogo
	@GetMapping(value = "/ejerciciocatalogo/{empresa}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa){
		List<Catalogo> catalogo = new ArrayList<>();
		ejercicioService.getListaCatalogo(empresa).forEach(catalogo::add);
		return catalogo;
	}	

	

}
