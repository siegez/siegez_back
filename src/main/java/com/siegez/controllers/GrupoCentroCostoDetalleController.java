package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.models.GrupoCentroCostoDetalle;
import com.siegez.service.interfaces.GrupoCentroCostoDetalleService;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class GrupoCentroCostoDetalleController {

	@Autowired
	GrupoCentroCostoDetalleService grupocentrocostodetalleService;
	
	// Nueva Grupo Centro Costo Detalle
	@RequestMapping(value = "/grupocentrocostodetallenuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody GrupoCentroCostoDetalle grupocentrocostodetalle) {
		grupocentrocostodetalleService.postGrupoCentroCostoDetalle(grupocentrocostodetalle);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	// Actualizar Grupo Centro Costo Detalle
	@RequestMapping(value = "/grupocentrocostodetalle", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody GrupoCentroCostoDetalle grupocentrocostodetalle) {
		GrupoCentroCostoDetalle _grupocentrocostodetalle = grupocentrocostodetalleService.getGrupoCentroCostoDetalle(grupocentrocostodetalle.getEmpresa(), grupocentrocostodetalle.getEjerciciocontable(), grupocentrocostodetalle.getGrupocentrocosto(), grupocentrocostodetalle.getCentrocosto());
		if (_grupocentrocostodetalle != null) {
			grupocentrocostodetalleService.putGrupoCentroCostoDetalle(grupocentrocostodetalle);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar Grupo centro costo Detalle
	@DeleteMapping(value = "/grupocentrocostodetalle")
	public ResponseEntity<String> elimina(@RequestBody GrupoCentroCostoDetalle grupocentrocostodetalle) {
		grupocentrocostodetalleService.deleteGrupoCentroCostoDetalle(grupocentrocostodetalle);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta centro costo detalle
	@GetMapping(value = "/grupocentrocostodetalle/{empresa}/{ejerciciocontable}/{grupocentrocosto}/{centrocosto}")
	public GrupoCentroCostoDetalle consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto, @PathVariable("centrocosto") String centrocosto) {
		GrupoCentroCostoDetalle _grupocentrocostodetalle = grupocentrocostodetalleService.getGrupoCentroCostoDetalle(empresa, ejerciciocontable, grupocentrocosto, centrocosto);
		return _grupocentrocostodetalle;	
	}
	
	// Listado 
	@GetMapping(value = "/grupocentrocostodetallelista/{empresa}/{ejerciciocontable}/{grupocentrocosto}")
	public List<GrupoCentroCostoDetalle> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto){
		List<GrupoCentroCostoDetalle> grupocentrocostodetalle = new ArrayList<>();
		grupocentrocostodetalleService.getListaGrupoCentroCostoDetalle(empresa, ejerciciocontable, grupocentrocosto).forEach(grupocentrocostodetalle::add);
		return grupocentrocostodetalle;
	}

	// Contador de Grupo centro costo detalle
	@GetMapping(value = "/grupocentrocostodetallecontador/{empresa}/{ejerciciocontable}/{grupocentrocosto}/{centrocosto}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto, @PathVariable("centrocosto") String centrocosto) {
		ValorContador _valorcontador = grupocentrocostodetalleService.getContadorRegistros(empresa, ejerciciocontable, grupocentrocosto, centrocosto);
		return _valorcontador;	
	}	
}
