package com.siegez.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.siegez.models.GrupoCentroCosto;
import com.siegez.models.GrupoCentroCostoRegistro;
import com.siegez.service.interfaces.GrupoCentroCostoService;
import com.siegez.views.Catalogo;
import com.siegez.views.ResultadoSP;
import com.siegez.views.ValorContador;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class GrupoCentroCostoController {

	@Autowired
	GrupoCentroCostoService grupocentrocostoService;
	
	// Registar Grupo Centro de Costo
	@RequestMapping(value = "/grupocentrocostoregistro", method = RequestMethod.POST)
	public ResultadoSP registrar(@RequestBody GrupoCentroCostoRegistro grupocentrocostoregistro) {
		ResultadoSP _resultadoSP = grupocentrocostoService.postRegistrarGrupoCentroCosto(grupocentrocostoregistro);
		return _resultadoSP;
	}
	
	// Nueva Grupo Centro Costo
	@RequestMapping(value = "/grupocentrocostonuevo", method = RequestMethod.POST)
	public ResponseEntity<String> inserta(@RequestBody GrupoCentroCosto grupocentrocosto) {
		grupocentrocostoService.postGrupoCentroCosto(grupocentrocosto);
		return new ResponseEntity<String>("ok", HttpStatus.OK);	
	}	
	
	// Actualizar Grupo Centro Costo
	@RequestMapping(value = "/grupocentrocosto", method = RequestMethod.PUT)
	public ResponseEntity<String> edita(@RequestBody GrupoCentroCosto grupocentrocosto) {
		GrupoCentroCosto _grupocentrocosto = grupocentrocostoService.getGrupoCentroCosto(grupocentrocosto.getEmpresa(), grupocentrocosto.getEjerciciocontable(), grupocentrocosto.getGrupocentrocosto());
		if (_grupocentrocosto != null) {
			grupocentrocostoService.putGrupoCentroCosto(grupocentrocosto);
			return new ResponseEntity<String>("ok", HttpStatus.OK);		
		} else {
			return new ResponseEntity<String>("error", HttpStatus.NOT_FOUND);
		} 
	}
	
	// Eliminar Grupo centro costo
	@DeleteMapping(value = "/grupocentrocosto")
	public ResponseEntity<String> elimina(@RequestBody GrupoCentroCosto grupocentrocosto) {
		grupocentrocostoService.deleteGrupoCentroCosto(grupocentrocosto);
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
	
	// Consulta centro costo
	@GetMapping(value = "/grupocentrocosto/{empresa}/{ejerciciocontable}/{grupocentrocosto}")
	public GrupoCentroCosto consulta(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto) {
		GrupoCentroCosto _grupocentrocosto = grupocentrocostoService.getGrupoCentroCosto(empresa, ejerciciocontable, grupocentrocosto);
		return _grupocentrocosto;	
	}
	
	// Listado 
	@GetMapping(value = "/grupocentrocostolista/{empresa}/{ejerciciocontable}")
	public List<GrupoCentroCosto> lista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<GrupoCentroCosto> grupocentrocosto = new ArrayList<>();
		grupocentrocostoService.getListaGrupoCentroCosto(empresa, ejerciciocontable).forEach(grupocentrocosto::add);
		return grupocentrocosto;
	}

	// Contador de Grupo centro costo
	@GetMapping(value = "/grupocentrocostocontador/{empresa}/{ejerciciocontable}/{grupocentrocosto}")
	public ValorContador contador(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto) {
		ValorContador _valorcontador = grupocentrocostoService.getContadorRegistros(empresa, ejerciciocontable, grupocentrocosto);
		return _valorcontador;	
	}
	
	// Catálogo lista
	@GetMapping(value = "/grupocentrocostocatalogolista/{empresa}/{ejerciciocontable}")
	public List<Catalogo> catalogolista(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable){
		List<Catalogo> catalogo = new ArrayList<>();
		grupocentrocostoService.getListaCatalogo(empresa, ejerciciocontable).forEach(catalogo::add);
		return catalogo;
	}
	
	// Catálogo filtro
	@GetMapping(value = "/grupocentrocostocatalogo/{empresa}/{ejerciciocontable}/{grupocentrocosto}")
	public List<Catalogo> catalogo(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("grupocentrocosto") String grupocentrocosto){
		List<Catalogo> catalogo = new ArrayList<>();
		grupocentrocostoService.getListaCatalogo(empresa, ejerciciocontable, grupocentrocosto).forEach(catalogo::add);
		return catalogo;
	}
	
}
