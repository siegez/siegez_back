package com.siegez.controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.siegez.reports.BalanceComprobacion;
import com.siegez.reports.CajaBanco;
import com.siegez.reports.EstadoResultado;
import com.siegez.reports.LibroDiario;
import com.siegez.reports.LibroDiarioSimplificado;
import com.siegez.reports.LibroMayor;
import com.siegez.reports.LibroMayorBiMoneda;
import com.siegez.reports.PLAMEPrestadorServicio;
import com.siegez.reports.PLELibroDiario;
import com.siegez.reports.PLERegistroCompra;
import com.siegez.reports.PLERegistroCompraNoDomiciliado;
import com.siegez.reports.PLERegistroVenta;
import com.siegez.reports.RegistroCompra;
import com.siegez.reports.RegistroVenta;
import com.siegez.reports.SaldoCuenta;
import com.siegez.reports.SituacionFinanciera;
import com.siegez.service.interfaces.ReporteService;

import net.sf.jasperreports.engine.JRException;

@CrossOrigin(origins = "*")
@RequestMapping("/api")
@RestController
public class ReporteController {
	
	@Autowired
	ReporteService reporteService;
	
	// Listado pleregistrocompra
	@GetMapping(value = "/pleregistrocompra/{empresa}/{ejerciciocontable}/{periodocontable}")
	public List<PLERegistroCompra> listapleregistrocompra(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable){
		List<PLERegistroCompra> pleregistrocompra = new ArrayList<>();
		reporteService.getListaPLERegistroCompra(empresa, ejerciciocontable, periodocontable).forEach(pleregistrocompra::add);
		return pleregistrocompra;
	}
	
	// Listado pleregistrocompranodom
	@GetMapping(value = "/pleregistrocompranodom/{empresa}/{ejerciciocontable}/{periodocontable}")
	public List<PLERegistroCompraNoDomiciliado> listapleregistrocompranodom(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable){
		List<PLERegistroCompraNoDomiciliado> pleregistrocompranodom = new ArrayList<>();
		reporteService.getListaPLERegistroCompraNoDom(empresa, ejerciciocontable, periodocontable).forEach(pleregistrocompranodom::add);
		return pleregistrocompranodom;
	}

	// Listado pleregistroventa
	@GetMapping(value = "/pleregistroventa/{empresa}/{ejerciciocontable}/{periodocontable}")
	public List<PLERegistroVenta> listapleregistroventa(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable){
		List<PLERegistroVenta> pleregistroventa = new ArrayList<>();
		reporteService.getListaPLERegistroVenta(empresa, ejerciciocontable, periodocontable).forEach(pleregistroventa::add);
		return pleregistroventa;
	}
	
	// Listado PLAME Prestador Servicios
	@GetMapping(value = "/plameperser/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}/{orden}")
	public List<PLAMEPrestadorServicio> listaplameperser(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable,
													@PathVariable("ruc") String ruc, @PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda, @PathVariable("orden") String orden){
		List<PLAMEPrestadorServicio> plameprestadorservicio = new ArrayList<>();
		reporteService.getListaPLAMEPreSer(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden).forEach(plameprestadorservicio::add);
		return plameprestadorservicio;
	}
	// Listado plelibrodiario
	@GetMapping(value = "/plelibrodiario/{empresa}/{ejerciciocontable}/{periodocontable}")
	public List<PLELibroDiario> listaplelibrodiario(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable){
		List<PLELibroDiario> plelibrodiario = new ArrayList<>();
		reporteService.getListaPLELibroDiario(empresa, ejerciciocontable, periodocontable).forEach(plelibrodiario::add);
		return plelibrodiario;
	}
	// Registro compra PDF
	@GetMapping(value = "/registrocompra/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}/{orden}")
	public void registrocompra(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("orden") String orden) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFRegistroCompra(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, orden, out);		
	}
	// Listado registrocompra
	@GetMapping(value = "/listaregistrocompra/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}/{orden}")
	public List<RegistroCompra> listaregistrocompra(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable,
													@PathVariable("ruc") String ruc, @PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda, @PathVariable("orden") String orden){
		List<RegistroCompra> registrocompra = new ArrayList<>();
		reporteService.getListaRegistroCompra(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden).forEach(registrocompra::add);
		return registrocompra;
	}
	// Registro venta PDF
	@GetMapping(value = "/registroventa/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}/{orden}")
	public void registroventa(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("orden") String orden) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFRegistroVenta(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, orden, out);		
	}
	// Listado registro venta
	@GetMapping(value = "/listaregistroventa/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}/{orden}")
	public List<RegistroVenta> listaregistroventa(@PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable,
													@PathVariable("ruc") String ruc, @PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda, @PathVariable("orden") String orden){
		List<RegistroVenta> registroventa = new ArrayList<>();
		reporteService.getListaRegistroVenta(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda, orden).forEach(registroventa::add);
		return registroventa;
	}
	// Libro diario PDF
	@GetMapping(value = "/librodiario/{empresa}/{ejerciciocontable}/{periodocontable}/{subini}/{subfin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public void librodiario(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("subini") String subini,
															@PathVariable("subfin") String subfin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFDiario(empresa, annoproceso, periodo, subini, subfin, ruc, nombreempresa, moneda, glosa, out);		
	}
	// Listado libro diario
	@GetMapping(value = "/listalibrodiario/{empresa}/{ejerciciocontable}/{periodocontable}/{subini}/{subfin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<LibroDiario> listalibrodiario(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable, @PathVariable("periodocontable") String periodocontable,
												@PathVariable("subini") String subini, @PathVariable("subfin") String subfin, @PathVariable("ruc") String ruc,
												@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
												@PathVariable("glosa") String glosa){
		List<LibroDiario> librodiario = new ArrayList<>();
		reporteService.getListaDiario(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, glosa).forEach(librodiario::add);
		return librodiario;
	}
	// Libro diario simplificado PDF
	@GetMapping(value = "/librodiariosim/{empresa}/{ejerciciocontable}/{periodocontable}/{subini}/{subfin}/{ruc}/{nombreempresa}/{moneda}/{categoria}")
	public void librodiariosim(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("subini") String subini,
															@PathVariable("subfin") String subfin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("categoria") String categoria) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFDiarioSim(empresa, annoproceso, periodo, subini, subfin, ruc, nombreempresa, moneda, categoria, out);		
	}
	// Listado libro diario simplificado
	@GetMapping(value = "/listalibrodiariosim/{empresa}/{ejerciciocontable}/{periodocontable}/{subini}/{subfin}/{ruc}/{nombreempresa}/{moneda}/{categoria}")
	public List<LibroDiarioSimplificado> listalibrodiariosim(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("subini") String subini,
															@PathVariable("subfin") String subfin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("categoria") String categoria){
		List<LibroDiarioSimplificado> librodiariosimplificado = new ArrayList<>();
		reporteService.getListaDiarioSim(empresa, ejerciciocontable, periodocontable, subini, subfin, ruc, nombreempresa, moneda, categoria).forEach(librodiariosimplificado::add);
		return librodiariosimplificado; 
	}
	// Libro mayor PDF
	@GetMapping(value = "/libromayor/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public void libromayor(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFMayor(empresa, annoproceso, periodo, ctaini, ctafin, ruc, nombreempresa, moneda, glosa, out);		
	}
	// Listado libro mayor
	@GetMapping(value = "/listalibromayor/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<LibroMayor> listalibromayor(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa){
		List<LibroMayor> libromayor = new ArrayList<>();
		reporteService.getListaMayor(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa).forEach(libromayor::add);
		return libromayor; 
	}
	// Libro mayor bimoneda PDF
	@GetMapping(value = "/libromayorbi/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public void libromayorbi(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFMayorBiMon(empresa, annoproceso, periodo, ctaini, ctafin, ruc, nombreempresa, moneda, glosa, out);		
	}
	// Listado libro mayor bimoneda
	@GetMapping(value = "/listalibromayorbinom/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<LibroMayorBiMoneda> listalibromayorbimon(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa){
		List<LibroMayorBiMoneda> libromayorbimoneda = new ArrayList<>();
		reporteService.getListaMayorBiMon(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa).forEach(libromayorbimoneda::add);
		return libromayorbimoneda; 
	}
	// Libro caja y banco PDF
	@GetMapping(value = "/cajabanco/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public void cajabanco(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFCajaBanco(empresa, annoproceso, periodo, ctaini, ctafin, ruc, nombreempresa, moneda, glosa, out);		
	}
	// Listado caja y banco
	@GetMapping(value = "/listacajabanco/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<CajaBanco> listacajabanco(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ctaini") String ctaini,
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda,
															@PathVariable("glosa") String glosa){
		List<CajaBanco> cajabanco = new ArrayList<>();
		reporteService.getListaCajaBanco(empresa, ejerciciocontable, periodocontable, ctaini, ctafin, ruc, nombreempresa, moneda, glosa).forEach(cajabanco::add);
		return cajabanco; 
	}
	// Libro balance comprobación PDF
	@GetMapping(value = "/balcom/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}")
	public void balancecomprobacion(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda
															) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFBalanceComprobacion(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, out);		
	}
	
	// Listado balance de comprobación
	@GetMapping(value = "/listabalancecomprobacion/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<BalanceComprobacion> listabalancecomprobacion(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda){
		List<BalanceComprobacion> balancecomprobacion = new ArrayList<>();
		reporteService.getListaBalanceComprobacion(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda).forEach(balancecomprobacion::add);
		return balancecomprobacion; 
	}
		

	// Saldo cuenta entidad documencto PDF
	@GetMapping(value = "/salctaentdoc/{empresa}/{ejerciciocontable}/{tiposaldo}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{fecha}")
	public void salctaentdoc(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("tiposaldo") String tiposaldo, @PathVariable("ctaini") String ctaini,														
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, 
															@PathVariable("fecha") String fecha) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFSaldoCtaEntDoc(empresa, annoproceso, tiposaldo, ctaini, ctafin, ruc, nombreempresa, fecha, out);		
	}

	// Listado Saldo cuenta entidad documencto
	@GetMapping(value = "/listasalctaentdoc/{empresa}/{ejerciciocontable}/{tiposaldo}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{fecha}")
	public List<SaldoCuenta> listasalctaentdoc(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("tiposaldo") String tiposaldo, @PathVariable("ctaini") String ctaini,														
															@PathVariable("ctafin") String ctafin, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa,
															@PathVariable("fecha") String fecha){
		List<SaldoCuenta> saldocuenta = new ArrayList<>();
		reporteService.getListaSaldoCuenta(empresa, ejerciciocontable, tiposaldo, ctaini, ctafin, ruc, nombreempresa, fecha).forEach(saldocuenta::add);
		return saldocuenta; 
	}
	
	// Estado Situación Financiera PDF
	@GetMapping(value = "/sitfin/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}")
	public void situacionfinanciera(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda
															) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFSitFin(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, out);		
	}
	
	// Estado Situación Financiera Vertical PDF
	@GetMapping(value = "/sitfinver/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}")
	public void situacionfinancieraver(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda
															) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFSitFinVer(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, out);		
	}
	
	// Listado Estado Situación Financiera Vertical
	@GetMapping(value = "/listasitfinver/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<SituacionFinanciera> listasituacionfinancieraver(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda){
		List<SituacionFinanciera> situacionfinanciera = new ArrayList<>();
		reporteService.getListaSitFinVer(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda).forEach(situacionfinanciera::add);
		return situacionfinanciera; 
	}
	
	// Estado Resultado por Naturaleza PDF
	@GetMapping(value = "/estresnat/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}")
	public void estadoresultadonat(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda
															) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFEstResNat(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, out);		
	}
	
	// Listado Estado Resultado por Naturaleza
	@GetMapping(value = "/listaestresnat/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<EstadoResultado> listaestadoresultadonat(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda){
		List<EstadoResultado> estadoresultado = new ArrayList<>();
		reporteService.getListaEstResNat(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda).forEach(estadoresultado::add);
		return estadoresultado; 
	}
	
	// Estado Resultado por Función PDF
	@GetMapping(value = "/estresfun/{empresa}/{ejerciciocontable}/{periodocontable}/{ruc}/{nombreempresa}/{moneda}")
	public void estadoresultadofun(HttpServletResponse response, @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer annoproceso, 
															@PathVariable("periodocontable") String periodo, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda
															) throws IOException, JRException{
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename\"person.pdf\""));
				
		OutputStream out = response.getOutputStream();
		reporteService.getPDFEstResFun(empresa, annoproceso, periodo, ruc, nombreempresa, moneda, out);		
	}
	
	// Listado Estado Resultado por Función
	@GetMapping(value = "/listaestresfun/{empresa}/{ejerciciocontable}/{periodocontable}/{ctaini}/{ctafin}/{ruc}/{nombreempresa}/{moneda}/{glosa}")
	public List<EstadoResultado> listaestadoresultadonfun(  @PathVariable("empresa") Integer empresa, @PathVariable("ejerciciocontable") Integer ejerciciocontable,
															@PathVariable("periodocontable") String periodocontable, @PathVariable("ruc") String ruc,
															@PathVariable("nombreempresa") String nombreempresa, @PathVariable("moneda") String moneda){
		List<EstadoResultado> estadoresultado = new ArrayList<>();
		reporteService.getListaEstResFun(empresa, ejerciciocontable, periodocontable, ruc, nombreempresa, moneda).forEach(estadoresultado::add);
		return estadoresultado; 
	}
	
}














